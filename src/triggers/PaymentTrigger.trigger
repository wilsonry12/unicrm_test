trigger PaymentTrigger on Payment__c (after insert, after update) {

    if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
        
        List<String> paymentIds = new List<String>();
        for(Payment__c pay : Trigger.new) {
            paymentIds.add(pay.Id);
        }

        List<Payment__c> paymentsUpdated = [SELECT Id, Name, Payment_Date__c, Payment_Status__c, Send_Receipt__c, Contact__c, Contact__r.Name, Company_Name__c, Street__c, Suburb__c, State__c, Postcode__c, Quantity__c, Course_Title__c, Amount_Ex_GST__c, GST_Allocation__c, Payment_Amount__c, Payment_Type__c, Course_Offering__r.Apply_GST__c FROM Payment__c WHERE Id IN:paymentIds];
        
        EmailTemplate template = [Select Id, Subject, HtmlValue from EmailTemplate where DeveloperName = 'Enrolment_Payment_Receipt'];
        
        OrgWideEmailAddress senderEmail = [SELECT Address,DisplayName,Id FROM OrgWideEmailAddress WHERE DisplayName = 'Monash Student Global'];
        Id studentGlobalId = senderEmail.Id;

        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();

        for(Payment__c payment : paymentsUpdated) {
            if(
                (Trigger.isUpdate && payment.Send_Receipt__c == true && Trigger.oldMap.get(payment.Id).Send_Receipt__c == false) ||
                (Trigger.isInsert && payment.Send_Receipt__c == true)

                ) {

                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setOrgWideEmailAddressId(studentGlobalId);
                mail.setTargetObjectId(payment.Contact__c);

                
                // Read custom setting
                List<String> bccAddresses = new List<String>();
                List<PaymentReceiptBCC__c> bcc = PaymentReceiptBCC__c.getall().values();

                if(bcc.size()>0) {
                    for(PaymentReceiptBCC__c bccEmail : bcc) {
                        if(bccEmail != null && String.isNotBlank(bccEmail.bccEmailAddress__c)) {
                            bccAddresses.add(bccEmail.bccEmailAddress__c);
                        }
                    }
                }
                
                if(bccAddresses.size()>0){
                    mail.setBccAddresses(bccAddresses);    
                }
                
                mail.setSaveAsActivity(false);

                // Merge Template fields
                mail.setSubject(template.Subject);

                String htmlBody = template.HtmlValue;
                htmlBody = htmlBody.replace('{!relatedTo.Name}', payment.Name);

                DateTime pDate = System.now();
                String paymentDate = pDate.format('dd/MM/yyyy HH:mm','Australia/Melbourne');
                if(payment.Payment_Date__c != null) {
                    paymentDate = payment.Payment_Date__c.format('dd/MM/yyyy HH:mm','Australia/Melbourne');
                }               
                htmlBody = htmlBody.replace('{!relatedTo.Payment_Date__c}', paymentDate);

                String name = '';
                if(String.isNotBlank(payment.Company_Name__c)) {
                    name = payment.Company_Name__c;
                } else {
                    name = payment.Contact__r.Name;
                }

                // Payment amount to show on receipt
                String paymentAmount = '';
                String paymentAmountPrefix = '';
                String paymentAmountOnReceipt = '';
                if(payment.Payment_Amount__c != null) {
                    if(payment.Payment_Amount__c < 0) {
                        paymentAmountPrefix = '-';    
                    }
                    paymentAmount = String.valueOf(System.Math.abs(payment.Payment_Amount__c));
                }
                paymentAmountOnReceipt = paymentAmountPrefix + '$' + paymentAmount;


                // Payment amount excluding GST to show on receipt
                String amountExGst = '';
                String amountExGstPrefix = '';
                String amountExGstOnReceipt = '';
                if(payment.Amount_Ex_GST__c != null) {
                    if(payment.Amount_Ex_GST__c < 0) {
                        amountExGstPrefix = '-';    
                    }
                    amountExGst = String.valueOf(System.Math.abs(payment.Amount_Ex_GST__c));
                }
                amountExGstOnReceipt = amountExGstPrefix + '$' + amountExGst;


                // Payment amount GST to show on receipt
                String gst = '';
                String gstPrefix = '';
                String gstOnReceipt = '';
                if(payment.Course_Offering__r.Apply_GST__c && payment.Payment_Amount__c != null) {
                    Decimal gstAmount = payment.Payment_Amount__c;
                    gstAmount = (gstAmount - (gstAmount/1.1)).setScale(2);
                    if(gstAmount < 0) {
                        gstPrefix = '-';    
                    }   
                    gst = String.valueOf(System.Math.abs(gstAmount));
                }  
                else {
                    gst = '0';
                }
                gstOnReceipt = gstPrefix + '$' + gst;

                htmlBody = htmlBody.replace('{{contactName}}', name);
                htmlBody = htmlBody.replace('{!relatedTo.Street__c}', payment.Street__c);
                htmlBody = htmlBody.replace('{!relatedTo.Suburb__c}', payment.Suburb__c);
                htmlBody = htmlBody.replace('{!relatedTo.State__c}', payment.State__c);
                htmlBody = htmlBody.replace('{!relatedTo.Postcode__c}', payment.Postcode__c);

                htmlBody = htmlBody.replace('{!relatedTo.Quantity__c}', String.valueOf(payment.Quantity__c));
                htmlBody = htmlBody.replace('{!relatedTo.Course_Title__c}', payment.Course_Title__c);
                htmlBody = htmlBody.replace('{!relatedTo.Payment_Type__c}', String.valueOf(payment.Payment_Type__c));

                htmlBody = htmlBody.replace('${!relatedTo.Payment_Amount__c}', paymentAmountOnReceipt);
                htmlBody = htmlBody.replace('${!relatedTo.Amount_Ex_GST__c}', amountExGstOnReceipt);
                htmlBody = htmlBody.replace('${!relatedTo.GST_Allocation__c}', gstOnReceipt);

                

                mail.setHtmlBody(htmlBody);
                allmsg.add(mail);    
            }   
        }

        Messaging.sendEmail(allmsg);

    }
}