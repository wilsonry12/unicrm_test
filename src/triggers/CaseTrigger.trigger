trigger CaseTrigger on Case (before insert, before update, after insert, after update) {
    
    if (TriggerCommon.doNotRunTrigger('Case')) { return; }
    
    /*Anterey Custodio, 29-Sept-2016, method that prepopulates the category according to the entered value through Web-to-Case*/
    if (Trigger.isBefore) {     
        if (Trigger.isInsert) {
            CaseServices.prepopulateCategory(Trigger.new);
        }
    }

    if (Trigger.isAfter && Trigger.isUpdate) {
        AttachmentServices.cloneEmailAttachmentToContactDocumentUsingCases(Trigger.oldMap, Trigger.newMap);
    }
    
    new Triggers()
    .bind(Triggers.Evnt.beforeInsert, new CaseServices.mapCourseCode())
    .bind(Triggers.Evnt.beforeInsert, new CaseServices.checkDefaultsSet())
    .bind(Triggers.Evnt.beforeInsert, new CaseServices.convertStringRecordTypeNameToAnId())
    .bind(Triggers.Evnt.beforeInsert, new CaseServices.setDefaultsOnNewCases())
    .bind(Triggers.Evnt.beforeInsert, new CaseServices.checkForAndLinkToContacts())
    .bind(Triggers.Evnt.beforeInsert, new CaseServices.maintainLastQueueOwner())
    
    .bind(Triggers.Evnt.afterInsert, new CaseServices.setupBusinessHours())
    .bind(Triggers.Evnt.afterInsert, new CaseServices.addCourseInterests())
    
    .bind(Triggers.Evnt.beforeUpdate, new CaseServices.performValidation())
    .bind(Triggers.Evnt.beforeUpdate, new CaseServices.checkDefaultsSet())
    .bind(Triggers.Evnt.beforeUpdate, new CaseServices.clearAccountFieldOnContactChange())
    .bind(Triggers.Evnt.beforeUpdate, new CaseServices.maintainLastQueueOwner())

    // Majid Reisi Dehkordi, 8-Sep-2017
    // IF the Application Course Preference of the Addmision Enquiry (Case) gets added
    // to the case - before that it was not linked to the enquiry - then the attachments of the related
    // enquiry should be pushed to Calista.
    //.bind(Triggers.Evnt.afterUpdate, new AttachmentServices.cloneEmailAttachmentToContactDocumentUsingCases())

    .bind(Triggers.Evnt.afterUpdate, new CaseServices.maintainBusinessHours())
    .bind(Triggers.Evnt.afterUpdate, new CaseServices.manageReopenedDateTime())
    .bind(Triggers.Evnt.afterUpdate, new CaseServices.copyCaseFieldsOverToLinkedContact())
    .bind(Triggers.Evnt.afterUpdate, new CaseServices.checkForActiveCall())
    
    .execute();
}