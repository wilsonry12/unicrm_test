trigger CampaignMemberTrigger on CampaignMember (before insert, after insert, after update, before update) {
 	
 	if (TriggerCommon.doNotRunTrigger('CampaignMember')) { return; }
    
    new Triggers()
    .bind(Triggers.Evnt.beforeInsert, new CampaignMemberServices.populateEmailToField())
    .bind(Triggers.Evnt.afterInsert, new CampaignMemberServices.qualifyCreateSubscriptionMember())
    .bind(Triggers.Evnt.afterUpdate, new CampaignMemberServices.manageChangesOfCohort())
    .bind(Triggers.Evnt.afterUpdate, new CampaignMemberServices.qualifyCreateSubscriptionMember())
    .execute();   
}