/*******************************************************************************
* @author       Anterey Custodio
* @date         3.Aug.2017
* @description  Contains all triggers on Attachment Object
* @revision     
*******************************************************************************/
trigger AttachmentTrigger on Attachment (before insert) {
	if (TriggerCommon.doNotRunTrigger('Attachment')) { return; }
    
    new Triggers()
    .bind(Triggers.Evnt.beforeInsert, new AttachmentServices.cloneEmailAttachmentToContactDocument())
    .execute();
}