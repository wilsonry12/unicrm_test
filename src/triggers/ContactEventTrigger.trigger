trigger ContactEventTrigger on Contact__e (after insert) {
	
	List<Contact__e> conEvent = new List<Contact__e>();

	for(Contact__e cEvent: trigger.new){
		//filter for Staff records only and inbound events
		if(cEvent.SAP_Staff_Id__c != null && cEvent.Event_Type__c == 'inbound'){
			conEvent.add(cEvent);
		}
	}
	
	if(conEvent.size() >0){ Contact_Events_Handler.createUpdateStaffContact(conEvent); }
}