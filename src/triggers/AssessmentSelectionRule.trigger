trigger AssessmentSelectionRule on Assessment_Selection_Rule__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

	new Triggers()
	.bind(Triggers.Evnt.beforeInsert, new AssessmentSelectionRuleServices.buildQueryStringBeforeInsert())
    .bind(Triggers.Evnt.beforeUpdate, new AssessmentSelectionRuleServices.applySelectionRuleOnUpdate())
    .bind(Triggers.Evnt.afterInsert, new AssessmentSelectionRuleServices.applySelectionRuleOnInsert())
    .bind(Triggers.Evnt.afterUpdate, new AssessmentSelectionRuleServices.unlinkInactiveSelectionRule())
    .execute();
}