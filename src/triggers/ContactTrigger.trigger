trigger ContactTrigger on Contact (before insert, before update, after delete, after update) {
    
    if (TriggerCommon.doNotRunTrigger('Contact')) { return; }
    
    new Triggers()
        .bind(Triggers.Evnt.beforeInsert, new ContactServices.populateNullPersonIdField())
        .bind(Triggers.Evnt.beforeInsert, new ContactServices.enableCopyCaseInfoToContact())
        .bind(Triggers.Evnt.beforeInsert, new ContactServices.addContactAccounts())
        
        .bind(Triggers.Evnt.afterDelete, new ContactServices.cleanUpOrphanAccounts())
        
        // Accept updates from integration sources only if (external/new) LastModifiedDate > (internal/old) SystemModStamp, i.e. SFDC has an older record.
        .bind(Triggers.Evnt.beforeUpdate, new ContactServices.rejectOutOfDateUpdate())
        .bind(Triggers.Evnt.beforeUpdate, new ContactServices.maintainEmailAddressHistory())
        
        .bind(Triggers.Evnt.beforeInsert, new ContactServices.maintainEmailAddressHistory())
        .bind(Triggers.Evnt.beforeInsert, new ContactServices.copyMultiselectToTextField())
        .bind(Triggers.Evnt.beforeUpdate, new ContactServices.copyMultiselectToTextField())
        
        .bind(Triggers.Evnt.afterUpdate, new ContactServices.maintainAccountName())
        
        // platform event for contact create/update/delete
        .bind(Triggers.Evnt.afterInsert, new ContactServices.createPlatformEvent())
        .bind(Triggers.Evnt.afterUpdate, new ContactServices.createPlatformEvent())
        //.bind(Triggers.Evnt.afterDelete, new ContactServices.createPlatformEvent()) // needs special consideration?
    .execute();
}