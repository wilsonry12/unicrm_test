trigger RoleTrigger on Roles__c (after insert, after update) {

	new Triggers()
        // platform event for contact create/update/delete
        .bind(Triggers.Evnt.afterInsert, new RoleAffiliationServices.createPlatformEvent())
        .bind(Triggers.Evnt.afterUpdate, new RoleAffiliationServices.createPlatformEvent())
        //.bind(Triggers.Evnt.afterDelete, new RoleAffiliationServices.createPlatformEvent()) // needs special consideration?
    .execute();
}