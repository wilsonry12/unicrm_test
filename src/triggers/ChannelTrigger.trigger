trigger ChannelTrigger on Channel__c (after insert, after update) {

	new Triggers()
        // platform event for contact create/update/delete
        .bind(Triggers.Evnt.afterInsert, new ChannelServices.createPlatformEvent())
        .bind(Triggers.Evnt.afterUpdate, new ChannelServices.createPlatformEvent())
        //.bind(Triggers.Evnt.afterDelete, new ChannelServices.createPlatformEvent()) // needs special consideration?
    .execute();

}