trigger ApplicationAndDocumentSubmission on Application__c (before insert, before update, after insert, after update) {

    if (Trigger.isInsert && Trigger.isBefore) {
        //ANT.CUSTODIO, 16.June.2017 - prevents the user from creating drafts if they already have an existing draft application
        ApplicationHandler.onlyAllowOneDraft(Trigger.new);
        
        //ANT.CUSTODIO, 07.Sept.2017 - automatically populate the Agent using the provided agent org unit code
        ApplicationHandler.populateAgentByOrgUnitCode(null, null, Trigger.new);
    }
    
    //ANT.CUSTODIO, 07.Sept.2017 - automatically populate the Agent using the provided agent org unit code
    if (Trigger.isUpdate && Trigger.isBefore) {
        ApplicationHandler.populateAgentByOrgUnitCode(Trigger.oldMap, Trigger.newMap, null);
    }
}