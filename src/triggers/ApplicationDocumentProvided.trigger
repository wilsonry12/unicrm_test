trigger ApplicationDocumentProvided on Application_Document_Provided__c (after insert, before update) {

    if(Trigger.isInsert && Trigger.isAfter) {
        ApplicationDocumentTriggerHandler.handleAfterInsert(Trigger.new);
    }

    //Ant Custodio, 16.Oct.2017 - tick the Document_submitted_successfully__c flag when
    //								the document is a duplicate from Callista SF-525
    if (Trigger.isBefore && Trigger.isUpdate) {
    	ApplicationDocumentTriggerHandler.tickFlagIfDuplicate(Trigger.oldMap, Trigger.newMap);
    }
}