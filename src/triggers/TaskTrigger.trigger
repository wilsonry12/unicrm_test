trigger TaskTrigger on Task (after insert, before insert, before update) {

    if (TriggerCommon.doNotRunTrigger('Task')) { return; }
    
    new Triggers()
    .bind(Triggers.Evnt.beforeInsert, new TaskServices.considerWrapUpTasks())
    .bind(Triggers.Evnt.afterInsert, new CaseServices.clearLastEmailReceived())
    .bind(Triggers.Evnt.afterInsert, new CaseServices.LogRecordingOnActivityInsert())
    .bind(Triggers.Evnt.afterInsert, new TaskServices.updateFirstResponseDateTime())  
    .bind(Triggers.Evnt.beforeInsert, new TaskServices.considerTaskFieldMappings())
    //.bind(Triggers.Evnt.afterInsert, new TaskServices.generatePlatformEvent())
    .execute(); 


    if(Trigger.isInsert && Trigger.isAfter){
        StudFirst_CRMInitiatedCommsToAP.notifyAgentByTask((List<Task>)Trigger.new);
    }
}