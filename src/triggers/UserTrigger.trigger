/*******************************************************************************
* @author       Ant Custodio
* @date         2.Apr.2017         
* @description  trigger for User - DO NOT ADD ANY LOGIC HERE.
* @revision     
*******************************************************************************/
trigger UserTrigger on User (after update) {
    
    if (TriggerCommon.doNotRunTrigger('User')) { return; }
    
    new Triggers()
    .bind(Triggers.Evnt.afterUpdate, new UserServices.updateAssociatedContact())
    .execute(); 
}