<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <fields>
        <fullName>ADP_Submitted_Count__c</fullName>
        <description>Counts the number of related Application Documents Provided that were submitted successfully. DO NOT SHOW ON PAGE LAYOUT</description>
        <externalId>false</externalId>
        <inlineHelpText>Counts the number of related Application Documents Provided that were submitted successfully.</inlineHelpText>
        <label>ADP Submitted Count</label>
        <summaryFilterItems>
            <field>Application_Document_Provided__c.Document_submitted_successfully__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>Application_Document_Provided__c.Contact_Document__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <externalId>false</externalId>
        <label>Comments</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Contact_Qualification__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Contact Qualification</label>
        <referenceTo>Contact_Qualification__c</referenceTo>
        <relationshipLabel>Qualification Documents</relationshipLabel>
        <relationshipName>Contact_Documents</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Contact Documents</relationshipLabel>
        <relationshipName>Contact_Documents</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Document_Type_Name__c</fullName>
        <externalId>false</externalId>
        <label>Document Type Name</label>
        <length>150</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Document_Type__c</fullName>
        <externalId>false</externalId>
        <label>Document Type</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>Document_Type</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Filename__c</fullName>
        <externalId>false</externalId>
        <label>Filename</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Has_API_Request__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field will be checked via Process Builder &quot;AA - ADP Processes&quot; if there is at least 1 generated request from its child Application Document Provided records</description>
        <externalId>false</externalId>
        <label>Has API Request</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Has_API_Response__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This field will be checked via Process Builder &quot;AA - ADP Processes&quot; if there is at least 1 response from Callista from its child Application Document Provided records</description>
        <externalId>false</externalId>
        <label>Has API Response</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Has_Submitted_Successfully__c</fullName>
        <description>This field will be checked if there is at least 1 submitted document from its child Application Document Provided records</description>
        <externalId>false</externalId>
        <formula>IF(ADP_Submitted_Count__c &gt; 0, true, false)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Has Submitted Successfully</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Reference_Count__c</fullName>
        <description>Roll up summary to determine if the record has been reference with the previous application</description>
        <externalId>false</externalId>
        <label>Reference Count</label>
        <summaryForeignKey>Application_Document_Provided__c.Contact_Document__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Certified name change documents uploaded with the on-line graduation application.</fullName>
                    <default>false</default>
                    <label>Certified name change documents uploaded with the on-line graduation application.</label>
                </value>
                <value>
                    <fullName>Certified previous qualification documents uploaded with the on-line graduation application.</fullName>
                    <default>false</default>
                    <label>Certified previous qualification documents uploaded with the on-line graduation application.</label>
                </value>
                <value>
                    <fullName>Course offer acceptance document</fullName>
                    <default>false</default>
                    <label>Course offer acceptance document</label>
                </value>
                <value>
                    <fullName>Credit application</fullName>
                    <default>false</default>
                    <label>Credit application</label>
                </value>
                <value>
                    <fullName>Curriculum vitae</fullName>
                    <default>false</default>
                    <label>Curriculum vitae</label>
                </value>
                <value>
                    <fullName>Diploma or degree certificate</fullName>
                    <default>false</default>
                    <label>Diploma or degree certificate</label>
                </value>
                <value>
                    <fullName>Employment reference</fullName>
                    <default>false</default>
                    <label>Employment reference</label>
                </value>
                <value>
                    <fullName>HDR application summary</fullName>
                    <default>false</default>
                    <label>HDR application summary</label>
                </value>
                <value>
                    <fullName>Higher Degree Research candidature offer or confirmation letter.</fullName>
                    <default>false</default>
                    <label>Higher Degree Research candidature offer or confirmation letter.</label>
                </value>
                <value>
                    <fullName>Image of artwork for Art and Design folio</fullName>
                    <default>false</default>
                    <label>Image of artwork for Art and Design folio</label>
                </value>
                <value>
                    <fullName>Invitation Letter for Higher Dreams Application</fullName>
                    <default>false</default>
                    <label>Invitation Letter for Higher Dreams Application</label>
                </value>
                <value>
                    <fullName>Learning Portfolio</fullName>
                    <default>false</default>
                    <label>Learning Portfolio</label>
                </value>
                <value>
                    <fullName>Open Universities exams special local invigilator verification</fullName>
                    <default>false</default>
                    <label>Open Universities exams special local invigilator verification</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Other document type not listed</fullName>
                    <default>false</default>
                    <label>Other document type not listed</label>
                </value>
                <value>
                    <fullName>Prior Application details for HDR</fullName>
                    <default>false</default>
                    <label>Prior Application details for HDR</label>
                </value>
                <value>
                    <fullName>Proof of change of name</fullName>
                    <default>false</default>
                    <label>Proof of change of name</label>
                </value>
                <value>
                    <fullName>Proof of Citizenship/Residency or Identity</fullName>
                    <default>false</default>
                    <label>Proof of Citizenship/Residency or Identity</label>
                </value>
                <value>
                    <fullName>Proof of employment</fullName>
                    <default>false</default>
                    <label>Proof of employment</label>
                </value>
                <value>
                    <fullName>Proof of English language medium of instruction</fullName>
                    <default>false</default>
                    <label>Proof of English language medium of instruction</label>
                </value>
                <value>
                    <fullName>Proof of professional registration</fullName>
                    <default>false</default>
                    <label>Proof of professional registration</label>
                </value>
                <value>
                    <fullName>Research proposal</fullName>
                    <default>false</default>
                    <label>Research proposal</label>
                </value>
                <value>
                    <fullName>Results forecast from secondary school</fullName>
                    <default>false</default>
                    <label>Results forecast from secondary school</label>
                </value>
                <value>
                    <fullName>Results from secondary school</fullName>
                    <default>false</default>
                    <label>Results from secondary school</label>
                </value>
                <value>
                    <fullName>Results from tertiary studies</fullName>
                    <default>false</default>
                    <label>Results from tertiary studies</label>
                </value>
                <value>
                    <fullName>Results of aptitude test</fullName>
                    <default>false</default>
                    <label>Results of aptitude test</label>
                </value>
                <value>
                    <fullName>Results of English language test</fullName>
                    <default>false</default>
                    <label>Results of English language test</label>
                </value>
                <value>
                    <fullName>Statement of Purpose</fullName>
                    <default>false</default>
                    <label>Statement of Purpose</label>
                </value>
                <value>
                    <fullName>Unit outline from tertiary studies</fullName>
                    <default>false</default>
                    <label>Unit outline from tertiary studies</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Contact Document</label>
    <nameField>
        <displayFormat>CNDC-{000000}</displayFormat>
        <label>Contact Document ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Contact Documents</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
