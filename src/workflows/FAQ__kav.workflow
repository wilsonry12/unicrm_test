<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FAQ_Approval_Process</fullName>
        <description>FAQ Approval Process Rejected Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>FAQ_Content_Author_1</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Assignment_Rejected</template>
    </alerts>
    <alerts>
        <fullName>FAQ_Approval_Process_Approval_Alert</fullName>
        <description>FAQ Approval Process Approval Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>FAQ_Content_Author_1</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approved</template>
    </alerts>
    <alerts>
        <fullName>FAQ_Approval_Reminder</fullName>
        <description>FAQ Approval Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Knowledge_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Send_Email</fullName>
        <description>Send Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>Knowledge_Managers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/FAQNotification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_to_False</fullName>
        <field>Pending_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Set to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_to_True</fullName>
        <field>Pending_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Set to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Validation_Status_Field</fullName>
        <field>ValidationStatus</field>
        <literalValue>Approved</literalValue>
        <name>Update Validation Status Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Alert Knowledge Manager group</fullName>
        <actions>
            <name>Send_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Alert Knowledge Manager group for FAQ creation and modifications</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>FAQ Approval Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>FAQ__kav.Pending_Approval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>FAQ_Approval_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
