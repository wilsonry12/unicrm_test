<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Auto_Response_Email</fullName>
        <description>Case - Auto Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Auto_responses/Future_monash_auto_response</template>
    </alerts>
    <alerts>
        <fullName>Case_Auto_Response_Email_INTL</fullName>
        <description>Case - Auto Response Email INTL</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>study@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Auto_responses/Study_monash_auto_response</template>
    </alerts>
    <alerts>
        <fullName>Case_DSS_Auto_Response_Email</fullName>
        <description>Case (DSS) - Auto Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Disability_Support_Services/DSS_Common_response_with_question_3</template>
    </alerts>
    <alerts>
        <fullName>Case_Email_New_Owner_Schools_Engagement</fullName>
        <description>Case - Email New Owner (Schools Engagement)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>All_templates/Channel_Mgt_New_Enquiry_Notification</template>
    </alerts>
    <alerts>
        <fullName>Case_Email_School_Visit_Submitter_Schools_Engagement</fullName>
        <description>Case - Email School Visit Submitter (Schools Engagement)</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Channel_Mgt_New_Enquiry_Notification_for_Submitter</template>
    </alerts>
    <alerts>
        <fullName>Case_MGE_Auto_Response_Email</fullName>
        <description>Case (MGE) - Auto Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Auto_responses/MGE_autoresponse_rule_email</template>
    </alerts>
    <alerts>
        <fullName>Case_MyDevelopment_Auto_Response_Email</fullName>
        <description>Case (MyDevelopment) - Auto Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>MGE_General/MGE_MyDevelopment_Auto_Response</template>
    </alerts>
    <alerts>
        <fullName>Case_in_a_referred_status_for_5_days</fullName>
        <description>Case in a referred status for 5 days</description>
        <protected>false</protected>
        <recipients>
            <recipient>Manager_HR</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>System_Templates/AUTO_Referred_Case_Is_Still_Open</template>
    </alerts>
    <alerts>
        <fullName>Email_received_for_a_closed_case_SEBS</fullName>
        <description>Email received for a closed case - SEBS</description>
        <protected>false</protected>
        <recipients>
            <recipient>SEBS_Managers</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>SEBS_Templates/Email_Closed_SEBS</template>
    </alerts>
    <alerts>
        <fullName>Email_received_for_a_closed_case_SEBS_Placements</fullName>
        <description>Email received for a closed case - SEBS Placements</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>SEBS_Templates/Email_Closed_SEBS</template>
    </alerts>
    <alerts>
        <fullName>Email_to_case_owner_regarding_On_Hold_task</fullName>
        <description>Email to case owner regarding On Hold task</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>SEBS_Templates/On_Hold_Date_Reminder</template>
    </alerts>
    <alerts>
        <fullName>M_Pass_webform_auto_response_Card_Office</fullName>
        <description>M-Pass webform auto response- Card Office</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>cardoffice.m-pass@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Auto_responses/M_Pass_webform_auto_response_CardOffice</template>
    </alerts>
    <alerts>
        <fullName>M_Pass_webform_auto_response_MC</fullName>
        <description>M-Pass webform auto response-MC</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>cardoffice.m-pass@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Auto_responses/Future_monash_auto_response1</template>
    </alerts>
    <alerts>
        <fullName>Monash_Online_Store_email_response</fullName>
        <description>Monash Online Store email response</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Auto_responses/MGE_autoresponse_rule_email</template>
    </alerts>
    <alerts>
        <fullName>PD_Case_Auto_Response_Email</fullName>
        <description>PD Case - Auto Response Email</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Auto_responses/PD_auto_response</template>
    </alerts>
    <alerts>
        <fullName>Send_Deferral_Request_Notification_to_Faculty</fullName>
        <ccEmails>mge-apply@monash.edu</ccEmails>
        <description>Send Deferral Request Notification to Faculty</description>
        <protected>false</protected>
        <recipients>
            <field>Faculty_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Supplied_Supervisors_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>mge-apply@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>MGE_Admissions/Deferral_Request</template>
    </alerts>
    <alerts>
        <fullName>Send_MC_survey</fullName>
        <description>Send MC survey</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Get_Feedback_Invite</template>
    </alerts>
    <alerts>
        <fullName>Send_a_notification_for_manual_Esclation</fullName>
        <description>Send a notification for manual Escalation</description>
        <protected>false</protected>
        <recipients>
            <recipient>chelsea.cincotta@monash.edu.unicrm</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>gurwinder.gill@monash.edu.unicrm</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>megan.m.lee@monash.edu.unicrm</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>System_Templates/Case_Manual_Escalation_Alert_Notification</template>
    </alerts>
    <alerts>
        <fullName>send_autoresponse_to_a_web_enquiry</fullName>
        <description>send autoresponse to a web enquiry</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Auto_responses/Branded_auto_response</template>
    </alerts>
    <fieldUpdates>
        <fullName>Actioned_Field_Update_to_Not_Actioned</fullName>
        <description>Update the field to &quot;Not Actioned&quot;</description>
        <field>Actioned__c</field>
        <literalValue>Not Actioned</literalValue>
        <name>Actioned Field Update to Not Actioned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Awaiting_feedback</fullName>
        <field>Awaiting_Feedback_Datetimestamp__c</field>
        <formula>now()</formula>
        <name>Awaiting feedback</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Enquiry_Type_to_Current_Course</fullName>
        <description>set Enquiry Type to current course</description>
        <field>Enquiry_Type__c</field>
        <literalValue>Current Course</literalValue>
        <name>Case:Enquiry Type to Current Course</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Enquiry_type_to_Future_Course</fullName>
        <description>set case enquiry type to Future Course</description>
        <field>Enquiry_Type__c</field>
        <literalValue>Future Course</literalValue>
        <name>Case:Enquiry Type to Future Course</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_record_type_to_PD</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Professional_Development</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change record type to PD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_to_MGE_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>MGE_Enquiry</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change to MGE Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_to_Standard_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Monash_Connect_Enquiry</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change to Standard Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Flag_Color_Value_SEBS</fullName>
        <description>Clear the value in the field &quot;Flag Color&quot;.</description>
        <field>Flag_Color__c</field>
        <name>Clear Flag Color Value - SEBS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Latest_Inbound_Email_Field</fullName>
        <field>Latest_Inbound_Email__c</field>
        <name>Clear Latest Inbound Email Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incoming_Email_Update2</fullName>
        <description>Update the field to display a icon to denote an email is received</description>
        <field>Incoming_Email__c</field>
        <literalValue>1</literalValue>
        <name>Incoming Email Update2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SEBS_Status_Update</fullName>
        <description>Change the status from Awaiting Customer Feedback to Closed after 2 weeks if there is no response from customer.</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>SEBS Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Channel_to_Email</fullName>
        <description>Set the channel for inbound email case</description>
        <field>Channel__c</field>
        <literalValue>Email</literalValue>
        <name>Set Case Channel to Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Channel_to_Voice_Mail</fullName>
        <description>update the channel to voice mail</description>
        <field>Channel__c</field>
        <literalValue>Voice Mail</literalValue>
        <name>Set Case Channel to Voice Mail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Origin_To_Ask_Monash</fullName>
        <field>Origin</field>
        <literalValue>ask.monash@monash.edu</literalValue>
        <name>Set Case Origin To Ask.Monash</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Priority_to_High</fullName>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Set Case Priority to High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Referred_Flag_to_True</fullName>
        <field>Referred__c</field>
        <literalValue>1</literalValue>
        <name>Set Case Referred Flag to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Status_to_InProgress</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Set Case Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Status_to_SLA</fullName>
        <description>Set the status to show the case has exceeded SLA</description>
        <field>Status</field>
        <literalValue>SLA Alert</literalValue>
        <name>Set Case Status to SLA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Category_1_to_MGE_Admissions</fullName>
        <field>Category_Level_1__c</field>
        <literalValue>MGE Admissions</literalValue>
        <name>Set Category 1 to MGE Admissions</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Category_2_to_Deferral</fullName>
        <field>Category_Level_2__c</field>
        <literalValue>Deferral</literalValue>
        <name>Set Category 2 to Deferral</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Enquiry_Origin_to_Blank</fullName>
        <field>Origin</field>
        <name>Set Enquiry Origin to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Enquiry_Type_to_Future_Student</fullName>
        <field>Enquiry_Type__c</field>
        <literalValue>Future Course</literalValue>
        <name>Set Enquiry Type to Future Student</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Escalated_Flag</fullName>
        <description>salesforce unchecks the field so we need to recheck it</description>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Set Escalated Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Faculty_Email_Address</fullName>
        <field>Faculty_Email__c</field>
        <formula>Faculty_Email_Mapping__c</formula>
        <name>Set Faculty Email Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Health_Related_Information_Flag</fullName>
        <field>Health_Related__c</field>
        <literalValue>1</literalValue>
        <name>Set Health Related Information Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Org_Code_on_Case</fullName>
        <field>Organisation_Unit_Code__c</field>
        <formula>Contact.Organisation_Unit_Code__c</formula>
        <name>Set Org Code on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Org_Name_on_a_Case</fullName>
        <description>Copy the org name from a contact onto a case</description>
        <field>Organisation_Unit__c</field>
        <formula>TEXT( Contact.Organisation_Unit__c )</formula>
        <name>Set Org Name on a Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Personal_information_FlagToTrue</fullName>
        <field>Personal_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Set Personal information Flag to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Personnel_Area_on_a_Case</fullName>
        <description>copy the Personnel Area from a contact to the case</description>
        <field>Personnel_Area__c</field>
        <formula>TEXT(Contact.Personnel_Area__c)</formula>
        <name>Set Personnel Area on a Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Personnel_Sub_Area_on_a_Case</fullName>
        <description>Copy the Personnel Sub Area from a contact record to a case</description>
        <field>Personnel_Sub_Area__c</field>
        <formula>TEXT( Contact.Personnel_Sub_Area__c )</formula>
        <name>Set Personnel Sub Area on a Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Priority_to_Medium</fullName>
        <field>Priority</field>
        <literalValue>Medium</literalValue>
        <name>Set Priority to Medium</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_SR_Enquiry</fullName>
        <field>RecordTypeId</field>
        <lookupValue>SR_Enquiry</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to SR Enquiry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Sensitive_information_Flag_to_True</fullName>
        <field>Sensitive__c</field>
        <literalValue>1</literalValue>
        <name>Set Sensitive information Flag to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Awaiting_Customer_Feedback</fullName>
        <field>Status</field>
        <literalValue>Awaiting Customer Feedback</literalValue>
        <name>Set Status to Awaiting Customer Feedback</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Student_Type_to_International</fullName>
        <field>Student_Type__c</field>
        <literalValue>International</literalValue>
        <name>Set Student Type to International</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Subject_for_International_Find_Cours</fullName>
        <field>Subject</field>
        <formula>&quot;International enquiry for course &quot;  &amp;  Course_Title_f__c</formula>
        <name>Set Subject for International Find Cours</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Subject_to_Deferral_Request</fullName>
        <field>Subject</field>
        <formula>&quot;Deferral Request&quot;</formula>
        <name>Set Subject to Deferral Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Title_on_a_Case</fullName>
        <field>Position_Title__c</field>
        <formula>Contact.Title</formula>
        <name>Set Title on a Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_Faculty_on_a_Case</fullName>
        <description>Set the contact&apos;s faculty/portfolio onto a case</description>
        <field>Faculty_Portfolio__c</field>
        <formula>TEXT( Contact.Faculty_Portfolio__c )</formula>
        <name>Set the Faculty on a Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_School_on_a_Case</fullName>
        <description>Set the contact&apos;s school/division onto the case</description>
        <field>School_Division__c</field>
        <formula>TEXT( Contact.School_Division_Name__c )</formula>
        <name>Set the School on a Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_response_received_flag</fullName>
        <field>Referral_Response_Received__c</field>
        <literalValue>0</literalValue>
        <name>Set the response received flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Incoming_Email_field2</fullName>
        <field>Incoming_Email__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Incoming Email - field2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Channel_to_Ask_Monash</fullName>
        <field>Channel__c</field>
        <literalValue>Web</literalValue>
        <name>Update Case Channel to Ask.Monash</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Category_1_to_Candidature_MGE</fullName>
        <field>Category_Level_1__c</field>
        <literalValue>Candidature</literalValue>
        <name>Update Category 1 to Candidature (MGE)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Category_2_to_Joint_Award_MGE</fullName>
        <field>Category_Level_2__c</field>
        <literalValue>Joint Award</literalValue>
        <name>Update Category 2 to Joint Award (MGE)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Course_Code</fullName>
        <description>Update course code from contact to case</description>
        <field>Course_Code__c</field>
        <formula>Contact.Course_Code__c</formula>
        <name>Update Course Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Priority</fullName>
        <description>Update case priority to High</description>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Update Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Suppress_Survey_field</fullName>
        <field>Supress_Survey__c</field>
        <literalValue>1</literalValue>
        <name>Update Suppress Survey field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Flag_In_Progres_status_SEBS</fullName>
        <field>Flag_Color__c</field>
        <formula>&quot;In Progress&quot;</formula>
        <name>Update the Flag In Progres status - SEBS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Flag_Open_status_SEBS</fullName>
        <description>Pass the value &quot;Open&quot; to display a flag for a case which is more than 24 hours in the queue.</description>
        <field>Flag_Color__c</field>
        <formula>&quot;Open&quot;</formula>
        <name>Update the Flag Open status - SEBS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CASE%3A set type if origin is airport pickup</fullName>
        <actions>
            <name>Case_Enquiry_Type_to_Current_Course</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - Airport Pickup</value>
        </criteriaItems>
        <description>If enquiries that come in from origin “Email – Airport Pickup”  have their “Enquiry Type” auto populated as “Current Course”</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CASE%3A set type if origin is web enquiry</fullName>
        <actions>
            <name>Case_Enquiry_type_to_Future_Course</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>course enquiry webform</value>
        </criteriaItems>
        <description>Enquiries that come in from origin  “Course enquiry webform” will have their “Enquiry Type” auto populated as “Future Course”</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case %28M-PASS%29 - Card Office  Auto Response Email</fullName>
        <actions>
            <name>M_Pass_webform_auto_response_Card_Office</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web - M-Pass</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Category_Level_2__c</field>
            <operation>equals</operation>
            <value>Account Closure</value>
        </criteriaItems>
        <description>Case (M-PASS) - Card Office  Auto Response Email</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case %28M-PASS%29 - Monash Connect Auto Response Email</fullName>
        <actions>
            <name>M_Pass_webform_auto_response_MC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web - M-Pass</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Category_Level_2__c</field>
            <operation>equals</operation>
            <value>Card Creating,Expired/Damaged Card,General Enquiry,Issues Topping Up,Lost/Stolen Card</value>
        </criteriaItems>
        <description>Case (M-PASS) - Monash Connect Auto Response Email</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case %28MGE%29 - Auto Response Email</fullName>
        <actions>
            <name>Case_MGE_Auto_Response_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>startsWith</operation>
            <value>Email MGE,Web - Research</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case %28Monash Online Store%29 - Auto Response Email</fullName>
        <actions>
            <name>Monash_Online_Store_email_response</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web - Online Store</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Category_Level_1__c</field>
            <operation>equals</operation>
            <value>Online Store</value>
        </criteriaItems>
        <description>Case (Monash Online Store) - Auto Response Email</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case %28MyDevelopment%29 - Auto Response Email</fullName>
        <actions>
            <name>Case_MyDevelopment_Auto_Response_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web - Research Current Students</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Category_Level_1__c</field>
            <operation>equals</operation>
            <value>MyDevelopment</value>
        </criteriaItems>
        <description>Case (MyDevelopment) - Auto Response Email</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case %28Schools Engagment%29 - Email New Owner</fullName>
        <actions>
            <name>Case_Email_New_Owner_Schools_Engagement</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Email_School_Visit_Submitter_Schools_Engagement</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Auto response rule for cases owned by Schools Engagement.</description>
        <formula>Owner:Queue.QueueName = &quot;Schools Engagement&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Auto Response Email</fullName>
        <actions>
            <name>Case_Auto_Response_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web,course enquiry webform,Email - System,Web - Research Future Students</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
            <value>,future@monash.edu,ask.monash@monash.edu.au,ask.monash@monash.edu,thomaskeer1960@gmail.com,maria.xplr@gmail.com,reonet@bestbar.com.au</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>Data Loading</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Auto Response Email INTL</fullName>
        <actions>
            <name>Case_Auto_Response_Email_INTL</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - study,Web - Future Students International,Web - Find Course International,Web - Monash College</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
            <value>study@monash.edu,future@monash.edu,ask.monash@monash.edu,ask.monash@monash.edu.au</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>notEqual</operation>
            <value>Data Loading</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Referral Response Received Create Task</fullName>
        <actions>
            <name>Enquiry_referral_response_received</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Referral_Response_Received__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>When a response for a case referral is received create a task and assign to the case owner</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case has triggered an SLA Alert</fullName>
        <actions>
            <name>Set_Case_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Case_Status_to_SLA</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Escalated_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Case_Was_Escalated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>Update status to &quot;SLA Alert&quot; if auto escalation rule is TRUE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Awaiting Cust status to Closed1 - SEBS</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting Customer Feedback</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS,SEBS Placements</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Actioned__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If the case status is &quot;Awaiting Customer Feedback&quot; for more than 2 weeks, change the status to &quot;Closed&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Actioned_Field_Update_to_Not_Actioned</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>SEBS_Status_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Change Awaiting Cust status to Closed2 - SEBS</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting Customer Feedback</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS,SEBS Placements</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Actioned__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>If the case status is &quot;Awaiting Customer Feedback&quot; for more than 2 weeks, change the status to &quot;Closed&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SEBS_Status_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Created - Email - Mge Joint Awards</fullName>
        <actions>
            <name>Update_Category_1_to_Candidature_MGE</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Category_2_to_Joint_Award_MGE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email MGE mge-jointawards</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Created - Email - sr-socialenquiry-l</fullName>
        <actions>
            <name>Set_Student_Type_to_International</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - sr-socialenquiry-l</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Created - Email - study</fullName>
        <actions>
            <name>Set_Student_Type_to_International</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - study</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Created - Web - Find Course International</fullName>
        <actions>
            <name>Case_Enquiry_type_to_Future_Course</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Student_Type_to_International</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Subject_for_International_Find_Cours</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web - Find Course International</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Created - Web - Future Students International</fullName>
        <actions>
            <name>Set_Enquiry_Type_to_Future_Student</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Student_Type_to_International</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web - Future Students International</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Created - Web - Monash College</fullName>
        <actions>
            <name>Set_Student_Type_to_International</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web - Monash College</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Created - Web - Research Current Students</fullName>
        <actions>
            <name>Case_Enquiry_Type_to_Current_Course</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Priority_to_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web - Research Current Students</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Created - Web - Research Deferral Request</fullName>
        <actions>
            <name>Send_Deferral_Request_Notification_to_Faculty</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Category_1_to_MGE_Admissions</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Category_2_to_Deferral</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Enquiry_Type_to_Future_Student</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Faculty_Email_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Priority_to_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Status_to_Awaiting_Customer_Feedback</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Subject_to_Deferral_Request</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web - Research Deferral Request</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Created - Web - Research Future Students</fullName>
        <actions>
            <name>Case_Enquiry_type_to_Future_Course</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Category_1_to_MGE_Admissions</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Priority_to_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Web - Research Future Students</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Display Flag for cases with In Progress for more than 2 days - SEBS</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS,SEBS Placements</value>
        </criteriaItems>
        <description>If the status is &quot;In Progress&quot; for more than 2 days then display flag on the case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_the_Flag_In_Progres_status_SEBS</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Display Flag for cases with Open status for more than 24 hours - SEBS</fullName>
        <active>true</active>
        <booleanFilter>1 AND ((2 AND 3) OR (4 AND 5))</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Timetabling - Staff</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS Placements</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>SEBS Placements</value>
        </criteriaItems>
        <description>When a case is created for SEBS and is in the queue - &quot;Timetabling - Staff&apos; for more than 24hours without taking ownership, then flag the case.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_the_Flag_Open_status_SEBS</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Live Agent Course Code</fullName>
        <actions>
            <name>Update_Course_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Enquiry_Origin_Category__c</field>
            <operation>equals</operation>
            <value>Chat</value>
        </criteriaItems>
        <description>Populate course code from Contact object to Case Object for Course Code field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Manual Escalation</fullName>
        <actions>
            <name>Send_a_notification_for_manual_Esclation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>Set the priority and send an email alert when the status is set to escalated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify User for Email received to a closed case 2 SEBS</fullName>
        <actions>
            <name>Email_received_for_a_closed_case_SEBS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Incoming_Email_Update2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Reopened</value>
        </criteriaItems>
        <description>If an email is received for a case having the status as &quot;Closed&quot; send a notification email to the user and manager.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify User for Email received to a closed case 2 SEBS Placements</fullName>
        <actions>
            <name>Email_received_for_a_closed_case_SEBS_Placements</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Incoming_Email_Update2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS Placements</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Reopened</value>
        </criteriaItems>
        <description>If an email is received for a case having the status as &quot;Closed&quot; send a notification email to the user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>On Hold Task - SEBS</fullName>
        <actions>
            <name>Case_On_Hold_SEBS</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS,SEBS Placements</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>On Hold</value>
        </criteriaItems>
        <description>When On Hold status is selected in a case, create a task to set a reminder and send email to the case owner.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_to_case_owner_regarding_On_Hold_task</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.On_Hold_Date__c</offsetFromField>
            <timeLength>8</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>PD Case - Auto Response Email</fullName>
        <actions>
            <name>PD_Case_Auto_Response_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>email-pd-monash-email-to-case,Web - Professional Development</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Standard Enquiry</value>
        </criteriaItems>
        <description>This is used for Professional Development when a form (Formstack) is submitted.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Queue Changed to MC - Change Record Type</fullName>
        <actions>
            <name>Change_to_Standard_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Monash Connect Enquiries</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Standard Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Flag_Owner_is_Queue__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When enquiry is changed to Monash Connect queue, the enquiry record type is changed to Standard Record Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue Changed to MGE - Change Record Type</fullName>
        <actions>
            <name>Change_to_MGE_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>startsWith</operation>
            <value>MGE</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Flag_Owner_is_Queue__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>MGE Enquiry</value>
        </criteriaItems>
        <description>When enquiry is changed to MGE queues, the enquiry record type is changed to MGE Record Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue Changed to SR - Change Record Type</fullName>
        <actions>
            <name>Set_Record_Type_to_SR_Enquiry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>equals</operation>
            <value>Student Rec Enquiries</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Flag_Owner_is_Queue__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>SR Enquiry</value>
        </criteriaItems>
        <description>When enquiry is changed to SR queue, the enquiry record type is changed to SR Record Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Queue Changed to Short courses- Change Record Type</fullName>
        <actions>
            <name>Change_record_type_to_PD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When enquiry is changed to Short Courses queue, the enquiry record type is changed to Professional Development record type</description>
        <formula>Owner:Queue.QueueName   = &apos;Short Courses - Education&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Referred Case is Still Open After 5 Days</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Referred</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>trigger a time based workflow to send an email notification is a referred case is still open after 5 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_in_a_referred_status_for_5_days</name>
                <type>Alert</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Case Channel Email</fullName>
        <actions>
            <name>Set_Case_Channel_to_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 or 4) and 3</booleanFilter>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>,Web - myDevelopment Staff</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notContain</operation>
            <value>ask.monash,unityconnection@monash.edu.au</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedName</field>
            <operation>contains</operation>
            <value>ask.monash</value>
        </criteriaItems>
        <description>Update the case channel to email for an inbound case that hasn&apos;t come from Ask.Monash</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Case Channel to Voice Mail</fullName>
        <actions>
            <name>Set_Case_Channel_to_Voice_Mail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Enquiry_Origin_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
            <value>unityconnection@monash.edu.au</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>Voice mails create an email from unityconnection@monash.edu.au to hr@monash.edu
Set channel to voice mail</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Case Channel to Web</fullName>
        <actions>
            <name>Set_Case_Origin_To_Ask_Monash</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Channel_to_Ask_Monash</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 or 3) and 2</booleanFilter>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>contains</operation>
            <value>ask.monash</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedName</field>
            <operation>contains</operation>
            <value>ask.monash</value>
        </criteriaItems>
        <description>When an email is sent from Ask.Monash, set the channel to Ask.Monash</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Health Related Information Flag</fullName>
        <actions>
            <name>Set_Health_Related_Information_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Health_Related_Information__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>Set Health Related Information Flag when the health related information field is populated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Personal information Flag</fullName>
        <actions>
            <name>Set_Personal_information_FlagToTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Personal_Information__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>Set Personal  information Flag when the Personal information field is populatec</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Referred flag when Case is Referred</fullName>
        <actions>
            <name>Set_Case_Referred_Flag_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_the_response_received_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Referred</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>Set the case referred flag when the status is changed to referred for reporting</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Sensitive Information Flag</fullName>
        <actions>
            <name>Set_Sensitive_information_Flag_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Sensitive_Information__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>Set Sensitive Information Flag when the  Sensitive Information field is populated</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Status to In Progress for Phone enquires</fullName>
        <actions>
            <name>Set_Case_Status_to_InProgress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>If a phone enquiry is saved for the first time with a status of New update the status to In Progress</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Status Changed - Clear Latest Inbound Email Field</fullName>
        <actions>
            <name>Clear_Latest_Inbound_Email_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When enquiry status is changed this workflow will clear the latest inbound email date field</description>
        <formula>AND(
 ISCHANGED( Status ),
 NOT( ISPICKVAL(Status, &apos;Open&apos;))
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Supress survey when owner is faculty or off system2</fullName>
        <actions>
            <name>Awaiting_feedback</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Suppress_Survey_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or 2</booleanFilter>
        <criteriaItems>
            <field>Case.Enquiry_Owner_Role__c</field>
            <operation>notContain</operation>
            <value>User,Monash Connect</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting Customer Feedback,Referred Off-System,Referred by Phone</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Contact Information on a Case</fullName>
        <actions>
            <name>Set_Org_Code_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Org_Name_on_a_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Personnel_Area_on_a_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Personnel_Sub_Area_on_a_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Title_on_a_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_the_Faculty_on_a_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_the_School_on_a_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>&quot;Stamp&quot; the case with staff member&apos;s organisational information</description>
        <formula>(ISNEW() &amp;&amp; ContactId &lt;&gt; &apos;&apos;) || ISCHANGED( ContactId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Flag Color field for In Progress status - SEBS</fullName>
        <actions>
            <name>Clear_Flag_Color_Value_SEBS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS,SEBS Placements</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>Workflow to clear the value in &apos;Flag Color&apos; after the user changes the enquiry status from &quot;In Progress&quot; to any other value.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Flag Color field for Open status - SEBS</fullName>
        <actions>
            <name>Clear_Flag_Color_Value_SEBS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>3 AND ((1 AND 2) OR (4 AND 5))</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>Timetabling - Staff</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS Placements</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>SEBS Placements</value>
        </criteriaItems>
        <description>Workflow to clear the value in &apos;Flag Color&apos; after the user takes ownership of the enquiry from the queue - &quot;Timetabling - Staff&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Status When Case is Accepted</fullName>
        <actions>
            <name>Set_Case_Status_to_InProgress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updated the case status when a case is taken from the queue by a user</description>
        <formula>AND( RecordType.DeveloperName = &apos;HR&apos;, TEXT(Status) = &apos;New&apos;, LEFT(PRIORVALUE(OwnerId), 3) = &apos;00G&apos;, Owner:Queue.QueueName = &apos;&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Unread Email Message after email is read 1 myResearch%2C SEBS</fullName>
        <actions>
            <name>Uncheck_Incoming_Email_field2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Reopened,Updated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS,SEBS Placements</value>
        </criteriaItems>
        <description>Uncheck the Incoming Email field if case status is not equal to &quot;Updated, Reopened&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>at wrap up%2C add a wrap up task</fullName>
        <actions>
            <name>Wrap_Up_Task</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>to enable consolidated reporting around activities, add a completed task to case when is &apos;wrapped up&apos;</description>
        <formula>AND(  IsClosed ,  NOT( $User.Alias = &quot;dload&quot;) /* exclude data load user */,  RecordType.DeveloperName &lt;&gt; &apos;Admissions&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Case_On_Hold_SEBS</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.On_Hold_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Case On Hold SEBS</subject>
    </tasks>
    <tasks>
        <fullName>Enquiry_referral_response_received</fullName>
        <assignedToType>owner</assignedToType>
        <description>An email response has been received for a enquiry that you own in a referred status</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Enquiry referral response received</subject>
    </tasks>
    <tasks>
        <fullName>Wrap_Up_Task</fullName>
        <assignedToType>owner</assignedToType>
        <description>Enquiry wrapped up</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Wrap Up Task</subject>
    </tasks>
</Workflow>
