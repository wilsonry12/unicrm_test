<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Enrolment_Payment_Receipt</fullName>
        <description>Enrolment Payment Receipt</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Monash_PD_Auto_Emails/Enrolment_Payment_Receipt_VF</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_GST_Allocation</fullName>
        <field>GST_Allocation__c</field>
        <formula>IF( Course_Offering__r.Apply_GST__c , (Payment_Amount__c-(Payment_Amount__c/1.1)) , 0)</formula>
        <name>Update GST Allocation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update GST Allocation</fullName>
        <actions>
            <name>Update_GST_Allocation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNULL(Payment_Amount__c)),NOT(ISNULL( Course_Offering__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
