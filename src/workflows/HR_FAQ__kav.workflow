<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Email</fullName>
        <description>Approval Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>nageen.hussain@pwc.com.prod</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Ask_Monash/HR_FAQ_Approved</template>
    </alerts>
    <alerts>
        <fullName>Approved_email</fullName>
        <description>Approved email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Ask_Monash/HR_FAQ_Approved</template>
    </alerts>
    <alerts>
        <fullName>HRFAQSuper</fullName>
        <ccEmails>vaibhav.x.bhargava@pwc.com</ccEmails>
        <description>HR FAQ Super</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Superannuation</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>HR_Approval_Final_Rejection_Alert</fullName>
        <description>HR Approval Final Rejection Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Assignment_Rejected</template>
    </alerts>
    <alerts>
        <fullName>HR_FAQ_Payroll</fullName>
        <description>HR FAQ Payroll</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Payroll_Services</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>HR_FAQ_Publish_on_Date</fullName>
        <description>HR FAQ Publish on Date</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Publish_on_Date</template>
    </alerts>
    <alerts>
        <fullName>HR_FAQ_Published</fullName>
        <description>HR FAQ Published</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Published</template>
    </alerts>
    <alerts>
        <fullName>HR_FAQ_Recruitment_Rejection_Alert</fullName>
        <description>HR FAQ Recruitment Rejection Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Assignment_Rejected</template>
    </alerts>
    <alerts>
        <fullName>HR_FAQ_Removal_Reminder</fullName>
        <description>HR FAQ Removal Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_FAQ_KM</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Ask_Monash/HR_FAQ_Removal_Reminder</template>
    </alerts>
    <alerts>
        <fullName>HR_FAQ_Superannuate_Rejection_Alert</fullName>
        <description>HR FAQ Superannuate Rejection Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Assignment_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Payroll_Reject</fullName>
        <description>Payroll Reject</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Assignment_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Access</fullName>
        <description>Reminder for Access</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Access</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Analytics</fullName>
        <description>Reminder for Analytics</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Workforce_Analytics</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Communication_Coordinator</fullName>
        <ccEmails>nageen.hussain@pwc.com</ccEmails>
        <description>Reminder for Communication Coordinator</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Communication_Coordinator</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>no.reply-future@f.e.monash.edu</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Employee_Assistance</fullName>
        <description>Reminder for Employee Assistance</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Employee_Assistance</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Ethical</fullName>
        <description>Reminder for Ethical</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Ethical_conduct</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Gender</fullName>
        <description>Reminder for Gender</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Gender_Equity</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Knowledge</fullName>
        <description>Reminder for Knowledge</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Knowledge</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Leadership</fullName>
        <description>Reminder for Leadership</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Leadership_Organisational_Developme</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Policy</fullName>
        <description>Reminder for Policy</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Workforce_Policy_Procedure</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Recruitment</fullName>
        <description>Reminder for Recruitment</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Recruitment</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Relations</fullName>
        <description>Reminder for Relations</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Workplace_Relations</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Remuneration</fullName>
        <description>Reminder for Remuneration</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Remuneration_Global_Mobility</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Reminder_for_Staff</fullName>
        <description>Reminder for Staff</description>
        <protected>false</protected>
        <recipients>
            <recipient>HR_Staff_Development</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ask_Monash/HR_FAQ_Approval_Reminder</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Pending_approver</fullName>
        <field>PendingApproval__c</field>
        <name>Clear Pending approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approval</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Leadership&quot;), &quot;Leadership&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Remuneration&quot;), &quot;Remuneration&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Staff&quot;), &quot;Staff&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Policy&quot;), &quot;Policy&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;)))))))</formula>
        <name>Set Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Super&quot;), &quot;Super&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Payroll&quot;), &quot;Payroll&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Recruitment&quot;), &quot;Recruitment&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Access&quot;), &quot;Access&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Employee&quot;), &quot;Employee&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Ethical&quot;), &quot;Ethical&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Gender&quot;), &quot;Gender&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Leadership&quot;), &quot;Leadership&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Remuneration&quot;), &quot;Remuneration&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Staff&quot;), &quot;Staff&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Policy&quot;), &quot;Policy&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;,
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;))))))))))))))</formula>
        <name>Set Pending Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver1</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Payroll&quot;), &quot;Payroll &quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Recruitment&quot;), &quot;Recruitment&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Access&quot;), &quot;Access&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Employee&quot;), &quot;Employee&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Ethical&quot;), &quot;Ethical&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Gender&quot;), &quot;Gender&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Leadership&quot;), &quot;Leadership&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Remuneration&quot;), &quot;Remuneration&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Staff&quot;), &quot;Staff&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Policy&quot;), &quot;Policy&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;)))))))))))))</formula>
        <name>Set Pending Approver1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver_10</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;))</formula>
        <name>Set Pending Approver 10</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver_11</fullName>
        <field>PendingApproval__c</field>
        <formula>&apos;Knowledge&apos;</formula>
        <name>Set Pending Approver 11</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver_12</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Employee&quot;), &quot;Employee&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Ethical&quot;), &quot;Ethical&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Gender&quot;), &quot;Gender&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Leadership&quot;), &quot;Leadership&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Remuneration&quot;), &quot;Remuneration&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Staff&quot;), &quot;Staff&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Policy&quot;), &quot;Policy&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;))))))))))</formula>
        <name>Set Pending Approver 12</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver_2</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Recruitment&quot;), &quot;Recruitment&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Access&quot;), &quot;Access&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Employee&quot;), &quot;Employee&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Ethical&quot;), &quot;Ethical&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Gender&quot;), &quot;Gender&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Leadership&quot;), &quot;Leadership&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Remuneration&quot;), &quot;Remuneration&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Staff&quot;), &quot;Staff&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Policy&quot;), &quot;Policy&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;))))))))))))</formula>
        <name>Set Pending Approver 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver_3</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Access&quot;), &quot;Access&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Employee&quot;), &quot;Employee&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Ethical&quot;), &quot;Ethical&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Gender&quot;), &quot;Gender&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Leadership&quot;), &quot;Leadership&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Remuneration&quot;), &quot;Remuneration&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Staff&quot;), &quot;Staff&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Policy&quot;), &quot;Policy&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;)))))))))))</formula>
        <name>Set Pending Approver 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver_4</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Ethical&quot;), &quot;Ethical&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Gender&quot;), &quot;Gender&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Leadership&quot;), &quot;Leadership&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Remuneration&quot;), &quot;Remuneration&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Staff&quot;), &quot;Staff&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Policy&quot;), &quot;Policy&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;)))))))))</formula>
        <name>Set Pending Approver 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver_5</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Gender&quot;), &quot;Gender&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Leadership&quot;), &quot;Leadership&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Remuneration&quot;), &quot;Remuneration&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Staff&quot;), &quot;Staff&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Policy&quot;), &quot;Policy&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;))))))))</formula>
        <name>Set Pending Approver 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver_6</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Remuneration&quot;), &quot;Remuneration&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Staff&quot;), &quot;Staff&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Policy&quot;), &quot;Policy&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;))))))</formula>
        <name>Set Pending Approver 6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver_8</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Policy&quot;), &quot;Policy&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;))))</formula>
        <name>Set Pending Approver 8</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_Approver_9</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;)))</formula>
        <name>Set Pending Approver 9</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Pending_approver_7</fullName>
        <field>PendingApproval__c</field>
        <formula>If(Contains (PendingApprovalFormula__c, &quot;Staff&quot;), &quot;Staff&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Policy&quot;), &quot;Policy&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Relations&quot;), &quot;Relations&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Analytics&quot;), &quot;Analytics&quot;, 
If(Contains (PendingApprovalFormula__c, &quot;Knowledge&quot;), &quot;Knowledge&quot;,&apos;&apos;)))))</formula>
        <name>Set Pending approver 7</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Pending_Approval</fullName>
        <field>PendingApproval__c</field>
        <formula>&apos;CommunicationCoordinator&apos;</formula>
        <name>Update Pending Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validation_Status</fullName>
        <field>ValidationStatus</field>
        <literalValue>Pending Approval</literalValue>
        <name>Validation Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validation_Status_Approved</fullName>
        <field>ValidationStatus</field>
        <literalValue>Approved</literalValue>
        <name>Validation Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validation_Status_Not_Approved</fullName>
        <field>ValidationStatus</field>
        <literalValue>Not Approved</literalValue>
        <name>Validation Status Not Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validation_Status_Published</fullName>
        <field>ValidationStatus</field>
        <literalValue>Published</literalValue>
        <name>Validation Status Published</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Validation_Status_update</fullName>
        <field>ValidationStatus</field>
        <literalValue>Approved</literalValue>
        <name>Validation Status update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>PublishHRFAQ</fullName>
        <action>Publish</action>
        <label>PublishHRFAQ</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>HR FAQ Access Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Access</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Access</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Access</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Access</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Analytics Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Analytics</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Analytics</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Analytics</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Analytics</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Comm Coord Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>CommunicationCoordinator</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Communication_Coordinator</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Communication_Coordinator</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Communication_Coordinator</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Employee Assistance Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Employee</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Employee_Assistance</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Employee_Assistance</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Employee_Assistance</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Ethical Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Ethical</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Ethical</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Ethical</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Ethical</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Gender Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Gender</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Gender</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Gender</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Gender</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Knowledge Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Knowledge</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Knowledge</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Knowledge</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Knowledge</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Leadership Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Leadership</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Leadership</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Leadership</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Leadership</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Payroll Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Payroll</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>HR_FAQ_Payroll</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>HR_FAQ_Payroll</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>HR_FAQ_Payroll</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Policy Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Policy</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Policy</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Policy</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Policy</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Recruitment Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Recruitment</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Recruitment</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Recruitment</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Recruitment</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Relations Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Relations</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Relations</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Relations</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Relations</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Removal reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.Proposed_Removal_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>HR_FAQ_Removal_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Remuneration Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Remuneration</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Remuneration</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Remuneration</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Remuneration</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Staff Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Staff</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Staff</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Staff</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_for_Staff</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>HR FAQ Super Pending Approval</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.PendingApproval__c</field>
            <operation>contains</operation>
            <value>Super</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>HRFAQSuper</name>
                <type>Alert</type>
            </actions>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>HRFAQSuper</name>
                <type>Alert</type>
            </actions>
            <timeLength>9</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>HRFAQSuper</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Publish HR FAQ</fullName>
        <actions>
            <name>HR_FAQ_Published</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Validation_Status_Published</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PublishHRFAQ</name>
            <type>KnowledgePublish</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.ValidationStatus</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>HR_FAQ__kav.AutomaticallyPublishOnApproval__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Publish On Publication Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>HR_FAQ__kav.AutomaticallyPublishOnApproval__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>HR_FAQ__kav.ValidationStatus</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>HR_FAQ_Publish_on_Date</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>PublishHRFAQ</name>
                <type>KnowledgePublish</type>
            </actions>
            <offsetFromField>HR_FAQ__kav.PlannedPublicationDate__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
