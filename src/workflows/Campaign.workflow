<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_to_create_list_view_on_SR_Lead_Call_object</fullName>
        <ccEmails>servicedesk@monash.edu</ccEmails>
        <description>Notification to create list view on SR Lead Call object</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Auto_responses/SR_Lead_Call_Create_Public_List_View_For_Campaign</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_delete_list_view_on_SR_Lead_Call_object</fullName>
        <ccEmails>servicedesk@monash.edu</ccEmails>
        <description>Notification to delete list view on SR Lead Call object</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Auto_responses/SR_Lead_Call_Delete_List_View</template>
    </alerts>
    <fieldUpdates>
        <fullName>BMSUpdateApprovedDateTimeFU</fullName>
        <field>Approved_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>BMSUpdateApprovedDateTimeFU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BMSUpdateApprovedDateTime</fullName>
        <actions>
            <name>BMSUpdateApprovedDateTimeFU</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Approved_Date_Time__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SR Lead Call Campaign Created - Create List View</fullName>
        <actions>
            <name>Notification_to_create_list_view_on_SR_Lead_Call_object</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>SR Campaign</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Type</field>
            <operation>equals</operation>
            <value>Domestic Post App,Domestic Pre App,International Post App,International Pre App</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SR Lead Call Campaign Ended - Delete List View</fullName>
        <actions>
            <name>Notification_to_delete_list_view_on_SR_Lead_Call_object</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>SR Campaign</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Type</field>
            <operation>equals</operation>
            <value>Domestic Post App,Domestic Pre App,International Post App,International Pre App</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.Status</field>
            <operation>equals</operation>
            <value>Completed,Aborted</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
