<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Course_Offering_Date_Cancelled</fullName>
        <description>When the Course Offering status is cancelled, the Date Cancelled/Postponed field is updated</description>
        <field>Date_Cancelled_Postponed__c</field>
        <formula>Today()</formula>
        <name>Update Course Offering Date Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_course_offering_status_to_Full</fullName>
        <description>Update status to Full when number of remaining places is equal or less than zero</description>
        <field>Status__c</field>
        <literalValue>Full</literalValue>
        <name>Update course offering status to Full</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Course Offering Date Cancelled</fullName>
        <actions>
            <name>Update_Course_Offering_Date_Cancelled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Course_Offering__c.Status__c</field>
            <operation>equals</operation>
            <value>Cancelled</value>
        </criteriaItems>
        <description>Update the Date Cancelled/Postponed field when the status is changed to Cancelled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update course offering status to Full</fullName>
        <actions>
            <name>Update_course_offering_status_to_Full</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Course_Offering__c.Number_of_Remaining_Places__c</field>
            <operation>lessOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Course_Offering__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Draft</value>
        </criteriaItems>
        <description>Updates the status of the course offering when the max. capacity is zero</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
