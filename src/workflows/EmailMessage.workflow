<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Case_Re_Open_myResearch</fullName>
        <description>Change Case Status to Open from any closed status when incoming email is true.</description>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Case Re-Open myResearch</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_status_to_Updated</fullName>
        <field>Status</field>
        <literalValue>Updated</literalValue>
        <name>Case status to Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Priority</fullName>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Change Priority</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_status_to_Reopened_SEBS</fullName>
        <description>Update the status from closed to reopened.</description>
        <field>Status</field>
        <literalValue>Reopened</literalValue>
        <name>Change status to Reopened - SEBS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_status_to_Updated_myResearcSEBS</fullName>
        <description>Update the status to &quot;Updated&quot;</description>
        <field>Status</field>
        <literalValue>Updated</literalValue>
        <name>Change status to Updated - myResearcSEBS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Copy_Email_Text_to_Case_Field</fullName>
        <description>Copy the email text body to Case Field.</description>
        <field>Email_Body_Message_Copy__c</field>
        <formula>TextBody</formula>
        <name>Copy Email Text to Case Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Incoming_Email_Update1</fullName>
        <description>Update the field to display a icon to denote an email is received.</description>
        <field>Incoming_Email__c</field>
        <literalValue>1</literalValue>
        <name>Incoming Email Update1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Re_open_enquiry</fullName>
        <field>Status</field>
        <literalValue>Reopened</literalValue>
        <name>Re-open enquiry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_response_received_flag1</fullName>
        <description>Update the response received flag on the case</description>
        <field>Referral_Response_Received__c</field>
        <literalValue>1</literalValue>
        <name>Set the response received flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Incoming_Email_field1</fullName>
        <description>Uncheck the incoming email field to clear the unread email icon in the console and layout.</description>
        <field>Incoming_Email__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Incoming Email field1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>General_HR</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_myResearch</fullName>
        <description>Update Case Owner when Case is re-opened and move to myResearch Support Queue.</description>
        <field>OwnerId</field>
        <lookupValue>myResearch_Support</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner myResearch</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Email_Received_for_an_Open_Status</fullName>
        <field>Email_Received_Open_Status__c</field>
        <literalValue>1</literalValue>
        <name>Update Email Received for an Open Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Case Re-Open myResearch</fullName>
        <actions>
            <name>Case_Re_Open_myResearch</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Owner_myResearch</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>myResearch</value>
        </criteriaItems>
        <description>Case Re-Open when an Email is sent to a case having status - Solved, Referred to Faculty, Referred to Remedy, Referred to Central Business Unit.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Display Email Icon for Incoming Email and change status to Updated - myResearch%2C SEBS</fullName>
        <actions>
            <name>Change_status_to_Updated_myResearcSEBS</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Incoming_Email_Update1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4</booleanFilter>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS,SEBS Placements</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>myResearch</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting Customer Feedback,In Progress,On Hold,Referred to Faculty,Referred to Remedy,Referred to Central Business Unit,Referred to Project,Awaiting approval</value>
        </criteriaItems>
        <description>Show Email icon when an email is received for a case and update the status to &quot;Updated&quot; for SEBS and myResearch Support.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Notification on case status</fullName>
        <actions>
            <name>Case_status_to_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress,Follow-up,Referred,Escalated</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>Email Notification on case status &apos;in progress&apos; or &apos;follow up</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Notify User for Email received to a closed case 1 SEBS</fullName>
        <actions>
            <name>Change_status_to_Reopened_SEBS</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Copy_Email_Text_to_Case_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS,SEBS Placements</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>If an email is received for a case having the status as &quot;Closed&quot;, change status to &quot;reopened&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reopen case for an incoming email</fullName>
        <actions>
            <name>Change_Priority</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Re_open_enquiry</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Unread Email Message after email is read 2 myResearch%2C SEBS</fullName>
        <actions>
            <name>Uncheck_Incoming_Email_field1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>SEBS,SEBS Placements</value>
        </criteriaItems>
        <description>Clear the value to remove the unread email icon from the list view.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update a Referred Case When Inbound Email Recieved</fullName>
        <actions>
            <name>Set_the_response_received_flag1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Referred</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <description>Update the repsonse received flag on status when an email received</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update the Email Received for an Open Status</fullName>
        <actions>
            <name>Update_Email_Received_for_an_Open_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Referred,Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>HR</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
