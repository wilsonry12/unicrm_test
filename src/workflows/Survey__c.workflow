<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Bad_survey_result</fullName>
        <description>Bad survey result</description>
        <protected>false</protected>
        <recipients>
            <recipient>MC_survey_monitors</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Survey_Templates/Bad_survey_response_alert</template>
    </alerts>
    <alerts>
        <fullName>Send_open_survey_3_hours_alert</fullName>
        <description>Send open survey &gt; 3 hours alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>MC_survey_monitors</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Survey_Templates/Survey_response_open_3_hours</template>
    </alerts>
    <fieldUpdates>
        <fullName>Status_Update_Date</fullName>
        <field>Status_Updated_Date__c</field>
        <formula>LastModifiedDate</formula>
        <name>Status Update - Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Survey_Status_Change</fullName>
        <field>Status_Update__c</field>
        <literalValue>1</literalValue>
        <name>Survey : Status Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Survey - Status Change</fullName>
        <actions>
            <name>Status_Update_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Survey_Status_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>not( 
and(ISCHANGED(Status__c ), ISPICKVAL(Status__c, &quot;Feedback Received&quot;) 
) 
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
