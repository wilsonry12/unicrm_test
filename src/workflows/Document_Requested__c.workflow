<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_RT_to_Requested</fullName>
        <description>Change the record type to Requested so that it uses the read only page layout (Submitted Document Request Layout (FB))</description>
        <field>RecordTypeId</field>
        <lookupValue>Requested</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change RT to Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change RT to Requested</fullName>
        <actions>
            <name>Change_RT_to_Requested</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Document_Requested__c.Document_Request_Status__c</field>
            <operation>equals</operation>
            <value>Requested</value>
        </criteriaItems>
        <description>This WF changes the record type to &apos;Requested&apos; when the document request status is requested. This was created so that the page layout used is read only (Submitted Document Request Layout (FB))</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
