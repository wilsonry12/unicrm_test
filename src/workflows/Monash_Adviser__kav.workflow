<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Untick_Confirm_URL_Change</fullName>
        <field>Confirm_URL_Change__c</field>
        <literalValue>0</literalValue>
        <name>Untick Confirm URL Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Untick Confirm URL Change</fullName>
        <actions>
            <name>Untick_Confirm_URL_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Monash_Adviser__kav.Confirm_URL_Change__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow will untick the Confrim URL Change checkbox everytime it ticked and saved.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
