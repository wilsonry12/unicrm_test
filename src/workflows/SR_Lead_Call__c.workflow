<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Call_Status_to_Close</fullName>
        <field>Call_Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Set Call Status to Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_to_SR_Lead_Call_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Student_Rec_Leads</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Set Owner to SR Lead Call Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Created - SR Lead Call</fullName>
        <actions>
            <name>Set_Owner_to_SR_Lead_Call_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SR_Lead_Call__c.OwnerId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Updated - Auto Close Call</fullName>
        <actions>
            <name>Set_Call_Status_to_Close</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SR_Lead_Call__c.Call_Result__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>SR_Lead_Call__c.Decision__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>SR_Lead_Call__c.Reason_for_Decision__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
