<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PD_Course_Type</fullName>
        <description>Auto update Course Type field to Short Course</description>
        <field>Type__c</field>
        <literalValue>Short_Course</literalValue>
        <name>PD_Course Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PD_Managing_Faculty_auto_update</fullName>
        <field>Managing_Faculty__c</field>
        <literalValue>Faculty of Education</literalValue>
        <name>PD_Managing Faculty auto update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>PD_Auto Update Fields</fullName>
        <actions>
            <name>PD_Course_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PD_Managing_Faculty_auto_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Auto populate Managing_Faculty__c and Type_c fields</description>
        <formula>$UserRole.Id = &apos;00E0k000000MAef&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
