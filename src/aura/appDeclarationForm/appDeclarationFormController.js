({
	doInit : function(component, event, helper) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var sParamId = '';
        var i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.
            for(var x = 0; x < sParameterName.length; x++){
                if(sParameterName[x] === 'appId'){
                   sParamId = sParameterName[x+1] === undefined ? 'Not found' : sParameterName[x+1];
                }
            }
        }
        
        component.set("v.appId", sParamId);

        if (sParamId != '') {
	        var action_retrieveAppRecord = component.get("c.retrieveApplication");
	        action_retrieveAppRecord.setParams({ "applicationId"   : sParamId });
	        action_retrieveAppRecord.setCallback(this, function(a) {
	            var appRecord = a.getReturnValue();
	            component.set("v.appRecord", appRecord);
	            if (appRecord.Status__c != 'Draft') {
	            	//redirect to dashboard
        			helper.gotoURL("/");
	            }
	        });

	        $A.enqueueAction(action_retrieveAppRecord);
        } 
        else {
        	//redirect to dashboard
        	helper.gotoURL("/");
        }
	},

	backToApplication : function(component, event, helper) {
		var applicationId = component.get("v.appId");
		helper.gotoURL('/applicationdetail?appId=' + applicationId);
	},

	submitApplication : function(component, event, helper) {
		var appbutton = component.find("acceptButton");
        $A.util.addClass(appbutton.getElement(), 'disabled');
        
		var applicationRecord = component.get("v.appRecord");

		var actionSubmitApplication = component.get("c.acceptDeclaration");
		actionSubmitApplication.setParams({"applicationId" : applicationRecord.Id});
		actionSubmitApplication.setCallback(this, function(response) {
			var appRecord = response.getReturnValue();
			var address = '/applicationsuccess?appId=' + appRecord.Id;

	        var urlEvent = $A.get("e.force:navigateToURL");
	        urlEvent.setParams({
	          "url": address,
	          "isredirect" :false
	        });
	        urlEvent.fire();
		});

		$A.enqueueAction(actionSubmitApplication);
	}
})