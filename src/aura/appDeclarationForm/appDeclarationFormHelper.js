({
	/*******************************************************************************
    * @author       Ant Custodio
    * @date         21.Jun.2017         
    * @description  redirects the users to the given URL
    * @revision     
    *******************************************************************************/
    gotoURL : function(address) {
    	var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": address,
          "isredirect" :false
        });
        urlEvent.fire();
    },

})