({
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  initial actions on page load - retrieve the contact details and
    *                   check if read only
    * @revision     AC, 13.Jun.2017 - moved the appId retrieval to helper class
    *******************************************************************************/
    doInit : function(component, event, helper) {
        //get appId from URL
        helper.retrieveAppIdFromURL(component);
        //retrieve the qualifications
        helper.retrieveQualificationList(component);

        /*******************************************************************************
        * @author       Ant Custodio
        * @date         3.Apr.2017         
        * @description  retrieve the contact details
        * @revision     
        *******************************************************************************/
        var action_retrieveInstitutionPrefix = component.get("c.retrieveInstitutionPrefix");
        action_retrieveInstitutionPrefix.setCallback(this, function(a) {
            component.set("v.institutionPrefix", a.getReturnValue());
        });
        $A.enqueueAction(action_retrieveInstitutionPrefix);

    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         10.Jul.2017         
    * @description  re-query the list of qualifications on the page
    * @revision    
    *******************************************************************************/
    reQuery : function(component, event, helper) {
        if (window.location.hash == '#documentList') {
            //retrieve the qualifications
            helper.retrieveQualificationList(component);

            var childCmp = component.find("addDocQual");
            //reinitialise the add document component
            childCmp.reInitialise();
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  shows/hides panel
    * @revision     
    *******************************************************************************/
    showHideComponent : function (component, event, helper) {
        var isExpanded = component.get("v.isExpanded");
        
        if (isExpanded) {
            isExpanded = false;
        } else {
            isExpanded = true;
        }
        component.set("v.isExpanded", isExpanded);
    },
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  shows/hides qualification form for inserting a new one
    * @revision     
    *******************************************************************************/
    showQualificationForm : function (component, event, helper) {

        //retrieve record types list
        helper.retrievePicklistValues(component.get("c.retrieveRecordTypeNames"), component.find("recordTypeOptions"));
        component.set("v.qualRecordType", "");
        component.set("v.isEdit", false);

        var childCmp = component.find("addDocQual");
        //reinitialise the add document component
        childCmp.reInitialise();

        helper.displayQualForm(component, null);
        
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Jul.2017         
    * @description  closes the form
    * @revision     
    *******************************************************************************/
    closeForm : function (component, event, helper) {
        //clear the attachment section
        helper.clearAllValues(component);
        helper.clearAllErrors(component);
        helper.hideAllScoreFields(component);
        
        component.set("v.showForm", false);
        component.set("v.isEdit", false);
        window.location.hash = '#myQualDiv';
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         27.Apr.2017         
    * @description  when qualification is not found
    * @revision     
    *******************************************************************************/
    secondaryQual_onChange : function(component, event, helper) {
        helper.lookupNotFound_onSelectChange(component, "qualNotFoundSecondary", "v.showOtherQualSecondary", "Other_Qualification__c");
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  when school is not found
    * @revision     
    *******************************************************************************/
    tertiarySchool_onChange : function(component, event, helper) {
        helper.lookupNotFound_onSelectChange(component, "schoolNotFoundTertiary", "v.showOtherSchoolTertiary", "Other_Institution__c");
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  when school is not found
    * @revision     
    *******************************************************************************/
    secondarySchool_onChange : function(component, event, helper) {
        helper.lookupNotFound_onSelectChange(component, "schoolNotFoundSecondary", "v.showOtherSchoolSecondary", "Other_Institution__c");
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  when school is not found
    * @revision     
    *******************************************************************************/
    isInstructionEnglish_onSelectChange : function(component, event, helper) {
        var showQualText = component.get("v.instructionInEnglish");
        var newRecord = component.get("v.contQualification");
        if (showQualText) {
            showQualText = false;
            newRecord.Instruction_in_English__c = false;
        } else {
            showQualText = true;
            newRecord.Instruction_in_English__c = true;
        }
    },
    
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  show the spinner when page is loading
    * @revision     
    *******************************************************************************/
    waiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "block";
        }
    },
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  hide the spinner when finished loading
    * @revision     
    *******************************************************************************/
    doneWaiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "none";
        }

        window.setTimeout(
            $A.getCallback(function() {
                if (accSpinner != null) {
                    if (accSpinner.style.display != 'none') {
                        accSpinner.style.display = "none";
                    }
                }
            }), 5000
        );
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  hide the spinner when finished loading
    * @revision     
    *******************************************************************************/
    toggleWaiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            if (component.get("v.toggleSpinner")) {
                accSpinner.style.display = "block";
                component.set("v.toggleSpinner", false);
            } else {
                accSpinner.style.display = "none";
                component.set("v.toggleSpinner", true)
            }
        }
    },

    /**
     * Handler for receiving the updateLookupIdEvent event
     */
    handleAccountIdUpdate : function(component, event, helper) {
        // Get the Id from the Event
        var qualRec = event.getParam("sObjectId");
        var recordTypeOption = component.find("recordTypeOptions");
        var recordTypeSelected = recordTypeOption.get("v.value");
        var institutionPrefix = component.get("v.institutionPrefix");
        if (recordTypeSelected == 'Tertiary Education') {
            component.set('v.selSchoolTertiary', qualRec);
        } else if (recordTypeSelected == 'Secondary Education') {
            if (qualRec.substring(0,3) == institutionPrefix) {
                // Set the Id bound to the View
                component.set('v.selSchoolSecondary', qualRec);
            } else {
                // Set the Id bound to the View
                component.set('v.selQualSecondary', qualRec);
            }
        }
        
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         2.May.2017         
    * @description  gets the contact document Id
    * @revision     
    *******************************************************************************/
    retrieveRelatedDocId : function(component, event, helper) {
        // Get the Id from the Event
        var contactDocId = event.getParam("contactDocId");
        component.set('v.contactDocId', contactDocId);

        /*******************************************************************************
        * @author       Ant Custodio
        * @date         3.Apr.2017         
        * @description  sets the record type Id
        * @revision     
        *******************************************************************************/
        var newRecord = component.get("v.contQualification");
        newRecord.sobjectType = 'Contact_Qualification__c';

        var recordTypeOption = component.find("recordTypeOptions");
        var recordTypeSelected = recordTypeOption.get("v.value");
        
        var dynamicCmp = component.find("recordTypeOptions");
        var action_retrieveRecordType = component.get("c.retrieveRecordTypeByName");

        action_retrieveRecordType.setParams({ "recordTypeName"   : dynamicCmp.get("v.value") });
        action_retrieveRecordType.setCallback(this, function(a) {
            //sets the record type Id
            newRecord.RecordTypeId = a.getReturnValue();
            /*******************************************************************************
            * @author       Ant Custodio
            * @date         3.Apr.2017         
            * @description  retrieve the custom setting and set it to the contact fields
            * @revision     TODO - Revisit institution object. Instead of saving 
                                Institution_Code__c and Institution_Name__c, use Id instead. 
                                This is because Institution Object is previously a Custom 
                                Setting and we cannot do a lookup to it
            *******************************************************************************/
            var action_retrieveCustomSetting = component.get("c.retrieveCustomSettingById");
            //populate the qualification and institution depending on the record type
            var institutionIdToUse = '';
            if (recordTypeSelected == 'Tertiary Education' && !component.get("v.showOtherSchoolTertiary")) {
                institutionIdToUse = component.get("v.selSchoolTertiary");
            }
            if (recordTypeSelected == 'Secondary Education') {
                if (!component.get("v.showOtherSchoolSecondary")) {
                    institutionIdToUse = component.get("v.selSchoolSecondary");
                } else {
                    newRecord.Institution_Code__c = '';
                    newRecord.Institution_Name__c = '';
                }
                if (!component.get("v.showOtherQualSecondary")) {
                    newRecord.Qualification__c = component.get("v.selQualSecondary");
                } else {
                    newRecord.Qualification__c = '';
                }
            }
            //capture the toggle information
            newRecord.Instruction_in_English__c = component.get("v.instructionInEnglish");

            action_retrieveCustomSetting.setParams({ "institutionId" :  institutionIdToUse});
            action_retrieveCustomSetting.setCallback(this, function(a) {
                var institutionRetrieved = a.getReturnValue();
                //only insert if not toggled to manual text
                if (institutionRetrieved.Id != null) {
                    newRecord.Institution_Code__c = institutionRetrieved.Institution_Code__c;
                    newRecord.Institution_Name__c = institutionRetrieved.Institution_Name__c;
                } else {
                    newRecord.Institution_Code__c = '';
                    newRecord.Institution_Name__c = '';
                }
                /*******************************************************************************
                * @author       Ant Custodio
                * @date         3.Apr.2017         
                * @description  inserts the new contact qualification
                * @revision     
                *******************************************************************************/
                var action_addNewQualification = component.get("c.upsertNewQualificationRecord");
                //if it has selected qualification
                if (component.get("v.selTestType") != '' && component.get("v.selTestType") != null) {
                    //if opened in application
                    if (component.get("v.isAppComponent")) {
                        action_addNewQualification = component.get("c.upsertNewQualificationRecordWithSelQualWithApp");
                        action_addNewQualification.setParams({  "qualToUpsert" : newRecord, 
                                                                "qualificationSelected" : component.get("v.selTestType"), 
                                                                "applicationId" : component.get("v.appId"),
                                                                "qualDocId" : component.get("v.contactDocId")
                                                            });
                    } else { //if opened in contactdetails
                        //change the class
                        action_addNewQualification = component.get("c.upsertNewQualificationRecordWithSelQual");
                        action_addNewQualification.setParams({  "qualToUpsert" : newRecord, 
                                                                "qualificationSelected" : component.get("v.selTestType"),
                                                                "qualDocId" : component.get("v.contactDocId")
                                                            });
                    }
                } else {
                    if (component.get("v.isAppComponent")) {
                        //change the class
                        action_addNewQualification = component.get("c.upsertNewQualificationRecordWithApp");
                        action_addNewQualification.setParams({  "qualToUpsert" : newRecord, 
                                                                "applicationId" : component.get("v.appId"),
                                                                "qualDocId" : component.get("v.contactDocId")
                                                            });
                    } else {
                        //do not change the class and use the original
                        action_addNewQualification.setParams({  "qualToUpsert" : newRecord,
                                                                "qualDocId" : component.get("v.contactDocId") });
                    }
                }
                action_addNewQualification.setCallback(this, function(a) {
                    var state = a.getState();
                    //4th action, retrieve qualification list
                    helper.retrieveConQualList(component, state);
                });
                //3rd action, insert contact qualification record
                $A.enqueueAction(action_addNewQualification);    
            });
            //2nd action, retrieve custom setting
            $A.enqueueAction(action_retrieveCustomSetting);
        });
        //1st action, retrieve record type
        $A.enqueueAction(action_retrieveRecordType);
    },
 
    /**
     * Handler for receiving the clearLookupIdEvent event
     */
    handleAccountIdClear : function(component, event, helper) {
        //clears the lookup field
        helper.clearLookupField(component, event);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  validate the contact details before saving
    * @revision     
    *******************************************************************************/
    addNewQualification : function(component, event, helper) {
        var newRecord = component.get("v.contQualification");
        newRecord.sobjectType = 'Contact_Qualification__c';
        newRecord.Date_Achieved__c = component.get("v.selectedDate");
        if (component.get("v.qualRecordType") == 'English Test') {
            newRecord.Date_Achieved__c = component.get("v.englishSelectedDate");  
        }
        var recordTypeOption = component.find("recordTypeOptions");
        var recordTypeSelected = recordTypeOption.get("v.value");
        helper.validateForm(component, newRecord);
        var hasError = component.get("v.hasErrors");
        if (!hasError) {
            /*******************************************************************************
            * @author       Ant Custodio
            * @date         26.Apr.2017         
            * @description  attach the document added on the form
            * @revision     
            *******************************************************************************/
            var childCmp = component.find("addDocQual");
            //insert contact document, then this will fire an event that inserts the contact qual
            childCmp.uploadDocumentMethod();
        } else {
            component.set("v.errorMessage", "There are errors on your page. Please review your form.");
            window.location.hash = '#appQual_errorDiv';
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         9.Apr.2017         
    * @description  set value on change of country 
    * @revision     
    *******************************************************************************/
    yearComp_onChange: function(component, event, helper) {
        var dynamicCmp = component.find("yearcompleteOptions");
        component.set("v.contQualification.Year_of_Completion__c", dynamicCmp.get("v.value"));

        helper.clearError_onChange(component, event);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         9.Apr.2017         
    * @description  set value on change of country 
    * @revision     
    *******************************************************************************/
    country_onChange: function(component, event, helper) {
        var dynamicCmp = component.find("countryOptions");
        component.set("v.contQualification.Country__c", dynamicCmp.get("v.value"));
        component.set("v.instructionInEnglish", false);
        if (dynamicCmp.get("v.value") == 'Australia') {
            component.set("v.instructionInEnglish", true);
        }
        
        helper.clearAllValues(component);
        helper.clearAllErrors(component);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         9.Apr.2017         
    * @description  set value on change of country 
    * @revision     
    *******************************************************************************/
    state_onChange: function(component, event, helper) {
        var dynamicCmp = component.find("stateOptions");
        component.set("v.contQualification.State__c", dynamicCmp.get("v.value"));

        //clear the lookups
        /*var qualSecondary = component.find("qualLookupSecondary");
        qualSecondary.clearSelected();
        var insSecondary = component.find("insLookupSecondary");
        insSecondary.clearSelected();*/

        //helper.clearErrors(component, "qualificationSecondary");
        helper.clearErrors(component, "otherQualificationSecondary");
        //helper.clearErrors(component, "schoolSecondary");
        helper.clearErrors(component, "otherSchoolSecondary");
        helper.clearErrors(component, "score");
        
        helper.clearError_onChange(component, event);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         9.Apr.2017         
    * @description  set value on change of state 
    * @revision     
    *******************************************************************************/
    changeState: function(component, event) {
        var dynamicCmp = component.find("stateOptions");
        component.set("v.selectedState", dynamicCmp.get("v.value"));
    },   

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         9.Apr.2017         
    * @description  set value on change of country 
    * @revision     
    *******************************************************************************/
    changeCountry: function(component, event) {
        var dynamicCmp = component.find("countryOptions");
        component.set("v.selectedCountry", dynamicCmp.get("v.value"));
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         9.Apr.2017         
    * @description  set value on change of country 
    * @revision     
    *******************************************************************************/
    recordType_onChange: function(component, event, helper) {
        //retrieve admission test types
        helper.retrievePicklistValues(component.get("c.retrieveAdmissionTestTypes"), component.find("admissionTestOptions"));

        //retrieve status options
        helper.retrievePicklistValues(component.get("c.retrieveStatusOptions"), component.find("statusOptions"));

        //retrieve countries list
        helper.retrievePicklistValues(component.get("c.retrieveCountries"), component.find("countryOptions"));

        //retrieve first year completion list
        helper.retrievePicklistValues(component.get("c.retrievefirstYearComplete"), component.find("firstYearCompOptions"));

        //retrieve last year completion list
        helper.retrievePicklistValues(component.get("c.retrievelastYearComplete"), component.find("lastYearCompOptions"));

        //retrieve year of completion list
        helper.retrievePicklistValues(component.get("c.retrieveYearofCompletion"), component.find("yearcompleteOptions"));

        //retrieve states list
        helper.retrievePicklistValues(component.get("c.retrieveStates"), component.find("stateOptions"));
        
        helper.clearValue(component, "countryOptions");

        if (component.get("v.qualRecordType") == 'English Test') {
            helper.clearValue(component, "engTestdateAchieved");
        }
    },

    initDatePicker : function (component, event, helper) {
        if(component.get("v.initializeDatePicker")){
            TinyDatePicker(document.querySelector('.ux-datepicker'), {
              // Used to convert a date into a string to be used as the value of input 
              format: function (date) {
                return moment(date).format('DD/MMM/YYYY');
              },
              // Used to parse a date string and return a date (e.g. parsing the input value)
              parse: function (str) {
                var date = moment(str,'DD/MMM/YYYY');
                return new Date(moment(str,'DD/MMM/YYYY'));
              },
              // Names of months, in order 
              months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
              // Names of days of week, in order 
              days: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
              // The text for the button used to set the date to today's date 
              today: 'Today',
              // The text for the button used to clear the input value 
              clear: 'Clear',
              // The text for the button used to close the form 
              close: 'Close',
              // Specifies the minimum date that can be selected DD/MM/YYYY
              min: moment().subtract(100,'y').format('DD/MMM/YYYY'),
              // Specifies the maximum date that can be selected 
              max: moment().add(3,'y').format('DD/MMM/YYYY'),
              // Place datepicker selector on this date if field is still empty 
              preselectedDate: moment().format('DD/MMM/YYYY'),
              // There are two modes: dp-modal (the default) and dp-below. 
              // dp-modal makes the date picker show up as a modal. 
              // dp-below makes it show up beneath its input element. 
              mode: 'dp-below',
              // Whether to use Monday as start of the week 
              weekStartsMonday: false,
              // A function which is called any time the date picker opens 
              onOpen: function (context) {
                // context is the datepicker context, detailed below 
              }, 
              // A function which is called any time the year is selected 
              // in the year menu 
              onSelectYear: function (context) {
                // context is the datepicker context, detailed below 
              },
              // A function which is called any time the month is selected 
              // in the month menu 
              onSelectMonth: function (context) {
                // context is the datepicker context, detailed below 
              },
            }).open();

            component.set("v.initializeDatePicker", false);
        }
        
    },

    initDateAchievePicker : function (component, event, helper) {
        if(component.get("v.initializeDatePicker")){
            TinyDatePicker(document.querySelector('.ux-dateachievepicker'), {
              // Used to convert a date into a string to be used as the value of input 
              format: function (date) {
                return moment(date).format('DD/MMM/YYYY');
              },
              // Used to parse a date string and return a date (e.g. parsing the input value)
              parse: function (str) {
                var date = moment(str,'DD/MMM/YYYY');
                return new Date(moment(str,'DD/MMM/YYYY'));
              },
              // Names of months, in order 
              months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
              // Names of days of week, in order 
              days: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
              // The text for the button used to set the date to today's date 
              today: 'Today',
              // The text for the button used to clear the input value 
              clear: 'Clear',
              // The text for the button used to close the form 
              close: 'Close',
              // Specifies the minimum date that can be selected DD/MM/YYYY
              min: moment().subtract(100,'y').format('DD/MMM/YYYY'),
              // Specifies the maximum date that can be selected 
              max: moment().add(3,'y').format('DD/MMM/YYYY'),
              // Place datepicker selector on this date if field is still empty 
              preselectedDate: moment().format('DD/MMM/YYYY'),
              // There are two modes: dp-modal (the default) and dp-below. 
              // dp-modal makes the date picker show up as a modal. 
              // dp-below makes it show up beneath its input element. 
              mode: 'dp-below',
              // Whether to use Monday as start of the week 
              weekStartsMonday: false,
              // A function which is called any time the date picker opens 
              onOpen: function (context) {
                // context is the datepicker context, detailed below 
              }, 
              // A function which is called any time the year is selected 
              // in the year menu 
              onSelectYear: function (context) {
                // context is the datepicker context, detailed below 
              },
              // A function which is called any time the month is selected 
              // in the month menu 
              onSelectMonth: function (context) {
                // context is the datepicker context, detailed below 
              },
            }).open();

            component.set("v.initializeDatePicker", false)
        }
        
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         17.May.2017         
    * @description  check date achieved/planned
    * @revision     
    *******************************************************************************/
    dateAchieved_onChange: function(component, event, helper) {
       var enteredDate = component.get("v.englishSelectedDate");
        var earliestDate = new Date(new Date().getFullYear()-99, 0, 1);
        var regDateFormat = /^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$/;
        
        $A.util.removeClass(component.find("engTestdateAchieved"), 'dateError');
        component.set("v.dateAchievedErrorMessage", "");
        component.set("v.invalidDateAchieved", false);

        if (enteredDate == 'Invalid Date') {
            component.set("v.dateAchievedErrorMessage", "Please enter a valid date (dd/mmm/yyyy).");
            $A.util.addClass(component.find("engTestdateAchieved"), 'dateError');
            component.set("v.invalidDateAchieved", true);
        } else if (!enteredDate.match(regDateFormat)) {
            component.set("v.dateAchievedErrorMessage", "Please enter a valid date (dd/mmm/yyyy).");
            $A.util.addClass(component.find("engTestdateAchieved"), 'dateError');
            component.set("v.invalidDateAchieved", true);
        } else {
            enteredDate = new Date(enteredDate.split("-")[0], enteredDate.split("-")[1]-1, enteredDate.split("-")[2]);
            var dateNow = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            if(enteredDate > dateNow && component.get("v.contQualification.isTestCompleted__c")) {
                component.set("v.dateAchievedErrorMessage", "Please check the year in your Date Achieved - You have entered a future date");
                $A.util.addClass(component.find("engTestdateAchieved"), 'dateError');
                component.set("v.invalidDateAchieved", true);
            } else if (enteredDate <= earliestDate && component.get("v.contQualification.isTestCompleted__c")) {
                component.set("v.dateAchievedErrorMessage", "You have entered an invalid date");
                $A.util.addClass(component.find("engTestdateAchieved"), 'dateError');
                component.set("v.invalidDateAchieved", true);
            } else if (enteredDate < dateNow && !component.get("v.contQualification.isTestCompleted__c")) {
                component.set("v.dateAchievedErrorMessage", "Please check the year in your Date Planned - You have entered a date from the past");
                $A.util.addClass(component.find("engTestdateAchieved"), 'dateError');
                component.set("v.invalidDateAchieved", true);
            }
        }

    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         17.May.2017         
    * @description  check date achieved/planned
    * @revision     
    *******************************************************************************/
    otherDateAchieved_onChange: function(component, event, helper) {
        var enteredDate = component.get("v.selectedDate");
        var earliestDate = new Date(new Date().getFullYear()-99, 0, 1);
        var regDateFormat = /^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$/;
        
        $A.util.removeClass(component.find("dateAchieved"), 'dateError');
        component.set("v.dateAchievedErrorMessage", "");
        component.set("v.invalidDateAchieved", false);

        if (enteredDate == 'Invalid Date') {
            component.set("v.dateAchievedErrorMessage", "Please enter a valid date (dd/mmm/yyyy).");
            $A.util.addClass(component.find("dateAchieved"), 'dateError');
            component.set("v.invalidDateAchieved", true);
        } else if (!enteredDate.match(regDateFormat)) {
            component.set("v.dateAchievedErrorMessage", "Please enter a valid date (dd/mmm/yyyy).");
            $A.util.addClass(component.find("dateAchieved"), 'dateError');
            component.set("v.invalidDateAchieved", true);
        } else {
            enteredDate = new Date(enteredDate.split("-")[0], enteredDate.split("-")[1]-1, enteredDate.split("-")[2]);
            var dateNow = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            if(enteredDate > dateNow) {
                component.set("v.dateAchievedErrorMessage", "Please check the year in your Date Achieved - You have entered a future date");
                $A.util.addClass(component.find("dateAchieved"), 'dateError');
                component.set("v.invalidDateAchieved", true);
            } else if (enteredDate <= earliestDate) {
                component.set("v.dateAchievedErrorMessage", "You have entered an invalid date");
                $A.util.addClass(component.find("dateAchieved"), 'dateError');
                component.set("v.invalidDateAchieved", true);
            }
        }

    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         9.Apr.2017         
    * @description  set value on change of country 
    * @revision     
    *******************************************************************************/
    isTestCompleted_Yes: function(component, event, helper) {
        helper.hasSelectedYes(component);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         9.Apr.2017         
    * @description  set value on change of country 
    * @revision     
    *******************************************************************************/
    isTestCompleted_No: function(component, event, helper) {
        helper.hasSelectedNo(component);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         19.Apr.2017         
    * @description  cancel the deletion
    * @revision     
    *******************************************************************************/
    showConfirmDeletePopup : function (component, event, helper){     
        /*var domEvent = event.getParams().domEvent;
        var bodySpan = domEvent.target.nextSibling;
        var qualId = bodySpan.dataset.id;*/
        var source = event.getSource(); // this would give that particular component
        var qualId = source.get("v.name"); // returns the id
    
        //assign Id and show the confirmation popup
        component.set("v.selRecToDelId", qualId);
        component.set("v.showConfirmPopup", true);
    }, 

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         19.Apr.2017         
    * @description  shows the view popup
    * @revision     
    *******************************************************************************/
    showviewQualPopup : function (component, event, helper){
        var source = event.getSource(); // this would give that particular component
        var qualId = source.get("v.name"); // returns the id
        //assign Id and show the confirmation popup
        helper.retrieveSelectedQualification(component, qualId, component.get("v.isAppComponent"));
        component.set("v.showViewPopup", true);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         5.Jul.2017         
    * @description  edits the selected qualification
    * @revision     
    *******************************************************************************/
    editQualification : function (component, event, helper){
        //retrieve countries list
        helper.retrievePicklistValues(component.get("c.retrieveCountries"), component.find("countryOptions"));

        //retrieve first year completion list
        helper.retrievePicklistValues(component.get("c.retrievefirstYearComplete"), component.find("firstYearCompOptions"));

        //retrieve last year completion list
        helper.retrievePicklistValues(component.get("c.retrievelastYearComplete"), component.find("lastYearCompOptions"));

        //retrieve year of completion list
        helper.retrievePicklistValues(component.get("c.retrieveYearofCompletion"), component.find("yearcompleteOptions"));

        //retrieve states list
        helper.retrievePicklistValues(component.get("c.retrieveStates"), component.find("stateOptions"));

        //retrieve record types list
        helper.retrievePicklistValues(component.get("c.retrieveRecordTypeNames"), component.find("recordTypeOptions"));

        //retrieve admission test types
        helper.retrievePicklistValues(component.get("c.retrieveAdmissionTestTypes"), component.find("admissionTestOptions"));

        //retrieve status options
        helper.retrievePicklistValues(component.get("c.retrieveStatusOptions"), component.find("statusOptions"));

        var source = event.getSource(); // this would give that particular component
        var qualId = source.get("v.name"); // returns the id
        //assign Id and show the confirmation popup
        //TODO
        helper.displayQualForm(component, qualId);

        //disable edit on qualification type and country
        component.set("v.isEdit", true);
    },
     
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  cancel the deletion
    * @revision     
    *******************************************************************************/
    cancelDelete : function (component, event, helper){     
        //close the confirmation popup
        component.set("v.showConfirmPopup", false);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         11.May.2017         
    * @description  cancel the deletion
    * @revision     
    *******************************************************************************/
    closeViewModal : function (component, event, helper){     
        //close the confirmation popup
        component.set("v.showViewPopup", false);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  deletes the contact qualification record
    * @revision     
    *******************************************************************************/
    deleteQualification : function (component, event, helper){     
        helper.deleteSelectedQualification(component);

        var childCmp = component.find("addDocQual");
        //reinitialise the add document component
        childCmp.reInitialise();
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         27.Apr.2017         
    * @description  set value on change of first year enrolled 
    * @revision     
    *******************************************************************************/
    firstYearComp_onChange: function(component, event, helper) {
        var dynamicCmp = component.find("firstYearCompOptions");
        component.set("v.contQualification.First_Year_Enrolled__c", dynamicCmp.get("v.value"));

        helper.clearError_onChange(component, event);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         27.Apr.2017         
    * @description  set value on change of first year enrolled 
    * @revision     
    *******************************************************************************/
    lastyear_onChange: function(component, event, helper) {
        helper.clearError_onChange(component, event);

        var dynamicCmp = component.find("lastYearCompOptions");
        component.set("v.contQualification.Last_Year_Enrolled__c", dynamicCmp.get("v.value"));
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         27.Apr.2017         
    * @description  set value on change of first year enrolled 
    * @revision     Ant Custodio, 17.Jul.2017 - clears the english scores on change
    *******************************************************************************/
    testType_onChange: function(component, event, helper) {
        var dynamicCmp = component.find("admissionTestOptions");
        component.set("v.selTestType", dynamicCmp.get("v.value"));
        helper.clearError_onChange(component, event);
        helper.hideAllScoreFields(component);
        if (component.get("v.isCompleted") == "YES") {
            //display the appropriate score fields
            helper.displayScoreFields(component);
            helper.clearAllScoreFields(component);
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         27.Apr.2017         
    * @description  set value on change of first year enrolled 
    * @revision     
    *******************************************************************************/
    status_onChange: function(component, event, helper) {
        var dynamicCmp = component.find("statusOptions");
        component.set("v.contQualification.Status__c", dynamicCmp.get("v.value"));

        helper.clearError_onChange(component, event);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         28.Apr.2017         
    * @description  on change general behaviour
    * @revision     
    *******************************************************************************/
    onChangeGeneralBehaviour: function(component, event, helper) {
        helper.clearError_onChange(component, event);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         28.Apr.2017         
    * @description  on change general behaviour
    * @revision     
    *******************************************************************************/
    domesticScore_onChange: function(component, event, helper) {
        helper.validateDomesticScore(component);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         17.Jul.2017         
    * @description  set value on change of english test
    * @revision     
    *******************************************************************************/
    englishScore_onChange: function(component, event, helper) {
        component.debug('englishScore_onChange');
        var source = event.getSource(); // this would give that particular component
        var fieldValue = source.get("v.value"); // returns the value
        component.debug('fieldValue: ' + fieldValue);
        var key = source.get("v.label");
        component.debug('key: ' + key);
    },

})