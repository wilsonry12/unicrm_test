({
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  retrieve the picklist values
    * @revision     
    *******************************************************************************/
    retrievePicklistValues : function(actionToRun, inputsel) {
        var opts=[];
        if (inputsel != null) {
            actionToRun.setCallback(this, function(a) {
                opts.push({"class": "optionClass", label: "--Select--", value: ""});
                for(var i=0;i< a.getReturnValue().length;i++){
                    var picklistLabel = a.getReturnValue()[i];
                    if (picklistLabel == 'Tertiary Education') {
                        picklistLabel += '/Post Secondary';
                    }
                    opts.push({"class": "optionClass", label: picklistLabel, value: a.getReturnValue()[i]});
                }
                inputsel.set("v.options", opts);

            });
            $A.enqueueAction(actionToRun); 
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  validates the form
    * @revision     
    *******************************************************************************/
    validateForm : function(component, newRecord) {
        var isValid = true;

        var recordTypeOption = component.find("recordTypeOptions");
        var recordTypeSelected = recordTypeOption.get("v.value");
        //SECONDARY AND TERTIARY SPECIFIC FIELD CHECKING
        if (recordTypeSelected == 'Secondary Education' || recordTypeSelected == 'Tertiary Education') {
            var countryOptions = component.find("countryOptions");
            if (!this.isFieldPopulated(newRecord.Country__c)) {
                countryOptions.set("v.errors", [{message:"Country is required"}]);
                isValid = false;
            } else {
                countryOptions.set("v.errors", null);
            }
        }
        //END - SECONDARY AND TERTIARY SPECIFIC FIELD CHECKING

        //SECONDARY SPECIFIC FIELD CHECKING
        if (recordTypeSelected == 'Secondary Education') {
            var yearcompleteOptions = component.find("yearcompleteOptions");
            if (!this.isFieldPopulated(newRecord.Year_of_Completion__c)) {
                yearcompleteOptions.set("v.errors", [{message:"Year of Completion is required"}]);
                isValid = false;
            } else {
                yearcompleteOptions.set("v.errors", null);
            }

            var stateOptions = component.find("stateOptions");
            if (newRecord.Country__c == 'Australia' && !this.isFieldPopulated(newRecord.State__c)) {
                stateOptions.set("v.errors", [{message:"State is required"}]);
                isValid = false;
            } else {
                stateOptions.set("v.errors", null);
            }

            //var qualificationSecondary = component.find("qualificationSecondary");
            var selQualSecondary = component.get("v.selQualSecondary"); 
            if (    !component.get("v.showOtherQualSecondary") &&
                    !this.isFieldPopulated(selQualSecondary) ) {
                component.set("v.qualSecondaryErr", "Qualification is required");
                isValid = false;
            } else {
                component.set("v.qualSecondaryErr", "");
            }
            var otherQualificationSecondary = component.find("otherQualificationSecondary");
            if (otherQualificationSecondary != null && otherQualificationSecondary != '' && otherQualificationSecondary != undefined) {
                if (    component.get("v.showOtherQualSecondary") &&
                        !this.isFieldPopulated(newRecord.Other_Qualification__c) ){
                    otherQualificationSecondary.set("v.errors", [{message:"Other Qualification is required"}]);
                    isValid = false;
                } else {
                    otherQualificationSecondary.set("v.errors", null);
                }
            }
            //FOR DOMESTIC VALIDATIONS
            if (newRecord.Country__c == 'Australia') {
                var selSchoolSecondary = component.get("v.selSchoolSecondary"); 
                if (    !component.get("v.showOtherSchoolSecondary") &&
                        !this.isFieldPopulated(selSchoolSecondary) ) {
                    component.set("v.schoolSecondaryErr", "School is required");
                    isValid = false;
                } else {
                    component.set("v.schoolSecondaryErr", "");
                }
                var otherSchoolSecondary = component.find("otherSchoolSecondary");
                if (otherSchoolSecondary != null && otherSchoolSecondary != '' && otherSchoolSecondary != undefined) {
                    if (    component.get("v.showOtherSchoolSecondary") &&
                            !this.isFieldPopulated(newRecord.Other_Institution__c) ){
                        otherSchoolSecondary.set("v.errors", [{message:"Other School is required"}]);
                        isValid = false;
                    } else {
                        otherSchoolSecondary.set("v.errors", null);
                    }
                }
                var score = component.find("score");
                if (score.get("v.value") != null && score.get("v.value") != '' ) {
                    if (!this.validateDomesticScore(component, score)) {
                        isValid = false;
                    } 
                } else {
                    score.set("v.errors", [{message:"Overall Score is required"}]);
                    isValid = false;
                }

            } else {
                var OSSchoolSecondary = component.find("OSSchoolSecondary");
                if (!this.isFieldPopulated(newRecord.Other_Institution__c)){
                    OSSchoolSecondary.set("v.errors", [{message:"School is required"}]);
                    isValid = false;
                } else {
                    OSSchoolSecondary.set("v.errors", null);
                }
            }
        }
        //END - SECONDARY SPECIFIC FIELD CHECKING

        //TERTIARY SPECIFIC FIELD CHECKING
        if (recordTypeSelected == 'Tertiary Education') {
            var otherQualificationTertiary = component.find("otherQualificationTertiary");
            if ( !this.isFieldPopulated(newRecord.Other_Qualification__c) ){
                otherQualificationTertiary.set("v.errors", [{message:"Qualification is required"}]);
                isValid = false;
            } else {
                otherQualificationTertiary.set("v.errors", null);
            }

            var selSchoolTertiary = component.get("v.selSchoolTertiary"); 
            if (    !component.get("v.showOtherSchoolTertiary") &&
                    !this.isFieldPopulated(selSchoolTertiary)) {
                component.set("v.schoolTertiaryErr", "Institution is required");
                isValid = false;
            } else {
                component.set("v.schoolTertiaryErr", "");
            }

            var otherSchoolTertiary = component.find("otherSchoolTertiary");
            if (    component.get("v.showOtherSchoolTertiary") &&
                    !this.isFieldPopulated(newRecord.Other_Institution__c) ){
                otherSchoolTertiary.set("v.errors", [{message:"Other Institution is required"}]);
                $A.util.addClass(otherSchoolTertiary, 'has-errors');
                isValid = false;
            } else {
                $A.util.removeClass(otherSchoolTertiary, 'has-errors');
                otherSchoolTertiary.set("v.errors", null);
            }

            var firstYearCompOptions = component.find("firstYearCompOptions");
            if (!this.isFieldPopulated(newRecord.First_Year_Enrolled__c)) {
                firstYearCompOptions.set("v.errors", [{message:"First Year Enrolled is required"}]);
                isValid = false;
            } else {
                firstYearCompOptions.set("v.errors", null);
            }

            var lastYearCompOptions = component.find("lastYearCompOptions");
            if (!this.isFieldPopulated(newRecord.Last_Year_Enrolled__c)) {
                lastYearCompOptions.set("v.errors", [{message:"Last Year Enrolled is required"}]);
                isValid = false;
            } else if (parseInt(newRecord.First_Year_Enrolled__c) > parseInt(newRecord.Last_Year_Enrolled__c)) {
                lastYearCompOptions.set("v.errors", [{message:"Last Year Enrolled should be greater than First Year Enrolled"}]);
                isValid = false;
            } else {
                lastYearCompOptions.set("v.errors", null);
            }

            var statusOptions = component.find("statusOptions");
            if (!this.isFieldPopulated(newRecord.Status__c)) {
                statusOptions.set("v.errors", [{message:"Level of Completion is required"}]);
                isValid = false;
            } else {
                statusOptions.set("v.errors", null);
            }
        }

        //OTHER QUALIFICATION SPECIFIC FIELD CHECKING
        if (recordTypeSelected == 'Other Qualification') {
            var otherQualificationOther = component.find("otherQualificationOther");
            if (!this.isFieldPopulated(newRecord.Other_Qualification__c)) {
                otherQualificationOther.set("v.errors", [{message:"Qualification is required"}]);
                isValid = false;
            } else {
                otherQualificationOther.set("v.errors", null);
            }

            var otherDateAchievedError = component.find("otherDateAchievedError");
            if (component.get("v.invalidDateAchieved")) {
                $A.util.addClass(component.find("dateAchieved"), 'has-errors');
                isValid = false;
            }

            if (component.get("v.invalidDateAchieved")) {
                $A.util.addClass(component.find("dateAchieved"), 'dateError');
                isValid = false;
            } 

            if (!this.isFieldPopulated(newRecord.Date_Achieved__c)) {
                component.set("v.dateAchievedErrorMessage", "Date Achieved is required");
                $A.util.addClass(component.find("dateAchieved"), 'dateError');
                isValid = false;
            }
        }
        //END - OTHER QUALIFICATION SPECIFIC FIELD CHECKING

        //ENGLISH TEST SPECIFIC FIELD CHECKING
        if (recordTypeSelected == 'English Test') {
            var admissionTestOptions = component.find("admissionTestOptions");
            var selTestType = component.get("v.selTestType"); 
            if (!this.isFieldPopulated(selTestType)) {
                admissionTestOptions.set("v.errors", [{message:"Admission Test Type is required"}]);
                isValid = false;
            } else {
                admissionTestOptions.set("v.errors", null);
            }

            var isCompleted = component.get("v.isCompleted"); 
            var isCompleteErrorMsg = component.find("isCompleteErrorMsg");
            if (!this.isFieldPopulated(isCompleted)) {
                isCompleteErrorMsg.set("v.errors", [{message:"Is this a completed test? is required"}]);
                isValid = false;
            } else {
                isCompleteErrorMsg.set("v.errors", null);
            }

            if (component.get("v.invalidDateAchieved")) {
                $A.util.addClass(component.find("engTestdateAchieved"), 'dateError');
                isValid = false;
            }

            //only validate the date if test is completed

            if (isCompleted == "YES") {
                if (!this.isFieldPopulated(newRecord.Date_Achieved__c)) {
                    component.set("v.dateAchievedErrorMessage", "Date Achieved is required");
                    $A.util.addClass(component.find("engTestdateAchieved"), 'dateError');
                    isValid = false;
                }
                var overallScore = component.find("overall");
                if (overallScore.get("v.value") != null && overallScore.get("v.value") != '' ) {
                    if (!this.validateDomesticScore(component, overallScore)) {
                        isValid = false;
                    } 
                }

            }
        }
        //END - ENGLISH TEST SPECIFIC FIELD CHECKING

        if (isValid) {
            component.set("v.hasErrors", false);
        } else {
            component.set("v.hasErrors", true);
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  on change event when qualificationTertiary is not found
    * @revision     
    *******************************************************************************/
    lookupNotFound_onSelectChange : function(component, sectionToToggle, otherInsOrQualValue, valueToClear) {
        // first get the div element. by using aura:id
        var changeElement = component.find(sectionToToggle);
        var isOtherDisplayed = component.get(otherInsOrQualValue);
        if (isOtherDisplayed) {
            isOtherDisplayed = false;
        } else {
            isOtherDisplayed = true;
        }
        component.set(otherInsOrQualValue, isOtherDisplayed);
        // by using $A.util.toggleClass add-remove slds-hide class
        $A.util.toggleClass(changeElement, "slds-hide");

        /*******************************************************************************
        * @author       Ant Custodio
        * @date         7.Apr.2017         
        * @description  remove the 'other' values when value is set to No and remove
                        the Ids when value is set to Yes
        * @revision     
        *******************************************************************************/
        var conQual = component.get("v.contQualification");
        if (!isOtherDisplayed) {
            if (valueToClear == 'Other_Institution__c') {
                conQual.Other_Institution__c = '';
            } else if (valueToClear == 'Other_Qualification__c') {
                conQual.Other_Qualification__c = '';
            }
        } else {
            if (valueToClear == 'Other_Institution__c') {
                conQual.Institution_Name__c = '';
                conQual.Institution_Code__c = '';
            } else if (valueToClear == 'Other_Qualification__c') {
                conQual.Qualification__c = '';
            }
        }
        component.set("v.contQualification", conQual);
    },
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         12.Apr.2017         
    * @description  retrieve the contact details
    * @revision     
    *******************************************************************************/
    retrieveQualificationList : function (component){
        var action_retrieveConQualList = component.get("c.retrieveConQualList");
        //checks which table to display/return
        if (component.get("v.isAppComponent") == true) {
            action_retrieveConQualList = component.get("c.retrieveAppQualProvidedList");
            action_retrieveConQualList.setParams({ "applicationId"   : component.get("v.appId") });
        }
        action_retrieveConQualList.setCallback(this, function(a) {
            component.set("v.qualList", a.getReturnValue());
            //determine whether to show credit intention section or not
            this.displayCreditIntentionSection(component, a.getReturnValue());
        });

        //Action 2, retrieve qualification list
        $A.enqueueAction(action_retrieveConQualList);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         11.May.2017         
    * @description  retrieve the selected qualification
    * @revision     
    *******************************************************************************/
    retrieveSelectedQualification : function (component, qualId, queryOnAQP){
        var action = component.get("c.retrieveAppQualificationById");
        if (!queryOnAQP) {
            action = component.get("c.retrieveQualificationById");
            action.setParams({ "qualId"   : qualId });
            action.setCallback(this, function(a) {
                component.set("v.selQualToView", a.getReturnValue());
            });
        } else {
            action.setParams({ "qualId"   : qualId });
            action.setCallback(this, function(a) {
                component.set("v.selAppQualToView", a.getReturnValue());
            });
        }
        $A.enqueueAction(action);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  clears a specific field error
    * @revision     
    *******************************************************************************/
    clearErrors : function(component, auraId) {
        var foundComponent = component.find(auraId);
        if (foundComponent != null) {
            foundComponent.set("v.errors", null);
            $A.util.removeClass(foundComponent, 'has-errors');
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  clears all form errors
    * @revision     
    *******************************************************************************/
    clearAllErrors : function(component) {
        this.clearErrors(component, "countryOptions");
        this.clearErrors(component, "yearcompleteOptions");
        this.clearErrors(component, "stateOptions");
        this.clearErrors(component, "qualificationSecondary");
        this.clearErrors(component, "otherQualificationSecondary");
        this.clearErrors(component, "schoolSecondary");
        this.clearErrors(component, "otherSchoolSecondary");
        this.clearErrors(component, "OSSchoolSecondary");
        this.clearErrors(component, "qualificationTertiary");
        this.clearErrors(component, "otherQualificationTertiary");
        this.clearErrors(component, "schoolTertiary");
        this.clearErrors(component, "otherSchoolTertiary");
        this.clearErrors(component, "firstYearCompOptions");
        this.clearErrors(component, "lastYearCompOptions");
        this.clearErrors(component, "otherQualificationOther");
        //this.clearErrors(component, "dateAchieved");
        this.clearErrors(component, "admissionTestOptions");
        this.clearErrors(component, "isCompleteErrorMsg");
        this.clearErrors(component, "statusOptions");
        this.clearErrors(component, "score");

        /*var secondaryQual = component.find("qualLookupSecondary");
        if (secondaryQual != null) {
            secondaryQual.clearSelected();
            $A.util.removeClass(secondaryQual, 'has-errors');
        }

        var secondaryIns = component.find("insLookupSecondary");
        if (secondaryIns != null) {
            secondaryIns.clearSelected();
            $A.util.removeClass(secondaryIns, 'has-errors');
        }

        var tertiaryIns = component.find("insLookupTertiary");
        if (tertiaryIns != null) {
            $A.util.removeClass(tertiaryIns, 'has-errors');
        }*/

        component.set("v.isCompleted", "");

        var isTestCompletedFalse = component.find("isTestCompletedFalse");
        isTestCompletedFalse.set("v.value", false);

        var isTestCompletedTrue = component.find("isTestCompletedTrue");
        isTestCompletedTrue.set("v.value", false);
        //clear Date variables
        component.set("v.invalidDateAchieved", false);
        component.set("v.dateAchievedErrorMessage", "");
        component.set("v.initializeDatePicker", true);
        component.set("v.selectedDate", "");

        component.set("v.hasErrors", false);
        component.set("v.errorMessage", "");
        var englishTestDate = component.find("engTestdateAchieved");
        if (englishTestDate != null) {
            $A.util.removeClass(englishTestDate, 'dateError');
        }

        var dateAchieved = component.find("dateAchieved");
        if (dateAchieved != null) {
            $A.util.removeClass(dateAchieved, 'dateError');
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         19.Jun.2017         
    * @description  clears the record values
    * @revision     
    *******************************************************************************/
    clearValue : function(component, auraId) {
        var foundComponent = component.find(auraId);
        if (foundComponent != null) {
            foundComponent.set("v.value", "");
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Jun.2017         
    * @description  hides the div
    * @revision     
    *******************************************************************************/
    hideDiv : function(component, auraId) {
        var foundComponent = component.find(auraId);
        if (foundComponent != null) {
            $A.util.addClass(foundComponent, "slds-hide");
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Jun.2017         
    * @description  removes the has-errors class
    * @revision     
    *******************************************************************************/
    removeErrors : function(component, auraId) {
        var foundComponent = component.find(auraId);
        if (foundComponent != null) {
            foundComponent.clearSelected();
            $A.util.removeClass(foundComponent, 'has-errors');
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  clears all form errors
    * @revision     
    *******************************************************************************/
    clearAllValues : function(component) {
        this.clearValue(component, "yearcompleteOptions");
        this.clearValue(component, "stateOptions");
        this.clearValue(component, "qualificationSecondary");
        this.clearValue(component, "otherQualificationSecondary");
        this.clearValue(component, "schoolSecondary");
        this.clearValue(component, "otherSchoolSecondary");
        this.clearValue(component, "OSSchoolSecondary");
        this.clearValue(component, "qualificationTertiary");
        this.clearValue(component, "otherQualificationTertiary");
        this.clearValue(component, "schoolTertiary");
        this.clearValue(component, "otherSchoolTertiary");
        this.clearValue(component, "firstYearCompOptions");
        this.clearValue(component, "lastYearCompOptions");
        this.clearValue(component, "otherQualificationOther");
        //this.clearValue(component, "dateAchieved");
        this.clearValue(component, "admissionTestOptions");
        this.clearValue(component, "isCompleteErrorMsg");
        this.clearValue(component, "statusOptions");
        this.clearValue(component, "score");
        this.clearValue(component, "overall");
        this.clearValue(component, "Listening");
        this.clearValue(component, "Reading");
        this.clearValue(component, "Speaking");
        this.clearValue(component, "Writing");
        this.clearValue(component, "twe");
        this.clearValue(component, "essay");
        //clear all selected qualifications/institutions on the lookup
        component.set("v.selQualSecondary", "");
        component.set("v.selSchoolSecondary", "");
        component.set("v.selSchoolTertiary", "");
        //removes the manual errors
        this.removeErrors(component, "qualLookupSecondary");
        this.removeErrors(component, "insLookupSecondary");
        this.removeErrors(component, "insLookupTertiary");
        //hides the divs
        /*this.hideDiv(component, "qualNotFoundTertiary");
        this.hideDiv(component, "schoolNotFoundTertiary");
        this.hideDiv(component, "qualNotFoundSecondary");
        this.hideDiv(component, "schoolNotFoundSecondary");*/
        //disable the toggles
        component.set("v.showOtherSchoolTertiary", false);
        component.set("v.schoolToggleTertiary", false);
        component.set("v.showOtherSchoolSecondary", false);
        component.set("v.showOtherQualSecondary", false);
        component.set("v.schoolToggleSecondary", false);
        component.set("v.qualToggleSecondary", false);
        component.set("v.isCompleted", "");
        //clear the attachment section
        var childCmp = component.find("addDocQual");
        childCmp.clearComponent();

        var isTestCompletedFalse = component.find("isTestCompletedFalse");
        isTestCompletedFalse.set("v.value", false);
        var isTestCompletedTrue = component.find("isTestCompletedTrue");
        isTestCompletedTrue.set("v.value", false);
        //clear Date variables
        component.set("v.invalidDateAchieved", false);
        component.set("v.dateAchievedErrorMessage", "");
        component.set("v.initializeDatePicker", true);
        component.set("v.selectedDate", "");
        component.set("v.hasErrors", false);
        component.set("v.errorMessage", "");
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         28.Apr.2017         
    * @description  clear the error on change
    * @revision     
    *******************************************************************************/
    clearError_onChange: function(component, event) {
        var eventSource = event.getSource();
        var auraId = eventSource.getLocalId();
        this.clearErrors(component, auraId);
        window.location.hash = '#'+auraId;
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         6.Jun.2017         
    * @description  validates the field if its populated
    * @revision     
    *******************************************************************************/
    isFieldPopulated: function(fieldToValidate) {
        var isValid = true;
        if (fieldToValidate == null) {
            isValid = false;
        } else {
            fieldToValidate = fieldToValidate.replace(new RegExp(' ', 'g'),"");
            if (fieldToValidate == '') {
                isValid = false;
            }
        }

        return isValid;
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         6.Jun.2017         
    * @description  returns true if its a valid score format (###.##)
    * @revision     
    *******************************************************************************/
    validateDomesticScore: function(component, score) {
        var isValid = true;
        var enteredScore = component.get("v.contQualification.Score__c");
    
        var regDateFormat = /^\d{0,3}(\.\d{0,2})?$/;

        if (!enteredScore.match(regDateFormat)) {
            score.set("v.errors", [{message:"You must enter a valid number (999.99) in the Overall Score field."}]);
            isValid = false;
        } else {
            score.set("v.errors", null);
        }
        return isValid;
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         13.Jun.2017         
    * @description  retrieves the application ID from the URL
    * @revision     
    *******************************************************************************/
    retrieveAppIdFromURL: function (component) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;
        var windowLoc = window.location.pathname;
        
        if (windowLoc.indexOf("applicationdetail") !=-1 || windowLoc.indexOf("testpage") !=-1) {
            var retrievedAppId = '';
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('='); //to split the key from the value.
                for (j = 0; j < sParameterName.length; j++) {
                    if (sParameterName[j] === 'appId') { //get the app Id from the parameter
                        retrievedAppId = sParameterName[j+1];
                    }
                }
            }
            if (retrievedAppId != '') {
                component.set("v.appId", retrievedAppId);
            }
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         13.Jun.2017         
    * @description  shows the qualification form
    * @revision     
    *******************************************************************************/
    displayQualForm: function (component, qualId) {
        var action_retrieveConQualRecord = component.get("c.retrieveConQualDetails");

        if (qualId != null) {
            action_retrieveConQualRecord = component.get("c.retrieveQualificationById");
            action_retrieveConQualRecord.setParams({ "qualId"   : qualId });
        }

        /*******************************************************************************
        * @author       Ant Custodio
        * @date         3.Apr.2017         
        * @description  retrieve the contact details
        * @revision     
        *******************************************************************************/
        action_retrieveConQualRecord.setCallback(this, function(a) {
            var state = a.getState();
            if (state == "ERROR") {
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                        component.set("v.errorMessage", "An Unexpected error has occured. Please contact your Administrator.");
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                var conQual = a.getReturnValue();
                component.set("v.contQualification", conQual);
                var isShown = component.get("v.showForm");
                
                if (isShown) {
                    isShown = false;
                } else {
                    isShown = true;
                }

                if (qualId == null) {
                    //clear all values only if it's a new record
                    this.clearAllValues(component);
                    this.clearAllScoreFields(component);
                    this.hideAllScoreFields(component);
                    this.clearValue(component, "countryOptions");
                    //clear fields from error
                    this.clearAllErrors(component);
                } else {
                    //clear the attachment section
                    var childCmp = component.find("addDocQual");
                    childCmp.clearComponent();
                    //prepopulate the variables with the query results
                    this.prePopulateFields(component, a.getReturnValue());
                }
                
                component.set("v.showForm", isShown);

                if (isShown) {
                    window.location.hash = '#newQualHeader';
                } else {
                    window.location.hash = '#myQualDiv';
                }
            }
        });
        //retrieve details
        $A.enqueueAction(action_retrieveConQualRecord);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         5.Jul.2017         
    * @description  pre populates the variables used on the page using the values
                    retrieved on the query
    * @revision     
    *******************************************************************************/
    prePopulateFields: function (component, qualRecord) {
        component.set("v.qualRecordType", qualRecord.RecordType.Name);
        component.set("v.initializeDatePicker", true);

        //display for tertiary
        if (qualRecord.RecordType.Name == 'Tertiary Education') {
            if (qualRecord.Other_Institution__c != null && qualRecord.Other_Institution__c != '') {
                component.set("v.showOtherSchoolTertiary", true);
                component.set("v.selSchoolTertiary", "");
                component.set("v.schoolToggleTertiary", true);
                var schoolNotFoundTertiary = component.find("schoolNotFoundTertiary");
                $A.util.removeClass(schoolNotFoundTertiary, "slds-hide");
            } else {
                var action = component.get("c.retrieveInstitutionIdByCode");
                action.setParams({  "insCode"   : qualRecord.Institution_Code__c,
                                    "insName"  : qualRecord.Institution_Name__c });
                action.setCallback(this, function(a) {
                    component.set("v.selSchoolTertiary", a.getReturnValue());
                });
                $A.enqueueAction(action);
                component.set("v.showOtherSchoolTertiary", false);
                component.set("v.schoolToggleTertiary", false);
            }
            component.set("v.instructionInEnglish", qualRecord.Instruction_in_English__c);
        }
        //END - display for tertiary

        //display for secondary
        else if (qualRecord.RecordType.Name == 'Secondary Education') {
            if (qualRecord.Other_Institution__c != null && qualRecord.Other_Institution__c != '') {
                component.set("v.showOtherSchoolSecondary", true);
                component.set("v.selSchoolSecondary", "");
                component.set("v.schoolToggleSecondary", true);
            } else {
                var action = component.get("c.retrieveInstitutionIdByCode");
                action.setParams({  "insCode"   : qualRecord.Institution_Code__c,
                                    "insName"  : qualRecord.Institution_Name__c });
                action.setCallback(this, function(a) {
                
                    component.set("v.selSchoolSecondary", a.getReturnValue());
                });
                $A.enqueueAction(action);
                component.set("v.showOtherSchoolSecondary", false);
                component.set("v.schoolToggleSecondary", false);
            }

            if (qualRecord.Other_Qualification__c != null && qualRecord.Other_Qualification__c != '') {
                component.set("v.showOtherQualSecondary", true);
                component.set("v.selQualSecondary", "");
                component.set("v.qualToggleSecondary", true);
            } else {
                component.set("v.selQualSecondary", qualRecord.Qualification__c);
                component.set("v.showOtherQualSecondary", false);
                component.set("v.qualToggleSecondary", false);
            }
            component.set("v.instructionInEnglish", qualRecord.Instruction_in_English__c);
        }
        //END - display for secondary

        //display for other
        else if (qualRecord.RecordType.Name == 'Other Qualification') {
            if (qualRecord.Date_Achieved__c != null && qualRecord.Date_Achieved__c != '') {
                component.set("v.selectedDate", qualRecord.Date_Achieved__c);
                var dateAchieved = component.find("dateAchieved");
                dateAchieved.set("v.value", qualRecord.Date_Achieved__c);
            }
        }
        //END - display for other

        //display for english
        else if (qualRecord.RecordType.Name == 'English Test') {

            if (qualRecord.Qualification__c != null) {
                component.set("v.selTestType", qualRecord.Qualification__r.Qualification_Name__c);
                if (qualRecord.isTestCompleted__c) {
                    this.hasSelectedYes(component);
                } else {
                    this.hasSelectedNo(component);
                }
            }

            if (qualRecord.Date_Achieved__c != null && qualRecord.Date_Achieved__c != '') {
                component.set("v.selectedDate", qualRecord.Date_Achieved__c);
                var engTestdateAchieved = component.find("engTestdateAchieved");
                engTestdateAchieved.set("v.value", qualRecord.Date_Achieved__c);
            }

            
        }
        //END - display for english

        //attachment comment and doc type
        if (qualRecord.Qualification_Document__c != null) {
            component.set("v.documentComments", qualRecord.Qualification_Document__r.Comments__c);
            component.set("v.selectedType", qualRecord.Qualification_Document__r.Document_Type_Name__c);
        }
        
        //END - attachment comment and doc type
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         10.Jul.2017         
    * @description  when no is selected
    * @revision     
    *******************************************************************************/
    hasSelectedNo: function (component) {
        if (component.get("v.contQualification.isTestCompleted__c")) {
            var englishTestDate = component.find("engTestdateAchieved");
            if (englishTestDate != null) {
                component.set("v.invalidDateAchieved", false);
                component.set("v.englishSelectedDate", "");
                $A.util.removeClass(englishTestDate, 'dateError');
            }
        }
        component.set("v.isCompleted", "NO");
        component.set("v.contQualification.isTestCompleted__c", false);
        component.set("v.invalidDateAchieved", false);
        component.set("v.dateAchievedErrorMessage", "");
        component.set("v.selectedDate", "");

        

        var isTestCompletedTrue = component.find("isTestCompletedTrue");
        isTestCompletedTrue.set("v.value", false);
        var isTestCompletedFalse = component.find("isTestCompletedFalse");
        isTestCompletedFalse.set("v.value", true);

        this.hideAllScoreFields(component);
        this.clearAllScoreFields(component);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         17.Jul.2017         
    * @description  when no is selected
    * @revision     
    *******************************************************************************/
    hideAllScoreFields: function (component) {
        component.set("v.eng_essay", false);
        component.set("v.eng_listening", false);
        component.set("v.eng_reading", false);
        component.set("v.eng_speaking", false);
        component.set("v.eng_writing", false);
        component.set("v.eng_twe", false);
        component.set("v.eng_overall", false);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         17.Jul.2017         
    * @description  when no is selected
    * @revision     
    *******************************************************************************/
    clearAllScoreFields: function (component) {
        this.clearValue(component, "overall");
        this.clearValue(component, "Listening");
        this.clearValue(component, "Reading");
        this.clearValue(component, "Speaking");
        this.clearValue(component, "Writing");
        this.clearValue(component, "twe");
        this.clearValue(component, "essay");
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         10.Jul.2017         
    * @description  when yes is selected
    * @revision     
    *******************************************************************************/
    hasSelectedYes: function (component) {

        if (!component.get("v.contQualification.isTestCompleted__c")) {
            var englishTestDate = component.find("engTestdateAchieved");
            if (englishTestDate != null) {
                component.set("v.invalidDateAchieved", false);
                component.set("v.englishSelectedDate", "");
                $A.util.removeClass(englishTestDate, 'dateError');
            }
        }

        component.set("v.isCompleted", "YES");
        component.set("v.contQualification.isTestCompleted__c", true);
        component.set("v.invalidDateAchieved", false);
        component.set("v.dateAchievedErrorMessage", "");
        component.set("v.selectedDate", "");

        //display the appropriate score fields
        this.displayScoreFields(component);

        var isTestCompletedTrue = component.find("isTestCompletedTrue");
        isTestCompletedTrue.set("v.value", true);
        var isTestCompletedFalse = component.find("isTestCompletedFalse");
        isTestCompletedFalse.set("v.value", false);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         14.Jul.2017         
    * @description  
    * @revision     
    *******************************************************************************/
    displayScoreFields: function (component) {
        var selTestType = component.get("v.selTestType");
        if (selTestType != null && selTestType != '') {
            this.hideAllScoreFields(component);
            var action = component.get("c.displayEnglishScoreFields");
            action.setParams({ "selTestType"   : selTestType });
            action.setCallback(this, function(a) {
                var englishFieldsList = a.getReturnValue();
                for (var i in englishFieldsList) {
                    if (englishFieldsList[i] == 'Essay_Rating__c') {
                        component.set("v.eng_essay", true);
                    } else if (englishFieldsList[i] == 'Listening__c') {
                        component.set("v.eng_listening", true);
                    } else if (englishFieldsList[i] == 'Reading__c') {
                        component.set("v.eng_reading", true);
                    } else if (englishFieldsList[i] == 'Speaking__c') {
                        component.set("v.eng_speaking", true);
                    } else if (englishFieldsList[i] == 'Writing__c') {
                        component.set("v.eng_writing", true);
                    } else if (englishFieldsList[i] == 'Test_of_Written_English__c') {
                        component.set("v.eng_twe", true);
                    }
                }
                component.set("v.eng_overall", true);
            });
            $A.enqueueAction(action);
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         13.Jun.2017         
    * @description  retrieves the application ID from the URL
    * @revision     
    *******************************************************************************/
    displayCreditIntentionSection: function (component, qualList) {
        var hasTertiary = false;
        //loop through and check if there's at least 1 tertiary education
        for (i=0; i<qualList.length; i++) {
            if (qualList[i].Contact_Qualification__c != null) { //go to this condition if the component is viewed as part of the application
                if (qualList[i].Contact_Qualification__r.RecordType.Name == 'Tertiary Education') {
                    hasTertiary = true;
                    break;
                }
            } else { //go to this condition if the component is viewed as part of the contact
                if (qualList[i].RecordType.Name == 'Tertiary Education') {
                    hasTertiary = true;
                    break;
                }
            }
        }
        component.set("v.showCreditIntentionSection", hasTertiary);

        //initialise the credit intention section if a tertiary has been found
        if (hasTertiary) {
            var childCmp = component.find("appCredIntention");
            childCmp.initialise();
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         13.Jun.2017         
    * @description  deletes the selected qualification
    * @revision     
    *******************************************************************************/
    deleteSelectedQualification: function (component) {
        var selRecToDelId = component.get("v.selRecToDelId");
        /*******************************************************************************
        * @author       Ant Custodio
        * @date         12.Apr.2017         
        * @description  retrieve the contact details
        * @revision     
        *******************************************************************************/
        var action_deleteQualRecord = component.get("c.deleteSelectedQualification");

        //checks which table to display/return
        if (component.get("v.isAppComponent") == true) {
            action_deleteQualRecord = component.get("c.deleteSelectedAppQualificationProvided");
            action_deleteQualRecord.setParams({ "appQualProvidedId"   : selRecToDelId });
        } else {
            action_deleteQualRecord.setParams({ "qualificationId"   : selRecToDelId });
        }
        
        action_deleteQualRecord.setCallback(this, function(a) {
            var state = a.getState();
            if (state == "ERROR") {
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                        var splitString = errors[0].message.split(":");
                        component.set("v.errorMessage", splitString[3] + ': ' + splitString[4]);
                    }
                } else {
                    console.log("Unknown error");
                }
            } else {
                this.retrieveQualificationList(component);

                //close the confirmation popup
                component.set("v.showConfirmPopup", false);
                window.location.hash = '#myQualDiv';
            }
        });
        //Action 1, delete the record
        $A.enqueueAction(action_deleteQualRecord);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Jul.2017         
    * @description  deletes the selected lookup field
    * @revision     
    *******************************************************************************/
    clearLookupField: function (component, event) {
        var recordTypeOption = component.find("recordTypeOptions");
        var recordTypeSelected = recordTypeOption.get("v.value");
        var institutionPrefix = component.get("v.institutionPrefix");
        if (recordTypeSelected == 'Tertiary Education') {
            //remove selected
            component.set("v.selSchoolTertiary", "");
            var schoolNotFoundTertiary = component.find("schoolNotFoundTertiary");
            $A.util.removeClass(schoolNotFoundTertiary, "slds-hide");
        } else if (recordTypeSelected == 'Secondary Education') {
            var objectId = event.getParam("sObjectId");
            if (objectId != null && objectId != '') {
                if (objectId.substring(0,3) == institutionPrefix) {
                    //remove selected
                    component.set("v.selSchoolSecondary", "");
                } else {
                    //remove selected
                    component.set("v.selQualSecondary", "");
                }
            }
        }
        
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         25.Jul.2017         
    * @description  retrieve the contact qualification list
    * @revision     
    *******************************************************************************/
    retrieveConQualList: function (component, state) {
        if (state == "ERROR") {
            var errors = a.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log("Error message: " + 
                             errors[0].message);
                    component.set("v.errorMessage", "An Unexpected error has occured. Please contact your Administrator.");
                }
            } else {
                console.log("Unknown error");
            }
        } else {
            var action_retrieveConQualList = component.get("c.retrieveConQualList");

            //checks which table to display/return
            if (component.get("v.isAppComponent") == true) {
                action_retrieveConQualList = component.get("c.retrieveAppQualProvidedList");
                action_retrieveConQualList.setParams({ "applicationId"   : component.get("v.appId") });
            }
            action_retrieveConQualList.setCallback(this, function(a) {
                component.set("v.qualList", a.getReturnValue());
                //determine whether to show credit intention section or not
                this.displayCreditIntentionSection(component, a.getReturnValue());
                //Lastly, close the form
                component.set("v.errorMessage", '');
                this.clearAllErrors(component);
                component.set("v.showForm", false);
                window.location.hash = '#myQualDiv';
            });
        }
        $A.enqueueAction(action_retrieveConQualList);
    }
})