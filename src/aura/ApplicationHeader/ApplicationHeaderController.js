({

  /*******************************************************************************
  * @author       Ant Custodio
  * @date         3.Apr.2017         
  * @description  initial actions on page load - retrieve the application details
  * @revision     
  *******************************************************************************/
  doInit : function(component, event) {

    var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
    var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
    var sParameterName;
    var sParamId = ''; 
    var i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('='); //to split the key from the value.
        for(var x = 0; x < sParameterName.length; x++){
            if(sParameterName[x] === 'appId'){
               sParamId = sParameterName[x+1] === undefined ? 'Not found' : sParameterName[x+1];
            }
        }
    }

    if(sParamId != ''){
      component.set("v.appId", sParamId);
    }
    
    var action_retrieveApplication = component.get("c.retrieveApplicationRecord");
    action_retrieveApplication.setParams({ "appIdParam": component.get("v.appId") });
    
    action_retrieveApplication.setCallback(this, function(a) {
        var appRecord = a.getReturnValue();
        component.set("v.applicationRecord", appRecord);
        //redirect to review if Application is already submitted
        if (appRecord.Status__c != 'Draft') {
          var address = '/';
          var urlEvent = $A.get("e.force:navigateToURL");
          urlEvent.setParams({
            "url": address,
            "isredirect" :false
          });
          urlEvent.fire();
        }
    });
    
    $A.enqueueAction(action_retrieveApplication);

  },

	submitApplication : function(component, event, helper) {
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         9.Apr.2017         
    * @description  inserts an application
    * @revision     
    *******************************************************************************/
    var applicationId = component.get("v.appId");
    var actionValidateApplicaton = component.get("c.checkApplicationRecord");
    actionValidateApplicaton.setParams({"appIdParam" : applicationId})

    actionValidateApplicaton.setCallback(this, function(response) {
      var message = response.getReturnValue();
      if(message === 'Valid'){
          var address = '/applicationreview?appId=' + applicationId;
          var urlEvent = $A.get("e.force:navigateToURL");
          urlEvent.setParams({
            "url": address,
            "isredirect" :false
          });
          urlEvent.fire();
      }
      else{
        component.set("v.errorMessage", message);
        window.scrollTo(0, 0);
      }
    });
    
    $A.enqueueAction(actionValidateApplicaton);  
    
  },

  /*******************************************************************************
  * @author       Ant Custodio
  * @date         19.Apr.2017         
  * @description  cancel the application
  * @revision     
  *******************************************************************************/
  showConfirmDeletePopup : function (component, event, helper){     
      component.set("v.showConfirmPopup", true);
  }, 

  /*******************************************************************************
  * @author       Ant Custodio
  * @date         7.Apr.2017         
  * @description  deletes the contact qualification record
  * @revision     
  *******************************************************************************/
  deleteApplication : function (component, event, helper){     
      /*******************************************************************************
      * @author       Ant Custodio
      * @date         12.Apr.2017         
      * @description  retrieve the contact details
      * @revision     
      *******************************************************************************/
      var action_deleteAppRecord = component.get("c.deleteApplicationRecord");
      action_deleteAppRecord.setParams({ "appIdParam"   : component.get("v.appId") });
      action_deleteAppRecord.setCallback(this, function(a) {
          var state = a.getState();
          if (state == "ERROR") {
              var errors = a.getError();
              if (errors) {
                  if (errors[0] && errors[0].message) {
                      console.log("Error message: " + 
                               errors[0].message);
                      var splitString = errors[0].message.split(":");
                      component.set("v.errorMessage", splitString[3] + ': ' + splitString[4]);
                  }
              } else {
                  console.log("Unknown error");
              }
          } else {
            //redirect to home page
            var address = '/';
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": address,
              "isredirect" :false
            });
            urlEvent.fire();
          }
      });

      //delete the record
      $A.enqueueAction(action_deleteAppRecord);
  },

  /*******************************************************************************
  * @author       Ant Custodio
  * @date         7.Apr.2017         
  * @description  cancel the deletion
  * @revision     
  *******************************************************************************/
  cancelDelete : function (component, event, helper){     
      //close the confirmation popup
      component.set("v.showConfirmPopup", false);
  },
  /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         Secondary
    * @description  show the spinner when page is loading
    * @revision     
    *******************************************************************************/
    waiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "block";
        }
        
    },
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  hide the spinner when finished loading
    * @revision     
    *******************************************************************************/
    doneWaiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "none";
        }
    },
})