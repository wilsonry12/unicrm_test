({

	doInit : function(component, event, helper) {
		helper.doInitImpl(component);
		helper.retrieveCourse(component);
	},

	handleEnquiry : function(component, event, helper) {
		alert('Hang in there buddy. The Enquiry component is coming soon !!!!!');
	},

	handleWaitListing : function(component, event, helper) {

		var state = 'Waiting List';
		component.set("v.state", state);
	    component.set("v.progression", "Progress: 50%");
		
		var selectedOffering = event.getSource().get("v.value");
	    console.log(selectedOffering);

	    // Change progress on progress bar
	    var progressBar = component.find('progressIndicator');
	    $A.util.removeClass(progressBar, 'progress25');
	    $A.util.addClass(progressBar, 'progress50');

	    var evt = $A.get("e.c:NavigateToEnrolmentDetails");
      	evt.setParams({ "selectedCourseOffering": selectedOffering,
      					"currentState": state });
      	evt.fire();
	},

	handleRegistration : function(component, event, helper) {

		var state = 'Course Registration';
	    component.set("v.state", state);
	    component.set("v.progression", "Progress: 50%");

	    var selectedOffering = event.getSource().get("v.value");
	    console.log(selectedOffering);

	    // Change progress on progress bar
	    var progressBar = component.find('progressIndicator');
	    $A.util.removeClass(progressBar, 'progress25');
	    $A.util.addClass(progressBar, 'progress50');

	    var evt = $A.get("e.c:NavigateToEnrolmentDetails");
      	evt.setParams({ "selectedCourseOffering": selectedOffering,
      					"currentState": state});
      	evt.fire();
	    
	},

	NavigateToEnrolmentCmp : function(component,event,helper) {
      		
      $A.createComponent(
         "c:EnrolmentDetails",
         {
           "selectedCourseOffering" : event.getParam("selectedCourseOffering"),
           "currentState" : event.getParam("currentState")
         },
         function(newCmp){
            if (component.isValid()) {
                component.set("v.enrolmentDetailsSection", newCmp);
            }
         }
      );
    },

}

})