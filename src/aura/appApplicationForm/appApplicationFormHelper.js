({
	retrieveApplication : function(component, event) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var sParamId = ''; 
        var i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.
            for(var x = 0; x < sParameterName.length; x++){
                if(sParameterName[x] === 'appId'){
                   sParamId = sParameterName[x+1] === undefined ? 'Not found' : sParameterName[x+1];
                }
            }
        }

        if(sParamId != ''){
          component.set("v.appId", sParamId);
        }
        
        var action_retrieveApplication = component.get("c.retrieveApplicationRecord");
        action_retrieveApplication.setParams({ "appIdParam": component.get("v.appId") });
        
        action_retrieveApplication.setCallback(this, function(a) {
            var appRecord = a.getReturnValue();
            component.set("v.applicationRecord", appRecord);
            //redirect to review if Application is already submitted
            if (appRecord.Status__c != 'Draft') {
              var address = '/';
              var urlEvent = $A.get("e.force:navigateToURL");
              urlEvent.setParams({
                "url": address,
                "isredirect" :false
              });
              urlEvent.fire();
            } else{
              var citizenType = appRecord.Citizenship_Type_Filter__c;
              if(citizenType == 'INTRNTNL' || citizenType == 'INT-TEP'){
                  component.set("v.isInternational", true);

                  //pass boolean to event
                  var appFormEvent = $A.get("e.c:appApplicationFormEvent");
                  appFormEvent.setParams({"isInternationalApplicant": component.get("v.isInternational")});
                  appFormEvent.fire();

                  this.setAdditionalQuestions(component, appRecord);
              }

            }
        });
        
        $A.enqueueAction(action_retrieveApplication);
    },

    setAdditionalQuestions : function(component, appRecord) {
        //var appRecord = component.get("v.applicationRecord");
        
        if(appRecord.Access_and_Equity_Question__c != null){
            var response = appRecord.Access_and_Equity_Question__c;
            if(response == 'Yes'){
                document.getElementById('disYes').checked = true;
                component.set("v.hasDisability", true);
            }
            else{
                document.getElementById('disNo').checked = true;
            }
        }

        if(appRecord.Refused_Visa_Question__c != null){
            var response = appRecord.Refused_Visa_Question__c;
            if(response == 'Yes'){
                document.getElementById('refusedYes').checked = true;
                component.set("v.hasRefusedVisa", true);
            }
            else{
                document.getElementById('refusedNo').checked = true;
            }
        }

        if(appRecord.Breached_Visa_Question__c != null){
            var response = appRecord.Breached_Visa_Question__c;
            if(response == 'Yes'){
                document.getElementById('breachedYes').checked = true;
                component.set("v.hasBreachedVisa", true);
            }
            else{
                document.getElementById('breachedNo').checked = true;
            }
        }

        if(appRecord.Convicted_Any_Crime_Question__c != null){
            var response = appRecord.Convicted_Any_Crime_Question__c;
            if(response == 'Yes'){
                document.getElementById('convictedYes').checked = true;
                component.set("v.hasConvictedCrime", true);
            }
            else{
                document.getElementById('convictedNo').checked = true;
            }
        }

        if(appRecord.Issued_Protection_Visa_Question__c != null){
            var response = appRecord.Issued_Protection_Visa_Question__c;
            if(response == 'Yes'){
                document.getElementById('protectionYes').checked = true;
                component.set("v.hasProtectionVisa", true);
            }
            else{
                document.getElementById('protectionNo').checked = true;
            }
        }

        if(appRecord.Medical_Health_Visa_Question__c != null){
            var response = appRecord.Medical_Health_Visa_Question__c;
            if(response == 'Yes'){
                document.getElementById('medicalYes').checked = true;
                component.set("v.hasMedical", true);
            }
            else{
                document.getElementById('medicalNo').checked = true;
            }
        }

        if(appRecord.Physical_Mental_Health_Question__c != null){
            var response = appRecord.Physical_Mental_Health_Question__c;
            if(response == 'Yes'){
                document.getElementById('physicalYes').checked = true;
                component.set("v.hasPhysical", true);
            }
            else{
                document.getElementById('physicalNo').checked = true;
            }
        }

        this.toggleHelpText(component);
    },

    toggleHelpText : function(component) { 
		if(component.get("v.hasRefusedVisa") || 
            component.get("v.hasBreachedVisa") || 
    		component.get("v.hasMedical") ||
    		component.get("v.hasPhysical") ||
    		component.get("v.hasProtectionVisa") ||
    		component.get("v.hasConvictedCrime")){

    		component.set("v.hasAnsweredYes", true);
    	} else {
    		component.set("v.hasAnsweredYes", false);
    	}
	}
})