({

  /*******************************************************************************
  * @author       Ant Custodio
  * @date         3.Apr.2017         
  * @description  initial actions on page load - retrieve the application details
  * @revision     
  *******************************************************************************/
  doInit : function(component, event, helper) {
    helper.retrieveApplication(component);
  },

	submitApplication : function(component, event, helper) {
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         9.Apr.2017         
    * @description  inserts an application
    * @revision     
    *******************************************************************************/
    var applicationId = component.get("v.appId");
    var actionValidateApplicaton = component.get("c.checkApplicationRecord");
    actionValidateApplicaton.setParams({"appIdParam" : applicationId,
                                        "appForUpdate" : component.get("v.applicationRecord"),
                                        "isIntApplicant" : component.get("v.isInternational")})

    actionValidateApplicaton.setCallback(this, function(response) {
      var message = response.getReturnValue();
      if(message === 'Valid'){
          var address = '/applicationreview?appId=' + applicationId;
          var urlEvent = $A.get("e.force:navigateToURL");
          urlEvent.setParams({
            "url": address,
            "isredirect" :false
          });
          urlEvent.fire();
      }
      else{
        component.set("v.errorMessage", message);
        window.scrollTo(0, 0);
      }
    });
    
    $A.enqueueAction(actionValidateApplicaton);  
    
  },

  /*******************************************************************************
  * @author       Ant Custodio
  * @date         19.Apr.2017         
  * @description  cancel the application
  * @revision     
  *******************************************************************************/
  showConfirmDeletePopup : function (component, event, helper){     
      component.set("v.showConfirmPopup", true);
  }, 

  /*******************************************************************************
  * @author       Ant Custodio
  * @date         7.Apr.2017         
  * @description  deletes the contact qualification record
  * @revision     
  *******************************************************************************/
  deleteApplication : function (component, event, helper){     
      /*******************************************************************************
      * @author       Ant Custodio
      * @date         12.Apr.2017         
      * @description  retrieve the contact details
      * @revision     
      *******************************************************************************/
      var action_deleteAppRecord = component.get("c.deleteApplicationRecord");
      action_deleteAppRecord.setParams({ "appIdParam"   : component.get("v.appId") });
      action_deleteAppRecord.setCallback(this, function(a) {
          var state = a.getState();
          if (state == "ERROR") {
              var errors = a.getError();
              if (errors) {
                  if (errors[0] && errors[0].message) {
                      console.log("Error message: " + 
                               errors[0].message);
                      var splitString = errors[0].message.split(":");
                      component.set("v.errorMessage", splitString[3] + ': ' + splitString[4]);
                  }
              } else {
                  console.log("Unknown error");
              }
          } else {
            //redirect to home page
            var address = '/';
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              "url": address,
              "isredirect" :false
            });
            urlEvent.fire();
          }
      });

      //delete the record
      $A.enqueueAction(action_deleteAppRecord);
  },

  /*******************************************************************************
  * @author       Ant Custodio
  * @date         7.Apr.2017         
  * @description  cancel the deletion
  * @revision     
  *******************************************************************************/
  cancelDelete : function (component, event, helper){     
      //close the confirmation popup
      component.set("v.showConfirmPopup", false);
  },

  onSelectDisability : function(component, event, helper) {
    var elem = event.target;
      var selected = elem.value;
      if(selected == 'yes'){
        component.set("v.hasDisability", true);
        component.set("v.applicationRecord.Access_and_Equity_Question__c", "Yes");
      } else {
        component.set("v.hasDisability", false);
        component.set("v.applicationRecord.Access_and_Equity_Question__c", "No");
      }
  },

  onSelectRefusedVisa : function(component, event, helper) {
    var elem = event.target;
      var selected = elem.value;
      if(selected == 'yes'){
        component.set("v.hasRefusedVisa", true);
        component.set("v.applicationRecord.Refused_Visa_Question__c", "Yes");
      } else {
        component.set("v.hasRefusedVisa", false);
        component.set("v.applicationRecord.Refused_Visa_Question__c", "No");
      }
      helper.toggleHelpText(component);
  },

  onSelectBreachedVisa : function(component, event, helper) {
    var elem = event.target;
      var selected = elem.value;
      if(selected == 'yes'){
        component.set("v.hasBreachedVisa", true);
        component.set("v.applicationRecord.Breached_Visa_Question__c", "Yes");
      } else {
        component.set("v.hasBreachedVisa", false);
        component.set("v.applicationRecord.Breached_Visa_Question__c", "No");
      }
      helper.toggleHelpText(component);
  },

  onSelectMedical : function(component, event, helper) {
    var elem = event.target;
      var selected = elem.value;
      if(selected == 'yes'){
        component.set("v.hasMedical", true);
        component.set("v.applicationRecord.Medical_Health_Visa_Question__c", "Yes");
      } else {
        component.set("v.hasMedical", false);
        component.set("v.applicationRecord.Medical_Health_Visa_Question__c", "No");
      }
      helper.toggleHelpText(component);
  },

  onSelectPhysical : function(component, event, helper) {
    var elem = event.target;
      var selected = elem.value;
      if(selected == 'yes'){
        component.set("v.hasPhysical", true);
        component.set("v.applicationRecord.Physical_Mental_Health_Question__c", "Yes");
      } else {
        component.set("v.hasPhysical", false);
        component.set("v.applicationRecord.Physical_Mental_Health_Question__c", "No");
      }
      helper.toggleHelpText(component);
  },

  onSelectProtectionVisa : function(component, event, helper) {
    var elem = event.target;
      var selected = elem.value;
      if(selected == 'yes'){
        component.set("v.hasProtectionVisa", true);
        component.set("v.applicationRecord.Issued_Protection_Visa_Question__c", "Yes");
      } else {
        component.set("v.hasProtectionVisa", false);
        component.set("v.applicationRecord.Issued_Protection_Visa_Question__c", "No");
      }
      helper.toggleHelpText(component);
  },

  onSelectConvictedCrime : function(component, event, helper) {
    var elem = event.target;
      var selected = elem.value; 
      if(selected == 'yes'){
        component.set("v.hasConvictedCrime", true);
        component.set("v.applicationRecord.Convicted_Any_Crime_Question__c", "Yes");
      } else {
        component.set("v.hasConvictedCrime", false);
        component.set("v.applicationRecord.Convicted_Any_Crime_Question__c", "No");
      }
      helper.toggleHelpText(component);
  },

  /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         Secondary
    * @description  show the spinner when page is loading
    * @revision     
    *******************************************************************************/
    waiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "block";
        }
        
    },
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  hide the spinner when finished loading
    * @revision     
    *******************************************************************************/
    doneWaiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "none";
        }
    },
})