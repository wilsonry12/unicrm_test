({
    MAX_FILE_SIZE: 4500000, /* 6 000 000 * 3/4 to account for base64 */
    CHUNK_SIZE: 750000, /* Use a multiple of 4 */

    doQuery : function(component) {
        var actionRetrieveDocuments = component.get("c.retrieveAppDocuments");
        var varAppId = component.get("v.applicationRecord");
        actionRetrieveDocuments.setParams({"appliId": varAppId});
        actionRetrieveDocuments.setCallback(this, function(response) {
            component.set("v.attachList", response.getReturnValue());

            var attachments = component.get("v.attachList");
            if(attachments.length > 0){
                var fileNames=new Array();
                for (var idx=0; idx<attachments.length; idx++) {
                    fileNames.push(attachments[idx].Contact_Document__r.Filename__c); 
                }
                component.set("v.fileNameList", fileNames);
            }
        });

        $A.enqueueAction(actionRetrieveDocuments);
    },

    retrieveDocList : function(component) {
        var actionRetrieveDocuments = component.get("c.retrieveAppDocuments");
        var varAppId = component.get("v.applicationRecord");
        actionRetrieveDocuments.setParams({"appliId": varAppId});
        actionRetrieveDocuments.setCallback(this, function(response) {
            component.set("v.attachList", response.getReturnValue());

            var attachments = component.get("v.attachList");
            if(attachments.length > 0){
                var fileNames=new Array();
                for (var idx=0; idx<attachments.length; idx++) {
                    fileNames.push(attachments[idx].Contact_Document__r.Filename__c); 
                }
                component.set("v.fileNameList", fileNames);
            }
            
            window.location.hash = '#documentList';
        });

        $A.enqueueAction(actionRetrieveDocuments);
    },

    retrievePicklistValues : function(actionToRun, inputsel) {
        var opts=[];
        actionToRun.setCallback(this, function(a) {
            opts.push({"class": "optionClass", label: "--Select--", value: ""});
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);

        });
        $A.enqueueAction(actionToRun); 
    },

    retrieveExistingDocument : function(component) {
        var actionRetrieveExistingDocument = component.get("c.editApplicationDocument");
        actionRetrieveExistingDocument.setParams({"appDocId" : component.get("v.selRecToEditId")});
        actionRetrieveExistingDocument.setCallback(this, function(response) {
            var appDoc = response.getReturnValue();
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.documentComments", appDoc.Contact_Document__r.Comments__c);
                component.set("v.selectedType", appDoc.Contact_Document__r.Document_Type_Name__c);
                component.set("v.existingFileName", appDoc.Contact_Document__r.Filename__c);
                component.set("v.showAttachList", false);
            }
        });

        $A.enqueueAction(actionRetrieveExistingDocument);

        window.location.hash = '#uploadFile';
    },

    save : function(component) {
        var validFileExtensions = [".pdf",".jpg",".doc",".docx",".gif",".rtf",".txt"];
        var existingFilenames = component.get("v.fileNameList");
        
        var fileInput = component.find("file").getElement();
        //only fire if theres an attachment
        if (fileInput.files.length > 0) {    
        	var file = fileInput.files[0];
            var fName = file.name;
            var docComments = component.get("v.documentComments");

            //document validations happen here...
            if (file.size > this.MAX_FILE_SIZE) {
                //alert('File size cannot exceed 5MB.');
        	    component.set("v.showErrorPopup", true);
                component.set("v.validationMessage", "File size cannot exceed 5MB.");
                return;
            }
            if(fName.length >0){
                var blnValid = false;
                var filenameExist = false;

                //check for valid file extensions
                for (var j = 0; j < validFileExtensions.length; j++) {
                    var sCurExtension = validFileExtensions[j];
                    if (fName.substr(fName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                    //alert("Sorry, " + fName + " is invalid, allowed extensions are: " + validFileExtensions.join(", "));
                    component.set("v.showErrorPopup", true);
                    component.set("v.validationMessage", "Sorry, " + fName + " is invalid, allowed extensions are: " + validFileExtensions.join(", "));
                    return;
                }
                //end check for valid file extensions

                //check if filename already exist
                for (var j = 0; j < existingFilenames.length; j++) {
                    var sCurFile = existingFilenames[j];
                    if (fName.toLowerCase() == sCurFile.toLowerCase()) {
                        filenameExist = true;
                        break;
                    }
                }

                if(filenameExist){
                    //alert("File with this name already exist, please upload a new file.");
                    component.set("v.showDuplicatePopup", true);
                    return;
                }
                //end check if filename already exist
            }

            if (docComments != null) {
                if(docComments.length > 255){
                    //alert('Maximum size of comments is 255 characters only.');
                    component.set("v.showErrorPopup", true);
                    component.set("v.validationMessage", "Maximum size of comments is 255 characters only.");
                    return;
                }
            }
            
            var fr = new FileReader();
            
            var self = this;
           	fr.onload = function() {
                var fileContents = fr.result;
        	    var base64Mark = 'base64,';
                var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;

                fileContents = fileContents.substring(dataStart);
            
        	    self.upload(component, file, fileContents);
            };

            fr.readAsDataURL(file);
        } else {
            //alert('No file attached.');
            var isEdit = component.get("v.isEditDocument");
            if(isEdit){
                console.debug('WENT HERE FOR UPDATE');
                var actionUpload = component.get("c.uploadDocument");
                actionUpload.setParams({
                    "appliId": component.get("v.applicationRecord"),
                    "fileName": "",
                    "base64Data": "", 
                    "contentType": "",
                    "docType": component.get("v.selectedType"),
                    "fileId": "",
                    "comments": component.get("v.documentComments"),
                    "existingDocument": component.get("v.selRecToEditId")
                });

                actionUpload.setCallback(this, function(response) {
                    this.retrieveDocList(component);
                    this.clearValues(component);
                    component.set("v.showAttachList", true);
                });
                    
                
                $A.enqueueAction(actionUpload); 
                
            } else {
                component.set("v.showErrorPopup", true);
                component.set("v.validationMessage", "No file attached.");
            }
        }
    },
        
    upload: function(component, file, fileContents) {
        var fromPos = 0;
        var toPos = Math.min(fileContents.length, fromPos + this.CHUNK_SIZE);
        
        // start with the initial chunk
        this.uploadChunk(component, file, fileContents, fromPos, toPos, '');
    },

    uploadChunk : function(component, file, fileContents, fromPos, toPos, attachId) {
        var actionUpload = component.get("c.saveTheChunk"); 
        var chunk = fileContents.substring(fromPos, toPos);
        var docuType = component.get("v.selectedType");

        actionUpload.setParams({
            "appliId": component.get("v.applicationRecord"),
            "fileName": file.name,
            "base64Data": encodeURIComponent(chunk), 
            "contentType": file.type,
            "docType": component.get("v.selectedType"),
            "fileId": attachId,
            "comments": component.get("v.documentComments"),
            "existingDocument": component.get("v.selRecToEditId")
        });

        actionUpload.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                attachId = response.getReturnValue();
            
                fromPos = toPos;
                toPos = Math.min(fileContents.length, fromPos + this.CHUNK_SIZE);
                
                if (fromPos < toPos) {
                    this.uploadChunk(component, file, fileContents, fromPos, toPos, attachId);  
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                alert("Error : " + JSON.stringify(errors));
            }

            this.retrieveDocList(component);
            this.clearValues(component);
            component.set("v.showAttachList", true);   
        });
            
        
        $A.run(function() {
            $A.enqueueAction(actionUpload); 
        });

    },

    clearValues : function(component) {
        component.find("file").getElement().value='';
        component.set("v.documentComments", "");
        component.set("v.selectedType", "");
        component.set("v.isEditDocument", false);
        component.set("v.existingFileName", ""); 
        component.set("v.selRecToEditId", "");
    }
})