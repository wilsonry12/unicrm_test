({
	doInit : function(component, event, helper) {
		//get and set Application Id
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        
        var sPortalUrl = decodeURIComponent(location.protocol+'//'+location.hostname);
        component.set("v.portalUrl", sPortalUrl);

        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var sParamId = '';
        var i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.
            for(var x = 0; x < sParameterName.length; x++){
                if(sParameterName[x] === 'appId'){
                   sParamId = sParameterName[x+1] === undefined ? 'Not found' : sParameterName[x+1];
                }
            }
        }
        component.set("v.applicationRecord", sParamId);

        
        helper.doQuery(component);

        //retrieve document types
        helper.retrievePicklistValues(component.get("c.retrieveDocumentType"), component.find("types"));
    },

    reQuery : function(component, event, helper) {
        if (window.location.hash == '#myQualDiv') {
            helper.doQuery(component);
            window.location.hash = '#qualSection';
        }
    }, 

    refreshList : function(component, event, helper) {
        helper.doQuery(component);
        window.location.hash = '#documentTable';
    }, 

	onUploadNewDocument : function(component, event, helper) {
        helper.clearValues(component);
        component.set("v.showAttachList", false);
        window.location.hash = '#newDocument';
    },

    onCancelUpload : function(component, event, helper) {
        helper.clearValues(component);
        component.set("v.showAttachList", true);
        window.location.hash = '#documentTable';
    },

    onEditExistingDocument : function(component, event, helper) {
        component.set("v.isEditDocument", true);

        var source = event.getSource(); // this would give that particular component
        var attachmentId = source.get("v.name"); // returns the id

        //assign Id and show the edit document screen
        component.set("v.selRecToEditId", attachmentId);
        
        helper.retrieveExistingDocument(component);
    },

    onSelectChange : function(component, event, helper) {
		var selected = component.find("types").get("v.value");
		component.set("v.selectedType", selected);
	},

	addDocument : function(component, event, helper) {
		var bShowList = component.get("v.showList");
        if(bShowList){
            component.set("v.showList", false);
        }
        else{
            component.set("v.showList", true);
        }
        
    },

	showHideComponent : function (component, event, helper) {
        var isExpanded = component.get("v.isExpanded");
        
        if (isExpanded) {
            isExpanded = false;
        } else {
            isExpanded = true;
        }
		component.set("v.isExpanded", isExpanded);
    },

    saveDocument : function(component, event, helper) {
        helper.save(component);
    },

    cancelAdd : function(component, event, helper) {
        component.set("v.showList", true);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  show the spinner when page is loading
    * @revision     
    *******************************************************************************/
    waiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "block";
        }
        
    },
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  hide the spinner when finished loading
    * @revision     
    *******************************************************************************/
    doneWaiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "none";
        }
    },
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         19.Apr.2017         
    * @description  cancel the deletion
    * @revision     
    *******************************************************************************/
    showConfirmDeletePopup : function (component, event, helper){     
        var source = event.getSource(); // this would give that particular component
        var attachmentId = source.get("v.name"); // returns the id

        //assign Id and show the confirmation popup
        component.set("v.selRecToDelId", attachmentId);
        component.set("v.showConfirmPopup", true);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  deletes the contact qualification record
    * @revision     
    *******************************************************************************/
    deleteAttachment : function (component, event, helper){     
        var selRecToDelId = component.get("v.selRecToDelId");

        var action_deleteAttRecord = component.get("c.deleteSelectedAttachment");
        
        action_deleteAttRecord.setParams({ "attachmentId" : selRecToDelId });
        action_deleteAttRecord.setCallback(this, function(a) {
            var state = a.getState();
            if (state == "ERROR") {
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                        component.set("v.errorMessage", "An Unexpected error has occured. Please contact your Administrator.");
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            //refresh Attach list and Filename list
            helper.retrieveDocList(component);


            component.set("v.existingFileName", ""); 
            component.set("v.selRecToEditId", "");

            //close the confirmation popup
            component.set("v.showConfirmPopup", false);
        });
        //Action 1, delete the record
        $A.enqueueAction(action_deleteAttRecord);
        window.location.hash = '#documentTable';
    },

    closeErrorModal : function (component, event, helper) {
        var isEdit = component.get("v.isEditDocument");

        component.set("v.showErrorPopup", false);
        component.set("v.showDuplicatePopup", false);
        component.set("v.validationMessage", "");
        
        if(!isEdit){
            component.find("file").getElement().value='';
            component.set("v.documentComments", "");
            component.set("v.selectedType", "");
            component.set("v.isEditDocument", false);
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  cancel the deletion
    * @revision     
    *******************************************************************************/
    cancelDelete : function (component, event, helper){     
        //close the confirmation popup
        component.set("v.showConfirmPopup", false);
    }
})