({
	doInit : function(component, event, helper) {
		//get and set Application Id
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.

            if (sParameterName[0] === 'appId') { //lets say you are looking for param name - appId
                sParameterName[1] === undefined ? 'Not found' : sParameterName[1];
            }
        }

        console.log('Param name: '+sParameterName[0]);
        console.log('Param value: '+sParameterName[1]);
        component.set("v.applicationRecord", sParameterName[1]);

        var action = component.get("c.retrieveAppCourses");
        var varAppId = component.get("v.applicationRecord");
        action.setParams({"appId": varAppId});
        action.setCallback(this, function(response) {
            component.set("v.applicationCourses", response.getReturnValue());
        });

        $A.enqueueAction(action);

        helper.retrievePicklistValues(component.get("c.retrieveAttendanceType"), component.find("attypes"));
    },

    showConfirmDeletePopup : function (component, event, helper){     
        var domEvent = event.getParams().domEvent;
        var bodySpan = domEvent.target.nextSibling;
        
        var appCourseId = bodySpan.dataset.id;

        //assign Id and show the confirmation popup
        component.set("v.selRecToDelId", appCourseId);
        component.set("v.showConfirmPopup", true);
    },

    deleteAppCourse : function (component, event, helper){     
        var courseId = component.get("v.selRecToDelId");

        var action_deleteCourseRecord = component.get("c.deleteSelectedCourse");
        
        action_deleteCourseRecord.setParams({ "deleteThisCourse" : courseId, 
                                            "applicationId": component.get("v.applicationRecord")});
        
        action_deleteCourseRecord.setCallback(this, function(a) {
            var state = a.getState();
            if (state == "ERROR") {
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                        component.set("v.errorMessage", "An Unexpected error has occured. Please contact your Administrator.");
                    }
                } else {
                    console.log("Unknown error");
                }
            }

            helper.queryApplicationCourse(component);
            //close the confirmation popup
            component.set("v.showConfirmPopup", false);
        });
        //Action 1, delete the record
        $A.enqueueAction(action_deleteCourseRecord);

    },

    cancelDelete : function (component, event, helper){     
        //close the confirmation popup
        component.set("v.showConfirmPopup", false);
    },

    searchCourses : function(component, event, helper) {
        helper.findCourseOfferings(component, "Search Courses");
    },

    ApplyFilter : function(component, event, helper) {
        helper.findCourseOfferings(component, "Apply Filters");
    },

    ResetFilter  : function(component, event, helper) {
        helper.findCourseOfferings(component, "Reset Filters");
    },

    showUnitSets : function(component, event, helper) {
        var domEvent = event.getParams().domEvent;
        var bodySpan = domEvent.target.nextSibling;
        
        var courseUniqueCode = bodySpan.dataset.uniquecode;
        console.debug('COURSE CODE: '+courseUniqueCode);
        component.set("v.selRecToView", courseUniqueCode);

        //helper.retrieveStatusMessage(component);

        component.set("v.showStatusPopup", true);
    },

    closeViewModal : function (component, event, helper){     
        //close the confirmation popup
        component.set("v.showStatusPopup", false);
    },

    //Pagination methods
    NextPage:function(component, event, helper) {
        //get filters
        var varSearch = component.get("v.searchStr");
        var varSearchCode = component.get("v.applicationRecord");
        var varAttendType = component.get("v.selectedAttendType");
        var varLocation = component.get("v.selectedLocation");
        var varPeriod = component.get("v.selectedPeriod");

        component.set("v.prev",false);
        var Currentpage = component.get("v.Currentpage");        
        var pageSize = component.get("v.pageSize"); 
        var Totalpages = component.get("v.Totalpages");
        if(Currentpage >= Totalpages){
            Currentpage=Currentpage-1;
        }

        var actionNextPage = component.get("c.findCourses");
        actionNextPage.setParams({  
            "searchStr": varSearch,
            "strCode": varSearchCode,
            "strAttType": varAttendType,
            "strLocation": varLocation,
            "strPeriod": varPeriod,
            "pagesize" : pageSize, 
            "pagenumber" : Currentpage 
        });
        actionNextPage.setCallback(this, function(a) {
            var courses = a.getReturnValue();
            var wrappers=new Array();
            for (var idx=0; idx<courses.length; idx++) {
                var wrapper = { "isSelected" : false,
                                "course" : courses[idx] 
                                };
                wrappers.push(wrapper);
            }
            component.set("v.courseList", wrappers);
            Currentpage=Currentpage+1;
            if(Currentpage >= Totalpages){
                Currentpage = Totalpages;
                component.set("v.Currentpage", Totalpages);
                component.set("v.next",true);
            }
            component.set("v.Currentpage",Currentpage);
        });
        $A.enqueueAction(actionNextPage);
    },

    PreviousPage:function(component, event, helper) {
        //get filters
        var varSearch = component.get("v.searchStr");
        var varSearchCode = component.get("v.applicationRecord");
        var varAttendType = component.get("v.selectedAttendType");
        var varLocation = component.get("v.selectedLocation");
        var varPeriod = component.get("v.selectedPeriod");
        
        component.set("v.next",false);
        var Currentpage = component.get("v.Currentpage");        
        var pageSize = component.get("v.pageSize"); 
        var Totalpages = component.get("v.Totalpages");
        Currentpage=Currentpage-2;
        if(Currentpage <= 0){
            Currentpage = 0;
            component.set("v.Currentpage", 0);
            component.set("v.prev",true);
        }

        var actionPreviousPage = component.get("c.findCourses");
        actionPreviousPage.setParams({  
            "searchStr": varSearch,
            "strCode": varSearchCode,
            "strAttType": varAttendType,
            "strLocation": varLocation,
            "strPeriod": varPeriod,
            "pagesize" : pageSize, 
            "pagenumber" : Currentpage 
        });
        actionPreviousPage.setCallback(this, function(a) {
            var courses = a.getReturnValue();
            var wrappers=new Array();
            for (var idx=0; idx<courses.length; idx++) {
                var wrapper = { "isSelected" : false,
                                "course" : courses[idx] 
                                };
                wrappers.push(wrapper);
            }
            component.set("v.courseList", wrappers);
            Currentpage=Currentpage+1;
            if(Currentpage >= Totalpages){
                Currentpage = Totalpages;
                component.set("v.Currentpage", Totalpages);
                component.set("v.next",true);
            }
            component.set("v.Currentpage",Currentpage);
        });
        $A.enqueueAction(actionPreviousPage);
    },

    LastPage:function(component, event, helper) {
        //get filters
        var varSearch = component.get("v.searchStr");
        var varSearchCode = component.get("v.applicationRecord");
        var varAttendType = component.get("v.selectedAttendType");
        var varLocation = component.get("v.selectedLocation");
        var varPeriod = component.get("v.selectedPeriod");

        component.set("v.next",true);
        component.set("v.prev",false);
        var Totalpages = component.get("v.Totalpages"); 
        var pageSize = component.get("v.pageSize");

        var actionLastPage = component.get("c.findCourses");
        actionLastPage.setParams({  
            "searchStr": varSearch,
            "strCode": varSearchCode,
            "strAttType": varAttendType,
            "strLocation": varLocation,
            "strPeriod": varPeriod,
            "pagesize" : pageSize, 
            "pagenumber" : Totalpages-1 
        });
        actionLastPage.setCallback(this, function(a) {
            var courses = a.getReturnValue();
            var wrappers=new Array();
            for (var idx=0; idx<courses.length; idx++) {
                var wrapper = { "isSelected" : false,
                                "course" : courses[idx] 
                                };
                wrappers.push(wrapper);
            }
            component.set("v.courseList", wrappers);
            component.set("v.Currentpage", Totalpages);
        });
        $A.enqueueAction(actionLastPage);
    }, 
    
    FirstPage:function(component, event, helper) {
        //get filters
        var varSearch = component.get("v.searchStr");
        var varSearchCode = component.get("v.applicationRecord");
        var varAttendType = component.get("v.selectedAttendType");
        var varLocation = component.get("v.selectedLocation");
        var varPeriod = component.get("v.selectedPeriod");

        component.set("v.next",false);
        component.set("v.prev",true);
        var Currentpage = 0;
        var pageSize = component.get("v.pageSize");

        var actionFirstPage = component.get("c.findCourses");
        actionFirstPage.setParams({  
            "searchStr": varSearch,
            "strCode": varSearchCode,
            "strAttType": varAttendType,
            "strLocation": varLocation,
            "strPeriod": varPeriod,
            "pagesize" : pageSize, 
            "pagenumber" : Currentpage 
        });
        actionFirstPage.setCallback(this, function(a) {
            var courses = a.getReturnValue();
            var wrappers=new Array();
            for (var idx=0; idx<courses.length; idx++) {
                var wrapper = { "isSelected" : false,
                                "course" : courses[idx] 
                                };
                wrappers.push(wrapper);
            }
            component.set("v.courseList", wrappers);
            component.set("v.Currentpage", 1);
        });
        $A.enqueueAction(actionFirstPage);
    },
    //End of Pagination

    onSelectAttendanceType : function(component, event, helper) {
        var selected = component.find("attypes").get("v.value");
        component.set("v.selectedAttendType", selected);
    },

    onSelectLocation : function(component, event, helper) {
        var selected = component.find("locations").get("v.value");
        component.set("v.selectedLocation", selected);
    },

    onSelectPeriod : function(component, event, helper) {
        var selected = component.find("periods").get("v.value");
        component.set("v.selectedPeriod", selected);
    },

    cancelSearch : function(component, event, helper) {
        component.set("v.showList", false);
        component.set("v.searchStr", "");
        //clear filters
        component.set("v.selectedAttendType", "");
        component.find("attypes").set("v.value","");
        component.set("v.selectedLocation", "");
        component.find("locations").set("v.value","");
        component.set("v.selectedPeriod", "");
        component.find("periods").set("v.value","");
        component.set("v.errorMessage", "");
    },

    addSelectedCourse : function(component, event, helper) {
        var action = component.get("c.addCourse");
        var currentCourses = component.get("v.applicationCourses");
        var appId = component.get("v.applicationRecord");
        
        var cntSelected = 0;
        //collect selected courses without unit sets
        var crsWrapper = component.get("v.courseList");
        var courseIds = new Array();

        for (var idx=0; idx<crsWrapper.length; idx++) {
            if (crsWrapper[idx].isSelected) {
                courseIds.push(crsWrapper[idx].course.Id);
                cntSelected++;
            }
        }
        console.debug('COURSE Without Sets: '+courseIds);

        //collect selected courses with unit sets
        var crsUnitSetWrapper = component.get("v.courseUnits");
        var courseUnitSetIds = new Array();

        for (var idx=0; idx<crsUnitSetWrapper.length; idx++) {
            if (crsUnitSetWrapper[idx].isSelected) {
                courseUnitSetIds.push(crsUnitSetWrapper[idx].course.Id);
                cntSelected++;
            }
        }
        console.debug('COURSE With Sets: '+courseUnitSetIds);

        //check Application courses list,maximum is 5 per application
        var cntCurrentCourse = currentCourses.length;
        var cntTotalRecords = cntCurrentCourse + cntSelected;
        
        if(cntTotalRecords > 5){
            component.set("v.errorMessage", "You may only apply for up to 5 course preferences. If you wish to add more, please submit this application and create a new application.");
        }
        else{
            component.set("v.errorMessage", "");
            
            var jsonIds = JSON.stringify(courseIds);
            var jsonIds_UnitSets = JSON.stringify(courseUnitSetIds);

            action.setParams({ 
                "jsonStr": jsonIds,
                "jsonStrUnitSets" : jsonIds_UnitSets,
                "applicationId": appId
            });
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var apps=response.getReturnValue()
                    component.set("v.applicationCourses", apps);
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    alert("Error : " + JSON.stringify(errors));
                }
            });

            $A.enqueueAction(action);
            
            helper.findCourseOfferings(component, "Apply Filters");
        }
    },

    deleteCourse : function(component, event, helper) {
    	var appCourseItem = component.get("v.appCourse");    
        var deleteEvent = $A.get("e.c:deleteCourseEvent");
        deleteEvent.setParams({ "appCourse": appCourseItem }).fire();
    },

    showHideComponent : function (component, event, helper) {
        var isExpanded = component.get("v.isExpanded");
        
        if (isExpanded) {
            isExpanded = false;
        } else {
            isExpanded = true;
        }
		component.set("v.isExpanded", isExpanded);
    },

    showCourseTable : function (component, event, helper) {
        var isShown = component.get("v.showList");
        
        if (isShown) {
            isShown = false;
        } else {
            isShown = true;
        }
		component.set("v.showForm", isShown);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  show the spinner when page is loading
    * @revision     
    *******************************************************************************/
    waiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "block";
        }
        
    },
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  hide the spinner when finished loading
    * @revision     
    *******************************************************************************/
    doneWaiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "none";
        }
    }
})