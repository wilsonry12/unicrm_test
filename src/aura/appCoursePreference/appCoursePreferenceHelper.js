({
	queryApplicationCourse : function(component, event, helper) {
	    var action = component.get("c.retrieveAppCourses");
        var varAppId = component.get("v.applicationRecord");
        action.setParams({"appId": varAppId});
        action.setCallback(this, function(response) {
            component.set("v.applicationCourses", response.getReturnValue());
        });

        $A.enqueueAction(action);
	},

	retrievePicklistValues : function(actionToRun, inputsel) {
        var opts=[];
        actionToRun.setCallback(this, function(a) {
            opts.push({"class": "optionClass", label: "--Select--", value: ""});
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);

        });
        $A.enqueueAction(actionToRun); 
	},

    findCourseOfferings : function(component, actionName) {
        //search filters
        var varSearch = component.get("v.searchStr");
        var varSearchCode = component.get("v.applicationRecord");
        var varAttendType = "";
        var varLocation = "";
        var varPeriod = "";

        if(actionName === "Apply Filters") {
            varAttendType = component.get("v.selectedAttendType");
            varLocation = component.get("v.selectedLocation");
            varPeriod = component.get("v.selectedPeriod");
        }

        else{
            //clear filters
            component.set("v.selectedAttendType", "");
            component.find("attypes").set("v.value","");
            component.set("v.selectedLocation", "");
            component.find("locations").set("v.value","");
            component.set("v.selectedPeriod", "");
            component.find("periods").set("v.value","");

            //retrive Location base on course location
            var actionSetLocationValues = component.get("c.retrieveLocation");
            actionSetLocationValues.setParams({"searchStr": varSearch});
            var optsLoc=[];
            actionSetLocationValues.setCallback(this, function(a) {
                optsLoc.push({"class": "optionClass", label: "--Select--", value: ""});
                for(var i=0;i< a.getReturnValue().length;i++){
                    optsLoc.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
                }
                component.find("locations").set("v.options", optsLoc);

            });
            $A.enqueueAction(actionSetLocationValues);

            //retrive Location base on course location
            var actionSetPeriodValues = component.get("c.retrieveCommencementPeriod");
            actionSetPeriodValues.setParams({"searchStr": varSearch});
            var optsPeriod=[];
            actionSetPeriodValues.setCallback(this, function(a) {
                optsPeriod.push({"class": "optionClass", label: "--Select--", value: ""});
                for(var i=0;i< a.getReturnValue().length;i++){
                    optsPeriod.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
                }
                component.find("periods").set("v.options", optsPeriod);

            });
            $A.enqueueAction(actionSetPeriodValues);
        }
        
        var Currentpage = 0; 
        var pageSize = component.get("v.pageSize");

        //get total number of records for pagination
        var actionGetTotalRecords = component.get("c.retrieveCoursesTotalRecord");
        actionGetTotalRecords.setParams({  
            "searchStr": varSearch,
            "strCode": varSearchCode,
            "strAttType": varAttendType,
            "strLocation": varLocation,
            "strPeriod": varPeriod
        });
        actionGetTotalRecords.setCallback(this, function(a) {
            var courseData = a.getReturnValue();
            component.set("v.TotalRecords", courseData);
            component.set("v.Totalpages", Math.ceil(courseData/pageSize));
            component.set("v.prev",true);
            if(courseData <= pageSize){
                component.set("v.next",true);
            }
            else{
                component.set("v.next",false);
            }
        });
        $A.enqueueAction(actionGetTotalRecords);

        //collect unique courses
        var action = component.get("c.findCourses");
        action.setParams({  
            "searchStr": varSearch,
            "strCode": varSearchCode,
            "strAttType": varAttendType,
            "strLocation": varLocation,
            "strPeriod": varPeriod,
            "pagesize" : pageSize, 
            "pagenumber" : Currentpage 
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var courses=response.getReturnValue()
                var wrappers=new Array();
                for (var idx=0; idx<courses.length; idx++) {
                    var wrapper = { "isSelected" : false,
                                    "course" : courses[idx] 
                                    };
                    wrappers.push(wrapper);
                }
                component.set("v.courseList", wrappers);
                console.debug('COURSE LIST: '+wrappers.length);

                Currentpage=Currentpage+1;
                component.set("v.Currentpage",Currentpage);
                
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                alert("Error : " + JSON.stringify(errors));
            }
        });
        $A.enqueueAction(action);

        //collect course with unit sets
        var actionGetCourseUnits = component.get("c.retrieveCoursesWithUnitSets");
        actionGetCourseUnits.setParams({  
            "searchStr": varSearch,
            "strCode": varSearchCode,
            "strAttType": varAttendType,
            "strLocation": varLocation,
            "strPeriod": varPeriod
        });
        actionGetCourseUnits.setCallback(this, function(a) {
            var courseUnitData = a.getReturnValue();
            var unitWrappers = new Array();
            for (var idx=0; idx<courseUnitData.length; idx++) {
                var wrapper = { "isSelected" : false,
                                "course" : courseUnitData[idx] 
                                };
                unitWrappers.push(wrapper);
            }
            component.set("v.courseUnits", unitWrappers);
            console.debug('COURSE WITH UNIT SETS: '+unitWrappers.length);
        });
        $A.enqueueAction(actionGetCourseUnits);

        component.set("v.showList", true);
    }
})