({
	/*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  validates the form
    * @revision     
    *******************************************************************************/
	validateForm : function(component) {
		var isValid = true;

        var salutationOptions = component.find("salutationOptions");
        if (!this.isFieldPopulated(component.get("v.userRec.App_Salutation__c"))) {
            salutationOptions.set("v.errors", [{message:"Title is required"}]);
            isValid = false;
        } else {
            this.clearErrors(component, "salutationOptions");
        }

        var fName = component.get("v.userRec.FirstName");
        if (this.isFieldPopulated(fName)) {
            if (fName.length > 40) {
                fName.set("v.errors", [{message:"First Given Name is too long"}]);
                isValid = false;
            }
        }

        var otherName = component.get("v.userRec.Other_Given_Name__c");
        if (this.isFieldPopulated(otherName)) {
            if (otherName.length > 255) {
                otherName.set("v.errors", [{message:"Other Given Names is too long"}]);
                isValid = false;
            }
        }

        var lName = component.find("lName");
        var lNameValue = component.get("v.userRec.LastName");
        if (!this.isFieldPopulated(lNameValue)) {
            lName.set("v.errors", [{message:"Family Name is required"}]);
            isValid = false;
        } else if (lNameValue.length > 80) {
            lName.set("v.errors", [{message:"Family Name is too long"}]);
            isValid = false;
        } else {
            this.clearErrors(component, "lName");
        }

        var prevSurname = component.get("v.userRec.App_Previous_Surname__c");
        if (this.isFieldPopulated(prevSurname)) {
            if (prevSurname.length > 100) {
                prevSurname.set("v.errors", [{message:"Previous Family Name is too long"}]);
                isValid = false;
            }
        }

        var GenderOptions = component.find("GenderOptions");
        if (!this.isFieldPopulated(component.get("v.userRec.App_Gender__c"))) {
            GenderOptions.set("v.errors", [{message:"Gender is required"}]);
            isValid = false;
        } else {
            this.clearErrors(component, "GenderOptions");
        }

        var campusOfStudyOptions = component.find("campusOfStudyOptions");
        if (!this.isFieldPopulated(component.get("v.campusOfStudy"))) {
            campusOfStudyOptions.set("v.errors", [{message:"Location/Campus of Study is required"}]);
            isValid = false;
        } else {
            this.clearErrors(component, "campusOfStudyOptions");
        }

        //Date of Birth validation
        var regDateFormat = /^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$/;
        var selectedBdate = component.get("v.selectedDOB");


        if (!this.isFieldPopulated(selectedBdate)) {
            component.set("v.dobError", "Date of Birth is required");
            $A.util.addClass(component.find("bday"), 'dateError');
            component.set("v.invalidDate", true);
            isValid = false;
        } else if (!selectedBdate.match(regDateFormat)) {
            component.set("v.dobError", "Please enter a valid date (dd/mm/yyyy).");
            $A.util.addClass(component.find("bday"), 'dateError');
            component.set("v.invalidDate", true);
            isValid = false; 
        } else if (selectedBdate != null) {
            selectedBdate = new Date(selectedBdate.split("-")[0], selectedBdate.split("-")[1]-1, selectedBdate.split("-")[2]);
            var dateNow = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            if(selectedBdate > dateNow){
                component.set("v.dobError", "Please check your year of birth - you have entered a future date");
                $A.util.addClass(component.find("bday"), 'dateError');
                component.set("v.invalidDate", true);
                isValid = false;
            }
        } else {
            $A.util.removeClass(component.find("bday"), 'dateError');
            component.set("v.dobError", "");
            component.set("v.invalidDate", false);
        }

        //Email Validation
        var email = component.find("email");
        var emailContent = component.get("v.userRec.Email");
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        
        if (!this.isFieldPopulated(emailContent)) {
            email.set("v.errors", [{message:"Email is required"}]);
            isValid = false;
        } else if (!emailContent.match(regExpEmailformat)) {
            email.set("v.errors", [{message: "Please enter a valid email address"}]);
            isValid = false;
        } else {
            this.clearErrors(component, "email");
        }

        var mobile = component.get("v.userRec.MobilePhone");
        var workPhone = component.get("v.userRec.Phone");
        var homePhone = component.get("v.userRec.App_Home_Phone__c");
        if (this.isFieldPopulated(mobile) && mobile.toString().length > 20) {
            mobile.set("v.errors", [{message:"Mobile is too long"}]);
            isValid = false;
        }

        if (this.isFieldPopulated(workPhone) && workPhone.toString().length > 20) {
            workPhone.set("v.errors", [{message:"Work Phone is too long"}]);
            isValid = false;
        }

        if (this.isFieldPopulated(homePhone) && homePhone.toString().length > 20) {
            homePhone.set("v.errors", [{message:"Home Phone is too long"}]);
            isValid = false;
        }

        var ResidencyStatusOptions = component.find("CitizenshipTypeOptions");
        if (!this.isFieldPopulated(component.get("v.userRec.App_Residency_Status__c"))) {
            ResidencyStatusOptions.set("v.errors", [{message:"Citizenship Type is required"}]);
            isValid = false;
        } else {
            this.clearErrors(component, "CitizenshipTypeOptions");
        }

        var street = component.find("street");
        var streetValue = component.get("v.userRec.Street");
        if (!this.isFieldPopulated(streetValue)) {
            street.set("v.errors", [{message:"Street Number and Street is required"}]);
            isValid = false;
        } else if (streetValue.length > 40) {
            street.set("v.errors", [{message:"Street Number and Street is too long"}]);
            isValid = false;
        } else {
            this.clearErrors(component, "street");
        }

        if (component.get("v.isPostalAustralia") == "Yes") {
            var city = component.find("city");
            var cityValue = component.get("v.userRec.City");
            if (!this.isFieldPopulated(cityValue)) {
                city.set("v.errors", [{message:"Suburb/Town is required"}]);
                isValid = false;
            } else if (cityValue.length > 40) {
                city.set("v.errors", [{message:"Suburb/Town is too long"}]);
                isValid = false;
            } else {
                this.clearErrors(component, "city");
            }

            var StateOptions = component.find("StateOptions");
            if (!this.isFieldPopulated(component.get("v.selectedState"))) {
                StateOptions.set("v.errors", [{message:"State is required"}]);
                isValid = false;
            } else {
                this.clearErrors(component, "StateOptions");
            }

            var postal = component.find("postal");
            if (!this.isFieldPopulated(component.get("v.selectedPostCode"))) {
                postal.set("v.errors", [{message:"Postal code is required"}]);
                isValid = false;
            } else {
                this.clearErrors(component, "postal");
            }

        } else {
            var countryOptions = component.find("countryOptions");
            if (!this.isFieldPopulated(component.get("v.userRec.Country"))) {
                countryOptions.set("v.errors", [{message:"Country is required"}]);
                isValid = false;
            } else {
                this.clearErrors(component, "countryOptions");
            }

            var osStateValue = component.get("v.selectedOSstate");
            if (this.isFieldPopulated(osStateValue)) {
                if (osStateValue.length > 40) {
                    city.set("v.errors", [{message:"State/Region is too long"}]);
                    isValid = false;
                }
            }
            var osPostalCode = component.get("v.selectedOSPostalCode");
            var ospostal = component.find("ospostal");
            if (this.isFieldPopulated(osPostalCode)) {
                if (osPostalCode.length > 10) {
                    ospostal.set("v.errors", [{message:"Postal Code is too long"}]);
                    isValid = false;
                } else {
                    this.clearErrors(component, "ospostal");
                }
            }
        }

        var addressType = component.find("addressType");
        if (!this.isFieldPopulated(component.get("v.isPostalAustralia"))) {
            addressType.set("v.errors", [{message:"Address Type is required"}]);
            isValid = false;
        } else {
            this.clearErrors(component, "addressType");
        }

        var atsiOptions = component.find("atsiOptions");
        var selectedATSI = component.get("v.selectedATSI");
        var showATSI = component.get("v.showATSIquestion");
        if(showATSI){
            if (!this.isFieldPopulated(selectedATSI)) {
                atsiOptions.set("v.errors", [{message:"Access and Equity Detail is required"}]);
                isValid = false;
            } else {
                this.clearErrors(component, "atsiOptions");
            }
        }
        
        var prevMonashId = component.get("v.userRec.App_Previous_Monash_ID__c");
        var studentid = component.find("studentid");
        if (this.isFieldPopulated(prevMonashId)) {
            if (prevMonashId.length > 20) {
                studentid.set("v.errors", [{message:"Previous Student Id is too long"}]);
                isValid = false;
            } else {
                this.clearErrors(component, "studentid");
            }
        }

        if (isValid) {
        	component.set("v.hasErrors", false);
        } else {
        	component.set("v.hasErrors", true);
        }
	},

	/*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  retrieve the user details
    * @revision     
    *******************************************************************************/
	retrieveUserRecord : function(component) {
        var action_retrieveUser = component.get("c.retrieveUserDetails");
        action_retrieveUser.setCallback(this, function(a) {
            component.set("v.userRec", a.getReturnValue());
            var userRecord = a.getReturnValue();
            var userRecord = component.get("v.userRec");

            //sets to edit mode if birthdate is null
            if (userRecord.App_Birthdate__c == null) {
                component.set("v.isEdit", true);
                component.set("v.disableCancel", true);
            } else {
                component.set("v.selectedDOB", userRecord.App_Birthdate__c);
                component.set("v.selectedDOB_ReadOnly", userRecord.App_Birthdate__c);
            }

            //sets the gender
            var selectedGender = '';
            if (userRecord.App_Gender__c == 'M') {
                selectedGender = 'Male';
            } else if (userRecord.App_Gender__c == 'F') {
                selectedGender = 'Female';
            } else if (userRecord.App_Gender__c == 'X') {
                selectedGender = 'Indeterminate / Intersex / Unspecified';
            }
            component.set("v.selectedGender", selectedGender);

            //sets the ATSI
            var selectedATSI = '';
            if (userRecord.App_Aboriginal_or_Torres_Strait_Islander__c == 'Yes') {
                selectedATSI = 'Yes';
            } else if (userRecord.App_Aboriginal_or_Torres_Strait_Islander__c == 'No') {
                selectedATSI = 'No';
            }
            component.set("v.selectedATSI", selectedATSI);

            //set the isPostalAustralia
            var isPostalAustralia = '';
            if (userRecord.App_Address_Type__c == 'POSTAL') {
                isPostalAustralia = 'Yes';
            } else if (userRecord.App_Address_Type__c == 'OS-POSTAL') {
                isPostalAustralia = 'No';
            }
            component.set("v.isPostalAustralia", isPostalAustralia);

            //set postcode
            component.set("v.selectedPostCode", userRecord.PostalCode);

            //set ATSI variable to show or not to show
            var citizenType = userRecord.App_Residency_Status__c;
            if(citizenType == 'INTRNTNL' || citizenType == 'INT-TEP'){
                component.set("v.showATSIquestion", false);
            }

            if (isPostalAustralia == 'Yes') {
                //set the state
                component.set("v.selectedState", userRecord.State); 
                //set the postcode
                component.set("v.selectedPostCode", userRecord.PostalCode);
            } else if (isPostalAustralia == 'No') {
                //set the state
                component.set("v.selectedOSstate", userRecord.State); 
                //set the postcode
                component.set("v.selectedOSPostalCode", userRecord.PostalCode);
            }

            /*******************************************************************************
            * @author       Ant Custodio
            * @date         5.Apr.2017         
            * @description  check if the student is a previoius student
            * @revision     
            *******************************************************************************/
            if (userRecord.App_Previously_Studied_at_Monash__c) {
                // first get the div element. by using aura:id
                var changeElement = component.find("DivID");
                // by using $A.util.toggleClass add-remove slds-hide class
                $A.util.removeClass(changeElement, "slds-hide");
            }

            action_retrieveCTByName = component.get("c.retrieveCitizenshipTypeByName");
            action_retrieveCTByName.setParams({ "ctName"   : userRecord.App_Residency_Status__c });
            action_retrieveCTByName.setCallback(this, function(a) {
                if (a.getReturnValue().Explanation__c != null && a.getReturnValue().Explanation__c != '') {
                    component.set("v.campusOfStudy", a.getReturnValue().Campus_of_Study__c);
                }
                
                component.set("v.CitizenshipType", a.getReturnValue().Explanation__c);

                //disable some personal details if there is already a submitted application
                var action_disableFields = component.get("c.hasSubmittedApplication");
                action_disableFields.setParams({ "contactId"   : userRecord.ContactId });
                action_disableFields.setCallback(this, function(a) {
                    component.set("v.isDisabled", a.getReturnValue());
                });
                $A.enqueueAction(action_disableFields); 
            });
            $A.enqueueAction(action_retrieveCTByName);
        });
        $A.enqueueAction(action_retrieveUser);
	},

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  retrieve the picklist values
    * @revision     
    *******************************************************************************/
    retrieveYESNOPicklistValues : function(inputsel) {
        var opts=[];
        opts.push({"class": "optionClass", label: "--Select--", value: ""});
        opts.push({"class": "optionClass", label: "Yes", value: "Yes"});
        opts.push({"class": "optionClass", label: "No", value: "No"});
        inputsel.set("v.options", opts);
    }, 

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         25.July.2017         
    * @description  retrieve Citizenship types and store in a list variable
    * @revision     
    *******************************************************************************/
    retrieveCitizenshipTypeList : function(component) {
        var actionGetCitTypes = component.get("c.retrieveCitizenshipTypes");
        actionGetCitTypes.setCallback(this, function(response) {
            var citType = response.getReturnValue();
            component.set("v.citizenshipTypeList", citType); 
        });

        $A.enqueueAction(actionGetCitTypes);
    },

	/*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  retrieve the picklist values
    * @revision     
    *******************************************************************************/
	retrievePicklistValues : function(actionToRun, inputsel) {
        var opts=[];
        actionToRun.setCallback(this, function(a) {
            opts.push({"class": "optionClass", label: "--Select--", value: ""});
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);

        });
        $A.enqueueAction(actionToRun); 
	}, 

    
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  retrieve the picklist values
    * @revision     
    *******************************************************************************/
    retrievePicklistValuesv2 : function(actionToRun, inputsel, campusOfStudy) {
        var opts=[];
        actionToRun.setParams({ "campusOfStudy"   : campusOfStudy });
        actionToRun.setCallback(this, function(a) {
            opts.push({"class": "optionClass", label: "--Select--", value: ""});
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i].split("~")[1], value: a.getReturnValue()[i].split("~")[0]});
            }
            inputsel.set("v.options", opts);

        });
        $A.enqueueAction(actionToRun); 
    }, 

	/*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  clears a specific field error
    * @revision     
    *******************************************************************************/
    clearErrors : function(component, auraId) {
        var foundComponent = component.find(auraId);
        if (foundComponent != null) {
            foundComponent.set("v.errors", null);
        }
    },

	/*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  clears all form errors
    * @revision     08.Aug.2017 - Majid - hiding the warnings - Jira: SF-291
    *******************************************************************************/
	clearAllErrors : function(component) {
        
		this.clearErrors(component, "salutationOptions");
		this.clearErrors(component, "lName");
		this.clearErrors(component, "GenderOptions");
        this.clearErrors(component, "campusOfStudyOptions");
		//this.clearErrors(component, "bdayError");
		this.clearErrors(component, "email");
		this.clearErrors(component, "studentid");
        this.clearErrors(component, "CitizenshipTypeOptions");
        this.clearErrors(component, "street");
        this.clearErrors(component, "city");
        this.clearErrors(component, "StateOptions");
        this.clearErrors(component, "osState");
        this.clearErrors(component, "postal");
        this.clearErrors(component, "ospostal");
        this.clearErrors(component, "countryOptions");
        this.clearErrors(component, "addressType");
        this.clearErrors(component, "atsiOptions");

        //Added in revision - clear the direction messages
        component.set("v.showRedirectBecauseOfCitizenshipType", false);
        
        //except the following error should be shown always
        //component.set("v.showRedirectToOldMonashPortal", false);
        //component.set("v.showRedirectBecauseOfCountry2", false);

        //Enbaling save again
        //component.set("v.disableSave", false);

        component.set("v.errorMessage", "");
        component.set("v.dobError", "");
        $A.util.removeClass(component.find("bday"), 'dateError');
        component.set("v.initializeDatePicker", true);
	},
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  populates citizenship type code on the backend
    * @revision     
    *******************************************************************************/
    populateCitizenshipType : function(component) {
        var citizenshipTypeCode = '';
        var citizenshipType = component.get("v.CitizenshipType");
       
        var citizenTypeList = component.get("v.citizenshipTypeList");
        for(var idx=0; idx<citizenTypeList.length; idx++){
            if(citizenshipType == citizenTypeList[idx].Explanation__c){
               citizenshipTypeCode = citizenTypeList[idx].Name;
            }
        }

        component.set("v.userRec.App_Residency_Status__c", citizenshipTypeCode);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         3.Apr.2017         
    * @description  saves the user reord
    * @revision     
    *******************************************************************************/
    updateUser : function (component, event) {
        var action = component.get("c.updateUserRecord");
        var userRec = component.get("v.userRec");
        action.setParams({ "userRec": userRec });
        var mobile = component.find("mobile");
        var phone = component.find("phone");
        var homephone = component.find("homephone");
        var bday = component.find("bdayError");

        if (!component.get("v.invalidDate")) {
            var selectedBdate = component.get("v.selectedDOB");

            if (selectedBdate != null) {
                selectedBdate = new Date(selectedBdate.split("-")[0], selectedBdate.split("-")[1]-1, selectedBdate.split("-")[2]);
                var date16YearsBack = new Date(new Date().getFullYear()-16, new Date().getMonth(), new Date().getDate());
                if (selectedBdate >= date16YearsBack) {
                    if (!component.get("v.isAgeAcknowledged")) {
                        component.set("v.showAgeCheckPopup", true);
                    }
                } else {
                    component.set("v.isAgeAcknowledged", true);
                }
            } else {
                component.set("v.invalidDate", true);
            }
        } else {
            component.set("v.invalidDate", true);
        }

        if (component.get("v.isAgeAcknowledged") || component.get("v.invalidDate")) {
            component.set("v.showAgeCheckPopup", false);

            this.validateForm(component);
        
            var hasError = component.get("v.hasErrors");
        
            if (    !this.isFieldPopulated(component.get("v.userRec.MobilePhone")) &&
                    !this.isFieldPopulated(component.get("v.userRec.Phone")) &&
                    !this.isFieldPopulated(component.get("v.userRec.App_Home_Phone__c")) 
                ) {
                mobile.set("v.errors", [{message:"At least 1 phone number is required"}]);
                phone.set("v.errors", [{message:"At least 1 phone number is required"}]);
                homephone.set("v.errors", [{message:"At least 1 phone number is required"}]);
                hasError = true;
            } else {
                mobile.set("v.errors", null);
                phone.set("v.errors", null);
                homephone.set("v.errors", null);
            }
            if (!hasError) {
                var postal = component.find("postal");
                //only validate postcode if in Australia
                var selectedPostCode = component.get("v.selectedPostCode");
                if (component.get("v.isPostalAustralia") == "Yes") {
                    if (selectedPostCode.length == 4) {
                        
                        var postCodeCheck = component.get("c.findPostCodeNumber");
                        postCodeCheck.setParams({ "postCode" : selectedPostCode });
                    
                        postCodeCheck.setCallback(this, function(a) {
                            var state = a.getState();
                            if (state === "ERROR") {
                                var errors = a.getError();
                                if (errors) {
                                    if (errors[0] && errors[0].message) {
                                        console.log("Error message: " + 
                                                 errors[0].message);
                                        component.set("v.errorMessage", "An Unexpected error has occured. Please contact your Administrator.");
                                    }
                                } else {
                                    console.log("Unknown error");
                                }
                            } else {
                                if (!a.getReturnValue()) {
                                    postal.set("v.errors", [{message:"The Postcode entered is not a valid Australian Postcode"}]);
                                    hasError = true;
                                } else {
                                    
                                    action.setCallback(this, function(response) {
                                        var state = response.getState();
                                        // display a message to your user, prompting them to close
                                        // the action modal
                                        if (state === "ERROR") {
                                            var errors = response.getError();
                                            if (errors) {
                                                if (errors[0] && errors[0].message) {
                                                    console.log("Error message: " + 
                                                             errors[0].message);
                                                    component.set("v.errorMessage", "An Unexpected error has occured. Please contact your Administrator.");
                                                }
                                            } else {
                                                console.log("Unknown error");
                                            }
                                        } else {
                                            component.set("v.isEdit", false);
                                        }
                                    });
                                }
                            }
                        });
                        $A.enqueueAction(postCodeCheck);
                        
                    } else {
                        postal.set("v.errors", [{message:"The Postcode entered is not a valid Australian Postcode"}]);
                        hasError = true;
                    }
                } else {
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        // display a message to your user, prompting them to close
                        // the action modal
                        if (state === "ERROR") {
                            var errors = response.getError();
                            if (errors) {
                                if (errors[0] && errors[0].message) {
                                    console.log("Error message: " + 
                                             errors[0].message);
                                    component.set("v.errorMessage", "An Unexpected error has occured. Please contact your Administrator.");
                                }
                            } else {
                                console.log("Unknown error");
                            }
                        } else {
                            component.set("v.isEdit", false);
                        }
                    });
                }
            }
            if (!hasError) {
                if (component.get("v.isPostalAustralia") == "Yes") {
                    //set the State
                    component.set("v.userRec.State", component.get("v.selectedState"));
                    //set the PostCode
                    component.set("v.userRec.PostalCode", component.get("v.selectedPostCode"));
                } else if (component.get("v.isPostalAustralia") == "No") {
                    //set the State if overseas
                    component.set("v.userRec.State", component.get("v.selectedOSstate"));
                    //set the postcode if overseas
                    component.set("v.userRec.PostalCode", component.get("v.selectedOSPostalCode"));
                }
                //sets the birthdate
                component.set("v.userRec.App_Birthdate__c", component.get("v.selectedDOB"));
                //set the citizenship type
                this.populateCitizenshipType(component);
                //set the ATSI
                component.set("v.userRec.App_Aboriginal_or_Torres_Strait_Islander__c", component.get("v.selectedATSI"));
                component.set("v.errorMessage", '');
                component.set("v.hasErrors", false);
                $A.util.removeClass(component.find("bday"), 'dateError');
                //clear all errors
                this.clearAllErrors(component);

                $A.enqueueAction(action);

            } else {
                component.set("v.errorMessage", 'There are errors on your page. Please review your form.');
                window.scrollTo(0, 0);
            }
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         28.Apr.2017         
    * @description  clear the error on change
    * @revision     
    *******************************************************************************/
    clearError_onChange: function(component, event) {
        var eventSource = event.getSource();
        var auraId = eventSource.getLocalId();
        this.clearErrors(component, auraId);
        window.location.hash = '#'+auraId;
    },

    /*******************************************************************************
    * @author       Majid Reisi Dehkordi
    * @date         28.Apr.2017         
    * @description  Check for Risky Countries and then it will show a message
                    if the country which is selected among the risky ones
    * @revision     
    *******************************************************************************/
    checkForHighRiskCountries: function(component, event, helper) {
        // default the following feild to false to make sure it is not show by default
        var showTheMessage = false;
        // get the selected country from the list (first condition)
        var country = component.get("v.userRec.Country");
        // get the campus of study (second condition)
        var campus = component.get("v.campusOfStudy");
        // get the citizenship type (third condition)
        var citizenshipType = component.get("v.CitizenshipType");

        // Making sure that none of the following messages is shown in the begining
        component.set("v.showRedirectBecauseOfCitizenshipType", false);
        component.set("v.showRedirectBecauseOfCountry2", false);

        // Enable save only if campus is not Malaysia or South africa
        if (campus != 'Malaysian campus' && campus != 'South African campus')
            component.set("v.disableSave", false);

        //If all of the following conditions are met
        if( country != null && country != "" && this.isRiskyCountry(country) && 
            citizenshipType != null && citizenshipType != "" && this.isRiskyCitizenshipType(citizenshipType) && 
            campus != null && campus != "" && this.isRiskyCampus(campus)){
            //show the following message
            component.set("v.showRedirectBecauseOfCitizenshipType", true);
            //disable save
            component.set("v.disableSave", true);
        }
        else if(country != null && country != "" && !this.isRiskyCountry(country) &&
                citizenshipType != null && citizenshipType != "" && this.isRiskyCitizenshipType(citizenshipType) && 
                campus != null && campus != "" && this.isRiskyCampus(campus))
        {
            // show the following message
            component.set("v.showRedirectBecauseOfCountry2", true);
        }



        //Show the follwoing message if the condititions are met SF-413
        //This is to remind the foreign students who live in Australia that
        //they could use the monash agents
        if(component.get("v.isPostalAustralia") == 'Yes' && citizenshipType != null && citizenshipType != "" && 
                this.isRiskyCitizenshipType(citizenshipType) && 
                campus != null && campus != "" && this.isRiskyCampus(campus)){
            // show the following message
            component.set("v.showRedirectBecauseOfCountry2", true);
        }
    },

    isRiskyCountry: function(country) {
        if(country.indexOf('   ') != -1){
            return true
        }
        return false;
    },

    isRiskyCitizenshipType: function(citizenshipType) {
        if(citizenshipType == 'I hold a non-student temporary visa (e.g. work or spouse visa)')
            return true;
        else if (citizenshipType == 'I hold or hope to hold an international student visa') 
            return true;
        else return false;
    },

    isRiskyCampus: function(campus) {
        if( campus == 'Australian campus and any location not listed below')
            return true;
        else
            return false;
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         6.Jun.2017         
    * @description  validates the field if its populated
    * @revision     
    *******************************************************************************/
    isFieldPopulated: function(fieldToValidate) {
        var isValid = true;
        if (fieldToValidate == null) {
            isValid = false;
        } else {
            fieldToValidate = fieldToValidate.replace(new RegExp(' ', 'g'),"");
            if (fieldToValidate == '') {
                isValid = false;
            }
        }

        return isValid;
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         5.Apr.2017         
    * @description  remove the values when value is set to no
    * @revision     
    *******************************************************************************/
    removeValuesIfFalse: function(component) {
        var action_removeValuesIfFalse = component.get("c.removeValuesWhenUnticked");
        var userRec = component.get("v.userRec");
        action_removeValuesIfFalse.setParams({"userToUpdate": userRec});
        action_removeValuesIfFalse.setCallback(this, function(c) {
            component.set("v.userRec", c.getReturnValue());
        });
        
        $A.enqueueAction(action_removeValuesIfFalse);
    },  

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         5.Apr.2017         
    * @description  validate the date of birth
    * @revision     
    *******************************************************************************/
    validateDOB: function(component) {
        //Date of Birth validation
        var regDateFormat = /^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$/;
        //var regDateFormat = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
        var selectedBdate = component.get("v.selectedDOB");

        $A.util.removeClass(component.find("bday"), 'dateError');
        component.set("v.dobError", "");
        component.set("v.invalidDate", false);

        if (!this.isFieldPopulated(selectedBdate)) {
            component.set("v.dobError", "Date of Birth is required");
            $A.util.addClass(component.find("bday"), 'dateError');
            component.set("v.invalidDate", true);
        } else if (!selectedBdate.match(regDateFormat)) {
            component.set("v.dobError", "Please enter a valid date (dd/mm/yyyy).");
            $A.util.addClass(component.find("bday"), 'dateError');
            component.set("v.invalidDate", true);
        } else if (selectedBdate != null) {
            selectedBdate = new Date(selectedBdate.split("-")[0], selectedBdate.split("-")[1]-1, selectedBdate.split("-")[2]);
            var dateNow = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
            var earliestDate = new Date(new Date().getFullYear()-99, new Date().getMonth(), new Date().getDate());
        

            if(selectedBdate > dateNow){
                component.set("v.dobError", "Please check your year of birth - you have entered a future date");
                $A.util.addClass(component.find("bday"), 'dateError');
                component.set("v.invalidDate", true);
            } else if(selectedBdate <= earliestDate) {
                component.set("v.dobError", "Please check your year of birth.");
                $A.util.addClass(component.find("bday"), 'dateError');
                component.set("v.invalidDate", true);
            }
        } 
        component.set("v.isAgeAcknowledged", false);
    },  

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         6.Jun.2017         
    * @description  populates the gender field
    * @revision     
    *******************************************************************************/
    populateGender: function(component) {
        var dynamicCmp = component.find("GenderOptions");
        var selectedGender = '';

        if (dynamicCmp.get("v.value") == 'Male') {
            selectedGender = 'M';
        } else if (dynamicCmp.get("v.value") == 'Female') {
            selectedGender = 'F';
        } else if (dynamicCmp.get("v.value") == 'Indeterminate / Intersex / Unspecified') {
            selectedGender = 'X';
        }
        component.set("v.selectedGender", dynamicCmp.get("v.value"));
        component.set("v.userRec.App_Gender__c", selectedGender);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         6.Jun.2017         
    * @description  validates the citizenship type field
    * @revision     
    *******************************************************************************/
    validateCitizenship: function(component) {
        var dynamicCmp = component.find("CitizenshipTypeOptions");
        component.set("v.userRec.App_Residency_Status__c", dynamicCmp.get("v.value"));

        if (    dynamicCmp.get("v.value") == 'I hold a non-student temporary visa (e.g. work or spouse visa)' ||
                dynamicCmp.get("v.value") == 'I hold or hope to hold an international student visa' ) {

            component.set("v.showATSIquestion", false);
        } else {
            component.set("v.showATSIquestion", true);
            this.retrieveYESNOPicklistValues(component.find("atsiOptions"));
        }
    },


    /*******************************************************************************
    * @author       Ant Custodio
    * @date         6.Jun.2017         
    * @description  populates the gender field
    * @revision     
    *******************************************************************************/
    populateAddressType: function(component) {
        var dynamicCmp = component.find("addressType");
        component.set("v.isPostalAustralia", dynamicCmp.get("v.value"));
        if (dynamicCmp.get("v.value") == "Yes") {
            component.set("v.userRec.App_Address_Type__c", "POSTAL");
        } else if (dynamicCmp.get("v.value") == "No") {
            component.set("v.userRec.App_Address_Type__c", "OS-POSTAL");
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         6.Jun.2017         
    * @description  validates the Campus of Study field
    * @revision     08/08/2017 - Majid - Changes to campus Malaysia and South Africa
    *               To show a message after choosing the mentioned campuses
    *               Jira: SF-383 
    *******************************************************************************/
    validateCampusOfStudy: function(component) {
        var dynamicCmp = component.find("campusOfStudyOptions");
        component.set("v.campusOfStudy", dynamicCmp.get("v.value"));
        component.set("v.CitizenshipType", "");
        var ctOptions = component.find("CitizenshipTypeOptions");
        ctOptions.set("v.errors", null);
        if (dynamicCmp.get("v.value") != 'Malaysian campus' && dynamicCmp.get("v.value") != 'South African campus') {
            //retrieve citizenship list
            //helper.retrievePicklistValuesv2(component.get("c.retrieveCitizenshipTypeOptions"), component.find("CitizenshipTypeOptions"), dynamicCmp.get("v.value"));
            
            component.set("v.showRedirectToOldMonashPortal", false);''
            component.set("v.disableSave", false);
            dynamicCmp.set("v.errors", null);
            component.set("v.hideDependendPicklist", false);
        } else {
            component.set("v.showRedirectToOldMonashPortal", true);
            dynamicCmp.set("v.errors", [{message:""}]);
            component.set("v.disableSave", true);
            component.set("v.hideDependendPicklist", true);
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         6.Jun.2017         
    * @description  populates the ATSI field
    * @revision     
    *******************************************************************************/
    populateATSI: function(component) {
        var dynamicCmp = component.find("atsiOptions");
        if (dynamicCmp.get("v.value") == "Yes") {
            component.set("v.userRec.App_Aboriginal_or_Torres_Strait_Islander__c", "Yes");
        } else if (dynamicCmp.get("v.value") == "No") {
            component.set("v.userRec.App_Aboriginal_or_Torres_Strait_Islander__c", "No");
        }
        component.set("v.selectedATSI", dynamicCmp.get("v.value"));
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         12.Jun.2017         
    * @description  disable some fields if there is at least one 
                        submitted application
    * @revision     
    *******************************************************************************/
    disableFieldsOnSubmit : function(contactIdToUse) {
        var actionToRun = component.get("c.hasSubmittedApplication");
        actionToRun.setParams({ "contactId"   : contactIdToUse });
        actionToRun.setCallback(this, function(a) {
            component.set("v.isDisabled", a.getReturnValue());
        });
        $A.enqueueAction(actionToRun); 
    }, 
})