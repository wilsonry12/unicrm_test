({
    doInit : function(component, event, helper) {
        helper.getUserFirstName(component);
    },
    redirectHomepage : function (component, event, helper) {
         helper.redirectHomepage(component, event);
    }
})