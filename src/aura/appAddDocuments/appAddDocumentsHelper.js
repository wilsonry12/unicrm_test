({
    MAX_FILE_SIZE: 4500000, /* 6 000 000 * 3/4 to account for base64 */
    CHUNK_SIZE: 750000, /* Use a multiple of 4 */

    doQuery : function(component) {
        //clear list first
        component.set("v.fileNameList", ""); 
        //get and set Application Id
        var action = component.get("c.retrieveAppDocuments");
        var varAppId = component.get("v.applicationRecord");
        
        action.setParams({"appliId": varAppId});
        action.setCallback(this, function(response) {
            //component.set("v.attachList", response.getReturnValue());
            var attachments = response.getReturnValue();
            if(attachments.length > 0){
                var fileNames=new Array();
                for (var idx=0; idx<attachments.length; idx++) {
                    fileNames.push(attachments[idx].Contact_Document__r.Filename__c);
                }
                component.set("v.fileNameList", fileNames);
            }
        });
        $A.enqueueAction(action);
    },
    
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  retrieve the picklist values
    * @revision     
    *******************************************************************************/
    retrievePicklistValues : function(actionToRun, inputsel) {
        var opts=[];
        actionToRun.setCallback(this, function(a) {
            opts.push({"class": "optionClass", label: "--Select--", value: ""});
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);

        });
        $A.enqueueAction(actionToRun); 
    },

    save : function(component) {
        var validFileExtensions = [".pdf",".jpg",".doc",".docx",".gif",".rtf",".txt"];
        var existingFilenames = component.get("v.fileNameList");

        var fileInput = component.find("file").getElement();
        var file = null;
        //if (fileInput != null) {
          //  file = fileInput.files[0];
        //}
        if (fileInput.files.length > 0) {
            file = fileInput.files[0];
            var fName = file.name;
            var docComments = component.get("v.documentComments");

            
            //document validations happen here...
            if (file.size > this.MAX_FILE_SIZE) {
                //alert('File size cannot exceed 5MB.');
                component.set("v.showErrorPopup", true);
                component.set("v.validationMessage", "File size cannot exceed 5MB.");
                return;
            }
            if(fName.length >0) {
                var blnValid = false;
                var filenameExist = false;

                for (var j = 0; j < validFileExtensions.length; j++) {
                    var sCurExtension = validFileExtensions[j];
                    if (fName.substr(fName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                    //alert("Sorry, " + fName + " is invalid, allowed extensions are: " + validFileExtensions.join(", "));
                    component.set("v.showErrorPopup", true);
                    component.set("v.validationMessage", "Sorry, " + fName + " is invalid, allowed extensions are: " + validFileExtensions.join(", "));
                    return;
                }

                //check if filename already exist
                for (var j = 0; j < existingFilenames.length; j++) {
                    var sCurFile = existingFilenames[j];
                    if (fName.toLowerCase() == sCurFile.toLowerCase()) {
                        filenameExist = true;
                        break;
                    }
                }

                if(filenameExist){
                    //alert("File with this name already exist, please upload a new file.");
                    component.set("v.showDuplicatePopup", true);
                    return;
                }
                //end check if filename already exist
            }

            if (docComments != null) {
                if(docComments.length > 255){
                    //alert('Maximum size of comments is 255 characters only.');
                    component.set("v.showErrorPopup", true);
                    component.set("v.validationMessage", "Maximum size of comments is 255 characters only.");
                    return;
                }
            }

            var types = component.find("types");
            if (component.get("v.selectedType") == null || component.get("v.selectedType") == '') {
                types.set("v.errors", [{message:"Document Type is required"}]);
                return;
            }
        
            var fr = new FileReader();
            
            var self = this;
            fr.onload = $A.getCallback(function() {
                var fileContents = fr.result;
                var base64Mark = 'base64,';
                var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;

                fileContents = fileContents.substring(dataStart);
            
                self.upload(component, file, fileContents);
            })

            fr.readAsDataURL(file);
        } else {
            //alert('No file attached.');
            if(component.get("v.contactDocId") != null && component.get("v.contactDocId") != '') {

                var types = component.find("types");
                if (component.get("v.selectedType") == null || component.get("v.selectedType") == '') {
                    types.set("v.errors", [{message:"Document Type is required"}]);
                    return;
                }

                var actionUpload = component.get("c.uploadDocumentv2");
                actionUpload.setParams({
                    "appliId": component.get("v.applicationRecord"),
                    "fileName": "",
                    "base64Data": "", 
                    "contentType": "",
                    "docType": component.get("v.selectedType"),
                    "fileId": "",
                    "comments": component.get("v.documentComments"),
                    "existingDocument": component.get("v.contactDocId")
                });

                actionUpload.setCallback(this, function(response) {
                    // Create the appContactDocIdEvent event
                    var contactDocIdEvent = component.getEvent("appContactDocIdEvent");
                     
                    // Populate the event with the selected Object Id
                    contactDocIdEvent.setParams({
                        "contactDocId" : ""
                    });
                    // Fire the event
                    contactDocIdEvent.fire();
                });
                    
                $A.enqueueAction(actionUpload);
            } else {
                // Create the appContactDocIdEvent event
                var contactDocIdEvent = component.getEvent("appContactDocIdEvent");
                 
                // Populate the event with the selected Object Id
                contactDocIdEvent.setParams({
                    "contactDocId" : ""
                });
                // Fire the event
                contactDocIdEvent.fire();
            }
        }
    },
        
    upload: function(component, file, fileContents) {
        var fromPos = 0;
        var toPos = Math.min(fileContents.length, fromPos + this.CHUNK_SIZE);
        
        // start with the initial chunk
        this.uploadChunk(component, file, fileContents, fromPos, toPos, null);
    },

    uploadChunk : function(component, file, fileContents, fromPos, toPos, attachId) {
        var action = component.get("c.saveTheChunkv2"); 
        var chunk = fileContents.substring(fromPos, toPos);
        var allowDocTypeSelect = component.get("v.allowDocTypeSelect");
        if (!allowDocTypeSelect) {
            component.set("v.selectedType", "Other");
        }
        action.setParams({
            "appliId": component.get("v.applicationRecord"),
            "fileName": file.name,
            "base64Data": encodeURIComponent(chunk), 
            "contentType": file.type,
            "docType": component.get("v.selectedType"),
            "fileId": attachId,
            "comments": component.get("v.documentComments"),
            "existingDocument": component.get("v.contactDocId"),
        });
        var self = this;
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var attachRec = response.getReturnValue();
                fromPos = toPos;
                toPos = Math.min(fileContents.length, fromPos + self.CHUNK_SIZE);
                
                if (fromPos < toPos) {
                    var attachmentId = attachRec.split("-")[0];
                    self.uploadChunk(component, file, fileContents, fromPos, toPos, attachmentId);  
                } else {
                    var parentId = attachRec.split("-")[1];
                    // Create the appContactDocIdEvent event
                    var contactDocIdEvent = component.getEvent("appContactDocIdEvent");
                     
                    // Populate the event with the selected Object Id
                    contactDocIdEvent.setParams({
                        "contactDocId" : parentId
                    });
                    // Fire the event
                    contactDocIdEvent.fire();
                    component.set("v.selectedType", "");
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log("Error : " + JSON.stringify(errors));
            }

            this.doQuery(component);
        });

         $A.run(function() {
            $A.enqueueAction(action); 
        });
    }
})