<aura:component implements="forceCommunity:availableForAllPageTypes" access="global" controller="ApplicationDocumentsCC">
	<aura:attribute name="showList" type="Boolean" default="true"/>
    <aura:attribute name="applicationRecord" type="String" default="" />
    <aura:attribute name="selectedType" type="String" default="" />
    <aura:attribute name="contactDocName" type="String" default=""/>
    <aura:attribute name="contactDocId" type="String" default=""/>

    <aura:attribute name="noDataText" type="String" default="No records to display." />

    <aura:attribute name="errorMessage" type="String" default="" />
    <aura:attribute name="showSaveButton" type="Boolean" default="false" />
    <aura:attribute name="allowDocTypeSelect" type="Boolean" default="false" />
    <aura:attribute name="documentLabel" type="String" default="Add Document" />
    <aura:attribute name="fileNameList" type="List" description="List of current filenames already uploaded"/>

    <aura:attribute name="showErrorPopup" type="Boolean" default="false"/>
    <aura:attribute name="showDuplicatePopup" type="Boolean" default="false"/>
    <aura:attribute name="validationMessage" type="String" default="" />

    <aura:registerEvent name="appContactDocIdEvent" type="c:appContactDocIdLookup"/>

    <aura:attribute name="newDocId" type="String" default="" />
    <aura:attribute name="documentIterate" type="String" default="" />

    <aura:method name="uploadDocumentMethod" action="{!c.saveDocument}" access="public"/>
    <aura:method name="clearComponent" action="{!c.clearComponent}" access="public"/>
    <aura:attribute name="documentComments" type="String" default=""/>
    <aura:handler name="init" value="{!this}" action="{!c.doInit}" />
    <aura:method name="reInitialise" action="{!c.doInit}" access="public"/>

    <div class="slds">
    	<!-- SPINNER -->
        <span id="Accspinner" style="display:none">
            <lightning:spinner aura:id="spinner" variant="neutral" size="medium" alternativeText="Loading"/>
        </span>
        <!-- END - SPINNER -->
        
        <!-- ERROR MESSAGE -->
        <aura:If isTrue="{!v.errorMessage != ''}">
            <div class="slds-notify slds-notify--alert slds-theme--error slds-theme--alert-texture" role="alert">
                <span class="slds-assistive-text">Error</span>
                {!v.errorMessage}
            </div>
        </aura:If>
        <!-- END - ERROR MESSAGE -->

        <!-- ERROR POPUP -->
        <aura:If isTrue="{!v.showErrorPopup}">
            <div role="alertdialog" tabindex="-1" aria-labelledby="prompt-heading-id" aria-describedby="prompt-message-wrapper" class="slds-modal slds-fade-in-open slds-modal--prompt">
                <div class="slds-modal__container">
                    <div class="slds-modal__header slds-theme--error slds-theme--alert-texture">
                        <div class="slds-float--right">
                            <ui:button class="x-button slds-button slds-notify__close slds-button--icon-inverse" press="{!c.closeErrorModal}">
                                <lightning:icon iconName="utility:close" size="x-small" alternativeText="close"/>
                                <span class="slds-assistive-text">Close</span>
                            </ui:button>
                        </div>
                        <h2 class="slds-text-heading--small" id="prompt-heading-id">Validation Error</h2>
                    </div>
                    <div class="slds-modal__content slds-p-around--medium">
                        <p>
                            {!v.validationMessage}
                        </p>
                    </div>
                </div>
            </div>
            <div class="slds-backdrop slds-backdrop--open"></div>
        </aura:If>
        <!-- END - ERROR POPUP -->

        <!-- DUPLICATE DOCUMENT POPUP -->
        <aura:If isTrue="{!v.showDuplicatePopup}">
            <div role="alertdialog" tabindex="-1" aria-labelledby="prompt-heading-id" aria-describedby="prompt-message-wrapper" class="slds-modal slds-fade-in-open slds-modal--prompt">
                <div class="slds-modal__container">
                    <div class="modal-brand slds-modal__header slds-notify slds-notify_alert slds-theme_alert-texture">
                        <div class="slds-float--right">
                            <ui:button class="x-button slds-button slds-notify__close slds-button--icon-inverse" press="{!c.closeErrorModal}">
                                <lightning:icon iconName="utility:close" size="x-small" alternativeText="close"/>
                                <span class="slds-assistive-text">Close</span>
                            </ui:button>
                        </div>
                        <h2 class="slds-text-heading--small" id="prompt-heading-id">Duplicate Document</h2>
                    </div>
                    <div class="slds-modal__content slds-p-around--medium">
                        <p>
                            <aura:unescapedHtml value="{!$Label.c.App_DuplicateDocumentMessage}" />
                        </p>
                    </div>
                </div>
            </div>
            <div class="slds-backdrop slds-backdrop--open"></div>
        </aura:If>
        <!-- END - DUPLICATE DOCUMENT POPUP -->

		<div class="container slds-p-top--large">
            <form class="slds-form--stacked">
                <div class="slds-text-title">
                    *** Only upload: pdf files, jpg files, gif files, word files (.doc, .docx), rtf files, txt files ***
                    <br/>
                    *** There is a document file size limit of 5 MB ***
                </div>
                <div class="slds-grid slds-wrap slds-grid--pull-padded slds-p-bottom--medium">
                    <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                        <div class="slds-form-element__control">
                            <input type="file" class="file" aura:id="file" />
                        </div>
                    </div>
                </div>
                <aura:if isTrue="{!and(v.contactDocName != '', v.contactDocName != null)}">
                    <div class="slds-grid slds-wrap slds-grid--pull-padded slds-p-bottom--medium">
                        <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                            <div class="slds-text--regular">
                                File Currently Attached &nbsp;
                            </div>
                            <lightning:icon iconName="utility:attach" size="x-small" alternativeText="attach"/>&nbsp;{!v.contactDocName}
                        </div>
                    </div>
                </aura:if>
                <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                    <div class="{!if(!v.allowDocTypeSelect, 'slds-hide', '')}">
                        <div class="slds-grid--pull-padded slds-p-bottom--x-large">
                            <div class="slds-form-element__control">
                                <ui:inputSelect label="Document Type" 
                                                class="dynamic" aura:id="types"
                                                labelClass="slds-form-element__label"
                                                value="{!v.selectedType}"
                                                change="{!c.onSelectChange}"
                                                required="true" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slds-grid slds-wrap slds-grid--pull-padded">
                    <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-2 slds-large-size--1-of-2">
                        <div class="slds-form-element__control">
                            <ui:inputTextArea aura:id="commentOther" label="Comments (255 characters max)"
                                      class="slds-input"
                                      labelClass="slds-form-element__label"
                                      value="{!v.documentComments}"
                                      blur="{!c.checkErrors_onBlur}"
                                      resizable="false"
                                      rows="4"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</aura:component>