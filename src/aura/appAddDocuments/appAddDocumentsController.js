({
	doInit : function(component, event, helper) {
		//get and set Application Id
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;

        var retrievedAppId = '';
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.
            for (j = 0; j < sParameterName.length; j++) {
                if (sParameterName[j] === 'appId') { //get the app Id from the parameter
                    retrievedAppId = sParameterName[j+1];
                }
            }
        }
        if (retrievedAppId != '') {
            component.set("v.applicationRecord", retrievedAppId);
        }
        //retrieve document types
        helper.retrievePicklistValues(component.get("c.retrieveDocumentType"), component.find("types"));

        helper.doQuery(component);
	},

	onSelectChange : function(component, event, helper) {
		var selected = component.find("types").get("v.value");
		component.set("v.selectedType", selected);
	},

	addDocument : function(component, event, helper) {
		component.set("v.showList", false);
	},

    saveDocument : function(component, event, helper) {
        helper.save(component);
    },

	cancelAdd : function(component, event, helper) {
		component.set("v.showList", true);
        component.set("v.selectedType", "");
	},	

	deleteDocument : function(component, event, helper) {
		var varBlob = component.find("fileInput").get("v.attachFile");
	},

    clearComponent : function(component) {
        component.set("v.documentComments", "");
        component.set("v.selectedType", "");
        var attachedFile = component.find("file");
        if (attachedFile.getElement() != null) {
            attachedFile.getElement().value = '';
        }
        var types = component.find("types");
        types.set("v.errors", null);
    },

    closeErrorModal : function (component, event, helper) {
        component.set("v.showErrorPopup", false);
        component.set("v.showDuplicatePopup", false);
        component.set("v.validationMessage", "");
        component.find("file").getElement().value='';
        component.set("v.documentComments", "");
        component.set("v.selectedType", "");
    },
})