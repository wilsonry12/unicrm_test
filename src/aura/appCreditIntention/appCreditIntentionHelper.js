({
	/*******************************************************************************
    * @author       Ant Custodio
    * @date         19.Jun.2017         
    * @description  retrieve the application Id from the URL
    * @revision     
    *******************************************************************************/
	retrieveAppIdFromURL: function (component) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;
        var windowLoc = window.location.pathname;
        
        if (windowLoc.indexOf("applicationdetail") !=-1 || 
            windowLoc.indexOf("applicationreview") !=-1 ||
            windowLoc.indexOf("applicationprint") !=-1 ||
            windowLoc.indexOf("testpage") !=-1) {

            component.set("v.isEditForm", false);
            if (windowLoc.indexOf("applicationdetail") !=-1) {
                component.set("v.isEditForm", true);
            }
            var retrievedAppId = '';
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('='); //to split the key from the value.
                for (j = 0; j < sParameterName.length; j++) {
                    if (sParameterName[j] === 'appId') { //get the app Id from the parameter
                        retrievedAppId = sParameterName[j+1];
                    }
                }
            }
            if (retrievedAppId != '') {
                component.set("v.appId", retrievedAppId);
            }
        }
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         19.Jun.2017         
    * @description  retrieve the application record using the retrieved appId
    * @revision     
    *******************************************************************************/
	retrieveApplicationRecord: function (component) {
        var action_retrieveApplication = component.get("c.retrieveApplicationById");
        action_retrieveApplication.setParams({ "appIdParam"   : component.get("v.appId") });
       	
       	action_retrieveApplication.setCallback(this, function(a) {
       		var state = a.getState();
            if (state == "ERROR") {
            	component.set("v.errorMessage", "An Unexpected error has occured. Please contact your Administrator.");
            } else {
            	var appRec = a.getReturnValue();
            	component.set("v.applicationRecord", appRec);
            	//retrieve the selection
		        component.set("v.applyForCredit", appRec.Applying_for_Credit__c);
                /*if (appRec.Applying_for_Credit__c == '' || appRec.Applying_for_Credit__c == null) {
                    component.set("v.isEditForm", true);
                }*/
            }
        });

        //retrieve application list
        $A.enqueueAction(action_retrieveApplication);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         19.Jun.2017        
    * @description  populate the yes/no picklist values
    * @revision     
    *******************************************************************************/
    retrieveYESNOPicklistValues : function(inputsel) {
        var opts=[];
        opts.push({"class": "optionClass", label: "--Select--", value: ""});
        opts.push({"class": "optionClass", label: "Yes", value: "Yes"});
        opts.push({"class": "optionClass", label: "No", value: "No"});
        inputsel.set("v.options", opts);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         19.Jun.2017        
    * @description  shows/hides edit mode
    * @revision     
    *******************************************************************************/
    toggleEditing : function (component) {
        var isEditForm = component.get("v.isEditForm");
        
        if (isEditForm) {
            isEditForm = false;
        } else {
            isEditForm = true;
        }
        component.set("v.isEditForm", isEditForm);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         19.Jun.2017         
    * @description  update the application record using the retrieved application
    * @revision     
    *******************************************************************************/
	updateApplicationRecord: function (component) {
        var action_updateApplication = component.get("c.updateApplication");
        var appRec = component.get("v.applicationRecord");
        appRec.Applying_for_Credit__c = component.get("v.applyForCredit");
        action_updateApplication.setParams({ "appRecord"   :  appRec});
       	
       	action_updateApplication.setCallback(this, function(a) {
       		var state = a.getState();
            if (state == 'SUCCESS') {
            	component.set("v.applicationRecord", a.getReturnValue());
            	//this.toggleEditing(component);
            } else {
            	component.set("v.errorMessage", "An Unexpected error has occured. Please contact your Administrator.");
            }
        });
        //retrieve application list
        $A.enqueueAction(action_updateApplication);
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Jun.2017       
    * @description  clear the error on change
    * @revision     
    *******************************************************************************/
    clearError_onChange: function(component, event) {
        var eventSource = event.getSource();
        var auraId = eventSource.getLocalId();
        this.clearErrors(component, auraId);
        window.location.hash = '#'+auraId;
    },

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         20.Apr.2017         
    * @description  clears a specific field error
    * @revision     
    *******************************************************************************/
    clearErrors : function(component, auraId) {
        var foundComponent = component.find(auraId);
        if (foundComponent != null) {
            foundComponent.set("v.errors", null);
            $A.util.removeClass(foundComponent, 'has-errors');
        }
    },
})