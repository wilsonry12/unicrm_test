({
    studentRedirect :  function(component, event, helper){
        window.open(component.get("v.URLSetting").SSO_URL__c,"_parent");   
    },
    init:function(component,event,helper){
        $('#intermediateSection').hide();         
        helper.getUserInfo(component, event, helper);        
    },
    externalRedirect:function(component,event,helper){
        component.set("v.isOpen",false);
        $('#intermediateSection').show(); 
    },    
    closeModel:function(component,event,helper){
        component.set("v.isOpen",false);
        $('#enquiryButton').show();
    },
    handleClickHr : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/contact-support-hr-page"
        });
        urlEvent.fire();
    },
    handleClickMPass : function(component, event, helper) {
        window.open(component.get("v.URLSetting").Help_URL__c, '_blank');
    },
    handleDivOnClick : function(component, event, helper) {
        window.open(component.get("v.URLSetting").Community_Home_Page__c, '_self');
    }
})