({  
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         3.Apr.2017         
    * @description  initial actions on page load - retrieve the contact details and
    *                   check if read only
    * @revision     
    *******************************************************************************/
    doInit : function(component, event, helper) {
        helper.retrieveDraft(component);
    },

	createNewApplication : function(component, event, helper) {
        var appbutton = component.find("newAppButton");
        $A.util.addClass(appbutton.getElement(), 'disabled');
        
        helper.checkAllowNewApplication(component);
    },

    resumeApplication : function(component, event, helper) {
        var draftApp = component.get("v.draftApplication");
        var address = '/applicationdetail?appId=' + draftApp[0].applicationId;

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": address,
          "isredirect" :false
        });
        urlEvent.fire();
    }
})