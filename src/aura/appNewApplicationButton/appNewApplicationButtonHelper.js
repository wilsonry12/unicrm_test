({
	retrieveDraft : function(component, event, helper) {
		var actionDraftApplication = component.get("c.retrieveDraftApplication");
		actionDraftApplication.setCallback(this, function(response) {
            component.set("v.draftApplication", response.getReturnValue());
        });

        $A.enqueueAction(actionDraftApplication);
	},

	checkAllowNewApplication : function(component, event, helper) {
		var actionCheckUserDetails = component.get("c.toggleCreateNewApplicationButton");
        actionCheckUserDetails.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "ERROR") {
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                        errors[0].message);
                    }
                } else {
                console.log("Unknown error");
                }
            } else {
            	component.set("v.newAppNotAllowed", a.getReturnValue());
                var allowApplication = component.get("v.newAppNotAllowed");
                if(!allowApplication){
                	this.proceedNewApplication(component);
                }
                else{
                    var appbutton = component.find("newAppButton");
                    $A.util.removeClass(appbutton.getElement(), 'disabled');
                }
            }
        })
        $A.enqueueAction(actionCheckUserDetails);
	},

	proceedNewApplication : function(component, event, helper) {
        var action_createNewApplication = component.get("c.insertNewApplication");
        action_createNewApplication.setCallback(this, function(a) {
            var state = a.getState();
            if(state === "SUCCESS"){
                component.set("v.appId", a.getReturnValue());

                var address = '/applicationdetail?appId=' + a.getReturnValue();

                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": address,
                  "isredirect" :false
                });
                urlEvent.fire();
            } else {
                component.set("v.errorMessage", "You can only have one draft application. Please click 'Home' to refresh your dashboard");
            }
        });
        $A.enqueueAction(action_createNewApplication);
    },
})