({
	doInit : function(component, event, helper) {
		helper.checkAllowNewApplication(component);
        helper.retrieveDraft(component);
        helper.retrieveSubmitted(component);
        //retrieves the community URL dynamically
        helper.retrieveCommunityName(component);
    },

	cancelDraft : function(component, event, helper) {
        var draftApp = component.get("v.draftApplication");
        //assign Id and show the confirmation popup
        component.set("v.selRecToCancel", draftApp[0].applicationId);
        component.set("v.showConfirmPopup", true);
	},

	undoCancelDraft : function(component, event, helper) {
        //close the confirmation popup
        component.set("v.showConfirmPopup", false);
	},

	resumeApplication : function(component, event, helper) {
		var draftApp = component.get("v.draftApplication");
		var address = '/applicationdetail?appId=' + draftApp[0].applicationId;

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": address,
          "isredirect" :false
        });
        urlEvent.fire();
	},

	cancelApplication : function(component, event, helper) {
		var appId = component.get("v.selRecToCancel");
        var actionCancelDraftApplication = component.get("c.cancelDraftApplication");
        actionCancelDraftApplication.setParams({"applicationId" : appId});
        actionCancelDraftApplication.setCallback(this, function(response){
            var state = response.getReturnValue();
            if(state === "SUCCESS"){
                helper.retrieveDraft(component);
            }

            component.set("v.showConfirmPopup", false);
        });

        $A.enqueueAction(actionCancelDraftApplication);
    },

	newApplication : function(component, event, helper) {
		var appbutton = component.find("newAppButton");
        $A.util.addClass(appbutton.getElement(), 'disabled');

        var action_createNewApplication = component.get("c.insertNewApplication");
	    action_createNewApplication.setCallback(this, function(a) {

            var state = a.getState();
            if(state === "SUCCESS"){
    	        component.set("v.appId", a.getReturnValue());

    	        var address = '/applicationdetail?appId=' + a.getReturnValue();

    	        //var address = '/applicationdetail';
    	        var urlEvent = $A.get("e.force:navigateToURL");
    	        urlEvent.setParams({
    	          "url": address,
    	          "isredirect" :false
    	        });
    	        urlEvent.fire();
            } else {
                component.set("v.errorMessage", "You can only have one draft application. Please click 'Home' to refresh your dashboard");
            }
	    });
	    $A.enqueueAction(action_createNewApplication);
	},

    showOutcomeStatus : function(component, event, helper) {
        /*var domEvent = event.getParams().domEvent;
        var bodySpan = domEvent.target.nextSibling;
        
        var courseId = bodySpan.dataset.id;*/

        var source = event.getSource(); // this would give that particular component
        var courseId = source.get("v.name"); // returns the id

        component.set("v.selRecToView", courseId);

        helper.retrieveStatusMessage(component);

        component.set("v.showStatusPopup", true);
    },

    closeViewModal : function (component, event, helper){     
        //close the confirmation popup
        component.set("v.appCourseStatus", null);
        component.set("v.showStatusPopup", false);
    },

	/*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  show the spinner when page is loading
    * @revision     
    *******************************************************************************/
    waiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "block";
        }
        
    },
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  hide the spinner when finished loading
    * @revision     
    *******************************************************************************/
    doneWaiting: function(component, event, helper) {
        var accSpinner = document.getElementById("Accspinner");
        if (accSpinner != null) {
            accSpinner.style.display = "none";
        }
    }
})