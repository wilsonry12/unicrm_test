({
	retrieveDraft : function(component, event, helper) {
		var actionDraftApplication = component.get("c.retrieveDraftApplication");
		actionDraftApplication.setCallback(this, function(response) {
            var drafts = response.getReturnValue();
            component.set("v.draftApplication", response.getReturnValue());
            
            if(drafts.length >0){
                component.set("v.showList", true);
            }
        });

        $A.enqueueAction(actionDraftApplication);
	},

	retrieveSubmitted : function(component, event, helper) {
		var actionSubmittedApplication = component.get("c.retrieveSubmittedApplication");
		actionSubmittedApplication.setCallback(this, function(response) {
            var submittedApps = response.getReturnValue();
            component.set("v.submittedApplications", response.getReturnValue());
            
            if(submittedApps.length >0){
                component.set("v.showList", true);
            }
        });

        $A.enqueueAction(actionSubmittedApplication);
	},

	retrieveStatusMessage : function(component, event, helper) {
		var crsId = component.get("v.selRecToView");
		var actionViewOutcomeStatus = component.get("c.viewOutcomeStatus");
		actionViewOutcomeStatus.setParams({"appCourseId" : crsId});
		actionViewOutcomeStatus.setCallback(this, function(response) {
			component.set("v.appCourseStatus", response.getReturnValue());
		});

		$A.enqueueAction(actionViewOutcomeStatus);
	},

	checkAllowNewApplication : function(component, event, helper) {
		var actionCheckUserDetails = component.get("c.toggleCreateNewApplicationButton");
        actionCheckUserDetails.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "ERROR") {
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                        errors[0].message);
                    }
                } else {
                console.log("Unknown error");
                }
            } else {
                component.set("v.disableButton", a.getReturnValue());
            }
        })
        $A.enqueueAction(actionCheckUserDetails);
	},

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         21.Jun.2017         
    * @description  retrieves the community name dynamically
    * @revision     
    *******************************************************************************/
    retrieveCommunityName : function(component) {
        var sPageURL = window.location.pathname;
        component.set("v.communityURL", sPageURL);
    },
})