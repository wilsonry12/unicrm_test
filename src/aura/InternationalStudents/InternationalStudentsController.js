({
    /*******************************************************************************
    * @author       Majid
    * @date         28.July.2017         
    * @description  initial actions on page load - retrieve the contact details and
    * 					check if read only
    * @revision     
    *******************************************************************************/
    doInit : function(component, event, helper) {
        //retrieve countries list
        helper.retrievePicklistValues(component.get("c.retrieveCountries"), component.find("countryOptions"));
    },

    showDirectToMonashOnlineApplicationPortalAfterPart1 : function (component, event, helper) {
        //show the message
        document.getElementById('directToMonashOnlineApplicationPortalAfterPart1').classList.remove('slds-hide');

        //hide the parts 2, 3 and 4 in this order
        document.getElementById('part2').classList.add('slds-hide');
        document.getElementById('part3').classList.add('slds-hide');
        document.getElementById('part4').classList.add('slds-hide');

        //Hide error messages from part 2
        document.getElementById('directToAgentPortalAfterPart2').classList.add('slds-hide');
    },

    showDirectToAgentPortalAfterPart2 : function (component, event, helper) {
        //show the message
        document.getElementById('directToAgentPortalAfterPart2').classList.remove('slds-hide');

        //hide the parts 3 and 4 in this order
        document.getElementById('part3').classList.add('slds-hide');
        document.getElementById('part4').classList.add('slds-hide');

        //hide error messages from part 3
        document.getElementById('directToMyApplicationPortalAfterPart3').classList.add('slds-hide');
    },

    showDirectToMyApplicationPortal3 : function (component, event, helper) {
        //Show the message
        document.getElementById('directToMyApplicationPortalAfterPart3').classList.remove('slds-hide');

         //Hide the part 4
        document.getElementById('part4').classList.add('slds-hide');
    },

    showDirectToAgentPortalAfterPart4 : function (component, event, helper) {
        //Show the message
        document.getElementById('directToAgentPortalAfterPart4').classList.remove('slds-hide');
    },

    showPart2 : function (component, event, helper) {
        //show part 2
        document.getElementById('part2').classList.remove('slds-hide');

        //hide error messages from part 1 (if there was any)
        document.getElementById('directToMonashOnlineApplicationPortalAfterPart1').classList.add('slds-hide');
        
    },

    showPart3 : function (component, event, helper) {
        //show part 3
        document.getElementById('part3').classList.remove('slds-hide');

        //hide error messages from part 2 (if there was any)
        document.getElementById('directToAgentPortalAfterPart2').classList.add('slds-hide');
    },

    showPart4 : function (component, event, helper) {
        //show part 4
        document.getElementById('part4').classList.remove('slds-hide');

        //Hide error messages from part 3 (if there was any)
        document.getElementById('directToMyApplicationPortalAfterPart3').classList.add('slds-hide');      
    },
    
    onCountryChange: function(component, event, helper){
    	//Three spaces added to the end of the text to make sure if the country is among the high risks,
    	//here we check that and show the message.
        if(component.get("v.country") == '')
        {
            //Hide error messages from part 4 (if there was any)
            document.getElementById('directToAgentPortalAfterPart4').classList.add('slds-hide');
            document.getElementById('directToMyApplicationPortalAfterPart4-1').classList.add('slds-hide');
        }
        else 
        if(component.get("v.country").indexOf('   ') != -1){
            //Show the following message
            document.getElementById('directToMyApplicationPortalAfterPart4-1').classList.add('slds-hide');
            //Hide the following message (if there was any)
            document.getElementById('directToAgentPortalAfterPart4').classList.remove('slds-hide');
        }
        else{
            //Show the following message
            document.getElementById('directToAgentPortalAfterPart4').classList.add('slds-hide');
            //Hide the following message (if there was any)
            document.getElementById('directToMyApplicationPortalAfterPart4-1').classList.remove('slds-hide');            
        }
	}
})