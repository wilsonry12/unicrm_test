({
	/*******************************************************************************
    * @author       Majid Reisi Dehkordi
    * @date         01.Aug.2017       
    * @description  retrieve the picklist values
    * @revision     
    *******************************************************************************/
	retrievePicklistValues : function(actionToRun, inputsel) {
        var opts=[];
        actionToRun.setCallback(this, function(a) {
            opts.push({"class": "optionClass", label: "--Select--", value: ""});
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);

        });
        $A.enqueueAction(actionToRun); 
	}
})