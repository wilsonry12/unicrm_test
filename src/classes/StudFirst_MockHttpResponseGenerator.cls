@istest
global class StudFirst_MockHttpResponseGenerator implements HttpCalloutMock {
    global HttpResponse respond(HttpRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        
        String endPoint = req.getEndpoint();
        
        if(endPoint.contains('/v1/admissions/applications') && req.getMethod() == 'POST') {
            res.setBody('{"applications":[{"success":true,"isNewApplicant":true,"externalApplicantId":"0030k000001tbfEAAQ","smsApplicantId":"809580","smsApplications":[{"applicationId":682348,"updatedDate":"2017-05-04T23:55:08+0000","isNewApplication":true,"coursePreferences":[{"transferStatus":"PENDING","preferenceNumber":1,"courseCode":"4102","courseVersionNumber":1,"locationCode":"CLAYTON","attendanceMode":"IN","attendanceType":"FT","academicCalendarType":"ACAD-YR","academicCiSequenceNumber":7717,"admissionCalendarType":"ADM-2","admissionCiSequenceNumber":7736,"typeOfPlaceCode":null}]}],"info":["New Applicant ID 809580 created for Agent Applicant ID 0030k000001tbfEAAQ.","Preference 1 for application 123 has been successfully loaded into Applicant Portal.","No document has been inserted as no information has been provided.","Applicant Application data has been inserted, even though error(s) did occur. The data that caused the error has been ignored or the error has been ignored."],"errors":[]}]}');
            res.setStatusCode(200);
            res.setStatus('OK');
        } else if(endPoint.contains('/v1/admissions/applicants') && req.getMethod() == 'POST') {
            res.setBody('{"success":"true"}');
            res.setStatusCode(201);
            res.setStatus('OK');    
        }
        return res;
    }     
}