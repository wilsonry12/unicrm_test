/*******************************************************************************
* @author       Ant Custodio
* @date         19.June.2017         
* @description  Controller class for appCreditIntention lightning component
* @revision     
*******************************************************************************/
public with sharing class appCreditIntentionCC {
	/*******************************************************************************
    * @author       Ant Custodio
    * @date         19.June.2017         
    * @description  retrieves the application record using the given ID
    * @revision     
    *******************************************************************************/
    @AuraEnabled
	public static Application__c retrieveApplicationById (String appIdParam) {
		Application__c applicationRecord = [SELECT Id, Applying_for_Credit__c
											FROM Application__c
											WHERE Id =: appIdParam ];
		return applicationRecord;
	}

	/*******************************************************************************
    * @author       Ant Custodio
    * @date         20.June.2017         
    * @description  updates the application record given
    * @revision     
    *******************************************************************************/
    @AuraEnabled
	public static Application__c updateApplication (Application__c appRecord) {

		try {
			update appRecord;
		} catch (Exception ex) {
			throw new AuraHandledException('Unable to update your details: ' + ex.getMessage());
		}

		return appRecord;
	}
}