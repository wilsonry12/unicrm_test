/*******************************************************************************
* @author		Ant Custodio
* @date         24.May.2017        
* @description  test class for appQualificationsCC
* @revision     
*******************************************************************************/
@isTest
public with sharing class appQualificationsCC_Test { 
	private static Qualification__c secondQualRec;
	private static Qualification__c englishTest;
	private static Institution__c tertInsRec;
	private static Institution__c secInsRec;
	/*******************************************************************************
	* @author       Ant Custodio
	* @date         24.May.2017        
	* @description  test the my qualification accessed from the my details screen
	* @revision     
	*******************************************************************************/
	static testMethod void test_Qual_AccessedFromMyDetails() {
		//create required records
		createSampleData();

		//create a community user
		User communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');
		test.startTest();
		//only run if there is a Domestic Applicant Profile User
		if (communityUserRec != null) {
			//document used in upload - this is outside the runAs() because
			//community users cannot put contact Id lookups to an object
			//workaround in Class: made it 'without sharing'
			Contact_Document__c docRec = StudFirst_TestHelper.createContactDocument(communityUserRec.ContactId);
			insert docRec;

			System.runAs(communityUserRec) {
				//user creates a new qualification

				Contact_Qualification__c conQualRec;
				Contact_Qualification__c insertedRecord;
				Institution__c retrievedIns;

				//initialise picklist values
				System.assert(!appQualificationsCC.retrieveCountries().isEmpty(), 'Country picklist is empty');
				System.assert(!appQualificationsCC.retrieveYearofCompletion().isEmpty(), 'Year of Completion picklist is empty');
				System.assert(!appQualificationsCC.retrieveStates().isEmpty(), 'State picklist is empty');
				System.assert(!appQualificationsCC.retrievefirstYearComplete().isEmpty(), 'Year picklist is empty');
				System.assert(!appQualificationsCC.retrievelastYearComplete().isEmpty(), 'Year picklist is empty');
				System.assert(!appQualificationsCC.retrieveStatusOptions().isEmpty(), 'Status picklist is empty');
				System.assert(!appQualificationsCC.retrieveAdmissionTestTypes().isEmpty(), 'Admission Type picklist is empty');
				System.assert(!appQualificationsCC.retrieveRecordTypeNames().isEmpty(), 'Record Type picklist is empty');
				//get the institution prefix
				String insPrefix = appQualificationsCC.retrieveInstitutionPrefix();
				System.assertNotEquals(null, insPrefix, 'no prefix retrieved');
				
				//////////////////////////////TERTIARY DETAILS//////////////////////////////
				conQualRec = appQualificationsCC.retrieveConQualDetails();
				conQualRec.Country__c = 'Australia';
				conQualRec.First_Year_Enrolled__c = String.valueOf(date.today().year()-4);
				conQualRec.Last_Year_Enrolled__c = String.valueOf(date.today().year());
				conQualRec.Other_Qualification__c = 'Certificate III';

				retrievedIns = appQualificationsCC.retrieveCustomSettingById(tertInsRec.Id);
				System.assertEquals('834', tertInsRec.Institution_Code__c, 'No institution code retrieved');
				System.assertEquals('CANNING COLLEGE, AUSTRALIA', tertInsRec.Institution_Name__c, 'No institution name retrieved');
				conQualRec.Institution_Code__c = tertInsRec.Institution_Code__c;
				conQualRec.Institution_Name__c = tertInsRec.Institution_Name__c;

				conQualRec.Status__c = 'CURRENTLY STUDYING';
				conQualRec.RecordTypeId = appQualificationsCC.retrieveRecordTypeByName('Tertiary Education');

				//user saves qualification
				insertedRecord = appQualificationsCC.upsertNewQualificationRecord(conQualRec, docRec.Id);
				System.assertNotEquals(null, insertedRecord.Id, 'Qualification record is not inserted');
				System.assertEquals(docRec.Id, insertedRecord.Qualification_Document__c, 'Attachment not linked');
				//////////////////////////////END - TERTIARY DETAILS//////////////////////////////


				//////////////////////////////SECONDARY DETAILS//////////////////////////////
				conQualRec = appQualificationsCC.retrieveConQualDetails();
				conQualRec.Country__c = 'Australia';
				conQualRec.Year_of_Completion__c = String.valueOf(date.today().year()-4);
				conQualRec.State__c = 'VIC';
				
				retrievedIns = appQualificationsCC.retrieveCustomSettingById(secInsRec.Id);
				System.assertEquals('196', secInsRec.Institution_Code__c, 'No institution code retrieved');
				System.assertEquals('ST LEONARD\'S COLLEGE - IB', secInsRec.Institution_Name__c, 'No institution name retrieved');
				conQualRec.Institution_Code__c = secInsRec.Institution_Code__c;
				conQualRec.Institution_Name__c = secInsRec.Institution_Name__c;

				conQualRec.Score__c = '1.75';
				conQualRec.RecordTypeId = appQualificationsCC.retrieveRecordTypeByName('Secondary Education');

				//user saves qualification
				insertedRecord = appQualificationsCC.upsertNewQualificationRecordWithSelQual(conQualRec, secondQualRec.Qualification_Name__c, docRec.Id);
				System.assertNotEquals(null, insertedRecord.Id, 'Qualification record is not inserted');
				//////////////////////////////END - SECONDARY DETAILS//////////////////////////////


				//////////////////////////////OTHER QUALIFICATION DETAILS//////////////////////////////
				conQualRec = appQualificationsCC.retrieveConQualDetails();
				conQualRec.Other_Qualification__c = 'Certificate III';
				conQualRec.Date_Achieved__c = date.today().addMonths(1);
				conQualRec.Other_Qualification_Comments__c = 'my comments';
				conQualRec.RecordTypeId = appQualificationsCC.retrieveRecordTypeByName('Other Qualification');

				insertedRecord = appQualificationsCC.upsertNewQualificationRecord(conQualRec, docRec.Id);
				System.assertNotEquals(null, insertedRecord.Id, 'Qualification record is not inserted');
				//////////////////////////////END - OTHER QUALIFICATION DETAILS//////////////////////////////

				//////////////////////////////ENGLISH TEST DETAILS//////////////////////////////
				conQualRec = appQualificationsCC.retrieveConQualDetails();
				conQualRec.isTestCompleted__c = true;
				conQualRec.Score__c = '8';
				conQualRec.Date_Achieved__c = date.today().addMonths(-2);
				conQualRec.RecordTypeId = appQualificationsCC.retrieveRecordTypeByName('English Test');

				insertedRecord = appQualificationsCC.upsertNewQualificationRecordWithSelQual(conQualRec, englishTest.Qualification_Name__c, docRec.Id);
				System.assertNotEquals(null, insertedRecord.Id, 'Qualification record is not inserted');
				//////////////////////////////END - ENGLISH TEST DETAILS//////////////////////////////

				List<Contact_Qualification__c> conQualList = appQualificationsCC.retrieveConQualList();
				System.assert(!conQualList.isEmpty(), 'No Qualifications retrieved');

				//VIEWING SPECIFIC QUALIFICATION//
				Contact_Qualification__c conQualRetrieve = appQualificationsCC.retrieveQualificationById(conQualList[0].Id);
				System.assertEquals(conQualRetrieve.Id, conQualList[0].Id, 'Record Id not retrieved');

				//DELETING QUALIFICATIONS//
				String deletedId = appQualificationsCC.deleteSelectedQualification(conQualList[0].Id);
				System.assertEquals(deletedId, conQualList[0].Id, 'Record not deleted');
			}
		}
		test.stopTest();
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         24.May.2017        
	* @description  test the my qualification accessed from the application screen
	* @revision     Ant Custodio, 11.Jul.2017 - added the method used for retrieving
						institution
	*******************************************************************************/
	static testMethod void test_Qual_AccessedFromApplicationForm() {
		//create required records
		createSampleData();

		//create a community user
		User communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');
		test.startTest();
		//only run if there is a Domestic Applicant Profile User
		if (communityUserRec != null) {
			//document used in upload - this is outside the runAs() because
			//community users cannot put contact Id lookups to an object
			//workaround in Class: made it 'without sharing'
			Contact_Document__c docRec = new Contact_Document__c();
			docRec.Comments__c = 'certificate';
			docRec.Contact__c = communityUserRec.ContactId;
			docRec.Document_Type__c = 'ACCEPT-ADM';
			docRec.Filename__c = 'certificate.pdf';
			insert docRec;

			System.runAs(communityUserRec) {
				//user creates a new qualification

				Contact_Qualification__c conQualRec;
				Contact_Qualification__c insertedRecord;
				Institution__c retrievedIns;

				//create a new draft application
				Application__c appRecord = StudFirst_TestHelper.createApplication(communityUserRec.ContactId);
				insert appRecord;
				System.assertNotEquals(null, appRecord.Id, 'No application created');

				//initialise picklist values
				System.assert(!appQualificationsCC.retrieveCountries().isEmpty(), 'Country picklist is empty');
				System.assert(!appQualificationsCC.retrieveYearofCompletion().isEmpty(), 'Year of Completion picklist is empty');
				System.assert(!appQualificationsCC.retrieveStates().isEmpty(), 'State picklist is empty');
				System.assert(!appQualificationsCC.retrievefirstYearComplete().isEmpty(), 'Year picklist is empty');
				System.assert(!appQualificationsCC.retrievelastYearComplete().isEmpty(), 'Year picklist is empty');
				System.assert(!appQualificationsCC.retrieveStatusOptions().isEmpty(), 'Status picklist is empty');
				System.assert(!appQualificationsCC.retrieveAdmissionTestTypes().isEmpty(), 'Admission Type picklist is empty');
				System.assert(!appQualificationsCC.retrieveRecordTypeNames().isEmpty(), 'Record Type picklist is empty');
				//get the institution prefix
				String insPrefix = appQualificationsCC.retrieveInstitutionPrefix();
				System.assertNotEquals(null, insPrefix, 'No prefix retrieved');
				
				//////////////////////////////TERTIARY DETAILS//////////////////////////////
				conQualRec = appQualificationsCC.retrieveConQualDetails();
				conQualRec.Country__c = 'Australia';
				conQualRec.First_Year_Enrolled__c = String.valueOf(date.today().year()-4);
				conQualRec.Last_Year_Enrolled__c = String.valueOf(date.today().year());
				conQualRec.Other_Qualification__c = 'Certificate III';

				retrievedIns = appQualificationsCC.retrieveCustomSettingById(tertInsRec.Id);
				System.assertEquals('834', tertInsRec.Institution_Code__c, 'No institution code retrieved');
				System.assertEquals('CANNING COLLEGE, AUSTRALIA', tertInsRec.Institution_Name__c, 'No institution name retrieved');
				conQualRec.Institution_Code__c = tertInsRec.Institution_Code__c;
				conQualRec.Institution_Name__c = tertInsRec.Institution_Name__c;

				conQualRec.Status__c = 'CURRENTLY STUDYING';
				conQualRec.RecordTypeId = appQualificationsCC.retrieveRecordTypeByName('Tertiary Education');

				//user saves qualification
				insertedRecord = appQualificationsCC.upsertNewQualificationRecordWithApp(conQualRec, appRecord.Id, docRec.Id);
				System.assertNotEquals(null, insertedRecord.Id, 'Qualification record is not inserted');
				System.assertEquals(docRec.Id, insertedRecord.Qualification_Document__c, 'Attachment not linked');
				//////////////////////////////END - TERTIARY DETAILS//////////////////////////////


				//////////////////////////////SECONDARY DETAILS//////////////////////////////
				conQualRec = appQualificationsCC.retrieveConQualDetails();
				conQualRec.Country__c = 'Australia';
				conQualRec.Year_of_Completion__c = String.valueOf(date.today().year()-4);
				conQualRec.State__c = 'VIC';
				
				retrievedIns = appQualificationsCC.retrieveCustomSettingById(secInsRec.Id);
				System.assertEquals('196', secInsRec.Institution_Code__c, 'No institution code retrieved');
				System.assertEquals('ST LEONARD\'S COLLEGE - IB', secInsRec.Institution_Name__c, 'No institution name retrieved');
				conQualRec.Institution_Code__c = secInsRec.Institution_Code__c;
				conQualRec.Institution_Name__c = secInsRec.Institution_Name__c;

				conQualRec.Score__c = '1.75';
				conQualRec.RecordTypeId = appQualificationsCC.retrieveRecordTypeByName('Secondary Education');

				//user saves qualification
				insertedRecord = appQualificationsCC.upsertNewQualificationRecordWithSelQualWithApp(conQualRec, secondQualRec.Qualification_Name__c, appRecord.Id, docRec.Id);
				System.assertNotEquals(null, insertedRecord.Id, 'Qualification record is not inserted');

				//user edits the form
				String insId = appQualificationsCC.retrieveInstitutionIdByCode(secInsRec.Institution_Code__c, secInsRec.Institution_Name__c);
				System.assertEquals(secInsRec.Id, insId, 'Institution not retrieved');
				//////////////////////////////END - SECONDARY DETAILS//////////////////////////////


				//////////////////////////////OTHER QUALIFICATION DETAILS//////////////////////////////
				conQualRec = appQualificationsCC.retrieveConQualDetails();
				conQualRec.Other_Qualification__c = 'Certificate III';
				conQualRec.Date_Achieved__c = date.today().addMonths(1);
				conQualRec.Other_Qualification_Comments__c = 'my comments';
				conQualRec.RecordTypeId = appQualificationsCC.retrieveRecordTypeByName('Other Qualification');

				insertedRecord = appQualificationsCC.upsertNewQualificationRecordWithApp(conQualRec, appRecord.Id, docRec.Id);
				System.assertNotEquals(null, insertedRecord.Id, 'Qualification record is not inserted');
				//////////////////////////////END - OTHER QUALIFICATION DETAILS//////////////////////////////

				//////////////////////////////ENGLISH TEST DETAILS//////////////////////////////
				conQualRec = appQualificationsCC.retrieveConQualDetails();
				conQualRec.isTestCompleted__c = true;
				conQualRec.Score__c = '8';
				conQualRec.Date_Achieved__c = date.today().addMonths(-2);
				conQualRec.RecordTypeId = appQualificationsCC.retrieveRecordTypeByName('English Test');

				//lists the available score fields for the selected test type
				List<String> englishScoreFields = appQualificationsCC.displayEnglishScoreFields(englishTest.Qualification_Name__c);
				System.assert(!englishScoreFields.isEmpty(), 'No field retrieved');

				insertedRecord = appQualificationsCC.upsertNewQualificationRecordWithSelQualWithApp(conQualRec, englishTest.Qualification_Name__c, appRecord.Id, docRec.Id);
				System.assertNotEquals(null, insertedRecord.Id, 'Qualification record is not inserted');
				//////////////////////////////END - ENGLISH TEST DETAILS//////////////////////////////

				List<Application_Qualification_Provided__c> appQualList = appQualificationsCC.retrieveAppQualProvidedList(appRecord.Id);
				System.assert(!appQualList.isEmpty(), 'No Qualifications retrieved');

				//VIEWING SPECIFIC QUALIFICATION//
				Application_Qualification_Provided__c appQualRetrieve = appQualificationsCC.retrieveAppQualificationById(appQualList[0].Id);
				System.assertEquals(appQualRetrieve.Id, appQualList[0].Id, 'Record Id not retrieved');

				//DELETING QUALIFICATIONS//
				String deletedId = appQualificationsCC.deleteSelectedAppQualificationProvided(appQualList[0].Id);
				System.assertEquals(deletedId, appQualList[0].Id, 'Record not deleted');
			}
		}
		test.stopTest();
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         24.May.2017        
	* @description  creates all sample data required
	* @revision     
	*******************************************************************************/
	static testMethod void createSampleData () {
		//Tertiary institution & qualification
		tertInsRec = StudFirst_TestHelper.createTertiaryInstitution();
		insert tertInsRec;

		Qualification__c tertqualRec = StudFirst_TestHelper.createTertiaryQualification();
		insert tertqualRec;

		//Secondary institution & qualification
		secInsRec = StudFirst_TestHelper.createSecondaryInstitution();
		insert secInsRec;

		secondQualRec = StudFirst_TestHelper.createSecondaryQualification();
		insert secondQualRec;

		//English Test
		englishTest = StudFirst_TestHelper.createEnglishTest();
		englishTest.Sub_score_Type__c = 'Listening;Reading;Speaking;Writing';
		insert englishTest;
	}
}