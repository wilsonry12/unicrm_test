/*******************************************************************************
* @author       Anterey Custodio
* @date         15.Nov.2016         
* @description  Controller class for BMSEditEmailBody page
* @revision     
*******************************************************************************/
public with sharing class BMSEditEmailBodyCC {
    public Campaign massEmailRecord {get;set;}
    public String massEmailRecordId {get;set;}
    public String selectedTemplateId {get;set;}
    public String errorMessage {get;set;}
    public Boolean showUrgentCheckbox {get;set;}
    public Boolean showTemplateSelection {get;set;}
    public Boolean showDateTime {get;set;}
    public Map<Id, EmailTemplate> templates {get;set;}
    //ACUSTODIO 27.Feb.2017 - boolean to determine if the user is a BMS Admin
    private Boolean isAdmin {get;set;}
    //ACUSTODIO 2.Mar.2017 - determines whether user wants to send Email or SMS
    public String sendType {get;set;}

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         15.Nov.2016         
    * @description  Constructor class to initialise variables and methods
    * @revision     ACUSTODIO, 23.Jan.2017 - added method to remove CRICOS and Base
                        signature on preview
                    ACUSTODIO, 27.Feb.2017 - added try catch blocks
    *******************************************************************************/
    public BMSEditEmailBodyCC() {
        massEmailRecordId = ApexPages.currentPage().getParameters().get('crmId');
        errorMessage = '';
        showUrgentCheckbox = true;
        showTemplateSelection = true;
        showDateTime = false;
        //retrieves the send type (Email or SMS)
        sendType = CommonUtilities.retrieveSendType();

        massEmailRecord = new Campaign();

        //check if BMS Admin
        isAdmin = checkUserIfAdmin();
        try {
            Folder bmsFolder = [SELECT DeveloperName,Id,Name,Type FROM Folder WHERE Name = 'BMS Emails'];
            if (massEmailRecordId != null) {
                massEmailRecord = [ SELECT  Id, Name, Email_Body__c, Send_Date_Time__c, Custom_Message__c, From__c, Notify__c, Message_Header__c, Additional_Email__c, 
                                    Reply_To__c, Email_Template__c, Add_Base_Signature__c, Add_CRICOS__c, SMS_Body__c
                                    FROM    Campaign
                                    WHERE   Id =: massEmailRecordId
                                    LIMIT   1   ];

                if(massEmailRecord.Email_Template__c != null){
                    showTemplateSelection = false;
                    
                    EmailTemplate emailTemp = [SELECT Id, HtmlValue, Name, FolderId, IsActive FROM EmailTemplate WHERE FolderId =:bmsFolder.Id AND Name =:massEmailRecord.Email_Template__c AND IsActive = true];
                    selectedTemplateId = emailTemp.Id;
                }
                //remove the CRICOS and Base Signature on preview
                massEmailRecord.Custom_Message__c = removeCRICOSAndBaseSignature(massEmailRecord);
            }

            templates = new Map<Id, EmailTemplate>([SELECT Id, HtmlValue, Name, FolderId, IsActive FROM EmailTemplate WHERE FolderId =:bmsFolder.Id AND IsActive = true]);

            String currentUserProfile = [SELECT Id, Name FROM Profile WHERE Id =: userinfo.getProfileId() ].Name;
            if(currentUserProfile != 'BMS Admin' && currentUserProfile != 'System Administrator'){
                showUrgentCheckbox = false;
            }
        } catch (Exception ex) {
            errorMessage = 'There seems to be a problem on the page. Please contact the administrator.';
            system.debug('### ex: '+ ex + ' ' + ex.getLineNumber());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage);
            ApexPages.addMessage(myMsg);
        } 
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         27.Feb.2017         
    * @description  returns true if the user profile is BMS Admin 
                        or System Administrator
    * @revision     
    *******************************************************************************/
    private Boolean checkUserIfAdmin () {
        Boolean isUserAdmin = false;
        Set<String> allowedToSearchStaffSet = new Set<String> {'System Administrator', 'BMS Admin'};
        Profile profileRec = [  SELECT  Id,Name 
                                FROM    Profile 
                                WHERE   Id =: userInfo.getProfileId() 
                                LIMIT   1 ];
        if (allowedToSearchStaffSet.contains(profileRec.Name)) {
            isUserAdmin = true;
        }
        return isUserAdmin;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         23.Jan.2016         
    * @description  method to remove the CRICOS and Base Signature to the Message
    * @revision     
    *******************************************************************************/
    private String removeCRICOSAndBaseSignature (Campaign massEmailRecToCheck) {
        String updatedString = massEmailRecToCheck.Custom_Message__c;
        if (updatedString != null) {
            updatedString = updatedString.replace('<br/><p style="font: 11pt Arial; margin: 0;">' + System.Label.BMS_CRICOS + '</p>', '');
            updatedString = updatedString.replace('<br/><p style="font: Italic 11pt Arial; margin: 0;">' + System.Label.BMS_Base_Signature + '</p>', '');
        }
        
        return updatedString;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         15.Nov.2016         
    * @description  method to save CRM Mass Email Record
    * @revision     ACUSTODIO, 23.Jan.2017 - added CRICOS and base signature,
                        added CSS styles to message upon save
                    ACUSTODIO, 02.Mar,2017 - Added SMS error checking
    *******************************************************************************/
    public PageReference saveMassEmailRecord() {
        PageReference redirect = null;
        //validate the message being sent if it has the required fields
        errorMessage = validateCRMMassEmail(sendType);
        //check the URL so that the validation only applies in BMS, not BSMS
        if (sendType == 'email') {
            massEmailRecord.Type = 'Email';
            if (errorMessage == '') {
                if(massEmailRecord.Email_Template__c == null){
                   massEmailRecord.Email_Template__c = templates.get(selectedTemplateId).Name; 
                }
                if(massEmailRecord.Custom_Message__c != null){
                    String strEmailBody = massEmailRecord.Custom_Message__c;
                    //add the CRICOS or Base Message at the bottom of the email if either one is checked
                    strEmailBody = addEmailLabelsifChecked(massEmailRecord);
                    //add CSS styles to the email
                    strEmailBody = addCSSStyles(strEmailBody);
                    massEmailRecord.Custom_Message__c = strEmailBody;
                }
            }
        } else if (sendType == 'sms') {
            massEmailRecord.Type = 'SMS';
            if(massEmailRecord.Send_Date_Time__c == null){
                massEmailRecord.Send_Date_Time__c = System.now();
            }
        } else {
            errorMessage = 'There seems to be a problem on the page URL. Please contact the administrator.';
        }

        //do not make updates if there are errors
        if (errorMessage != '') {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage);
            ApexPages.addMessage(myMsg);
        } else {
            upsert massEmailRecord;

            redirect = Page.BMSEmailPreview;
            redirect.getParameters().put('etId', selectedTemplateId);
            redirect.getParameters().put('crmId', massEmailRecord.Id);
            redirect.getParameters().put('view', 'EmailView');
            redirect.getParameters().put('type', sendType);
        }
        return redirect;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         23.Jan.2017         
    * @description  check errors on the page
    * @revision     
    *******************************************************************************/
    private String validateCRMMassEmail (String sendType) {
        String errorMessage = '';
        if (sendType == 'email') {
            if ((massEmailRecord.Name == '' || massEmailRecord.Name == null)) {
                errorMessage += 'Email subject should not be empty <br/>';
            }
            if ((massEmailRecord.Message_Header__c == '' || massEmailRecord.Message_Header__c == null)) {
                errorMessage += 'Message header should not be empty <br/>';
            }
            if (selectedTemplateId == '' || selectedTemplateId == null) {
                errorMessage += 'Please select which email template to use <br/>';
            }
        } else if (sendType == 'sms') {
            if ((massEmailRecord.Name == '' || massEmailRecord.Name == null)) {
                errorMessage += 'Message name should not be empty <br/>';
            }
            if ((massEmailRecord.SMS_Body__c == '' || massEmailRecord.SMS_Body__c == null)) {
                errorMessage += 'Message content should not be empty <br/>';
            }
        } else {
            errorMessage = 'There seems to be a problem on the page URL. Please contact the administrator.';
        }
        return errorMessage;
    }
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         23.Jan.2017         
    * @description  Method to add CRICOS message at the bottom of the email
    * @revision     
    *******************************************************************************/
    private String addEmailLabelsifChecked(Campaign massEmailRecToCheck) {
        String updatedString = massEmailRecToCheck.Custom_Message__c;
        ///if CRICOS is ticked, add it at the bottom of the page
        if (massEmailRecToCheck.Add_CRICOS__c) {
            updatedString = updatedString + '<br/><p style="font: 11pt Arial; margin: 0;">' + System.Label.BMS_CRICOS + '</p>';
        }
        if (massEmailRecToCheck.Add_Base_Signature__c) {
            updatedString = updatedString + '<br/><p style="font: Italic 11pt Arial; margin: 0;">' + System.Label.BMS_Base_Signature + '</p>';
        }

        return updatedString;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         19.Jan.2017         
    * @description  Method to populate the html string with styles
    * @revision     ACUSTODIO, 6.Feb.2017 - added styles to <li> tags
    *******************************************************************************/
    private String addCSSStyles(String strEmailBody) {
        String updatedString = strEmailBody;
        updatedString = updatedString.replace('<h1>', '<h1 style=\"font: Bold 16pt Arial; color: rgb(0,108,171); margin: 5pt 0pt 11pt 0pt;\">');
        updatedString = updatedString.replace('<h2>', '<h2 style=\"font: Bold 14pt Arial; margin: 5pt 0pt 11pt 0pt;\">');
        updatedString = updatedString.replace('<h3>', '<h3 style=\"font: Bold 12pt Arial; margin: 5pt 0pt 11pt 0pt;\">');
        updatedString = updatedString.replace('<p>', '<p style=\"font: 11pt Arial; margin: 5pt 0pt 11pt 0pt;\">');
        updatedString = updatedString.replace('<li>', '<li style=\"padding-bottom: 4pt; font: 11pt Arial;\">');

        //remove existing style before adding the same style
        updatedString = updatedString.replace('<td style=\"vertical-align: top; padding: 8px; border: 1pt solid black\"', '<td ');
        updatedString = updatedString.replace('<td ', '<td style=\"vertical-align: top; padding: 8px; border: 1pt solid black\"');
        return updatedString;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         15.Nov.2016         
    * @description  method to go back to page
    * @revision     
    *******************************************************************************/
    public PageReference cancelEdit() {
        PageReference redirect = gotoBulkMessagingHome(false);
        return redirect;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         10.Nov.2016         
    * @description  method for previewing email
    * @revision     
    *******************************************************************************/
    public PageReference gotoEmailTemplate() {
        PageReference redirect = Page.BMSEmailPreview; 
        redirect.getParameters().put('etId',selectedTemplateId);
        redirect.getParameters().put('crmId',massEmailRecordId);
        redirect.getParameters().put('view','TemplateView');
        return redirect;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         10.Nov.2016         
    * @description  gives the list of campaigns the user has
    * @revision     ACUSTODIO, 27.Feb.2017 - made Staff template only available for Admins
    *******************************************************************************/
    public List<SelectOption> getEmailTemplateOptions() {
        List<SelectOption> campaignOptions = new List<SelectOption>();
        campaignOptions.add(new SelectOption('', '-- None --'));
        if (templates != null) {
            for(EmailTemplate temp: templates.values()){
                if (temp.Name == 'Staff Communication' && isAdmin) {
                    campaignOptions.add(new SelectOption(temp.Id, temp.Name));
                } else if (temp.Name == 'Student Communication') {
                    campaignOptions.add(new SelectOption(temp.Id, temp.Name));
                }
            }
        }
        return campaignOptions;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         15.Nov.2016         
    * @description  method to redirect back to the home page
    * @revision     
    *******************************************************************************/
    private PageReference gotoBulkMessagingHome (Boolean isUpdateEnabled) {
        PageReference redirect = Page.BMSHome; 
        //only update when boolean is true
        if (isUpdateEnabled) {
            update massEmailRecord;
        }
        //only put id if mass email record is not null
        /*if (massEmailRecord.Id != null) {
            redirect.getParameters().put('crmId',massEmailRecord.Id);
        }*/
        return redirect;
    }
}