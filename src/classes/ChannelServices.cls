/**************************************************
* Author: Ryan Wilson
* Date: 30.AUG.2017
* Description: contains all the trigger handlers for Channels
***************************************************/

public with sharing class ChannelServices {
	/**************************************************
	* Author: Ryan Wilson
	* Date: 29.AUG.2017
    * Creates a platform event for an inserted/updated Role
    ***************************************************/
	public class createPlatformEvent implements Triggers.Handler {
        public void run() {
        	Set<Id> channelIds = new Set<Id>();
        	List<Channels__e> channelEvents = new List<Channels__e>();

        	for(Channel__c channel: (List<Channel__c>) Trigger.new){
        		channelIds.add(channel.Id);
        	}

        	if(CommonUtilities.allowPublishEvent()){
                for(Channel__c channel: CommonUtilities.getChannels(channelIds)){
            		String eventType = (Trigger.isInsert || Trigger.isUpdate)? 'upsert': 'delete';
            		channelEvents.add(CommonUtilities.createChannelEvent(channel, eventType));
            	}

            	if(channelEvents.size() >0){
                    EventBus.publish(channelEvents);
                }
            }
        }
    }
}