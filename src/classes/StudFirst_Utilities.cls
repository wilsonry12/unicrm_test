public with sharing class StudFirst_Utilities {
	/*******************************************************************************
	* @author       Ant Custodio
	* @date         3.Oct.2017
	* @description  converts the string to a 7Bit ACII accepted string
					this replaces non 7Bit to a desired replacement character
	* @revision     
	*******************************************************************************/
	public static String convertTo7BitAcceptedString(String stringToUpdate, String replacementStr) {
		String convertedString = '';
		if (stringToUpdate != '' && stringToUpdate != null) {
			List<Integer> chars = stringToUpdate.getChars();
			for (Integer i=0; i<chars.size(); i++) {
				if (chars[i] < 0 || chars[i] > 127) { //invalid characters
					convertedString += replacementStr;
				} else {
					convertedString += stringToUpdate.substring(i, i+1);
				}
			}
		}
		return convertedString;
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         17.Oct.2017
	* @description  ticks the "Document submitted successfully?" flag if the API
						Response Message says the document is a duplicate
	* @param		adpRec - Record you want to check
					identifierStr - Set of String that determines if the record is
						a duplicate
					isCaseSensitive - determines whether the identifier is case 
						sensitive or not
	* @revision     
	*******************************************************************************/
	public static Application_Document_Provided__c tickADPFlagIfDuplicate(Application_Document_Provided__c adpRec, Set<String> identifierStr, Boolean isCaseSensitive) {
		if (!isCaseSensitive) {
			Set<String> identifierStrToLower = new Set<String>();
			for (String str: identifierStr) {
				identifierStrToLower.add(str.toLowerCase());
			}
			identifierStr = identifierStrToLower;
		}

		if (adpRec != null) {
			String respMsg = adpRec.API_Response_Message__c.toLowerCase();
			for (String respToFind: identifierStr) {
				if (respMsg.contains(respToFind)) {
					adpRec.Document_submitted_successfully__c = true;
				}
			}
		}

		return adpRec;
	}
}