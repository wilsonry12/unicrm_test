/**
 * Services class to provide Account sObject centric logical support
 * 
 * @author Carl Vescovi, PwC
 * 
 */

public with sharing class AccountServices {
    
    public static final String ACCOUNT_INDIVIDUAL_TYPE = 'Individual';		// reference to record type developer name for type used on individuals
    public static final String ACCOUNT_ORGANISATION_TYPE = 'Organization';		// reference to record type developer name for type used on organisations
    
}