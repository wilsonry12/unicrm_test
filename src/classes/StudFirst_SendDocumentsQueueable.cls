/*******************************************************************************
* @author       Ant Custodio
* @date         12.Sept.2017
* @description  Class used to send a request to MIX to push the attachments to
					Callista
* @revision     
*******************************************************************************/
public class StudFirst_SendDocumentsQueueable implements Queueable, Database.AllowsCallouts {
	private List<String> requestList;

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         12.Sept.2017
	* @description  constructor that accepts a map of Case and the Wrapper
	* @parameters	key = Case.Id
	* 				value = StudFirst_CRMInitiatedCommsToAP_Request record
	* @revision     
	*******************************************************************************/
	public StudFirst_SendDocumentsQueueable(String reqStr) {
		requestList = new List<String>{reqStr};
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         12.Sept.2017
	* @description  constructor that accepts a map of Case and the Wrapper
	* @parameters	List of JSON requests
	* @revision     
	*******************************************************************************/
	public StudFirst_SendDocumentsQueueable(List<String> reqList) {
		requestList = reqList;
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         12.Sept.2017
	* @description  Queue Execute
	* @revision     
	*******************************************************************************/
	public void execute(QueueableContext context) {
        try {
        	for (String requestStr: requestList) {

				// Prepare request
		        HttpRequest request = new HttpRequest();

		        // Retrieve integration settings
		        IntegrationSettings_SubmitApplication__c is = IntegrationSettings_SubmitApplication__c.getInstance();
		        
		        // Endpoint
		        String endpoint = '';
		        endpoint = is.Base_Url__c + is.Events_Path__c;
		        if(is.Enable_Mock__c) {
		            endpoint = is.Mock_Endpoint_Events__c;
		        }
		        request.setEndpoint(endpoint);
		        
		        // HTTP method
		        request.setMethod(is.Method__c);    
		        
		        // HTTP timeout
		        request.setTimeout(Integer.valueOf(is.Timeout__c));
		        
		        // HTTP headers
		        request.setHeader('Content-Type', is.Header_ContentType__c);
		        request.setHeader('client_id', is.Header_ClientId__c);
		        request.setHeader('client_secret', is.Header_ClientSecret__c);
		        
		        // HTTP body
		        request.setBody(requestStr);

		        // Send Request
		        Http httpClient = new Http();
		        HttpResponse resp = httpClient.send(request);

		        // Get request & response messages
		        Integer respStatusCode = 1;
		        String respStatus = '';
		        String respBody = '';
		        if(resp != null) {
		            respStatusCode = resp.getStatusCode();
		            respStatus = resp.getStatus();
		            respBody = resp.getBody();
		            System.debug('@@@@ StudFirst_SendDocumentsQueueable resp.getBody(): ' + resp.getBody());
		        }
		        //Show error if response is not 'OK'
		        if(resp != null && resp.getStatusCode() != 200) {
		        	ExLog.write('StudFirst_SendDocumentsQueueable','execute','Response not created. Response Status: ' + resp.getStatusCode(), 'Response Body: ' + resp.getBody());
		        }
		    }

		} catch (Exception ex) {
			ExLog.add('Error on Queueable Class', 'StudFirst_SendDocumentsQueueable', 'execute' , ex);
		}
	}
}