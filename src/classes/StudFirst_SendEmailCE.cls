/*******************************************************************************
* @author       Anterey Custodio
* @date         24.Jul.2017         
* @description  extension class for StudFirst_SendEmail global action page that
				lets CRM users send an email through the defined object
* @revision     
*******************************************************************************/
public with sharing class StudFirst_SendEmailCE {

	public Application_Course_Preference__c acpRecord {get;set;}
	public Case caseRec {get;set;}
	private final String admissionsRecTypeId = CommonServices.recordTypeId('Case', 'Admissions');
	public String toAddressesList {get;set;}
	private final Set<String> SRA_QUEUES = new Set<String>{'SRA_IA_MU_Docs', 'SRA_IA_MC_Docs', 'SRA_IA_IRS_Fees'};
	public String roleName {get;set;}
	public Boolean notifyAgent {get;set;}

	/*******************************************************************************
	* @author       Anterey Custodio
	* @date         24.Jul.2017        
	* @description  constructor used if it is called as a controller
	* @revision     
	*******************************************************************************/
	public StudFirst_SendEmailCE() {
		String acpId = ApexPages.currentPage().getParameters().get('id');
		if (acpId != null && acpId != '') {
			acpRecord = [  	SELECT 	Id, Application__c, 
										Application__r.Applicant__c, 
										Application__r.Applicants_Email__c,
										Agent_Email__c,
									(	SELECT	Id, Notify_Agent__c 
										FROM 	Cases__r 
										WHERE 	RecordTypeId =: admissionsRecTypeId
										ORDER BY CreatedDate DESC LIMIT 1 )
	                        FROM 	Application_Course_Preference__c 
	                        WHERE 	Id =: acpId ];
	        caseRec = new Case();
	        notifyAgent = false;

	        toAddressesList = acpRecord.Application__r.Applicants_Email__c;
	        if (acpRecord.Agent_Email__c != '' && acpRecord.Agent_Email__c != null) {
	        	notifyAgent = true;
	        	toAddressesList += ',' + acpRecord.Agent_Email__c;
	        }

	        if (!acpRecord.Cases__r.isEmpty()) {
	        	//if no emails have been sent for the latest enquiry OR if there is a draft email message in it, reuse it instead
	        	Boolean reuseCase = true;
	        	for (EmailMessage emlMsgRec: [	SELECT	Id, Status 
        										FROM 	EmailMessage 
        										WHERE	ParentId =: acpRecord.Cases__r[0].Id ]) {
	        		if (emlMsgRec.Status != '5') { //5 means draft
	        			reuseCase = false;
	        		}
	        	}

	        	if (reuseCase) {
	        		caseRec = acpRecord.Cases__r[0];
	        		caseRec.Notify_Agent__c = notifyAgent;
	        		caseRec.Status = 'Draft';
	        	}
	        }

	        //get running user's role name
	        List<UserRole> userRole = [	SELECT	Id, Name 
										FROM 	UserRole 
										WHERE 	Id =: userInfo.getUserRoleId() ];
			if (!userRole.isEmpty()) {
				roleName = userRole[0].Name;
			}
		} else {
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'There seems to be a problem with this page. Please contact the administrator.'));
			ExLog.add('Send Email Global Action', 'StudFirst_SendEmailCE.cls', 'StudFirst_SendEmailCE()' , 'Application Course Preference Id not found');
		}
	}

	/*******************************************************************************
	* @author       Anterey Custodio
	* @date         24.Jul.2017        
	* @description  only create a case when there aren't any
	* @revision     
	*******************************************************************************/
	public void createCaseIfNoneRelated() {
		try {
			//relate a case to it if it does not have an existing one
			if (caseRec.Id == null) {
				createCase();
			} else {
				update caseRec;
			}
		} catch (Exception ex) {
			ExLog.add('Error on updating/inserting Case: ', 'StudFirst_SendEmailCE', 'createCaseIfNoneRelated' , ex);
		}
	}

	/*******************************************************************************
	* @author       Anterey Custodio
	* @date         27.Jul.2017        
	* @description  creates a case record with admissions record type
	* @revision     
	*******************************************************************************/
	private void createCase() {
		caseRec = new Case();
		caseRec.Origin = 'Email - mu.documents';
		if (roleName == 'SRA Fees Mgr' || roleName == 'SRA Fees Staff') {
			caseRec.Origin = 'Email - irsfees';
		} else if (roleName == 'SRA Admissions Mgr (MC)' || roleName == 'SRA Admissions Staff (MC)') {
			caseRec.Origin = 'Email - mc.documents';
		}
		caseRec.ContactId = acpRecord.Application__r.Applicant__c;
		caseRec.Application_Course_Preference__c = acpRecord.Id;
		caseRec.RecordTypeId = admissionsRecTypeId;
		caseRec.Notify_Agent__c = notifyAgent;
		caseRec.Status = 'Draft';

		//Fetching the assignment rules on case
		AssignmentRule AR = new AssignmentRule();
		AR = [	SELECT 	Id 
				FROM 	AssignmentRule 
				WHERE 	SobjectType = 'Case' AND 
						Active = true 
				LIMIT 	1 ];

		//Creating the DMLOptions for "Assign using active assignment rules" checkbox
		Database.DMLOptions dmlOpts = new Database.DMLOptions();
		dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;

		//Setting the DMLOption on Case instance
		caseRec.setOptions(dmlOpts);
		insert caseRec;
	}
}