/*
    AUTHOR : Vineeth Batreddy
    DESCRIPTION : This class maintains the request data model for Application document.
*/

public class StudFirst_DocumentDTO {
	
    public Agent agent;
    public String externalApplicantId;
    public Document document;
    
    public class Agent {
    	public String agentPersonId;
	}

	public class Document {
        public String type;
        public String contentType;
        public String filename;
        public String binary;
        public String comments;
    }

}