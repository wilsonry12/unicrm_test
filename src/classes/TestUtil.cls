/**************************************************************************************************   
Apex Class Name      :  TestUtil
Version              :  1.0                                                                          
Created Date         :                                                              
Function             :  This is test class to create Data.
Story                :                                                                               
Work Task            :                                                                               
Modification Log     :                                                                               
*--------------------------------------------------------------------------------------------------  
    Developer                        Date                        Description
    Vaibhav B                    03/07/2017                        Created.
***************************************************************************************************/
@isTest
public class TestUtil
{
    public static void createArticles() {
        faq__kav faq=new faq__kav();
        faq.question__c='hi';
        faq.answer__c='there';
        faq.Title='How';
        faq.UrlName='test454365';
        insert faq;

        HR_FAQ__kav hrfaq=new HR_FAQ__kav ();
        hrfaq.question__c='hi';
        hrfaq.answer__c='there';
        hrfaq.Title='How';
        hrfaq.UrlName='test123';
        hrfaq.AutomaticallyPublishOnApproval__c=true;
        insert hrfaq;       
        faq=[select id,KnowledgeArticleId from faq__kav where id=:faq.Id];
        hrfaq=[select id,KnowledgeArticleId from HR_FAQ__kav where id=:hrfaq.Id];
        KbManagement.PublishingService.publishArticle(String.valueOf(faq.KnowledgeArticleId),true);
        KbManagement.PublishingService.publishArticle(String.valueOf(hrfaq.KnowledgeArticleId),true); 
    }
    public static void createCustomSettings() {
        CustomURLs__c customURL=new CustomURLs__c();
        customURL.EmailToCase__c='a@a.com';
        customURL.Exclude_Token__c='?tabset-fe996=2';
        customURL.Help_URL__c='http://login-qa.monash.edu/adfs/services/trust';
        customURL.PageSizeTopicDetail__c=3;
        customURL.Pagination_Size__c=3;
        customURL.SearchResultsPageSize__c=3;
        customURL.SSO_URL__c='http://login-qa.monash.edu/adfs/services/trust';
		customURL.Community_Home_Page__c ='www.homepageURL.com';
        insert customURL;
    }
    public static topic createTopic(String name) {
        topic t=new topic();
        t.name=name;
        t.description='xyz';
        insert t;
        return t;
    }
    public Static TopicAssignment createTopicAssignment(Id topicId,Id articleId) {
        TopicAssignment ta=new TopicAssignment();
        ta.TopicId=topicId;
        ta.EntityId=articleId;
        return ta;
    }
}