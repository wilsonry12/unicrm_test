/*******************************************************************************
* @author       Anterey Custodio
* @date         17.Nov.2016         
* @description  Batch class that deletes Flagged Campaigns
* @revision     
*******************************************************************************/
global class BMSDeleteFlaggedCampaign implements Database.Batchable<sObject> {
	
	String query;
	/*******************************************************************************
	* @author       Anterey Custodio
	* @date         17.Nov.2016         
	* @description  Constructor
	* @revision     
	*******************************************************************************/
	global BMSDeleteFlaggedCampaign() {
		
	}
	/*******************************************************************************
	* @author       Anterey Custodio
	* @date         17.Nov.2016         
	* @description  retrieve flagged campaigns
	* @revision     
	*******************************************************************************/
	global Database.QueryLocator start(Database.BatchableContext BC) {
		//query = 'SELECT Id FROM Campaign WHERE Status = \'Delete\'';
		return Database.getQueryLocator(query);
	}
	/*******************************************************************************
	* @author       Anterey Custodio
	* @date         17.Nov.2016         
	* @description  delete flagged campaigns
	* @revision     
	*******************************************************************************/
   	global void execute(Database.BatchableContext BC, List<Campaign> scope) {
		/*List<Campaign> campaignToDeleteList = new List<Campaign>();
		
		for (Campaign campaignRecord: scope) {
			campaignToDeleteList.add(campaignRecord);
		}

		if (!campaignToDeleteList.isEmpty()) {
			delete campaignToDeleteList;
		}*/
	}
	/*******************************************************************************
	* @author       Anterey Custodio
	* @date         17.Nov.2016         
	* @description  Finish
	* @revision     
	*******************************************************************************/
	global void finish(Database.BatchableContext BC) {
		
	}
	
}