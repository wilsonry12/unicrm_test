/*******************************************************************************
* @author       Ryan Wilson
* @date         29.Jun.2017       
* @description  Test class for Apply Selection rule batch
* @revision     
*******************************************************************************/
@isTest
private class ApplySelectionRule_Test {
	
	@isTest static void insertUpdateAssessmentRule_test() {
		RecordType assessRecType = [SELECT Id FROM RecordType WHERE Name ='Template'];
		
		Assessor_Group__c assessorGroup = StudFirst_TestHelper.createAssessorGroup();
		insert assessorGroup;

		Assessment_Plan__c assessmentPlan = StudFirst_TestHelper.createAssessmentPlan();
		assessmentPlan.RecordTypeId = assessRecType.Id;
		insert assessmentPlan;

		List<Course_Offering__c> courseOfferings = StudFirst_TestHelper.createCourseOfferingForAssessment();
		insert courseOfferings;

		Test.startTest();
		
		//Insert 2 overlaping Selection Rules
		Assessment_Selection_Rule__c selectionRule_A = StudFirst_TestHelper.createAssessmentSelectionRule(assessorGroup.Id, assessmentPlan.Id);
		selectionRule_A.Course_Filter__c = 'Include';
        selectionRule_A.Course_Codes__c = 'M00001';
        selectionRule_A.Location__c = 'CITY';
        selectionRule_A.Admission_Category__c = 'FULL-FEE';
        selectionRule_A.Attendance_Mode__c = 'IN';
		insert selectionRule_A;

		Assessment_Selection_Rule__c selectionRule_B = StudFirst_TestHelper.createAssessmentSelectionRule(assessorGroup.Id, assessmentPlan.Id);
		selectionRule_B.Course_Filter__c = 'Include';
        selectionRule_B.Course_Codes__c = 'M00001';
        selectionRule_B.Location__c = 'CITY';
        selectionRule_B.Admission_Category__c = 'FULL-FEE';
        selectionRule_B.Attendance_Mode__c = 'IN';
		insert selectionRule_B;

		selectionRule_B.Override__c = true;
		update selectionRule_B;
		//END --- Insert 2 overlaping Selection Rules

		//Insert new Rule using Exclude filter
		Assessment_Selection_Rule__c selectionRule_C = StudFirst_TestHelper.createAssessmentSelectionRule(assessorGroup.Id, assessmentPlan.Id);
		selectionRule_C.Course_Filter__c = 'Exclude';
        selectionRule_C.Course_Codes__c = 'M00001';
        selectionRule_C.Location__c = 'CITY';
        selectionRule_C.Admission_Category__c = 'FULL-FEE';
        selectionRule_C.Attendance_Mode__c = 'IN';
		insert selectionRule_C;
		//END --- Insert new Rule using Exclude filter

		Test.stopTest();
	}
	
	
	@isTest static void deactivateAssessmentRule_test() {
		RecordType assessRecType = [SELECT Id FROM RecordType WHERE Name ='Template'];
		
		Assessor_Group__c assessorGroup = StudFirst_TestHelper.createAssessorGroup();
		insert assessorGroup;

		Assessment_Plan__c assessmentPlan = StudFirst_TestHelper.createAssessmentPlan();
		assessmentPlan.RecordTypeId = assessRecType.Id;
		insert assessmentPlan;

		List<Course_Offering__c> courseOfferings = StudFirst_TestHelper.createCourseOfferingForAssessment();
		insert courseOfferings;

		Test.startTest();
		
		Assessment_Selection_Rule__c selectionRule_A = StudFirst_TestHelper.createAssessmentSelectionRule(assessorGroup.Id, assessmentPlan.Id);
		selectionRule_A.Course_Filter__c = 'Include';
        selectionRule_A.Course_Codes__c = 'M00001';
        selectionRule_A.Location__c = 'CITY';
        selectionRule_A.Admission_Category__c = 'FULL-FEE';
        selectionRule_A.Attendance_Mode__c = 'IN';
		insert selectionRule_A;

		selectionRule_A.Inactive__c = true;
		update selectionRule_A;

		Test.stopTest();
	}
	
}