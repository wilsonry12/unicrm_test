/**************************************************
* Author: Ryan Wilson
* Date: 29.AUG.2017
* Description: contains all the trigger handlers for Roles
***************************************************/

public without sharing class RoleAffiliationServices {
    /**************************************************
    * Author: Ryan Wilson
    * Date: 29.AUG.2017
    * Creates a platform event for an inserted/updated Role
    ***************************************************/
    public class createPlatformEvent implements Triggers.Handler {
        public void run() {
            Set<Id> roleIds = new Set<Id>();
            List<Role__e> roleEvents = new List<Role__e>();

            for(Roles__c role: (List<Roles__c>) Trigger.new){
                roleIds.add(role.Id);
            }

            if(CommonUtilities.allowPublishEvent()){
                for(Roles__c role: CommonUtilities.getRoles(roleIds)){
                    String eventType = (Trigger.isInsert || Trigger.isUpdate)? 'upsert': 'delete';
    				roleEvents.add(CommonUtilities.createRoleEvent(role, eventType));
    			}

                if(roleEvents.size() >0){
                    EventBus.publish(roleEvents);
                }
            }
        }
    }
}