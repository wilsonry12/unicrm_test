/*******************************************************************************
* @author       Anterey Custodio
* @date         24.Aug.2017        
* @description  Class that's called on Process Builder to put the email content
					on the Summary field on the parent Case
* @revision     
*******************************************************************************/
public with sharing class StudFirst_UpdateCaseSummaryPBC {

	@InvocableMethod(label='Update Case Summary' description='Copy the email details and put it on the Parent.Summary Field')
	public static void updateCaseSummary (List<String> emailMsgIds) {
		
		Map<String, EmailMessage> caseMap = new Map<String, EmailMessage>();

		for (EmailMessage emlMsg: [	SELECT 	Id, ParentId,
											Subject,
											TextBody,
											FromAddress,
											ToAddress,
											CcAddress,
											MessageDate
									FROM 	EmailMessage
									WHERE 	Id IN: emailMsgIds ]) {
			caseMap.put(emlMsg.ParentId, emlMsg);
		}

		List<Case> caseToUpdateList = new List<Case>();

		for (Case caseRec: [SELECT Id, Summary__c 
							FROM Case 
							WHERE Id IN: caseMap.KeySet() ]) {

			EmailMessage emlMsg = caseMap.get(caseRec.Id);
			String summaryBody = 'From: ' + emlMsg.FromAddress + '\n';
			summaryBody += 'Sent: ' + emlMsg.MessageDate.format() + '\n';
			summaryBody += 'To: ' + emlMsg.ToAddress + '\n';
			if (emlMsg.CcAddress != null) {
				summaryBody += 'CC: ' + emlMsg.CcAddress + '\n';
			}
			if (emlMsg.Subject != null) {
				summaryBody += 'Subject: ' + emlMsg.Subject + '\n';
			}
			summaryBody += '\n' + emlMsg.TextBody;
			caseRec.Summary__c = summaryBody;
			caseToUpdateList.add(caseRec);
		}

		if (!caseToUpdateList.isEmpty()) {
			update caseToUpdateList;
		}
	}
}