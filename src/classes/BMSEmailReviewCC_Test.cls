/*******************************************************************************
* @author       Anterey Custodio
* @date         21.Nov.2016         
* @description  test class for BMSEmailReviewCC
* @revision     
*******************************************************************************/
@isTest
public class BMSEmailReviewCC_Test {
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         21.Nov.2016          
    * @description  Page Navigation: idle view - user just accessing the page
    * @revision     
    *******************************************************************************/
    static testMethod void test_ApproveAll() {
        //insert sample Contacts
        String KnownToMonashRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Known to Monash systems').getRecordTypeId();
        List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
        insert contactRecordList;

        List<Contact> changeContactRecordType = new List<Contact>();
        for (Contact contactRecord: [SELECT Id, RecordTypeId FROM Contact WHERE Id IN: contactRecordList]) {
            contactRecord.RecordTypeId = KnownToMonashRecTypeId;
            changeContactRecordType.add(contactRecord);
        }

        if (!changeContactRecordType.isEmpty()) {
            update changeContactRecordType;
        }
        
        Campaign crmMassEmailRecord = BMS_TestDataHelper.buildSampleCRMMassEmail('Student Communication');
        crmMassEmailRecord.Status = 'On Hold';
        insert crmMassEmailRecord;
        
        Test.startTest();
            //user goes to URL
            PageReference pageRef = Page.BulkMessagingSystem;
            Test.setCurrentPage(pageRef);
            //page calls the controller
            BMSEmailReviewCC controller = new BMSEmailReviewCC();
            system.assert(!controller.wMassEmailList.isEmpty());
            //auto populate picklist
            controller.getStatusOptions();
            //checks the email first
            controller.selectedMassEmailId = crmMassEmailRecord.Id;
            controller.selectedEmailTempId = 'Student Communication';
            controller.previewEmailRecord();
            //approves all
            controller.approveAllEmails();
            //submit
            controller.updateCRMMassEmailStatus();
            
        Test.stopTest();
    }
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         21.Nov.2016          
    * @description  Page Navigation: idle view - user just accessing the page
    * @revision     
    *******************************************************************************/
    static testMethod void test_CancelAll() {
        //insert sample Contacts
        String KnownToMonashRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Known to Monash systems').getRecordTypeId();
        List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
        insert contactRecordList;

        List<Contact> changeContactRecordType = new List<Contact>();
        for (Contact contactRecord: [SELECT Id, RecordTypeId FROM Contact WHERE Id IN: contactRecordList]) {
            contactRecord.RecordTypeId = KnownToMonashRecTypeId;
            changeContactRecordType.add(contactRecord);
        }

        if (!changeContactRecordType.isEmpty()) {
            update changeContactRecordType;
        }
        
        Campaign crmMassEmailRecord = BMS_TestDataHelper.buildSampleCRMMassEmail('Student Communication');
        crmMassEmailRecord.Status = 'On Hold';
        insert crmMassEmailRecord;
        
        Test.startTest();
            //user goes to URL
            PageReference pageRef = Page.BulkMessagingSystem;
            Test.setCurrentPage(pageRef);
            //page calls the controller
            BMSEmailReviewCC controller = new BMSEmailReviewCC();
            system.assert(!controller.wMassEmailList.isEmpty());
            //cancels all
            controller.cancelAllEmails();
            //submit
            controller.updateCRMMassEmailStatus();
        Test.stopTest();
    }
}