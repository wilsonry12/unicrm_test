/*******************************************************************************
* @author       Ryan Wilson
* @date         08.Dec.2016         
* @description  Controller class for uploading CSV for contact searching 
                using unique id
* @revision     
*******************************************************************************/
public class BMSCSVSearch {
    public Blob contentFile {get;set;}
    public String fileName {get;set;}
    
    public Boolean bShowDrafEmail {get;set;}
    public Boolean displayEmailPanel {get;set;}
    public String newCampaignNameStr {get;set;}
    public String errorMessage {get;set;}
    public String selectedEmailId {get;set;}

    public Campaign massEmailRecord {get;set;}
    public Attachment attachmentRec {get;set;}
    public Map<String, Document> csvTemplateMap {get;set;}
    public String selectedCSVOption {get;set;}

    //private Map<String, String> emailCampaignMap {get;set;} ---future use
    private final Set<String> emailStatusSet = new Set<String> {'On Hold', 'In Progress'};

    public Boolean isPersonId {get;set;}
    public String sendType {get;set;}

    public BMSCSVSearch() {
        massEmailRecord = new Campaign();
        attachmentRec = new Attachment();
        bShowDrafEmail = false;
        newCampaignNameStr = '';
        errorMessage = '';
        selectedEmailId = 'New'; //default to always new for now
        displayEmailPanel = false;
        sendType = CommonUtilities.retrieveSendType();
        //numberOfContacts = 0;
        //populateEmailList();

        //default From and Reply To field
        massEmailRecord.From__c = 'noreply@f.e.monash.edu';
        massEmailRecord.Reply_To__c = userInfo.getUserEmail();

        csvTemplateMap = new Map<String, Document>();
        for (Document csvDocuments: [SELECT Id, Name FROM Document WHERE Folder.Name = 'BMSLogos' AND (Name = 'PersonId_CSV' OR Name = 'NameAndEmail_CSV' OR Name = 'MobilePhone_CSV') LIMIT 3]) {
            csvTemplateMap.put(csvDocuments.Name, csvDocuments);
        }

        checkIfUploadingPersonId();
    }

    /*******************************************************************************
    * @author      Anterey Custodio
    * @date        02.Mar.2017       
    * @description  check if the user wants to upload Person Id or Email Address
    * @revision    Keep. Future use 
    *******************************************************************************/
    public void checkIfUploadingPersonId() {
        isPersonId = false;
        selectedCSVOption = 'EMAIL';

        if (ApexPages.currentPage().getUrl().toLowerCase().contains('bmsuploadids')) {
            selectedCSVOption = 'PERSONID';
            isPersonId = true;
        }
    }

    

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         08.Dec.2016         
    * @description  method to redirect to email draft page
    * @revision     
    *******************************************************************************/
    public PageReference draftEmail() {
        PageReference redirect = null;
        errorMessage = '';
        
        //validate the campaign name if null
        errorMessage += insertCampaignRecordIfNew();
        
        //create a savepoint
        SavePoint savePointDML = Database.setSavepoint();
        try {
            //validate the csv file
            errorMessage += ReadFile();

            if (massEmailRecord.Additional_Email__c != null && massEmailRecord.Additional_Email__c != '') {
                massEmailRecord.Additional_Email__c = formatAdditionalEmails(massEmailRecord.Additional_Email__c);
            }
            if (massEmailRecord.Notify__c != null && massEmailRecord.Notify__c != '') {
                massEmailRecord.Notify__c = formatAdditionalEmails(massEmailRecord.Notify__c);
            }
            
            //validate the crm mass email record
            errorMessage += validateCRMMassEmailRecord(massEmailRecord);

            if (errorMessage == '') {
                upsert massEmailRecord;
                CommonUtilities.assignCampaignMemberStatus(massEmailRecord.Id);

                redirect = Page.BMSEditEmailBody;
                redirect.getParameters().put('crmId',massEmailRecord.Id);
                redirect.getParameters().put('type',sendType);
                
                if(sendType == 'email'){
                    BMSInsertCampaignMemberCSVQueueable campaignMemberJob = new BMSInsertCampaignMemberCSVQueueable(attachmentRec.Id, massEmailRecord.Id, true);
                    if (selectedCSVOption == 'EMAIL') {
                        campaignMemberJob = new BMSInsertCampaignMemberCSVQueueable(attachmentRec.Id, massEmailRecord.Id, false);
                    }

                    Id jobID = System.enqueueJob(campaignMemberJob);
                }
                
            } else {
                System.debug('@@@errorMessage: '+errorMessage);
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage);
                ApexPages.addMessage(myMsg);
            }
        } catch (Exception ex) {
            //rollback the insertion
            Database.rollback(savePointDML);
            errorMessage = 'There seems to be a problem on the page. Please contact the administrator.';
            system.debug('### ex: '+ ex + ' ' + ex.getLineNumber());
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage);
            ApexPages.addMessage(myMsg);
        }

        return redirect;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         08.Dec.2016         
    * @description  method to split additionalemails and format it
    * @revision     
    *******************************************************************************/
    private String formatAdditionalEmails (String additionalEmailsStr) {
        String trimmedEmailStr = additionalEmailsStr.trim();
        String additionalEmailList = '';
        List<String> splitValuesList = new List<String>();
    
        //check first if user entered a list of emails
        if (trimmedEmailStr.contains(';')){
            //remove spaces before splitting
            trimmedEmailStr = trimmedEmailStr.replaceAll('(\\s+)', '');
            //split the values to get the list of emails
            splitValuesList = trimmedEmailStr.split(';');
            //add to email list
            additionalEmailList = retrieveAdditionalEmailList(splitValuesList);

        } else if (trimmedEmailStr.contains(' ') || trimmedEmailStr.contains('\n')){
            //split the values to get the list of emails
            splitValuesList = (trimmedEmailStr.contains('\n'))? trimmedEmailStr.split('\n'): trimmedEmailStr.split(' ');
            List<String> trimmedStringList = new List<String>();
            //add to email list
            for (String trimmedString: splitValuesList) {
                trimmedStringList.add(trimmedString.replaceAll('(\\s+)', ''));
            }
            additionalEmailList = retrieveAdditionalEmailList(trimmedStringList);
        } else { 
            if (validateEmail(trimmedEmailStr)) {
                //add the email address
                additionalEmailList = trimmedEmailStr;
            } else {
                errorMessage = '- You have entered an invalid email from Additional/Notify emails. <br/>';
            }
        }
        return additionalEmailList;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         08.Dec.2016         
    * @description  method to put the string of emails to a list
    *               TODO - can possibly be moved to a 'Services' Class
    * @revision     
    *******************************************************************************/
    private String retrieveAdditionalEmailList(List<String> splitValuesList) {
        
        String additionalEmailList = '';
        for(Integer i=0; i<splitValuesList.size(); i++) {
            //validate if the emails are correct
            if (validateEmail(splitValuesList[i])) {
                additionalEmailList += splitValuesList[i];
                if (i != splitValuesList.size()-1) {
                    additionalEmailList += ';';
                }
            } else {
                errorMessage = '- You have entered an invalid email from the list of Additional/Notify emails.';
            }
        }
        return additionalEmailList;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         08.Dec.2016         
    * @description  method to validate email address
    *               TODO - can possibly be moved to a 'Services' Class
    * @revision     
    *******************************************************************************/
    private Boolean validateEmail(String email) {
        Boolean isEmailValid = true;
        
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern emailPattern = Pattern.compile(emailRegex);
        Matcher emailMatcher = emailPattern.matcher(email);

        //returns false if email is invalid
        if (!emailMatcher.matches()) {
            isEmailValid = false;
        }

        return isEmailValid;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         11.Jan.2017         
    * @description  method to save the csv as attachment
    * @revision     
    *******************************************************************************/
    private String ReadFile(){
        String attachError = '';
        if((selectedCSVOption == 'PERSONID' || selectedCSVOption == 'EMAIL') && contentFile != null) {
            if(!fileName.contains('csv')) {
                attachError = 'Accepts CSV file only. Please check the file and try again. <br/>';
            } else {
                if (newCampaignNameStr != '') {
                    massEmailRecord.Name = newCampaignNameStr;
                    insert massEmailRecord;

                    attachmentRec.Body = contentFile;
                    attachmentRec.Name = fileName;
                    attachmentRec.ParentId = massEmailRecord.Id; 
                    insert attachmentRec;

                    bShowDrafEmail = true;
                } else{
                    attachError = 'Subject should not be empty <br/>';
                }
            }
        } else if (selectedCSVOption == null) {
            attachError = 'Please select which upload method to use. <br/>';
        } else {
            attachError = 'No file has been selected. Please select a file to continue. <br/>';
        }
        return attachError; 
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         08.Dec.2016         
    * @description  Method to validate the CRM Mass Email Record
    * @revision     
    *******************************************************************************/
    private String validateCRMMassEmailRecord (Campaign crmMassEmailToValidate) {
        String massEmailError = '';
        
        if (crmMassEmailToValidate.From__c == '') {
            massEmailError += '- From address should not be empty <br/>';
        } else if (!validateEmail(crmMassEmailToValidate.From__c)) {
            massEmailError += '- Please enter valid From address <br/>';
        }
        
        if (crmMassEmailToValidate.Reply_To__c == '') {
            massEmailError += '- Reply To address should not be empty <br/>';
        } else if (!validateEmail(crmMassEmailToValidate.Reply_To__c)) {
            massEmailError += '- Please enter valid Reply To address <br/>';
        }
        
        return massEmailError;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         8.Dec.2016         
    * @description  private method to insert a Campaign
    * @revision     
    *******************************************************************************/
    private String insertCampaignRecordIfNew() {
        String errorMessage = '';
        if (newCampaignNameStr != '') {
            Id BMSRecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('BMS').getRecordTypeId();
            massEmailRecord.Name = newCampaignNameStr;
            massEmailRecord.Status = 'In Progress';
            massEmailRecord.RecordTypeId = BMSRecordTypeId;
            massEmailRecord.IsActive = true;
        }
        else {
            errorMessage = 'Message subject should not be empty <br/>';
        }
        return errorMessage;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         8.Feb.2017     
    * @description  returns types of CSV formats they can use in adding recipients
    * @revision     
    *******************************************************************************/
    public List<SelectOption> getCSVOptions() {
        List<SelectOption> csvOptions = new List<SelectOption>();
        csvOptions.add(new SelectOption('PERSONID', 'Add recipients using Person Ids'));
        if (System.Label.BMS_AllowSendUsingEmailCSV == 'true') {
            csvOptions.add(new SelectOption('EMAIL', 'Add recipients using Email Addresses'));
        }
 
        return csvOptions;
    }

}