/**
* @author           Nick Guia
* @date             07/08/2017
* @description      - Apex Controller for BMSNewEmailPageV2 page
**/
global with sharing class BMSNewEmailPageV2CC {

    public static Blob contentFile {get;set;}
    public static String fileName {get;set;}
    public static Map<String, Document> csvTemplateMap {get;set;}

    public String filterFieldOptionWrapper {
        get { return JSON.serialize(BMSUtils.getFilterFieldOptionWrapper()); }
    }

    public String getAllStaffOptionWrapper {
        get { return JSON.serialize(BMSUtils.getAllStaffOptionWrapper()); }
    }

    public String uOfferingFieldOptionWrapper {
        get { return JSON.serialize(BMSUtils.getUOfferingOptionWrapper()); }
    }

    public List<SelectOption> cohortOptions {
        get { return BMSUtils.getCohortOptions(); }
    }

    public List<SelectOption> emailTemplates {
        get { 
            return BMSUtils.getEmailOptions(); }
    }

    public Boolean isAdmin {
        get { return BMSUtils.checkIfAdmin(UserInfo.getProfileId()); }
    }

    private final transient Id knownToMonashRecTypeId = Schema.SObjectType.Contact
                                                            .getRecordTypeInfosByName()
                                                            .get('Known to Monash systems')
                                                            .getRecordTypeId();

    /**
    * @description class contructor
    **/
    public BMSNewEmailPageV2CC() {
        csvTemplateMap = new Map<String, Document>();
        for (Document csvDocuments : [SELECT Id, Name 
                                        FROM Document 
                                        WHERE Folder.Name = 'BMSLogos' 
                                        AND (Name = 'PersonId_CSV' OR Name = 'NameAndEmail_CSV' OR Name = 'MobilePhone_CSV') 
                                        LIMIT 3]) 
        {
            csvTemplateMap.put(csvDocuments.Name, csvDocuments);
        }
    }

    /**
    * @author       Nick Guia
    * @date         02/08/2017
    * @description  - Remote Action method for fetching picklist values of a field
    **/
    @RemoteAction
    global static List<String> getPickListValues(String sObjectName, String fieldName) {
        return BMSUtils.getPickListValues(Schema.getGlobalDescribe().get(sObjectName), fieldName);
    }

    /**
    * @author       Nick Guia
    * @date         11/08/2017
    * @description  remote action method for searching records based on defined filters
    * @param        sJSON : JSON string value of filter wrapper
    * @return       JSON value of wrapper class
    **/
    @RemoteAction
    global static String searchRecords(String sJSON) {
        List<SObject> sObjList;
        BMSUtils.PreviewTableWrapper ptw;
        try {
            BMSUtils.QueryWrapper qw = buildSOQL(sJSON);
            //count actual result without limiting query
            Integer count = Database.countQuery(
                                String.format(
                                    'SELECT COUNT() ' +
                                    'FROM {0} {1}',
                                new List<String> {
                                    qw.obj,
                                    qw.filter
                                }));
            System.debug('*** count : ' + count);
            count = (count != null) ? count : 0;

            //store final query string for batch job to process
            String finalQueryString = String.format(
                                        'SELECT {0} ' +
                                        'FROM {1} {2}',
                                    new List<String> {
                                        String.join(qw.fields, ','),
                                        qw.obj,
                                        qw.filter
                                    });
            System.debug('finalQueryString : ' + finalQueryString);
            //add limit based on configuration in BMS Util
            qw.filter += ' LIMIT ' + BMSUtils.BMS_NEW_EMAIL_SEARCH_LIMIT;
            sObjList = (List<SObject>) Database.query(
                                            String.format(
                                                'SELECT {0} ' +
                                                'FROM {1} {2}',
                                            new List<String> {
                                                String.join(qw.fields, ','),
                                                qw.obj,
                                                qw.filter
                                            }));
            ptw = new BMSUtils.PreviewTableWrapper(sObjList, count, finalQueryString);
        } catch(Exception e) {
            //TO DO: INSERT EXCEPTION LOGS
            System.debug('*** an error occurred while trying to search for records : ' + e);
        } 
        System.debug('*** PreviewTableWrapper : ' + ptw);
        return JSON.serialize(ptw);
    }

    /**
    * @author       Nick Guia
    * @date         15/08/2017
    * @description  remote action method for building html of preview path
    * @param        payload from page. structure is based on BMSUtils.BuildPreviewEmailWrapper
    * @return       String value of html for preview
    **/
    @RemoteAction
    global static String buildPreview(String sJSON) {
        BMSUtils.BuildPreviewEmailWrapper bpew = 
            (BMSUtils.BuildPreviewEmailWrapper) JSON.deserializeStrict(sJSON, BMSUtils.BuildPreviewEmailWrapper.class);
        String htmlBody = constructHTMLBody(bpew);
        return htmlBody;
    }

    /**
    * @author       Nick Guia
    * @date         15/08/2017
    * @description  remote action method to sync campaign record details from the page
    * @param        payload : payload from page. structure is based on BMSUtils.NewEmailSaveWrapper
    *               isEdit : Boolean to check if this is for updating campaign
    * @return       dummy return value for our page to know if transaction is successful
    **/
    @RemoteAction
    global static Boolean saveCampaign(String sJSON, Boolean isEdit) {
        Boolean returnVal = true;
        try {
            BMSUtils.NewEmailSaveWrapper nesw = (BMSUtils.NewEmailSaveWrapper) JSON.deserializeStrict(sJSON, BMSUtils.NewEmailSaveWrapper.class);
            System.debug('*** nesw fileUpload : ' + nesw.fileUpload);
            Campaign massEmailRecord = nesw.massEmailRecord;

            //assign initial campaign details
            massEmailRecord.Type = 'Email';
            massEmailRecord.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('BMS').getRecordTypeId();
            massEmailRecord.IsActive = true;
            massEmailRecord.From__c = UserInfo.getUserEmail();
            massEmailRecord.HTML_Email__c = customiseMessage(massEmailRecord.Custom_Message__c, 
                                                                massEmailRecord.Add_CRICOS__c, 
                                                                massEmailRecord.Add_Base_Signature__c);

            ////process additional emails
            if (massEmailRecord.Additional_Email__c != null && massEmailRecord.Additional_Email__c != '') {
                massEmailRecord.Additional_Email__c = formatAdditionalEmails(massEmailRecord.Additional_Email__c);
                System.debug('Additional_Email__c emails : ' + massEmailRecord.Additional_Email__c);
            }
            if (massEmailRecord.Notify__c != null && massEmailRecord.Notify__c != '') {
                massEmailRecord.Notify__c = formatAdditionalEmails(massEmailRecord.Notify__c);
                System.debug('Notify__c emails : ' + massEmailRecord.Notify__c);
            }
            if(!isEdit) massEmailRecord.Status = 'On Hold'; //only assign status on insert

            System.debug('***massEmailRecord : ' + massEmailRecord);
            upsert massEmailRecord;

            //Only process audience on insert
            if(!isEdit) {
                CommonUtilities.assignCampaignMemberStatus(massEmailRecord.Id);

                //execute batch jobs
                if (nesw.audienceType == 'search') {
                    BMSInsertCampaignMemberQueueable campaignMemberJob = 
                        new BMSInsertCampaignMemberQueueable(nesw.finalQueryString, massEmailRecord.Id, null);
                    Id jobID = System.enqueueJob(campaignMemberJob);
                } else if(nesw.audienceType == 'upload') {
                    //PROCESS ATTACHMENT
                    if(nesw.fileUpload != null) {
                        System.debug('processing attachment');
                        Attachment attachmentRec = new Attachment();
                        attachmentRec.Body = nesw.fileUpload;
                        attachmentRec.Name = nesw.fileName;
                        attachmentRec.ParentId = massEmailRecord.Id;
                        insert attachmentRec;

                        Boolean isPersonId = (nesw.attachmentContent == 'Person IDs') ? true : false;
                        BMSInsertCampaignMemberCSVQueueable campaignMemberJob = 
                            new BMSInsertCampaignMemberCSVQueueable(attachmentRec.Id, massEmailRecord.Id, isPersonId);
                        Id jobID = System.enqueueJob(campaignMemberJob);
                        System.debug('BMSInsertCampaignMemberCSVQueueable has been enqueued');
                    } else {
                        System.debug('No Attachment found. ContentFile is null');
                    }
                }
            }
        } catch(Exception e) {
            System.debug('An error occurred while saving campaign : ' + e);
            returnVal = false;
        }
        return returnVal;
    }

    /**
    * @author       Nick Guia
    * @date         28/08/2017
    * @description  remote action method for retrieving campaign details and prepopulating
    *               fields for edit
    **/
    @RemoteAction
    global static String retrieveCampaignDetails(String recId) {
        Campaign cmp = [SELECT 
                            Id, Email_Template__c, Name, Message_Header__c, Custom_Message__c,
                            Reply_To__c, Add_CRICOS__c, Add_Base_Signature__c, Additional_Email__c,
                            Notify__c, HTML_Email__c
                        FROM Campaign
                        WHERE Id = :recId
                        LIMIT 1];
        System.debug('cmp : ' + cmp);
        return JSON.serialize(cmp);
    }

    /**
    * @author       Nick Guia
    * @date         11/08/2017
    * @description  method for processing payload and building SOQL query
    * @param        sJSON : payload from VF Remoting
    **/
    private static BMSUtils.QueryWrapper buildSOQL(String sJSON) {
        
        BMSUtils.FilterWrapper fw = 
            (BMSUtils.FilterWrapper) JSON.deserialize(sJSON, BMSUtils.FilterWrapper.class); //deserialize payload
        Map<Integer, String> tokenizedFilter = buildTokenizedFilter(fw.filters);            //tokenize filters into integers and filter string
        String filter = applyFilterLogic(tokenizedFilter, fw.filterLogic);                  //apply advance filter logic i.e. 1 AND 2 OR (3 AND 4)

        List<String> fields;
        String sObj = '';

        if(fw.cohort == 'Unit Offerings') {
            fields = getUnitOfferingFields();                           //get unit enrolment fields
            sObj = 'Unit_Enrolment__c';
            if(!String.isBlank(filter)) filter = 'WHERE ' + filter;
        } else {
            //for including/excluding inactive students
            String isEnrolled = '(Number_of_Enrolled_Units__c > 0 OR Course_Attempt_Status__c != \'\')';
            //if the checkbox is not ticked, only query students that are enrolled 
            if (!fw.isIncludeInactive) {
                isEnrolled = '(Number_of_Enrolled_Units__c > 0 OR Course_Attempt_Status__c = \'ENROLLED\')';
            }
            
            String contactCohortFilter = searchByContactCohort(fw.cohort); //contact cohort filter

            if(fw.cohort == 'All Staff') {
                fields = getAllStaffFields();   //get staff fields
                isEnrolled = '';                //do not include this filter for all staff. replace with empty string
                sObj = 'Contact';
            } else {
                fields = getContactFields();    //get student fields
                sObj = 'Contact';
            }

            /**collate all filters - based on the assumption that cohort filter will never be empty
               if requirement changes, this needs to be changed
            **/
            filter = ' WHERE ' + contactCohortFilter + appendFilter(isEnrolled) + appendFilter(filter);
        }
        //build wrapper class for the soql string sequence so that this logic can be re-used
        return new BMSUtils.QueryWrapper(fields, sObj, filter);
    }

    /**
    * @author       Nick Guia
    * @date         11/08/2017
    * @description  method for tokenizing filters to key pair values.
    * @param        filters : list of filters indicated by user on page
    **/
    private static Map<Integer, String> buildTokenizedFilter(List<BMSUtils.FilterRowWrapper> filters) {
        Map<Integer, String> returnVal = new Map<Integer, String>();
        Integer ctr = 1;    //id counter
        for(BMSUtils.FilterRowWrapper frw : filters) {
            if(frw.field != BMSUtils.NONE_STR) {
                String filter = '';
                filter += frw.field + ' ';                                          //field
                String operator = BMSUtils.soqlOperators.get(frw.criteria);         //operator
                filter +=  operator + ' ';

                if(frw.criteria == 'contains (separate by comma or space)') {       //split by comma or space
                    List<String> filterVals = new List<String>();
                    for(String split : frw.value.split('[\\s,]')) {
                        if(split != '') filterVals.add('\''+ split + '\'');         //enclose each value with single quotes
                                                
                    }
                    System.debug('***filterVals : ' + filterVals);
                    filter += filterVals;
                } else if(BMSUtils.listTypeOperators.contains(operator) ) {         //check if operator is for type list
                    List<String> filterVals = new List<String>();
                    for(String split : frw.value.split(';')) {                      //split by semi colon
                        filterVals.add('\''+ split + '\'');                         //enclose each value with single quotes
                    }
                    System.debug('***filterVals : ' + filterVals);
                    filter += filterVals;
                } else {                                               
                    try {
                        Date convertDate = Date.valueOf(frw.value);                    //check if filter is a date
                        System.debug('filter is a date : ' + filter);
                        filter += frw.value;                                           //filter is a date, do not append single quotes on query
                    } catch(Exception e) {
                        System.debug('unable to parse string to date : ' + e);
                        filter += '\''+ frw.value + '\'';                           //filter is not a date, append single quotes on query
                        System.debug('filter is not a date : ' + filter);
                    }
                }
                returnVal.put(ctr, filter);
                ctr++;
            }
        }
        System.debug('***buildTokenizedFilter-returnVal : ' + returnVal);
        return returnVal;
    }

    /**
    * @author       Nick Guia
    * @date         11/08/2017
    * @description  method for apply advanced filter logic indicated by user.
    *               Concatenates all filters with 'AND' operator otherwise.
    * @param        tokenized : tokenized filter <order, filterString>
    *               filterLogic : advanced filter logic i.e. (1 AND 2) OR 3
    **/
    private static String applyFilterLogic(Map<Integer, String> tokenized, String filterLogic) {
        String returnVal = '';
        if(!tokenized.isEmpty()) {
            if(!String.isBlank(filterLogic)) { //if filter logic provided
                for(String logic : filterLogic.split('')) {
                    if(logic.isNumeric()) {
                        Integer num = Integer.valueOf(logic);
                        if(tokenized.containsKey(num)) {
                            returnVal += ' ' + tokenized.get(num) + ' ';
                        }
                    } else {
                        returnVal += logic;
                    }
                }
            } else {    //concatenate filters with AND
                for(Integer i : tokenized.keySet()) {
                    returnVal += tokenized.get(i);
                    //do not add AND if last token
                    if(i != tokenized.size()) {
                        returnVal += 'AND ';
                    }
                }
            }
        }
        System.debug('*** applyFilterLogic-returnVal : ' + returnVal);
        return returnVal;
    }

    /**
    * @author       Anterey Custodio
    * @date         17.Oct.2016
    * @description  method that search the student by Cohort
    **/
    private static String searchByContactCohort(String cohort) {

        String cohortQueryStr = '';
        
        //only add to the query if the selected value is not 'All students'
        if ( cohort== 'All Students') {
            cohortQueryStr = cohortQueryStr + '( Course_Type_Group__c != null )';

        } else if (cohort == 'All HDR Students') {
            cohortQueryStr = cohortQueryStr + '( Course_Type_Group__c = \'HDR\' )';

        } else if (cohort == 'All Students except HDR') {
            cohortQueryStr = cohortQueryStr + '( Course_Type_Group__c != \'HDR\' AND Course_Type_Group__c != \'\' )';

        } else if (cohort == 'All Postgraduate Students') {
            cohortQueryStr = cohortQueryStr + '( Course_Type_Group__c = \'Postgraduate\' )';

        } else if (cohort == 'All Undergraduate Students') {
            cohortQueryStr = cohortQueryStr + '( Course_Type_Group__c = \'Undergraduate\' OR Course_Type_Group__c = \'Undergraduate\')';
        
        } else if (cohort == 'All Staff') {
            cohortQueryStr = cohortQueryStr + '( SAP_Staff_Id__c != null )';
        }
        
        return cohortQueryStr;
    }

    /**
    * @author       Nick Guia
    * @date         11/08/2017
    * @description  returns contact fields available for display
    **/
    private static List<String> getContactFields() {
        return new List<String> {
            'Id', 'Name', 'Monash_Email_Address__c', 'Course_Type_Group__c',
            'Person_ID_unique__c', 'Course_Code__c', 'Course_Name__c', 'Campus__c',
            'Commencement_Date__c', 'Faculty__c', 'MobilePhone'
        };
    }

    /**
    * @author       Nick Guia
    * @date         11/08/2017
    * @description  returns unit enrolment fields available for display
    **/
    private static List<String> getUnitOfferingFields() {
        return new List<String> {
            'Id', 'Name', 'ACAD_YR__c', 'CAL_TYPE__c', 'Student__c', 
            'Student__r.Person_ID_unique__c', 'Student__r.Name',
            'SUA_LOCATION_CD__c', 'UNIT_CLASS__c', 'UNIT_MODE__c', 'Status__c', 'UNIT_CD__c'
        };
    }

    /**
    * @author       Nick Guia
    * @date         11/08/2017
    * @description  returns staff contact fields available for display
    **/
    private static List<String> getAllStaffFields() {
        return new List<String> {
            'Id', 'Name', 'Title', 'SAP_Staff_Id__c', 'Email', 'Personnel_Area__c', 
            'Personnel_Sub_Area__c', 'Faculty_Portfolio__c', 'Organisation_Unit__c',
            'Contract_Type__c', 'School_Division_Name__c', 'MobilePhone'
        };
    }

    /**
    * @author       Nick Guia
    * @date         14/08/2017
    * @description  appends AND to filter if not blank.
    **/
    private static String appendFilter(String filter) {
        if(!String.isBlank(filter)) {
            filter = ' AND ' + filter;
        }
        return filter;
    }

    /** REUSE ORIGINAL CODES **/

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         19.Jan.2017         
    * @description  Method to populate the html string with styles
    * @revision     ACUSTODIO, 6.Feb.2017 - added styles to <li> tags
    *******************************************************************************/
    private static String addCSSStyles(String strEmailBody) {
        String updatedString = strEmailBody;
        updatedString = updatedString.replace('<h1>', '<h1 style=\"font: Bold 16pt Arial; color: rgb(0,108,171); margin: 5pt 0pt 11pt 0pt;\">');
        updatedString = updatedString.replace('<h2>', '<h2 style=\"font: Bold 14pt Arial; margin: 5pt 0pt 11pt 0pt;\">');
        updatedString = updatedString.replace('<h3>', '<h3 style=\"font: Bold 12pt Arial; margin: 5pt 0pt 11pt 0pt;\">');
        updatedString = updatedString.replace('<p>', '<p style=\"font: 11pt Arial; margin: 5pt 0pt 11pt 0pt;\">');
        updatedString = updatedString.replace('<li>', '<li style=\"padding-bottom: 4pt; font: 11pt Arial;\">');

        //remove existing style before adding the same style
        updatedString = updatedString.replace('<td style=\"vertical-align: top; padding: 8px; border: 1pt solid black\"', '<td ');
        updatedString = updatedString.replace('<td ', '<td style=\"vertical-align: top; padding: 8px; border: 1pt solid black\"');
        return updatedString;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         08.Dec.2016         
    * @description  method to split additionalemails and format it
    * @revision     
    *******************************************************************************/
    private static String formatAdditionalEmails (String additionalEmailsStr) {
        String trimmedEmailStr = additionalEmailsStr.trim();
        String additionalEmailList = '';
        List<String> splitValuesList = new List<String>();
    
        //check first if user entered a list of emails
        if (trimmedEmailStr.contains(';')){
            //remove spaces before splitting
            trimmedEmailStr = trimmedEmailStr.replaceAll('(\\s+)', '');
            //split the values to get the list of emails
            splitValuesList = trimmedEmailStr.split(';');
            //add to email list
            additionalEmailList = retrieveAdditionalEmailList(splitValuesList);

        } else if (trimmedEmailStr.contains(' ') || trimmedEmailStr.contains('\n')){
            //split the values to get the list of emails
            splitValuesList = (trimmedEmailStr.contains('\n'))? trimmedEmailStr.split('\n'): trimmedEmailStr.split(' ');
            List<String> trimmedStringList = new List<String>();
            //add to email list
            for (String trimmedString: splitValuesList) {
                trimmedStringList.add(trimmedString.replaceAll('(\\s+)', ''));
            }
            additionalEmailList = retrieveAdditionalEmailList(trimmedStringList);
        } else { 
            if (validateEmail(trimmedEmailStr)) {
                //add the email address
                additionalEmailList = trimmedEmailStr;
            } else {
                //TO DO: ADD ERROR CATCHER
                //errorMessage = '- You have entered an invalid email from Additional/Notify emails. <br/>';
            }
        }
        System.debug('*** additionalemails : ' + additionalEmailList);
        return additionalEmailList;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         08.Dec.2016         
    * @description  method to validate email address
    *               TODO - can possibly be moved to a 'Services' Class
    * @revision     
    *******************************************************************************/
    private static Boolean validateEmail(String email) {
        Boolean isEmailValid = true;
        
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern emailPattern = Pattern.compile(emailRegex);
        Matcher emailMatcher = emailPattern.matcher(email);

        //returns false if email is invalid
        if (!emailMatcher.matches()) {
            isEmailValid = false;
        }

        return isEmailValid;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         08.Dec.2016         
    * @description  method to put the string of emails to a list
    *               TODO - can possibly be moved to a 'Services' Class
    * @revision     
    *******************************************************************************/
    private static String retrieveAdditionalEmailList(List<String> splitValuesList) {
        
        String additionalEmailList = '';
        for(Integer i=0; i<splitValuesList.size(); i++) {
            //validate if the emails are correct
            if (validateEmail(splitValuesList[i])) {
                additionalEmailList += splitValuesList[i];
                if (i != splitValuesList.size()-1) {
                    additionalEmailList += ';';
                }
            } else {
                //errorMessage = '- You have entered an invalid email from the list of Additional/Notify emails.';
            }
        }
        return additionalEmailList;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         02.Mar.2017         
    * @description  constructs the HTML email body
    * @revision     
    *   24.08.2017  Nick Guia   modified method to parse BMSUtils.BuildPreviewEmailWrapper instead
    *                           so that we don't need to insert Campaign record during preview.
    *******************************************************************************/
    private static String constructHTMLBody (BMSUtils.BuildPreviewEmailWrapper bpew) {
        String emailHTMLBody ='';
        String strEmailBody = '';

        EmailTemplate emailTemplateRecord = retrieveEmailTemplateRecord(bpew.emailTemplate);
        emailHTMLBody = emailTemplateRecord.HtmlValue;

        if (bpew.name != null) {
            emailHTMLBody = emailHTMLBody.replace('[SUBJECT]', bpew.name);
        }

        if (bpew.messageHeader != null) {
            emailHTMLBody = emailHTMLBody.replace('[MESSAGE_HEADER]', bpew.messageHeader);
        }

        //CUSTOMISE MESSAGE
        strEmailBody = customiseMessage(bpew.customMessage, bpew.isAddCricos, bpew.isAddBaseSignature);
        emailHTMLBody = emailHTMLBody.replace('[CAMPAIGN_MESSAGE]', strEmailBody);

        if(emailTemplateRecord.Name.contains('Student')){
            emailHTMLBody = emailHTMLBody.replace('[CONTACT_NAME]', 'Student');
        } else if (emailTemplateRecord.Name.contains('Staff')) {
            emailHTMLBody = emailHTMLBody.replace('[CONTACT_NAME]', 'Colleague');
        }

        return emailHTMLBody;
    }

    /**
    * @author   Nick Guia
    * @date     24.08.2017
    * @description  method for customising email body to add css styles as required by the business.
    **/
    private static String customiseMessage(String customMessage, Boolean isAddCricos, Boolean isAddBaseSignature) {
        String strEmailBody = '';
        if(customMessage != null) {
            strEmailBody = customMessage;
            ///if CRICOS is ticked, add it at the bottom of the page
            if (isAddCricos) {
                strEmailBody += '<br/><p style="font: 11pt Arial; margin: 0;">' + System.Label.BMS_CRICOS + '</p>';
            }
            if (isAddBaseSignature) {
                strEmailBody += '<br/><p style="font: Italic 11pt Arial; margin: 0;">' + System.Label.BMS_Base_Signature + '</p>';
            }
            //add CSS styles to the email
            strEmailBody = addCSSStyles(strEmailBody);
        }
        return strEmailBody;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         02.Mar.2017         
    * @description  retrieves the campaign record
    * @revision     
    *******************************************************************************/
    private static EmailTemplate retrieveEmailTemplateRecord (String emailTemplateId) {
        EmailTemplate emailTemplateRecord = new EmailTemplate();
        List<EmailTemplate> templateRetrieved = new List<EmailTemplate>();
        if (emailTemplateId != null) {
            templateRetrieved = [   SELECT  Id, HtmlValue, Name 
                                    FROM    EmailTemplate 
                                    WHERE   Id =: emailTemplateId];
        }
        if (!templateRetrieved.isEmpty()) {
            emailTemplateRecord = templateRetrieved[0];
        }
        return emailTemplateRecord;
    }
}