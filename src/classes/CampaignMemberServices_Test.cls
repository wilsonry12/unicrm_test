/**
 * This class contains unit tests for validating the behavior of Service level class methods
 * these methods are part of the testing framework implemented throughout UniCRM
 * @history
 *  30/08/2017  Nick Guia   added test coverage for qualifyCreateSubscriptionMember
**/
@isTest
private class CampaignMemberServices_Test {

    @testSetup
    private static void init() {
        //create test leads
        List<Lead> leadList = TestHelper.createLeads(100);
        insert leadList;
        System.assert(leadList.size() == 100);

        //create test contact
        List<Contact> contactList = TestHelper.createOrphanContactRecords(100);
        insert contactList;
        System.assert(contactList.size() == 100);

        //create test campaign
        Campaign cmp = TestHelper.createCampaign(1)[0];
        insert cmp;
        System.assert(cmp != null);

        //create test subscription lists
        List<Subscription_List__c> slList = TestHelper.createSubscriptionList(2);
        insert slList;
        System.assert(slList.size() == 2);

        //create test subscription method to recreate scenario of already subscribed leads or contacts
        List<Subscripton_Member__c> smList = new List<Subscripton_Member__c>();
        //create subscription member for lead
        smList.add(TestHelper.createSubscriptionMember(cmp.Id,
                                                    slList[0].Id,
                                                    leadList[0].Id));
        //create subscription member for contact
        smList.add(TestHelper.createSubscriptionMember(cmp.Id,
                                                    slList[0].Id,
                                                    contactList[0].Id));
        insert smList;
    }

    static testMethod void confirmTaskCleanUpAtCohortChange() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        //create account
        
        List<Account>acct= TestHelper.createStudent(10);
        insert acct;
        
        List<Contact> conList= TestHelper.createStudentContacts(10, acct);
        
        insert conList;
        
        Set<ID>accountID = new Set<ID>();
        
        for(Account a:acct){
            accountID.add(a.ID);
        }
        
        //create Campaign
        
        List<Campaign> camp=  TestHelper.createCampaign(1);
        insert camp;
        
        //create Cohorts
        
        List<Cohort__c> cohorts = TestHelper.createCohort(2);
        insert cohorts;
        
        set<ID> contactID= new Set<ID> ();
        for(Contact con : conList){
            contactID.add(con.Id);
        }
        list<CampaignMember> cm= TestHelper.createCampaignMember(u.Id, cohorts[0].id, camp[0].Id,contactID );
        insert cm;
        
        list<CampaignMember> tmpCM= new List<CampaignMember>();
        tmpCM=[SELECT ID,contactID,cohort__c,CampaignID FROM CampaignMember WHERE Cohort__c=:cohorts[0].ID AND CampaignID=:camp[0].Id ];
        
        Test.startTest();
        
        list<Round__c> rList= TestHelper.createRounds(tmpCM[0].Cohort__c, tmpCM[0].CampaignId, 4); 
        insert rList;
        
        Set<id> roundIds = new Set<Id>();
        for(Round__c round : rList) roundIds.add(round.id);
        
        Test.stopTest();
        
        List<Task> taskList = [select id,whoid,whatid from Task where whatid in:roundIds ];
        system.assertNotEquals(0,
        					   taskList.size(),
        					   'Tasks were not setup');
        					   
        system.debug('**tasks '+taskList);		   
        system.assertNotEquals(0, tmpCM.size(),'no campaign members to test on');
        
        integer roundTasksCount = taskList.size();
        
        // change a cohort on campaign member
        
        tmpCM[0].Cohort__c = cohorts[1].Id;
        
        update tmpCM[0];
        
        system.assertNotEquals(roundTasksCount,
					  [select COUNT() from Task where whatid in:roundIds ],
					  'expected task count to drop due to move away from cohort');
        
        
    }

    static testMethod void populateEmailToFieldTest() {
        List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
        insert contactRecordList;

        EmailTemplate template = BMS_TestDataHelper.buildTemplate();

        Campaign massEmail = BMS_TestDataHelper.buildSampleCRMMassEmail(template.Name);
        massEmail.Batch_Finished__c = true;
        insert massEmail;

        CampaignMember campMem = new CampaignMember();
        campMem.ContactId = contactRecordList[0].Id;
        campMem.CampaignId = massEmail.Id;
        campMem.Status = 'Ready to Send';
        insert campMem;
    }

    static testMethod void test_qualifyCreateSubscriptionMember() {
        Map<Id, Lead> leadMap = new Map<Id, Lead>([SELECT Id FROM Lead]);
        Map<Id, Contact> contactMap = new Map<Id, Contact>([SELECT Id FROM Contact]);
        Map<Id, Subscription_List__c> slMap = new Map<Id, Subscription_List__c>([SELECT Id FROM Subscription_List__c]);
        Campaign cmp = [SELECT Id FROM Campaign LIMIT 1];

        //set container of contact and lead Ids
        Set<Id> parentId = new Set<Id>();
        //add lead and contact IDs
        parentId.addAll(leadMap.keySet());
        parentId.addAll(contactMap.keySet());

        //create test campaign members
        List<CampaignMember> cmList = TestHelper.createCampaignMembersWithParent(parentId, cmp.Id);
        for(CampaignMember cm : cmList) {
            //join subscription list ids using colon
            cm.Subscriptions_List__c = String.join(new List<Id>(slMap.keySet()), ':');
        }

        cmList[0].Channel_Preference__c = 'both';
        cmList[1].Channel_Preference__c = 'sms';
        cmList[2].Channel_Preference__c = 'email';

        Test.startTest();
            insert cmList;
        Test.stopTest();
    }
}