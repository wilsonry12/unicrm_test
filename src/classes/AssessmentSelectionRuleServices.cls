/*******************************************************************************
* @author       Ryan Wilson
* @date         20.Jun.2017       
* @description  Service class for AssessmentRuleTrigger events
* @revision     
*******************************************************************************/
public class AssessmentSelectionRuleServices
{
	public class applySelectionRuleOnUpdate implements Triggers.Handler {
        public void run(){
            Map<Id, Assessment_Selection_Rule__c> oldSelectionRule = (Map<Id, Assessment_Selection_Rule__c>)Trigger.oldMap;
            Map<Id, Assessment_Selection_Rule__c> newSelectionRule = (Map<Id, Assessment_Selection_Rule__c>)Trigger.newMap;
            
            List<Assessment_Selection_Rule__c> selectionRulesToApply = new List<Assessment_Selection_Rule__c>();

            if(oldSelectionRule.size() > 0){
            	for(Id assRuleId: newSelectionRule.keySet()){
            		Assessment_Selection_Rule__c assRuleNew = newSelectionRule.get(assRuleId);
            		Assessment_Selection_Rule__c assRuleOld = oldSelectionRule.get(assRuleId);

            		if((assRuleNew.Inactive__c == false) && 
            			((assRuleNew.Course_Codes__c != assRuleOld.Course_Codes__c) ||
            			(assRuleNew.Course_Filter__c != assRuleOld.Course_Filter__c) ||
            			(assRuleNew.Admission_Category__c != assRuleOld.Admission_Category__c) ||
            			(assRuleNew.Attendance_Mode__c != assRuleOld.Attendance_Mode__c) ||
            			(assRuleNew.Location__c != assRuleOld.Location__c) || 
                        (assRuleNew.Override__c && (assRuleNew.Override__c != assRuleOld.Override__c)) || 
                        (assRuleNew.Inactive__c != assRuleOld.Inactive__c))) {
            			
            			selectionRulesToApply.add(assRuleNew);
            		}
            	}
            } 

            if(selectionRulesToApply.size() > 0){
            	runBatchApplySelectionRule(selectionRulesToApply, true);
            }
        }
    }

    public class buildQueryStringBeforeInsert implements Triggers.Handler {
        public void run(){
            List<Assessment_Selection_Rule__c> selectionRulesToApply = new List<Assessment_Selection_Rule__c>();

            for(Assessment_Selection_Rule__c rule: (List<Assessment_Selection_Rule__c>)Trigger.new){
            	if(rule.Inactive__c == false){
            		String queryString = buildQuery(rule);
    				rule.SelectionQuery__c = queryString;
            	}
            }
		}
    }

    public class applySelectionRuleOnInsert implements Triggers.Handler {
        public void run(){
            List<Assessment_Selection_Rule__c> selectionRulesToApply = new List<Assessment_Selection_Rule__c>();

            for(Assessment_Selection_Rule__c rule: (List<Assessment_Selection_Rule__c>)Trigger.new){
            	if(rule.Inactive__c == false && rule.SelectionQuery__c != null){
            		selectionRulesToApply.add(rule);
            	}
            }

            if(selectionRulesToApply.size() > 0){
            	runBatchApplySelectionRule(selectionRulesToApply, false);
            }
		}
    }

    public class unlinkInactiveSelectionRule implements Triggers.Handler {
        public void run(){
            Set<String> selectionRuleUpdate = new Set<String>();
            Map<Id, Assessment_Selection_Rule__c> newSelectionRule = (Map<Id, Assessment_Selection_Rule__c>)Trigger.newMap;
            
            for(Id assRuleId: newSelectionRule.keySet()){
        		Assessment_Selection_Rule__c assRuleNew = newSelectionRule.get(assRuleId);

        		if((assRuleNew.Inactive__c)){
        			selectionRuleUpdate.add(assRuleId);
        		}
        	}

            if(selectionRuleUpdate.size() >0){
			//	Call batch job to assign selection rule.....
				try {
					Id batchJobId = Database.executeBatch(new ApplySelectionRuleBatch(selectionRuleUpdate));
					system.debug('@@@batchJobId'+batchJobId);
				} catch (Exception ex) {
					system.debug('@@@ApplySelectionRuleBatch ERROR: ' + ex);
				}
			}
        }
    }

    private static void runBatchApplySelectionRule(List<Assessment_Selection_Rule__c> selectionRules, Boolean isUpdate) {
    	//build query string
    	Set<String> selectionRuleUpdate = new Set<String>();

    	for(Assessment_Selection_Rule__c rule: selectionRules){
    		if(isUpdate){
    			String queryString = buildQuery(rule);
    			rule.SelectionQuery__c = queryString;
    		}
    		
    		selectionRuleUpdate.add(rule.Id);
		}

		if(selectionRuleUpdate.size() >0){
		//	Call batch job to assign selection rule.....
			try {
				Id batchJobId = Database.executeBatch(new ApplySelectionRuleBatch(selectionRuleUpdate));
				system.debug('@@@batchJobId'+batchJobId);
			} catch (Exception ex) {
				system.debug('@@@ApplySelectionRuleBatch ERROR: ' + ex);
			}
		}
    }

    private static String buildQuery(Assessment_Selection_Rule__c rule){
    	String strfaculty = rule.Rule_Faculty__c;
		String srchCourseCodes = '';
		String srchLocations = '';
		String srchAdmissionCategories = '';
		String srchAttendanceModes = '';
		String queryString = '';

		if(rule.Location__c != null || rule.Admission_Category__c != null || rule.Attendance_Mode__c != null){
			queryString = 'SELECT Id,Active__c,Assessment_Plan_Selection_Rule__c,Attendance_Mode_Description__c,'
							+'Course_Code__c,Course_Title__c,Faculty__c,Location_Code__c,Type_of_Place_Description__c,'
							+'Attendance_Mode__c,Type_of_Place_Code__c '
							+'FROM Course_Offering__c '
							+'WHERE Active__c = true AND Faculty__c = \''+strfaculty+'\'';

    		if(rule.Course_Filter__c == 'Include' && rule.Course_Codes__c != null){
    			List<String> courseCodes = new List<String>();
    			courseCodes = rule.Course_Codes__c.split(';');
    			for(Integer i=0; i<courseCodes.size(); i++){
    				srchCourseCodes += '\'' + courseCodes[i] + '\'';
                    if (i != courseCodes.size()-1) {
                        srchCourseCodes += ',';
                    }
    			}

				queryString += ' AND Course_Code__c IN (' + srchCourseCodes + ')';    			
    		}

    		if(rule.Course_Filter__c == 'Exclude' && rule.Course_Codes__c != null){
    			List<String> courseCodes = new List<String>();
    			courseCodes = rule.Course_Codes__c.split(';');
    			for(Integer i=0; i<courseCodes.size(); i++){
    				srchCourseCodes += '\'' + courseCodes[i] + '\'';
                    if (i != courseCodes.size()-1) {
                        srchCourseCodes += ',';
                    }
    			}

				queryString += ' AND Course_Code__c NOT IN (' + srchCourseCodes + ')';    			
    		}

    		if(rule.Location__c != null){
    			List<String> locations = new List<String>();
    			locations = rule.Location__c.split(';');
    			for(Integer i=0; i<locations.size(); i++){
    				srchLocations += '\'' + locations[i] + '\'';
                    if (i != locations.size()-1) {
                        srchLocations += ',';
                    }
    			}

				queryString += ' AND Location_Code__c IN (' + srchLocations + ')';
    		}

    		if(rule.Admission_Category__c != null){
    			List<String> categories = new List<String>();
    			categories = rule.Admission_Category__c.split(';');
    			for(Integer i=0; i<categories.size(); i++){
    				srchAdmissionCategories += '\'' + categories[i] + '\'';
                    if (i != categories.size()-1) {
                        srchAdmissionCategories += ',';
                    }
    			}

				queryString += ' AND Type_of_Place_Code__c IN (' + srchAdmissionCategories + ')';
    		}

    		if(rule.Attendance_Mode__c != null){
    			List<String> modes = new List<String>();
    			modes = rule.Attendance_Mode__c.split(';');
    			for(Integer i=0; i<modes.size(); i++){
    				srchAttendanceModes += '\'' + modes[i] + '\'';
                    if (i != modes.size()-1) {
                        srchAttendanceModes += ',';
                    }
    			}

				queryString += ' AND Attendance_Mode__c IN (' + srchAttendanceModes + ')';
    		}
		}

		return queryString;
    }
}