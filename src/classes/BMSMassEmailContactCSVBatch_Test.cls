/*******************************************************************************
* @author       Ryan Wilson
* @date         12.Jan.2017         
* @description  test class for BMSMassEmailContactCSVBatch, BMSCSVSerach
* @revision     
*******************************************************************************/
@isTest
private class BMSMassEmailContactCSVBatch_Test {
	
	@isTest(SeeAllData=true) 
	static void testBMSUploadCSV_success() {
		//insert sample Contacts
        String KnownToMonashRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Known to Monash systems').getRecordTypeId();
        List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
        insert contactRecordList;

        List<Contact> changeContactRecordType = new List<Contact>();
        for (Contact contactRecord: [SELECT Id, RecordTypeId FROM Contact WHERE Id IN: contactRecordList]) {
            contactRecord.RecordTypeId = KnownToMonashRecTypeId;
            changeContactRecordType.add(contactRecord);
        }

        if (!changeContactRecordType.isEmpty()) {
            update changeContactRecordType;
        }

        String strCsvContent = 'PERSON_ID'+'\r\n'+'1234567890'+'\r\n'+'1324567890'+'\r\n'+'1474567890'+'\r\n';
        Blob csvBlob = Blob.valueOf(strCsvContent);

        test.startTest();
        PageReference pageRef = Page.BMSUploadIds;
        Test.setCurrentPage(pageRef);

        apexpages.currentpage().getparameters().put('type','email');

        BMSCSVSearch bmsCSV = new BMSCSVSearch();
        bmsCSV.selectedCSVOption = 'PERSONID';
        bmsCSV.contentFile = csvBlob;
        bmsCSV.fileName = 'testCSVFile.csv';
        bmsCSV.newCampaignNameStr = 'Test Subject 00001';
        bmsCSV.massEmailRecord.Additional_Email__c = 'test1@monash.edu;test2@monash.edu';
        bmsCSV.massEmailRecord.Notify__c = 'test1@monash.edu;test2@monash.edu';
        bmsCSV.massEmailRecord.From__c = 'fromsomeon3e@testmonash.edu';
        bmsCSV.draftEmail();
        
        system.assertEquals(bmsCSV.massEmailRecord.From__c, 'fromsomeon3e@testmonash.edu');

        test.stopTest();
	}

	@isTest(SeeAllData=true)
	static void testBMSUploadCSV_fail() {
		//insert sample Contacts
        String KnownToMonashRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Known to Monash systems').getRecordTypeId();
        List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
        insert contactRecordList;

        List<Contact> changeContactRecordType = new List<Contact>();
        for (Contact contactRecord: [SELECT Id, RecordTypeId FROM Contact WHERE Id IN: contactRecordList]) {
            contactRecord.RecordTypeId = KnownToMonashRecTypeId;
            changeContactRecordType.add(contactRecord);
        }

        if (!changeContactRecordType.isEmpty()) {
            update changeContactRecordType;
        }

        String strCsvContent = 'PERSON_ID'+'\r\n'+'1234567890'+'\r\n'+'1324567890'+'\r\n'+'1474567890'+'\r\n';
        Blob csvBlob = Blob.valueOf(strCsvContent);

        test.startTest();
        PageReference pageRef = Page.BMSUploadCSV;
        Test.setCurrentPage(pageRef);

        BMSCSVSearch bmsCSV = new BMSCSVSearch();
        bmsCSV.selectedCSVOption = 'PERSONID';
        bmsCSV.fileName = 'testCSVFile.csv';
        bmsCSV.newCampaignNameStr = '';
        bmsCSV.massEmailRecord.From__c = '';
        bmsCSV.massEmailRecord.Reply_To__c = '';
        bmsCSV.massEmailRecord.Additional_Email__c = 'test1@monash.edu test2@monash.edu';
        bmsCSV.massEmailRecord.Notify__c = 'test1@monash.edu';
        bmsCSV.draftEmail();

        test.stopTest();
	}

    @isTest(SeeAllData=true)
    static void testBMSUploadCSV_badURL() {
        //insert sample Contacts
        String KnownToMonashRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Known to Monash systems').getRecordTypeId();
        List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
        insert contactRecordList;

        List<Contact> changeContactRecordType = new List<Contact>();
        for (Contact contactRecord: [SELECT Id, RecordTypeId FROM Contact WHERE Id IN: contactRecordList]) {
            contactRecord.RecordTypeId = KnownToMonashRecTypeId;
            changeContactRecordType.add(contactRecord);
        }

        if (!changeContactRecordType.isEmpty()) {
            update changeContactRecordType;
        }

        String strCsvContent = 'PERSON_ID'+'\r\n'+'1234567890'+'\r\n'+'1324567890'+'\r\n'+'1474567890'+'\r\n';
        Blob csvBlob = Blob.valueOf(strCsvContent);

        test.startTest();
        PageReference pageRef = Page.BMSUploadCSV;
        Test.setCurrentPage(pageRef);

        BMSCSVSearch bmsCSV = new BMSCSVSearch();
        bmsCSV.selectedCSVOption = 'PERSONID';
        bmsCSV.fileName = 'testCSVFile.csv';
        bmsCSV.newCampaignNameStr = '';
        bmsCSV.massEmailRecord = null;
        bmsCSV.draftEmail();

        test.stopTest();
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         8.Feb.2017     
    * @description  testing email CSVs
    * @revision     
    *******************************************************************************/
    @isTest(SeeAllData=true) 
    static void testBMSUploadEmailCSV_success() {
        //insert sample Contacts
        String KnownToMonashRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Known to Monash systems').getRecordTypeId();
        List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
        contactRecordList[0].Email = 'jbloggs@monashtest.edu';
        insert contactRecordList;

        List<Contact> changeContactRecordType = new List<Contact>();
        for (Contact contactRecord: [SELECT Id, RecordTypeId FROM Contact WHERE Id IN: contactRecordList]) {
            contactRecord.RecordTypeId = KnownToMonashRecTypeId;
            changeContactRecordType.add(contactRecord);
        }

        if (!changeContactRecordType.isEmpty()) {
            update changeContactRecordType;
        }

        String strCsvContent = 'FirstName,LastName,Email'+'\r\n'+'Joe,Bloggs,jbloggs@monashtest.edu'+'\r\n'+'Marie,Claire,some1email@monashtest.edu'+'\r\n'+'Ant,Custodio,ant.custodio@monashtest.edu'+'\r\n';
        Blob csvBlob = Blob.valueOf(strCsvContent);

        test.startTest();
        PageReference pageRef = Page.BMSUploadCSV;
        Test.setCurrentPage(pageRef);

        BMSCSVSearch bmsCSV = new BMSCSVSearch();
        bmsCSV.getCSVOptions();
        bmsCSV.selectedCSVOption = 'EMAIL';
        bmsCSV.contentFile = csvBlob;
        bmsCSV.fileName = 'testCSVFile.csv';
        bmsCSV.newCampaignNameStr = 'Test Subject 00001';
        bmsCSV.massEmailRecord.Additional_Email__c = 'test1@monash.edu;test2@monash.edu';
        bmsCSV.massEmailRecord.Notify__c = 'test1@monash.edu;test2@monash.edu';
        bmsCSV.massEmailRecord.From__c = 'fromsomeon3e@testmonash.edu';
        bmsCSV.draftEmail();
        
        system.assertEquals(bmsCSV.massEmailRecord.From__c, 'fromsomeon3e@testmonash.edu');

        test.stopTest();
    }

    @isTest(SeeAllData=true) 
    static void testBMSUploadEmailCSV_CSVfail() {
        //insert sample Contacts
        String KnownToMonashRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Known to Monash systems').getRecordTypeId();
        List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
        contactRecordList[0].Email = 'jbloggs@monashtest.edu';
        insert contactRecordList;

        List<Contact> changeContactRecordType = new List<Contact>();
        for (Contact contactRecord: [SELECT Id, RecordTypeId FROM Contact WHERE Id IN: contactRecordList]) {
            contactRecord.RecordTypeId = KnownToMonashRecTypeId;
            changeContactRecordType.add(contactRecord);
        }

        if (!changeContactRecordType.isEmpty()) {
            update changeContactRecordType;
        }

        String strCsvContent = 'FirstName,LastName,Email'+'\r\n'+'Jo€,Bloggs,jbloggs@monashtest.edu'+'\r\n'+'Marie,Claire,some1email@monashtest.edu'+'\r\n'+'Ant,Custodio,ant.custodio@monashtest.edu'+'\r\n';
        Blob csvBlob = Blob.valueOf(strCsvContent);

        test.startTest();
        PageReference pageRef = Page.BMSUploadCSV;
        Test.setCurrentPage(pageRef);

        BMSCSVSearch bmsCSV = new BMSCSVSearch();
        bmsCSV.getCSVOptions();
        bmsCSV.selectedCSVOption = 'EMAIL';
        bmsCSV.contentFile = csvBlob;
        bmsCSV.fileName = 'testCSVFile.csv';
        bmsCSV.newCampaignNameStr = 'Test Subject 00001';
        bmsCSV.massEmailRecord.Additional_Email__c = 'test1@monash.edu;test2@monash.edu';
        bmsCSV.massEmailRecord.Notify__c = 'test1@monash.edu;test2@monash.edu';
        bmsCSV.massEmailRecord.From__c = 'fromsomeon3e@testmonash.edu';
        bmsCSV.draftEmail();
        
        system.assertEquals(bmsCSV.massEmailRecord.From__c, 'fromsomeon3e@testmonash.edu');

        test.stopTest();
    }
}