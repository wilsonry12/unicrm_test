/*******************************************************************************
* @author       Ant Custodio
* @date         16.Jun.2017
* @description  Handles all DML and events called by ApplicationAndDocumentSubmission
* @revision     
*******************************************************************************/
public class ApplicationHandler {
    public static final String DOMESTIC_APPLICANT_PROFILE = 'Domestic Applicants';
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         16.Jun.2017
    * @description  This method limits the user to have only one draft application 
    * @revision     
    *******************************************************************************/
    public static void onlyAllowOneDraft(List<Application__c> applicationList) {
        Profile profileRec = [  SELECT  Id, Name 
                                FROM    Profile 
                                WHERE   Id =: userInfo.getProfileId() 
                                LIMIT   1 ];
        //ONLY RUN FOR DOMESTIC APPLICANTS
        if (profileRec.Name == DOMESTIC_APPLICANT_PROFILE) {
            //query on the user to get the contact Id
            User userRec = [    SELECT  Id,
                                        ContactId
                                FROM    User
                                WHERE   Id =: userInfo.getUserId() ];
            //count the number of draft applications. If this is found at least 1, stop the insert
            Integer appCount = [SELECT  COUNT()
                            FROM    Application__c
                            WHERE   Applicant__c =: userRec.ContactId
                                    AND Status__c = 'Draft' ];
            if (appCount > 0) {
                //display error on the page. this will stop all application inserts on bulk
                for (Application__c appRecord: applicationList) {
                    if (appRecord.Status__c == 'Draft' ) {
                        appRecord.addError('You can only have one Draft Application');
                    }
                }
            }
        }
    }
    
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         07.Sept.2017
    * @description  This method automatically links the Application__r.Agent__c 
    *                   using the provided Agent_Org_Unit_Code__c
    * @revision     
    *******************************************************************************/
    public static void populateAgentByOrgUnitCode(Map<Id, Application__c> appMapOld, Map<Id, Application__c> appMap, List<Application__c> appList) {
       try {
            Map<String, Application__c> appToUpdateMap = new Map<String, Application__c>();
            if (appMap != null) {
                appList = appMap.values();
            }
            for (Application__c appRec: appList) {
                //only perform operation when Agent_Org_Unit_Code__c is changed
                Boolean isChanged = true;
                if (appMapOld != null) {
                    isChanged = appMapOld.get(appRec.Id).Agent_Org_Unit_Code__c != appMap.get(appRec.Id).Agent_Org_Unit_Code__c;
                }
                if (isChanged) {
                    //remove the agent when org unit code is removed
                    appRec.Agent__c = null;
                    if (appRec.Agent_Org_Unit_Code__c != '' && appRec.Agent_Org_Unit_Code__c != null) {
                        //create a map using unit code as a key
                        appToUpdateMap.put(appRec.Agent_Org_Unit_Code__c, appRec); 
                    }
                }
            }
            if (!appToUpdateMap.isEmpty()) {
                //clear it first before updating
                for (Application__c appRec: appToUpdateMap.values()) {
                     appRec.Agent__c = null;
                }
                //populate the agent using the org unit code
                for (Account accRec: [  SELECT  Id,
                                                Callista_Org_Unit_Code__c
                                        FROM    Account
                                        WHERE   Callista_Org_Unit_Code__c IN: appToUpdateMap.KeySet() ]) {
                    Application__c appToUpdate = appToUpdateMap.get(accRec.Callista_Org_Unit_Code__c);
                    appToUpdate.Agent__c = accRec.Id;
                }
            }
        } catch (Exception ex) {
            ExLog.add('ApplicationHandler','Account','populateAgentByOrgUnitCode', ex);
        }
    }
}