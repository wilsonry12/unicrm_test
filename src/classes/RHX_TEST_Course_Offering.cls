@isTest(SeeAllData=true)
public class RHX_TEST_Course_Offering {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM Course_Offering__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new Course_Offering__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}