/**************************************************************************************************   
Apex Controller Name :  CaseServices_Assignment 
Version              :  1.0                                                                          
Created Date         :                                                              
Function             :  This class will invoke case assignment rules for case record types that are mentioned in Case_RecordType_CS__c custom settings.
Authur               :  Veera
Story                :                                                                               
Work Task            :                                                                               
Modification Log     :                                                                               
*--------------------------------------------------------------------------------------------------  
***************************************************************************************************/

public class CaseServices_Assignment {
    
    public static boolean recursiveFlag = true;
    
    /**
	* @Method Name     : invokeCaseAssignmentRule
	* @Input Parameters: List of case records
	* @Return Type     : void
	* @Description     : This method triggers assignment rule on case. 
	**/
    
    public static void invokeCaseAssignmentRule(list<case> newcaseList){
        
        if(recursiveFlag == true) {
            try{ 
                set<Id> caseSet = new set<Id>();
                map<string,Case_RecordType_CS__c>  recordtypeCSMap = getRecordtypeCustomsettingMap();
                
                if(!recordtypeCSMap.isEmpty()) {
                    for(case cas:newcaseList) {  
                        Case_RecordType_CS__c caseCS= recordtypeCSMap.containsKey(cas.recordtypeId)? recordtypeCSMap.get(cas.recordtypeId):null;
                        system.debug('caseCS>>>>'+caseCS);
                        if(caseCS!=null && caseCS.Check_Case_Origin__c == true && caseCS.Case_Origin__c !='' && cas.Origin != '') {
                            set<string> casOriginSet = new  set<string>();
                            casOriginSet.addAll(caseCS.Case_Origin__c.toLowerCase().split(',')); 
                            system.debug('++++++'+casOriginSet);
                            if(!casOriginSet.isEmpty()){
                                for(string checkOrigin :casOriginSet) {
                                    if(checkOrigin == cas.Origin) {
                                        caseSet.add(cas.Id); 
                                    }                                	
                                }
                            }  
                        } else {
                            caseSet.add(cas.Id);
                        }
                    }
                }
                
                if(!caseSet.isEmpty()) {
                    list<case> caseList = new list<case>();
                    for(Id caseId:caseSet) {
                        caseList.add(new case(Id=caseId));
                    }
                    
                    AssignmentRule rule = new AssignmentRule();
                    Database.DMLOptions dmlOpts = new Database.DMLOptions();
                    dmlOpts.assignmentRuleHeader.useDefaultRule = true;
                    Database.SaveResult[] sr= Database.update(caseList,dmlOpts);
                }
            } catch(exception ex) {
                ExLog.add('CaseServices_Assignment','case','invokeCaseAssignmentRule', ex);
            }
            recursiveFlag = false;    
        }
    }
    
    /**
	* @Method Name     : getRecordtypeCustomsettingMap
	* @Input Parameters: 
	* @Return Type     : map of string,object
	* @Description     : This method will return which case record type to which custom setting record
	**/
    
    public static map<string,Case_RecordType_CS__c> getRecordtypeCustomsettingMap(){
        map<string,Case_RecordType_CS__c> recordtypeOrginMap = new map<string,Case_RecordType_CS__c>();
        Map<String,Schema.RecordTypeInfo> mapCaseRecordtypes = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName();
        
        if(!mapCaseRecordtypes.isEmpty()){
            for(Case_RecordType_CS__c caseCs: Case_RecordType_CS__c.getAll().values()){
                if(caseCs.Case_Assignment_Trigger__c = true) {
                    for(string recTypeName: mapCaseRecordtypes.keySet()){
                        if(recTypeName.toLowerCase() == caseCs.Recordtype_Name__c.toLowerCase().trim()){
                            recordtypeOrginMap.put(mapCaseRecordtypes.get(recTypeName).getRecordTypeId(), caseCs);
                        }
                    }
                }
            }
        }
        
        system.debug('>>>>>'+recordtypeOrginMap);
        return recordtypeOrginMap;
    }
    
}