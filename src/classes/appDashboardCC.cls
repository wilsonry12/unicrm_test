/*******************************************************************************
* @author       Ryan Wilson
* @date         11.MAY.2017         
* @description  controller class for appDashboard.cmp
* @revision     
*******************************************************************************/
public without sharing class appDashboardCC {
    public static final User userRecord = [ SELECT  Id, ContactId, 
                                                    App_Salutation__c,
                                                    LastName,
                                                    App_Gender__c,
                                                    App_Birthdate__c,
                                                    Email,
                                                    App_Residency_Status__c,
                                                    Street,
                                                    City,
                                                    App_Address_Type__c,
                                                    State,
                                                    PostalCode,
                                                    MobilePhone,
                                                    Phone,
                                                    Country,
                                                    App_Home_Phone__c 
                                            FROM    User 
                                            WHERE   Id =: userInfo.getUserId()];

    public static final List<Callista_Outcome_Status__c> callistaOutcomeStatus = [SELECT Name, Status__c, Explanation__c, FSPortal_Status__c FROM Callista_Outcome_Status__c]; 
    
    @AuraEnabled
    public static Boolean toggleCreateNewApplicationButton () {
        Boolean disableButton = false;
        
        if (userRecord.App_Salutation__c == '' || userRecord.App_Salutation__c == null) {
            disableButton = true;
        } else if (userRecord.LastName == '' || userRecord.LastName == null) {
            disableButton = true;
        } else if (userRecord.App_Gender__c == '' || userRecord.App_Gender__c == null) {
            disableButton = true;
        } else if (userRecord.App_Birthdate__c == null) {
            disableButton = true;
        } else if (userRecord.Email == '' || userRecord.Email == null) {
            disableButton = true;
        } else if (userRecord.App_Residency_Status__c == '' || userRecord.App_Residency_Status__c == null) {
            disableButton = true;
        } else if (userRecord.Street == '' || userRecord.Street == null) {
            disableButton = true;
        } 
        else if(userRecord.App_Address_Type__c == 'POSTAL'){
            if (userRecord.City == '' || userRecord.City == null) {
                disableButton = true;
            } else if (userRecord.State == '' || userRecord.State == null) {
                disableButton = true;
            } else if (userRecord.PostalCode == '' || userRecord.PostalCode == null) {
                disableButton = true;
            }
        }
        else if(userRecord.App_Address_Type__c == 'OS-POSTAL'){
            if(userRecord.Country == '' || userRecord.Country == null){
                disableButton = true;
            }
        }
        else if ( (userRecord.MobilePhone == '' || userRecord.MobilePhone == null) &&
                    (userRecord.Phone == '' || userRecord.Phone == null) &&
                    (userRecord.App_Home_Phone__c == '' || userRecord.App_Home_Phone__c == null)) {
            disableButton = true;
        }

        return disableButton;
    }

    @AuraEnabled
    public static CourseWrapper viewOutcomeStatus(String appCourseId) {
        Application_Course_Preference__c acp = new Application_Course_Preference__c();
        CourseWrapper course = new CourseWrapper();

        if(appCourseId != null){
            acp = [SELECT Academic_Calendar_Type__c,Academic_CI_Sequence_Number__c,Admission_Calendar_Type__c,
                    Admission_CI_Sequence_Number__c,Applicant__c,Application__c,Attendance_Type__c,Attendance_Mode_Description__c,
                    Commencement_Period__c,Attendance_Type_Description__c,Attendance_Mode__c, Type_of_Place_Description__c,
                    Course_Code__c,Course_Offering__c,Course_Title__c,Course_Version_Number__c,Course__c,CreatedDate,Id,
                    Location_Code__c,Name,Outcome_Status__c,Preference_Number__c,Status__c,Unit_Set_Code__c, Unit_Set_Description__c,
                    Offer_Response_Status__c,Offer_Condition__c, Offer_Date__c, Offer_Response_Date__c, Actual_Offer_Response_Date__c 
                    FROM Application_Course_Preference__c 
                    WHERE Id =:appCourseId];
            System.debug('****ACP: '+acp);

            course = builCourseWrapper(acp);
        }
        
        return course;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         11.MAY.2017         
    * @description  inserts a new application
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static String insertNewApplication () {
        Application__c applicationRecord = new Application__c();
        applicationRecord.Applicant__c = userRecord.ContactId;
        String returnValue = '';
        try {
            Integer appDrafts = [SELECT COUNT() FROM Application__c WHERE Status__c = 'Draft' AND Applicant__c =: userRecord.ContactId];
            if (appDrafts > 0) {
                throw new AuraHandledException('You can only have one draft application. Please click \'Home\' to refresh your dashboard ');
            } else {
                applicationRecord.From_Portal__c = true; //R.Wilson added indicator for Callista Migration.
                applicationRecord.Status__c = 'Draft'; //R.Wilson added default status for PD merge.
                insert applicationRecord;

                //insert junction object records if there are any
                insertChildRecords(applicationRecord.Id);

                returnValue = applicationRecord.Id;
            }
            
        } catch (Exception ex) {
            throw new AuraHandledException('Unable to update your details: ' + ex.getMessage());
        }
        return returnValue;
    }

    @AuraEnabled
    public static String cancelDraftApplication (String applicationId) {
        String result = '';
        Application__c applicationRecord = new Application__c();

        try{
            if(applicationId != null){
                applicationRecord = [SELECT Id, Name, Status__c FROM Application__c WHERE Id=: applicationId];
                applicationRecord.Status__c = 'Cancelled';
                update applicationRecord;

                result = 'SUCCESS';
            }
        }
        catch(exception ex){
            result = 'ERROR';
        }
        
        return result;
    }

    @AuraEnabled
    public static List<ApplicationWrapper> retrieveDraftApplication() {
        List<Application__c> applicationRecord = new List<Application__c>();

        applicationRecord = [SELECT Id, Name, Applicant__c, Status__c, CreatedDate, LastModifiedDate, Submitted_Date__c,  
                            (SELECT Academic_Calendar_Type__c,Academic_CI_Sequence_Number__c,Admission_Calendar_Type__c,
                                Admission_CI_Sequence_Number__c,Applicant__c,Application__c,Attendance_Type__c,Attendance_Mode_Description__c,
                                Commencement_Period__c,Attendance_Type_Description__c,Attendance_Mode__c, Type_of_Place_Description__c,
                                Course_Code__c,Course_Offering__c,Course_Title__c,Course_Version_Number__c,Course__c,CreatedDate,Id,
                                Location_Code__c,Name,Outcome_Status__c,Preference_Number__c,Status__c,Unit_Set_Code__c, Unit_Set_Description__c,
                                Offer_Response_Status__c,Offer_Condition__c, Offer_Date__c, Offer_Response_Date__c, Actual_Offer_Response_Date__c     
                                FROM Application_Course_Preferences__r ORDER BY Preference_Number__c ASC) 
                            FROM Application__c 
                            WHERE Applicant__c =:userRecord.ContactId 
                            AND Status__c = 'Draft' 
                            ORDER BY CreatedDate DESC 
                            LIMIT 1];

        List<ApplicationWrapper> appList = buildApplicationWrapper(applicationRecord);
        
        return appList;
    }

    @AuraEnabled
    public static List<ApplicationWrapper> retrieveSubmittedApplication() {
        List<Application__c> applicationRecord = new List<Application__c>();

        applicationRecord = [SELECT Id, Name, Applicant__c, Status__c, CreatedDate, LastModifiedDate, Submitted_Date__c, 
                            (SELECT Academic_Calendar_Type__c,Academic_CI_Sequence_Number__c,Admission_Calendar_Type__c,
                                Admission_CI_Sequence_Number__c,Applicant__c,Application__c,Attendance_Type__c,Attendance_Mode_Description__c,
                                Commencement_Period__c,Attendance_Type_Description__c,Attendance_Mode__c,Type_of_Place_Description__c,
                                Course_Code__c,Course_Offering__c,Course_Title__c,Course_Version_Number__c,Course__c,CreatedDate,Id,
                                Location_Code__c,Name,Outcome_Status__c,Preference_Number__c,Status__c,Unit_Set_Code__c, Unit_Set_Description__c,
                                Offer_Response_Status__c,Offer_Condition__c, Offer_Date__c, Offer_Response_Date__c, Actual_Offer_Response_Date__c    
                                FROM Application_Course_Preferences__r ORDER BY Preference_Number__c ASC) 
                            FROM Application__c 
                            WHERE Applicant__c =:userRecord.ContactId 
                            AND (Status__c != 'Draft' AND Status__c != 'Cancelled') 
                            ORDER BY Submitted_Date__c DESC 
                            LIMIT 10];

        List<ApplicationWrapper> appList = buildApplicationWrapper(applicationRecord);
        return appList;
    }

    
    private static List<ApplicationWrapper> buildApplicationWrapper(List<Application__c> applicationRecord) {
        List<ApplicationWrapper> appList = new List<ApplicationWrapper>();

        if(applicationRecord.size() >0){
            for(Application__c app: applicationRecord){
                ApplicationWrapper appWrap = new ApplicationWrapper();
                appWrap.applicationId = app.Id;
                appWrap.applicationName = app.Name;
                appWrap.lastUpdatedDate = app.LastModifiedDate;
                appWrap.submittedDate = app.Submitted_Date__c;

                List<CourseWrapper> listCourse = new List<CourseWrapper>();
                for(Application_Course_Preference__c appCourse: app.Application_Course_Preferences__r){
                    listCourse.add(builCourseWrapper(appCourse));
                }

                appWrap.courses = listCourse;
                appList.add(appWrap);
            }
        }

        return appList;
    }

    
    private static CourseWrapper builCourseWrapper(Application_Course_Preference__c appCourse) {
        Map<String, Callista_Outcome_Status__c> mapPortalStatus = new Map<String, Callista_Outcome_Status__c>();
        if(callistaOutcomeStatus.size() >0){
            for(Callista_Outcome_Status__c outcome: callistaOutcomeStatus){
                mapPortalStatus.put(outcome.Name, outcome);
            }
        }

        CourseWrapper course = new CourseWrapper();
        if(appCourse != null){
            course.courseId = appCourse.Id;
        
            course.courseDetail = appCourse.Course_Code__c+'-'+appCourse.Course_Title__c;
            if(appCourse.Unit_Set_Description__c != null){
                course.courseDetail += '/ '+appCourse.Unit_Set_Description__c;
            }
            if(appCourse.Type_of_Place_Description__c != null){
                course.courseDetail += '/ '+appCourse.Type_of_Place_Description__c; 
            }

            course.attMode = appCourse.Attendance_Type_Description__c+' '+appCourse.Attendance_Mode_Description__c;
            
            course.location = appCourse.Location_Code__c;
            course.commencementPeriod = appCourse.Commencement_Period__c;
            course.outcomeStatus = appCourse.Outcome_Status__c;
            course.offerDate = appCourse.Offer_Date__c;
            course.offerResponseStatus = appCourse.Offer_Response_Status__c;
            course.offerCondition = appCourse.Offer_Condition__c;
            course.offerResponseDate = appCourse.Offer_Response_Date__c;
            course.actualResponseDate = appCourse.Actual_Offer_Response_Date__c;

            if(appCourse.Outcome_Status__c != null){
                course.outcomePortalStatus = mapPortalStatus.get(appCourse.Outcome_Status__c).FSPortal_Status__c;
                course.statusExplanation = mapPortalStatus.get(appCourse.Outcome_Status__c).Explanation__c;
            }

            if(appCourse.Offer_Response_Status__c != null){
                course.responsePortalStatus = mapPortalStatus.get(appCourse.Offer_Response_Status__c).FSPortal_Status__c;
            }
            
        }
        
        return course;
    }

    

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         1.May.2017         
    * @description  link existing contact qualifications and work experience to the
                        new application
    * @revision     ACUSTODIO, 3.May.2017 - added Application_Document_Provided__c
                        junction upon creation
    *******************************************************************************/
    private static void insertChildRecords (String applicationId) {
        
        if (userRecord.ContactId != null && applicationId != null) {
            List<Application_Qualification_Provided__c> appQualProvidedList = new List<Application_Qualification_Provided__c>();
            List<Application_Document_Provided__c> appDocumentProvidedList = new List<Application_Document_Provided__c>();

            for(Contact_Document__c cd: [SELECT Id, Filename__c, Contact__c, (SELECT Id FROM Attachments) 
                                            FROM Contact_Document__c 
                                            WHERE Contact__c =: userRecord.ContactId]){

                Application_Document_Provided__c appDocProvided = new Application_Document_Provided__c();
                appDocProvided.Application__c = applicationId;
                appDocProvided.Contact_Document__c = cd.Id;
                appDocumentProvidedList.add(appDocProvided);
            }

            for (Contact_Qualification__c contactQualification:  [  SELECT  Id, Qualification_Document__c
                                                                    FROM    Contact_Qualification__c
                                                                    WHERE   Contact__c =: userRecord.ContactId ]) {
                Application_Qualification_Provided__c appQualProvided = new Application_Qualification_Provided__c();
                appQualProvided.Application__c = applicationId;
                appQualProvided.Contact_Qualification__c = contactQualification.Id;
                appQualProvidedList.add(appQualProvided);
                
                //create a junction of documents from qualification to the application object 
                // remove this....
                /*if (contactQualification.Qualification_Document__c != null) {
                    Application_Document_Provided__c appDocProvided = new Application_Document_Provided__c();
                    appDocProvided.Application__c = applicationId;
                    appDocProvided.Contact_Document__c = contactQualification.Qualification_Document__c;
                    appDocumentProvidedList.add(appDocProvided);
                }*/
            }

            if (!appQualProvidedList.isEmpty()) {
                insert appQualProvidedList;
            }

            if (!appDocumentProvidedList.isEmpty()) {
                insert appDocumentProvidedList;
            }

            List<Application_Work_Experience_Provided__c> workExpProvidedList = new List<Application_Work_Experience_Provided__c>();
            for (Work_Experience__c contactQualification:  [    SELECT  Id
                                                                FROM    Work_Experience__c
                                                                WHERE   Contact__c =: userRecord.ContactId ]) {
                Application_Work_Experience_Provided__c workExpProvided = new Application_Work_Experience_Provided__c();
                workExpProvided.Application__c = applicationId;
                workExpProvided.Work_Experience__c = contactQualification.Id;
                workExpProvidedList.add(workExpProvided);
            }

            if (!workExpProvidedList.isEmpty()) {
                insert workExpProvidedList;
            }
        }
    }

    public class ApplicationWrapper {
        @AuraEnabled public String applicationId {get; set;}
        @AuraEnabled public String applicationName {get; set;}
        @AuraEnabled public DateTime lastUpdatedDate {get; set;}
        @AuraEnabled public DateTime submittedDate {get; set;}
        @AuraEnabled public List<CourseWrapper> courses {get; set;}
    }

    public class CourseWrapper {
        @AuraEnabled public String courseId {get; set;}
        @AuraEnabled public String courseDetail {get; set;}
        @AuraEnabled public String attMode {get; set;}
        @AuraEnabled public String location {get; set;}
        @AuraEnabled public String commencementPeriod {get; set;}
        @AuraEnabled public String outcomePortalStatus {get; set;}
        @AuraEnabled public String responsePortalStatus {get; set;}
        @AuraEnabled public String outcomeStatus {get; set;}
        @AuraEnabled public Date offerDate {get; set;}
        @AuraEnabled public String offerResponseStatus {get; set;}
        @AuraEnabled public String offerCondition {get; set;}
        @AuraEnabled public Date offerResponseDate {get; set;}
        @AuraEnabled public Date actualResponseDate {get; set;}
        @AuraEnabled public String statusExplanation {get; set;}
    }
}