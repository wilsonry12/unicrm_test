/**************************************************************************************************   
Apex Class Name      :  TestLightningCreateCaseController
Version              :  1.0                                                                          
Created Date         :                                                              
Function             :  This is test class for LightningCreateCaseController.
Story                :                                                                               
Work Task            :                                                                               
Modification Log     :                                                                               
*--------------------------------------------------------------------------------------------------  
    Developer                        Date                        Description
    Veera Gandi                    03/07/2017                        Created.
***************************************************************************************************/
@istest
public class TestLightningCreateCaseController {
    
    @testSetup
    public static void createData() {
        TestUtil.createArticles();
    }
    
    
    public static testmethod void createCaseTestMethod(){
        LightningCreateCaseController.getReasonofEnquiry();
        LightningCreateCaseController.getUserType();
        case newcase = new case(Channel__c='Web',
                                Origin='ask.monash@monash.edu',
                                Community_Status__c='I need help with accepting my employment offer from Monash',
                                Description='testcase description');        
        string caseId = LightningCreateCaseController.createCase(newcase);
        string validateCaseId = [select id from case where Id=:caseId].Id;
        system.assertEquals(validateCaseId,caseId);
        Id fileId=LightningCreateCaseController.saveTheChunk(caseId,'testFile',' test data','.txt',null);
        system.assertnotEquals(fileId,null);
        Id fileId2=LightningCreateCaseController.saveTheChunk(caseId,'testFile',' test data 2','.txt',fileId);     
        system.assertnotEquals(fileId2,null);   
    }
    
    public static testmethod void articleSuggestionTestMethod(){
        faq__kav faq=[select id,question__c,answer__c,title,UrlName from faq__kav where UrlName='test454365' and PublishStatus='Online' and language='en_US'];
        sobject objectRec = LightningCreateCaseController.getArticledetail(faq.Id);
        LightningCreateCaseController.getArticalSuggestions('How Monash Works'); 
        system.assertEquals(faq.Id, objectRec.Id);
        string FirstName = LightningCreateCaseController.getUserFirstName();
        string validateFirstName = ' '+[select id,FirstName from User where id =:userinfo.getUserId()].FirstName;
        system.assertEquals(FirstName, validateFirstName);
    }    
}