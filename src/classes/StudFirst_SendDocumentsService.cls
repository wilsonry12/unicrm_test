/*******************************************************************************
* @author		Ant Custodio
* @date         12.Sept.2017     
* @description  Web service for sending a notification to MIX to send a new
					document to Callista
* @revision     
*******************************************************************************/
global class StudFirst_SendDocumentsService {
	
	private static final String AGENT_PERSON_ID = Label.CALLISTA_AGENT_PERSON_ID;
	private static final String API_SOURCE = 'unicrm';
	private static final String API_TYPE = 'document';

	/*******************************************************************************
	* @author		Ant Custodio
	* @date         13.Sept.2017     
	* @description  Method that accepts a single Application Id that can be called
						if we want to submit documents to Callista (Through MIX)
	* @revision     
	*******************************************************************************/
	WebService static void sendToCallistaByAppId(String appId) {
		if (appId != '') {
			sendToCallistaByAppIds(new Set<String>{appId});
		}
	}

	/*******************************************************************************
	* @author		Ant Custodio
	* @date         13.Sept.2017     
	* @description  Method that accepts a set of allication Ids that can be called
						if we want to submit documents to Callista (Through MIX)
	* @revision     
	*******************************************************************************/
	public static void sendToCallistaByAppIds(Set<String> appIds) {
		if (!appIds.isEmpty()) {
			//create a map of application and ADP
			Map<String, List<Application_Document_Provided__c>> adpMap = new Map<String, List<Application_Document_Provided__c>>();
			try {
				adpMap = getDocProvidedByApplicationId(appIds);
				if (!adpMap.isEmpty()) {
					generateRequest(adpMap);
				}
			} catch (Exception ex) {
				Exlog.add('Apex Class','StudFirst_SendDocumentsService','sendToCallistaByAppIds', ex + ' - ' + ex.getLineNumber() +' -- adpMap:' + adpMap);
			}
		}
	}

	/*******************************************************************************
	* @author		Ant Custodio
	* @date         19.Oct.2017     
	* @description  Method that accepts a set of Application_Document_Provided__c 
						Ids that can be called if we want to submit documents 
						to Callista (Through MIX)
	* @revision     Ant Custodio, 31.Oct.2017 - added Contact_Document__r.ADP_Submitted_Count__c 
						on checking whether the document is submitted already - SF-639
	*******************************************************************************/
	public static void sendToCallistaByADPIds(Set<String> adpIds) {
		if (!adpIds.isEmpty()) {
			//create a map of application and ADP
			Map<String, List<Application_Document_Provided__c>> adpMap = new Map<String, List<Application_Document_Provided__c>>();
			List<Application_Document_Provided__c> adpToUpdateList = new List<Application_Document_Provided__c>();
			for (Application_Document_Provided__c adpRec: [	SELECT	Application__r.Callista_Applicant_Id__c,
																	Application__r.Applicant__c,
																	Id,
																	Application__c,
																	Contact_Document__c,
																	Contact_Document__r.Document_Type__c,
																	Contact_Document__r.Name,
																	Contact_Document__r.ADP_Submitted_Count__c,
																	Contact_Document__r.Comments__c,
																	Document_submitted_successfully__c
															FROM 	Application_Document_Provided__c
															WHERE 	Id IN: adpIds
																	AND Document_submitted_successfully__c = false
																	AND Application__r.Callista_Application_Id__c != '' ]) {
				if (adpRec.Contact_Document__r.ADP_Submitted_Count__c > 0) {
					//if the document is already submitted to Callista, tick the box to true
					Application_Document_Provided__c adpToUpdate = adpRec;
					adpToUpdate.Document_submitted_successfully__c = true;
					adpToUpdate.API_Response_Message__c = System.Label.App_DocPreviouslySubmitted;
					adpToUpdateList.add(adpToUpdate);
				} else {
					if (adpMap.containsKey(adpRec.Application__c)) {
						List<Application_Document_Provided__c> adpList = adpMap.get(adpRec.Application__c);
						adpList.add(adpRec);
						adpMap.put(adpRec.Application__c, adpList);
					} else {
						adpMap.put(adpRec.Application__c, new List<Application_Document_Provided__c> {adpRec});
					}
				}
			}

			try {
				//update the flags
				if (!adpToUpdateList.isEmpty()) {
					update adpToUpdateList;
				}

				//generate the request
				if (!adpMap.isEmpty()) {
					generateRequest(adpMap);
				}
			} catch (Exception ex) {
				Exlog.add('Apex Class','StudFirst_SendDocumentsService','sendToCallistaByADPIds', ex + ' - ' + ex.getLineNumber() +' -- adpIds:' + adpIds);
			}
		}
	}

	/*******************************************************************************
	* @author		Ant Custodio
	* @date         19.Oct.2017     
	* @description  Generates a request using a map of Application and ADP
	* @param 		KEY = Application__c
	* 				VALUE = List of child Application_Document_Provided__c
	* @revision     
	*******************************************************************************/
	private static void generateRequest (Map<String, List<Application_Document_Provided__c>> adpMap) {
		List<Application_Document_Provided__c> adpToUpdateList = new List<Application_Document_Provided__c>();
		//retrieve all contact document Ids related to the application
		Set<String> contDocIds = getContDocIdsByDocProvided(adpMap);
		//create a map of contact document and attachment
		Map<String, List<Attachment>> attMap = getAttachmentByContactDocId(contDocIds);

		List<StudFirst_SendDocumentsServiceRequest> requestList = new List<StudFirst_SendDocumentsServiceRequest>();
		for (String appId: adpMap.keySet()) {
			StudFirst_SendDocumentsServiceRequest request = new StudFirst_SendDocumentsServiceRequest();
			request.source = API_SOURCE;
			request.sourceTime = String.valueOf(System.now());
			request.type = API_TYPE;
			request.events = new List<StudFirst_SendDocumentsServiceRequest.AppDocument>();
			//loop through the document provided map to create the request
			for (Application_Document_Provided__c adpRec: adpMap.get(appId)) {
				if (attMap.containsKey(adpRec.Contact_Document__c)) {
					for (Attachment attRec: attMap.get(adpRec.Contact_Document__c)) {	
						StudFirst_SendDocumentsServiceRequest.AppDocument appDoc = new StudFirst_SendDocumentsServiceRequest.AppDocument();
						appDoc.agentPersonId = AGENT_PERSON_ID;
						appDoc.applicantId = adpRec.Application__r.Callista_Applicant_Id__c;
						appDoc.externalApplicantId = adpRec.Application__r.Applicant__c;
						appDoc.contentType = attRec.ContentType;
						appDoc.filename = generateFileName(attRec.Name, adpRec.Contact_Document__r.Name);
						appDoc.id = attRec.Id;
						appDoc.applicationDocumentId = adpRec.Id;
						appDoc.contactDocumentId = adpRec.Contact_Document__c;
						appDoc.documentType = adpRec.Contact_Document__r.Document_Type__c;
						appDoc.comments = adpRec.Contact_Document__r.Comments__c;
						request.events.add(appDoc);
					}
				}
			}

			String requestStr = StudFirst_Utilities.convertTo7BitAcceptedString(JSON.serializePretty(request), '?');
			for (Application_Document_Provided__c adpRec: adpMap.get(appId)) {
				//update the field with the request message
				adpRec.API_Request_Message__c = requestStr;
				adpToUpdateList.add(adpRec);
			}
			
			requestList.add(request);
		}

		//update the request message
		if (!adpToUpdateList.isEmpty()) {
			update adpToUpdateList;
		}

		//send a request to MIX so that the Attachments are retrieved from Salesforce 
		//	and then transferred to Callista
		if (!requestList.isEmpty()) {
			List<String> requestStrList = new List<String>();
			for (StudFirst_SendDocumentsServiceRequest req: requestList) {
				String requestStr = StudFirst_Utilities.convertTo7BitAcceptedString(JSON.serialize(req), '?');
				requestStrList.add(requestStr);
			}
			StudFirst_SendDocumentsQueueable campaignMemberJob = new StudFirst_SendDocumentsQueueable(requestStrList);
	        Id jobID = System.enqueueJob(campaignMemberJob);
		}
	}

	/*******************************************************************************
	* @author		Ant Custodio
	* @date         28.Sept.2017     
	* @description  generates a callista filename using the attachment name and
						the contact document number
	* @return		String Format: 'FileName_000001.pdf'
	* @param 		attName = Attachment.Name
					conDocName = Contact_Document__r.Id		
	* @revision     Ant Custodio, 26.Oct.2017 - removed the CNDC prefix (TEMP FIX SF-525)
					Ant Custodio, 6.Nov.2017 - added back the CNDC prefix
	*******************************************************************************/
	private static String generateFileName (String attName, String conDocName) {
		String fileName = '';

		if (attName != '' && conDocName != '') {
			if (conDocName.contains('CNDC-')) {
			    conDocName = conDocName.replace('CNDC-', '');
			}

			String extension = '';
			Integer i = attName.lastIndexOf('.');
			if (i > 0) {
			    extension = attName.substring(i+1);
			    attName = attName.substring(0,i);
			}

			fileName = attName + '_' + conDocName + '.' +extension;
			//fileName = attName + '.' +extension;

			//I've set it to 200 to catch the maximum size that callista can handle
			if (fileName.length() > 200) {
				fileName = fileName.Substring(0, 200);
			}
		}

		return fileName;
	}

	/*******************************************************************************
	* @author		Ant Custodio
	* @date         12.Sept.2017     
	* @description  retrieves the Application Documents Provided related to the
						application that were not submitted to Callista yet
	* @return		key = Application_Document_Provided__r.Application__c
	*				value = Application_Document_Provided__c
	* @revision     Ant Custodio, 31.Oct.2017 - added Contact_Document__r.ADP_Submitted_Count__c 
						on checking whether the document is submitted already - SF-639
	*******************************************************************************/
	private static Map<String, List<Application_Document_Provided__c>> getDocProvidedByApplicationId (Set<String> appIds) {
		Map<String, List<Application_Document_Provided__c>> adpMap = new Map<String, List<Application_Document_Provided__c>>();
		List<Application_Document_Provided__c> adpToUpdateList = new List<Application_Document_Provided__c>();
		for (Application_Document_Provided__c adpRec: [	SELECT	Application__r.Callista_Applicant_Id__c,
																Application__r.Applicant__c,
																Id,
																Application__c,
																Contact_Document__c,
																Contact_Document__r.Document_Type__c,
																Contact_Document__r.Name,
																Contact_Document__r.ADP_Submitted_Count__c,
																Contact_Document__r.Comments__c,
																Document_submitted_successfully__c
														FROM 	Application_Document_Provided__c
														WHERE 	Application__c IN: appIds
																AND Document_submitted_successfully__c = false
																AND Application__r.Callista_Application_Id__c != '' ]) {
			if (adpRec.Contact_Document__r.ADP_Submitted_Count__c > 0) {
				//if the document is already submitted to Callista, tick the box to true
				Application_Document_Provided__c adpToUpdate = adpRec;
				adpToUpdate.Document_submitted_successfully__c = true;
				adpToUpdate.API_Response_Message__c = System.Label.App_DocPreviouslySubmitted;
				adpToUpdateList.add(adpToUpdate);
			} else {
				if (adpMap.containsKey(adpRec.Application__c)) {
					List<Application_Document_Provided__c> adpList = adpMap.get(adpRec.Application__c);
					adpList.add(adpRec);
					adpMap.put(adpRec.Application__c, adpList);
				} else {
					adpMap.put(adpRec.Application__c, new List<Application_Document_Provided__c> {adpRec});
				}
			}
		}

		//update the flags
		if (!adpToUpdateList.isEmpty()) {
			update adpToUpdateList;
		}

		return adpMap;
	}

	/*******************************************************************************
	* @author		Ant Custodio
	* @date         12.Sept.2017     
	* @description  retrieves the Attachment related to the Application Document
						Provided
	* @return		key = Application_Document_Provided__r.Id
	*				value = Attachment
	* @revision     
	*******************************************************************************/
	private static Map<String, List<Attachment>> getAttachmentByContactDocId (Set<String> contDocIds) {
		Map<String, List<Attachment>> attMap = new Map<String, List<Attachment>>();
		for (Attachment attRec: [	SELECT	ContentType,
											Name,
											Id,
											ParentId
									FROM 	Attachment
									WHERE 	ParentId IN: contDocIds ]) {
			if (attMap.containsKey(attRec.ParentId)) {
				List<Attachment> attList = attMap.get(attRec.ParentId);
				attList.add(attRec);
				attMap.put(attRec.ParentId, attList);
			} else {
				attMap.put(attRec.ParentId, new List<Attachment>{attRec});
			}
		}
		return attMap;
	}

	/*******************************************************************************
	* @author		Ant Custodio
	* @date         12.Sept.2017     
	* @description  retrieves the Contact Document Ids related to the
						application document provided record
	* @return		Set of Contact Document Ids
	* @revision     
	*******************************************************************************/
	private static Set<String> getContDocIdsByDocProvided (Map<String, List<Application_Document_Provided__c>> adpMap) {
		Set<String> contDocIds = new Set<String>();
		for (String appId: adpMap.keySet()) {
			for (Application_Document_Provided__c adpRec: adpMap.get(appId)) {
				contDocIds.add(adpRec.Contact_Document__c);
			}
		}
		return contDocIds;
	}

}