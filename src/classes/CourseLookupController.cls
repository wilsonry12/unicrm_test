/*
    AUTHOR : Vineeth Batreddy
    DESCRIPTION : This class acts as the controller for CourseLookup lightning component
*/

public class CourseLookupController {
    
    @AuraEnabled
    public static Course__c getCourseDetails(String courseCode){
        List<Course__c> course = [SELECT Id, Name, Course_Description__c FROM Course__c WHERE Name =:courseCode];
        if(course != null && course.size() >0){
            return course[0];
        }
        return null;
    }

    @AuraEnabled
    public static List<Course_Offering__c> getCourseOffering(String courseCode) {

        List<Course_Offering__c> courseOfferings = [SELECT Id, Start_Date__c, End_Date__c, Fee__c, Concession_Fee__c, No_of_Sessions__c, Course_Code_F__c, Number_of_Remaining_Places__c, Course__r.Course_Description__c, Venue__r.Name, Status__c, Apply_GST__c, Part_of_the_Day__c, Customised_course_offering__c, Course__r.Managing_Faculty__c FROM Course_Offering__c WHERE Course_Code_F__c = :courseCode AND Start_Date__c > TODAY AND (Status__c = 'Available' OR Status__c = 'Full') ORDER BY Start_Date__c];
        if(courseOfferings != null && courseOfferings.size() > 0) {
            return courseOfferings;
        }      
        System.debug('!@#$% Course Offerings retrieved : ' + courseOfferings); 
        return null;
       
    }

}