public class PDTestHelper {
    public static Venue__c createVenue() {
        Venue__c venue = new Venue__c();
        venue.Name = 'Test venue';
        venue.Legacy_Venue_ID__c = '1';
        venue.Details__c = 'details';
        venue.Parking_Details__c = 'parking details';
        venue.Minimum_Capacity__c = 1;
        venue.Maximum_Capacity__c = 5;
        insert venue;
        return venue;   
    }

    public static Application__c createApplication(Contact contact, Case enquiry) {
        Application__c application = new Application__c();
        application.Probability__c = '10';
        application.Status__c = 'Registered - Not paid';
        application.Enquiry_Number__c = enquiry.Id;
        application.Applicant__c = contact.Id;

        insert application;
        return application;
    }

    public static Course__c createCourse() {
        Course__c course = new Course__c();
        course.Name = 'PDD0001';
        course.Cost_Centre__c = '123';
        course.Course_Description__c = 'Course Description';
        course.Fee__c = 1;
        course.Fund__c = 'Fund';
        course.GL_Account__c = 'GL Account';
        course.Inactive__c = false;
        course.Legacy_Course_ID__c = '1';
        insert course; 
        return course; 
    }

    public static Course_Offering__c createCourseOffering() {
        Course_Offering__c co = new Course_Offering__c();
        co.Course_Code__c = 'PDD0001';
        co.Apply_GST__c = false;
        co.Campus__c = 'Clayton';
        co.Legacy_Cancellation__c = true;
        co.Legacy_Course_ID__c = '1';
        co.Legacy_Presenter_ID__c = '1';
        co.Legacy_Time_ID__c = '1';
        co.Legacy_Venue_ID__c = '1';
        co.Legacy_Course_Offering_ID__c = '1';
        co.Start_Date__c = System.today();
        co.Start_Date__c.addDays(10);
        co.Concession_Fee__c = 800;
        co.Fee__c = 1000;
        insert co;
        return co; 
    }

    public static void createPresenter() {
        Presenter__c p = new Presenter__c();
        p.Name = '1';
        p.Full_Name__c = 'Fred';
        insert p;
    }

    public static Contact createContact() {
        Contact cont = new Contact();
        cont.LastName = 'Laster';
        cont.Legacy_Contact_ID__c = '1';
        cont.Email = 'vineeth.batreddy@monash.edu';
        insert cont;
        return cont;
    }

    public static Contact createContact2(String givenName, String familyName, String email) {
        Contact cont = new Contact();
        cont.FirstName = givenName;
        cont.LastName = familyName;
        cont.Email = email;        
        insert cont;
        return cont;
    }    

    public static Case createEnquiry() {
        Case enq = new Case();
        enq.Subject = 'Online Course Registration';
        enq.Description = 'Test';
        enq.Heard_About_From__c = 'Banners';
        enq.Origin = 'Web';     
        enq.Student_Type__c = 'Domestic';
        enq.Enquiry_Type__c = 'Future Student';
        enq.Managing_Faculty__c = 'Education';
        enq.Status = 'Open';
        enq.Course__c = createCourse().Id;
        enq.Legacy_Course_Id__c = '1';
        enq.Legacy_Contact_Id__c = '1';
        enq.Legacy_Preferred_Time_ID__c = '1';
        enq.Legacy_Processed_Flag__c = false;
        enq.RecordTypeId = getRecordTypes('Case').get('Standard Enquiry').Id;
        insert enq;
        return enq;
    }

    public static Attachment createAttachment(Id parentId) {
        Attachment attachment = new Attachment();
        attachment.parentId = parentId;
        attachment.Name = 'test attachment';
        String content = 'Something';
        attachment.body = EncodingUtil.base64Decode(content);
        insert attachment;
        return attachment;
    }

    public static Payment__c createPayment(Course_Offering__c co, Contact contact, Application__c application) {
        Payment__c payment = new Payment__c();
        payment.Application__c = application.Id;
        payment.Contact__c = contact.Id;        
        payment.Course_Offering__c = co.Id;
        payment.Street__c = '1 Brighton Road';
        payment.Suburb__c = 'Hampton';
        payment.State__c = 'Victoria';
        payment.Postcode__c = '3163';
        payment.Payment_Type__c = 'Credit Card';
        payment.Payment_Status__c = 'Not Processed';
        payment.Send_Receipt__c = false;
        payment.Quantity__c = 1;
        payment.Payment_Amount__c = 100;

        insert payment; 
        return payment; 
    }

    public static ChargentOrders__ChargentOrder__c createChargentOrder(Payment__c payment) {
        ChargentOrders__ChargentOrder__c co = new ChargentOrders__ChargentOrder__c();
        co.Payment__c = payment.Id;
        co.ChargentOrders__Billing_First_Name__c = 'Luigi';
        co.ChargentOrders__Billing_Last_Name__c = 'Longman';
        co.ChargentOrders__Billing_Phone__c = '0433433433';
        co.ChargentOrders__Billing_Email__c = 'luigi.longman@mailinator.com';
        co.ChargentOrders__Billing_Address__c = '1 Toorak Road';
        co.ChargentOrders__Billing_City__c = 'Toorak';
        co.ChargentOrders__Billing_State__c = 'Victoria';
        co.ChargentOrders__Billing_Zip_Postal__c = '3153';
        co.ChargentOrders__Billing_Country__c = 'Australia';
        co.ChargentOrders__Payment_Method__c = 'Credit Card';
        co.ChargentOrders__Subtotal__c = 100;
        
        insert co;
        return co;

    }

    public static ChargentOrders__Payment_Request__c createPaymentRequest(ChargentOrders__ChargentOrder__c co) {
        ChargentOrders__Payment_Request__c paymentRequest = new ChargentOrders__Payment_Request__c();
        paymentRequest.ChargentOrders__ChargentOrder__c = co.Id;
        insert paymentRequest;
        return paymentRequest;
    }

    public static void makePayment(ChargentOrders__Payment_Request__c paymentRequest, ChargentOrders__ChargentOrder__c co) {
        
        ChargentOrders__Transaction__c tran = new ChargentOrders__Transaction__c();
        tran.ChargentOrders__Credit_Card_Name__c = 'Payee Name';
        tran.ChargentOrders__Credit_Card_Number__c = '4111111111111111';
        tran.ChargentOrders__Card_Last_4__c = '1111';
        tran.ChargentOrders__Order__c = co.Id;
        tran.ChargentOrders__Payment_Request__c = paymentRequest.Id;
        //insert tran;

        paymentRequest.ChargentOrders__Status__c = 'paid';
        update paymentRequest;
    }

    public static Map<String,RecordType> getRecordTypes(String obj) {
        //Query for the record types of an object
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType= :obj and isActive=true];
     
        //Create a map between the RecordType Name and RecordType
        Map<String,RecordType> recordTypes = new Map<String,RecordType>{};
        for(RecordType rt: rtypes)
            recordTypes.put(rt.Name,rt);
     
        return recordTypes;
    }
}