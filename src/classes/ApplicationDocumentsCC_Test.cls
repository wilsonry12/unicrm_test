/*******************************************************************************
* @author		Ant Custodio
* @date         25.May.2017        
* @description  test class for ApplicationDocumentsCC
* @revision     
*******************************************************************************/
@isTest
public with sharing class ApplicationDocumentsCC_Test {
	private static Application__c appRecord;
	/*******************************************************************************
	* @author       Ant Custodio
	* @date         25.May.2017        
	* @description  test the application success page
	* @revision     
	*******************************************************************************/
	static testMethod void test_uploadDocuments_AdditionalDocs() {
		//create a community user
		User communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create a new draft application
		appRecord = StudFirst_TestHelper.createApplication(communityUserRec.ContactId);
		appRecord.OwnerId = communityUserRec.Id;
		insert appRecord;

		test.startTest();
		//only run if there is a Domestic Applicant Profile User
		if (communityUserRec != null) {
			System.runAs(communityUserRec) {

				String attachmentChunkId = ApplicationDocumentsCC.saveTheChunk(	appRecord.Id, 
																			'filename.docx', 
																			'testBase64', 
																			'image/jpeg', 
																			'Proof of employment',
																			null, 
																			'test comment','' );

				System.assertEquals (1, [SELECT COUNT() FROM Contact_Document__c], 'Contact_Document__c record not inserted');
				System.assertEquals (1, [SELECT COUNT() FROM Attachment], 'Document not attached');
				System.assertEquals (1, [SELECT COUNT() FROM Application_Document_Provided__c], 'Application_Document_Provided__c record not inserted');

				//recall the method to imitate chunking
				String attachmentId = ApplicationDocumentsCC.saveTheChunk(	appRecord.Id, 
																			'filename.docx', 
																			'testBase64', 
																			'image/jpeg', 
																			'Proof of employment',
																			attachmentChunkId, 
																			'test comment','' );
				System.assertEquals (1, [SELECT COUNT() FROM Attachment WHERE Id =: attachmentId], 'Document not attached');

				//document type not listed
				attachmentId = ApplicationDocumentsCC.saveTheChunk(	appRecord.Id, 
																	'filename.docx', 
																	'testBase64', 
																	'image/jpeg', 
																	'',
																	attachmentChunkId, 
																	'test comment','' );
				System.assertNotEquals('', attachmentId); 

				List<Application_Document_Provided__c> appDocument = 
							new List<Application_Document_Provided__c>([SELECT Id 
																		FROM Application_Document_Provided__c 
																		WHERE Application__c =:appRecord.Id 
																		LIMIT 1]);
				
				String updateAttachId = ApplicationDocumentsCC.saveTheChunk(appRecord.Id, 
																			'filename.docx', 
																			'testBase64', 
																			'image/jpeg', 
																			'Curriculum vitae',
																			null, 
																			'test comment back',
																			appDocument[0].Id);
			}
		}
		test.stopTest();
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         25.May.2017        
	* @description  test the application success page
	* @revision     
	*******************************************************************************/
	static testMethod void test_uploadDocuments_MyQualifications() {
		//create a community user
		User communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create a new draft application
		appRecord = StudFirst_TestHelper.createApplication(communityUserRec.ContactId);
		appRecord.OwnerId = communityUserRec.Id;
		insert appRecord;

		test.startTest();
		//only run if there is a Domestic Applicant Profile User
		if (communityUserRec != null) {
			System.runAs(communityUserRec) {
				//retrieve picklist
				System.assert(!ApplicationDocumentsCC.retrieveDocumentType().isEmpty(), 'Picklist not retrieved');

				String attachmentChunkId = ApplicationDocumentsCC.saveTheChunkv2(	appRecord.Id, 
																			'filename.docx', 
																			'testBase64', 
																			'image/jpeg', 
																			'Proof of employment',
																			null, 
																			'test comment','' );

				System.assertEquals (1, [SELECT COUNT() FROM Contact_Document__c], 'Contact_Document__c record not inserted');
				System.assertEquals (1, [SELECT COUNT() FROM Application_Document_Provided__c], 'Application_Document_Provided__c record not inserted');
				
				String attId = attachmentChunkId.split('-')[0];
				String parentId = attachmentChunkId.split('-')[1];
				
				Attachment attachmentRec = [SELECT Id, ParentId FROM Attachment WHERE Id =: attId];
				System.assertEquals(parentId, attachmentRec.ParentId,'Parent Id is different from the queried one');


				//recall the method to imitate chunking
				String attachmentId = ApplicationDocumentsCC.saveTheChunkv2(	appRecord.Id, 
																			'filename.docx', 
																			'testBase64', 
																			'image/jpeg', 
																			'Proof of employment',
																			attId, 
																			'test comment','' );
				attId = attachmentId.split('-')[0];
				parentId = attachmentId.split('-')[1];
				
				attachmentRec = [SELECT Id, ParentId FROM Attachment WHERE Id =: attId];
				System.assertEquals(parentId, attachmentRec.ParentId,'Parent Id is different from the queried one');
				List<Application_Document_Provided__c> appDocList = ApplicationDocumentsCC.retrieveAppDocuments(appRecord.Id);
				System.assert(!appDocList.isEmpty(), 'Application_Document_Provided__c not found');
				ApplicationDocumentsCC.deleteSelectedAttachment(appDocList[0].Id);
			}
		}
		test.stopTest();
	}
}