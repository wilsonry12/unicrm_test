/*******************************************************************************
* @author       Anterey Custodio
* @date         21.Nov.2016         
* @description  test class for BulkMessagingSystemCC
* @revision     
*******************************************************************************/
@isTest
public class BulkMessagingSystemCC_Test {
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         21.Nov.2016          
    * @description  Page Navigation: idle view - user just accessing the page
    * @revision     
    *******************************************************************************/
    static testMethod void test_Idle() {
        insertEssentialRecords();
        
        Test.startTest();
            //user goes to URL
            PageReference pageRef = Page.BulkMessagingSystem;
            Test.setCurrentPage(pageRef);
            //page calls the controller
            BulkMessagingSystemCC controller = new BulkMessagingSystemCC();
            //delete incomplete campaigns
            controller.flagExistingCampaignToDelete();
            //page calls the picklist values
            controller.getCohortOptions();
            controller.getCriteriaOptions();
            controller.getFilterOptions();
            controller.getContactFieldOptions();
            controller.getUnitEnrolmentOptions();
            controller.getEmailTemplateOptions();
            controller.getStaffFieldOptions();
            controller.getPicklistCriteriaOptions();
            controller.rerenderPage();
            //change pickilst to unit offering
            controller.displayAppropriateFieldSet();
            //go to CSV upload page
            controller.gotoBMSUploadCSV();
        Test.stopTest();
    }
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         21.Nov.2016          
    * @description  Page Navigation: user performs searches
    * @revision     Anterey Custodio 8.Dec.2016 - added staff search
    *******************************************************************************/
    static testMethod void test_Searches() {
        insertEssentialRecords();
        
        Test.startTest();
            //user goes to URL
            PageReference pageRef = Page.BulkMessagingSystem;
            Test.setCurrentPage(pageRef);
            //page calls the controller
            BulkMessagingSystemCC controller = new BulkMessagingSystemCC();
            
            //user checks the help text
            controller.toggleFilterHelpView();
            //turned it off
            controller.toggleFilterHelpView();

            //Scenario 1: no filters (Staff)
            controller.selectedFilter = 'ALLSTAFF';
            controller.searchByFilter();
            
            //Scenario 1.5: no filters
            controller.selectedFilter = 'ALLSTUDENTS';
            controller.searchByFilter();
            system.assertEquals(3, controller.wStudentFirst100List.size());
            
            //Scenario 2: searching ALL students that has an email (Email != '')
            //              And with person id
            controller.selectedFilter = 'ALLSTUDENTS';
            controller.wFilterList[0].filterField = 'Email';
            controller.wFilterList[0].filterCriteria = '!=';
            controller.wFilterList[0].contactRecord.Email = '';
            controller.wFilterList[0].filterCondition = 'AND';
            
            controller.wFilterList[1].filterField = 'Person_ID_unique__c';
            controller.wFilterList[1].filterCriteria = 'LIKE';
            controller.wFilterList[1].filterTextAreaValue = '1234567890\', \'1324567890\', \'1474567890';
            controller.wFilterList[1].filterCondition = 'AND';
            
            controller.addMoreFilters();
            controller.wFilterList[2].filterField = 'Campus__c';
            controller.wFilterList[2].filterCriteria = 'IN';
            controller.wFilterList[2].filterTextAreaValue = 'Caulfield\nClayton';
            controller.wFilterList[2].filterCondition = 'AND';
            
            //adds more filters
            controller.addMoreFilters();
            controller.wFilterList[3].filterField = 'Commencement_Date__c';
            controller.wFilterList[3].filterCriteria = '=';
            controller.wFilterList[3].contactRecord.Commencement_Date__c = Date.newInstance(2016, 10, 10);
            controller.wFilterList[3].filterCondition = 'AND';
            
            //add one more
            controller.addMoreFilters();
            controller.wFilterList[4].filterField = 'Campus__c';
            controller.wFilterList[4].filterCriteria = 'NOT IN';
            controller.wFilterList[4].filterTextAreaValue = 'Berwick\nGippsland';
            //performs search
            controller.searchByFilter();
            system.assertEquals(3, controller.wStudentFirst100List.size());
            
            //called by javascript, select Clayton and Caulfield on the popup
            controller.selectedFieldAPIRowNumber = 2;
            controller.retrieveAppropriateDataType();
            controller.togglePicklistPopup();
            controller.populatePicklistTextArea();
            
            controller.displayAppropriateFieldSet();
            
            //Scenario 3: other student types
            controller.selectedFilter = 'POSTGRAD';
            controller.searchByFilter();
            system.assertEquals(2, controller.wStudentFirst100List.size());

            controller.selectedFilter = 'ALLEXCEPTHDR';
            controller.searchByFilter();
            system.assertEquals(3, controller.wStudentFirst100List.size());

            controller.selectedFilter = 'HDR';
            controller.searchByFilter();
            system.assert(controller.wStudentFirst100List.isEmpty());

            controller.selectedFilter = 'UNDERGRAD';
            controller.searchByFilter();
            system.assertEquals(1, controller.wStudentFirst100List.size());

        Test.stopTest();
    }
    
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         21.Nov.2016          
    * @description  Page Navigation: user performs complex filters
    * @revision     
    *******************************************************************************/
    static testMethod void test_AndOrCriteria() {
        insertEssentialRecords();
        
        Test.startTest();
            //user goes to URL
            PageReference pageRef = Page.BulkMessagingSystem;
            Test.setCurrentPage(pageRef);
            //page calls the controller
            BulkMessagingSystemCC controller = new BulkMessagingSystemCC();

            //Scenario 1: searching complex criteria using ANDs and ORs
            controller.selectedFilter = 'ALLSTUDENTS';
            controller.wFilterList[0].filterField = 'Email';
            controller.wFilterList[0].filterCriteria = '!=';
            controller.wFilterList[0].contactRecord.Email = '';
            controller.wFilterList[0].filterCondition = 'OR';
            controller.wFilterList[1].filterField = 'Person_ID_unique__c';
            controller.wFilterList[1].filterCriteria = 'LIKE';
            controller.wFilterList[1].filterTextAreaValue = '1234567890, 1324567890, 1474567890';
            controller.wFilterList[1].filterCondition = 'AND';
            controller.addMoreFilters();
            controller.wFilterList[2].filterField = 'Course_Code__c';
            controller.wFilterList[2].filterCriteria = '=';
            controller.wFilterList[2].contactRecord.Course_Code__c = '1123';
            //adds more filters
            controller.addMoreFilters();
            controller.wFilterList[2].filterCondition = 'OR';
            controller.wFilterList[3].filterField = 'Commencement_Date__c';
            controller.wFilterList[3].filterCriteria = '=';
            controller.wFilterList[3].contactRecord.Commencement_Date__c = Date.newInstance(2016, 10, 10);
            //performs search
            controller.searchByFilter();
            system.assert(controller.wStudentFirst100List.isEmpty());
            
            //user will see a new text field and update it
            controller.filterLogic = '(1 OR 2) AND (3 OR 4)';
            //performs the search again
            controller.searchByFilter();
            system.assert(!controller.wStudentFirst100List.isEmpty());

        Test.stopTest();
    }
    
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         21.Nov.2016          
    * @description  Page Navigation: user performs unit offering searches
    * @revision     
    *******************************************************************************/
    static testMethod void test_UnitOfferingSearches() {
        insertEssentialRecords();

        Test.startTest();
            //user goes to URL
            PageReference pageRef = Page.BulkMessagingSystem;
            Test.setCurrentPage(pageRef);
            //page calls the controller
            BulkMessagingSystemCC controller = new BulkMessagingSystemCC();
            
            //Scenario 1: no filters
            controller.selectedFilter = 'UNITOFFERING';
            controller.searchByFilter();
            //system.assertEquals(3, controller.wStudentFirst100List.size());
            
            //Scenario 2: 2 filters
            controller.selectedFilter = 'UNITOFFERING';
            controller.wFilterList[0].filterField = 'Name';
            controller.wFilterList[0].filterCriteria = '!=';
            controller.wFilterList[0].unitEnrolmentRecord.UNIT_CD__c = '';
            controller.wFilterList[0].filterCondition = 'AND';
            controller.wFilterList[1].filterField = 'SUA_LOCATION_CD__c';
            controller.wFilterList[1].filterCriteria = 'LIKE';
            controller.wFilterList[1].filterTextAreaValue = 'Clayton \n Caulfield';
            controller.searchByFilter();
            //system.assertEquals(3, controller.wStudentFirst100List.size());
            
            //add new campaign
            controller.toggleSendEmailView();
            controller.newCampaignNameStr = 'Campaign ABC v1';
            //add additional emails and notify
            controller.massEmailRecord.Additional_Email__c = 'ant.monash@monash.edx; test.data@monash.edx; test.sample@monash.edx';
            controller.massEmailRecord.Notify__c = 'ant.monash@monash.edx test.data@monash.edx test.sample@monash.edx';

            //user adds an email body
            controller.draftEmail();
            system.assertEquals('', controller.errorMessage);
            //assert if a CRM Mass email record is created
            //system.assertEquals(1, [SELECT COUNT() FROM Campaign]);
        Test.stopTest();
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         21.Nov.2016          
    * @description  Page Navigation: user creates an email
    * @revision     
    *******************************************************************************/
    static testMethod void test_DraftEmail() {
        insertEssentialRecords();
        
        Test.startTest();
            //user goes to URL
            PageReference pageRef = Page.BulkMessagingSystem;
            Test.setCurrentPage(pageRef);
            //page calls the controller
            BulkMessagingSystemCC controller = new BulkMessagingSystemCC();
            
            //Scenario 1: simple filters
            controller.selectedFilter = 'ALLSTUDENTS';
            controller.searchByFilter();
            system.assertEquals(3, controller.wStudentFirst100List.size());

            //add new campaign
            controller.toggleSendEmailView();
            controller.newCampaignNameStr = 'Campaign ABC v1';
            //add additional emails and notify
            controller.massEmailRecord.Additional_Email__c = 'ant.monash@monash.edx; test.data@monash.edx; test.sample@monash.edx';
            controller.massEmailRecord.Notify__c = 'ant.monash@monash.edx test.data@monash.edx test.sample@monash.edx';

            //user adds an email body
            controller.draftEmail();
            system.assertEquals('', controller.errorMessage);
            //assert if a CRM Mass email record is created
            //system.assertEquals(1, [SELECT COUNT() FROM Campaign]);
            
        Test.stopTest();
    }
    
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         21.Nov.2016          
    * @description  Page Navigation: user creates an email
    * @revision     
    *******************************************************************************/
    static testMethod void test_Errors() {
        insertEssentialRecords();
        
        Test.startTest();
            //user goes to URL
            PageReference pageRef = Page.BulkMessagingSystem;
            Test.setCurrentPage(pageRef);
            //page calls the controller
            BulkMessagingSystemCC controller = new BulkMessagingSystemCC();
            
            //insert a new campaign
            Campaign campaignToDelete = new Campaign(Name = 'Delete Me');
            insert campaignToDelete;
            
            //delete incomplete campaigns
            controller.flagExistingCampaignToDelete();
            
            //Scenario 1: no filter
            controller.selectedFilter = 'ALLSTUDENTS';
            controller.searchByFilter();
            system.assertEquals(3, controller.wStudentFirst100List.size());
            
            //Scenario 2: invalid search (Unit Offering)
            controller.selectedFilter = 'UNITOFFERING';
            //blank first row
            controller.wFilterList[0].filterField = 'NONE';
            controller.wFilterList[0].filterCriteria = '';
            controller.wFilterList[0].filterCondition = 'AND';
            
            controller.wFilterList[1].filterField = 'UNIT_CD__c';
            controller.wFilterList[1].filterCriteria = '!=';
            controller.wFilterList[1].unitEnrolmentRecord.UNIT_CD__c = '';
            controller.searchByFilter();
            system.assertNotEquals('', controller.errorMessage);
            
            //Scenario 3: invalid search
            controller.selectedFilter = 'ALLSTUDENTS';
            controller.wFilterList[1].filterField = 'Faculty__c';
            controller.wFilterList[1].filterCriteria = '!=';
            controller.wFilterList[1].contactRecord.Faculty__c = '';
            controller.searchByFilter();
            system.assertNotEquals('', controller.errorMessage);
            
            //Scenario 4: using LIKE on date
            controller.selectedFilter = 'ALLSTUDENTS';
            controller.wFilterList[0].filterField = 'Commencement_Date__c';
            controller.wFilterList[0].filterCriteria = 'LIKE';
            controller.wFilterList[0].filterTextAreaValue = '10-10-2016';
            controller.searchByFilter();
            system.assertNotEquals('', controller.errorMessage);
            
            //Scenario 5: invalid campaign
            controller.toggleSendEmailView();
            controller.newCampaignNameStr = '';
            //add invalid additional emails and notify
            controller.massEmailRecord.Additional_Email__c = 'ant.monash; test.data@monash.edx; test.sample@monash.edx';
            controller.massEmailRecord.Notify__c = 'ant.monash; test.data@monash.edx test.sample@monash.edx';
            //add invalid from and reply to
            controller.massEmailRecord.From__c = 'ant.monash;';
            controller.massEmailRecord.Reply_To__c = 'ant.monash;';
            controller.draftEmail();
            system.assertNotEquals('', controller.errorMessage);

            //Scenario 6: blank email
            controller.massEmailRecord.From__c = '';
            controller.massEmailRecord.Reply_To__c = '';
            controller.draftEmail();
            system.assertNotEquals('', controller.errorMessage);
            
        Test.stopTest();
    }
    
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         21.Nov.2016          
    * @description  private method that inserts essential records
    * @revision     
    *******************************************************************************/
    static testMethod void insertEssentialRecords () {
        //insert sample Contacts
        List<Contact> contactRecordList = new List<Contact>();
        for (Contact contactRec: BMS_TestDataHelper.buildSampleContacts()) {
            contactRec.Commencement_Date__c = Date.newInstance(2016, 10, 10);
            contactRec.Campus__c = 'Caulfield';
            contactRecordList.add(contactRec);
        }
        
        insert contactRecordList;
        system.assertEquals(3, contactRecordList.size());
        
        Campaign crmMassEmailRecord = BMS_TestDataHelper.buildSampleCRMMassEmail('Student Communication');
        crmMassEmailRecord.Status = 'Aborted';
        insert crmMassEmailRecord;
        
        /*Course__c courseRecord = BMS_TestDataHelper.buildSampleCourse();
        insert courseRecord;*/
        
        /*List<Course_Unit__c> courseUnitRecord = BMS_TestDataHelper.buildSampleCourseUnit(2, null);
        insert courseUnitRecord;*/
        
        List<Unit_Enrolment__c> unitEnrolmentToInsertList = new List<Unit_Enrolment__c>();
        
        for (Contact contactRecord: [SELECT Id FROM Contact WHERE Id IN: contactRecordList]) {

            Unit_Enrolment__c unitEnrolmentToInsert = BMS_TestDataHelper.buildSampleUnitEnrolment(contactRecord.Id);
            unitEnrolmentToInsertList.add(unitEnrolmentToInsert);
        }

        if (!unitEnrolmentToInsertList.isEmpty()) {
            insert unitEnrolmentToInsertList;
        }
        
        system.assert(!unitEnrolmentToInsertList.isEmpty());
    }
}