/*******************************************************************************
* @author       Anterey Custodio
* @date         17.Nov.2016         
* @description  Controller class for BMSEmailReview page
* @revision     
*******************************************************************************/
public with sharing class BMSEmailReviewCC {
    public List<wCRMMassEmail> wMassEmailList {get;set;}
    public String selectedMassEmailId {get;set;}
    public String selectedEmailTempId {get;set;}
    public Boolean hasOnHold {get;set;}
    public String currentUserProfile {get;set;}
    private final String currentUserId = userinfo.getUserId();
    public String batchNotComplete {get;set;}

    public Map<String, EmailTemplate> templates {get;set;}
    public String selectedMassEmailSendType {get;set;}
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         17.Nov.2016         
    * @description  Constructor
    * @revision     
    *******************************************************************************/
    public BMSEmailReviewCC() {
        hasOnHold = false;
        batchNotComplete = 'Currently adding recipients...';
        selectedMassEmailId = '';
        selectedMassEmailSendType = '';
        currentUserProfile = [  SELECT Id, Name FROM Profile WHERE Id =: userinfo.getProfileId() ].Name;
        wMassEmailList = retrieveOwnedCRMMassEmails();

        Folder bmsFolder = [SELECT DeveloperName,Id,Name,Type FROM Folder WHERE Name = 'BMS Emails'];
        templates = new Map<String, EmailTemplate>();

        for(EmailTemplate temp: [SELECT Id, HtmlValue, Name, FolderId FROM EmailTemplate WHERE FolderId =:bmsFolder.Id]){
            templates.put(temp.Name, temp);
        }
    }
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         17.Nov.2016         
    * @description  retrieve Mass Emails owned by current user
    * @revision     Anterey Custodio - 9.Feb.2017 - Updated SOQL to show only statuses
                        of 'On Hold', 'Approved', and 'Cancelled'
                    Anterey Custodio - 20.Feb.2017 - Reduced the LIMIT from 500 to 200
                        - removed unused fields on the SOQL to prevent veiw state errors
                    ACUSTODIO, 02.Mar.2017 - added SMS body and Send Type fields
    *******************************************************************************/
    public List<wCRMMassEmail> retrieveOwnedCRMMassEmails() {
        List<wCRMMassEmail> wCrmMassEmailList = new List<wCRMMassEmail>();
        String queryString = '  SELECT  Id, Name, SMS_Count__c, Status, Total_Members__c, ' + 
                            '           Owner_Name__c, CreatedDate, Email_Template__c, ' +
                            '           Batch_Finished__c, SMS_Body__c, Type ' +
                            '   FROM    Campaign '+
                            '   WHERE   (Status = \'On Hold\' OR Status = \'Approved\' OR Status = \'Cancelled\') ';
        
        if (currentUserProfile != 'BMS Admin' && currentUserProfile != 'System Administrator') {
            queryString += '    AND     OwnerId = \''+currentUserId+'\' ';
        }
        
        //add the limit to prevent view state error (only get the last 200 created)
        queryString += '        ORDER BY CreatedDate DESC LIMIT 200';

        for (Campaign crmMassEmailRecord: Database.query(queryString)) {
            if (!hasOnHold && crmMassEmailRecord.Status == 'On Hold') {
                hasOnHold = true;
            }
            wCRMMassEmail wMassEmailToInsert = new wCRMMassEmail(crmMassEmailRecord);
            if(crmMassEmailRecord.Type == 'Email'){
                if (!crmMassEmailRecord.Batch_Finished__c) {
                    wMassEmailToInsert.selectedStatus = batchNotComplete;
                }
            }
            wCrmMassEmailList.add(wMassEmailToInsert);
        }
        return wCrmMassEmailList;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         17.Nov.2016         
    * @description  button method to set all the mass email's status to approve
    * @revision     
    *******************************************************************************/
    public void approveAllEmails () {
        for (wCRMMassEmail wCRMMassEmail: wMassEmailList) {
            if (wCRMMassEmail.crmMassEmailRecord.Status == 'On Hold') {
                wCRMMassEmail.selectedStatus = 'APPROVE';
            }
        }
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         17.Nov.2016         
    * @description  button method to set all the mass email's status to cancel
    * @revision     
    *******************************************************************************/
    public void cancelAllEmails () {
        for (wCRMMassEmail wCRMMassEmail: wMassEmailList) {
            if (wCRMMassEmail.crmMassEmailRecord.Status == 'On Hold') {
                wCRMMassEmail.selectedStatus = 'CANCEL';
            }
        }
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         17.Nov.2016         
    * @description  updates the CRM Mass Email record status
    * @revision     
    *******************************************************************************/
    public void updateCRMMassEmailStatus () {
        List<Campaign> crmMassEmailToUpdate = new List<Campaign>();
        List<Campaign> approvedCRMMassEmail = new List<Campaign>();
        
        Map<String, Boolean> campaignApprovalMap = new Map<String, Boolean>();
        
        for (wCRMMassEmail wCRMMassEmail: wMassEmailList) {
            if (wCRMMassEmail.selectedStatus != 'NONE' && wCRMMassEmail.crmMassEmailRecord.Status == 'On Hold') {
                if (wCRMMassEmail.selectedStatus == 'APPROVE') {
                    wCRMMassEmail.crmMassEmailRecord.Status = 'Approved';
                    
                    approvedCRMMassEmail.add(wCRMMassEmail.crmMassEmailRecord);
                } else if (wCRMMassEmail.selectedStatus == 'CANCEL') {
                    wCRMMassEmail.crmMassEmailRecord.Status = 'Cancelled';
                }
                crmMassEmailToUpdate.add(wCRMMassEmail.crmMassEmailRecord);
            }
        }
        
        SavePoint savePointDML = Database.setSavepoint();
        try {
            if (!crmMassEmailToUpdate.isEmpty()) {
                update crmMassEmailToUpdate;
            }
            
            //send email to additonal recipients when approved
            if (!approvedCRMMassEmail.isEmpty()) {
                //consolidate Email and SMS message
                List<String> campaignIds = new List<String>();
                List<Campaign> massEmail = new List<Campaign>();
                Set<Id> setSMS = new Set<Id>();

                for(Campaign crm: approvedCRMMassEmail){
                    if(crm.Type == 'SMS'){
                        setSMS.add(crm.Id);
                    }
                    else{
                        campaignIds.add(crm.Id);
                        massEmail.add(crm);
                    }
                }

                if(campaignIds.size() > 0){
                    CommonUtilities.sendBMSEmail(massEmail);
                    //call new Queueable batch to update campaign Member status to "Ready to Send"
                    BMSUpdateCampaignMemStatusQueueable campaignMemberJob = new BMSUpdateCampaignMemStatusQueueable(campaignIds);
                    Id jobID = System.enqueueJob(campaignMemberJob);
                }
                
                System.debug('****SET SMS Ids: '+setSMS);
                Map<String, String> mapSMS = new Map<String, String>();
                for(Attachment att: [SELECT Id, ParentId FROM Attachment WHERE ParentId IN:setSMS]){
                    mapSMS.put(att.Id, att.ParentId);
                }

                System.debug('****MAP SMS: '+mapSMS);
                if(mapSMS.size() >0){
                    for(String str: mapSMS.keySet()){
                        BMSSendSMSQueueable campaignMemberJob = new BMSSendSMSQueueable(str, mapSMS.get(str));
                        Id jobID = System.enqueueJob(campaignMemberJob);
                    }
                }

            }
            
            String apexMessage = 'The messages has been updated successfully';
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, apexMessage);
            ApexPages.addMessage(myMsg);
            
        } catch (Exception ex) {
            //rollback the insertion
            Database.rollback(savePointDML);
            String errorMessage = 'There seems to be a problem on the page. Please contact the administrator.'+ex.getLineNumber()+ex.getCause()+ex.getStackTraceString();
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage);
            ApexPages.addMessage(myMsg);
        }
        
    }
    
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         17.Nov.2016         
    * @description  button method to set all the mass email's status to cancel
    * @revision     
    *******************************************************************************/
    public PageReference previewEmailRecord () {
        PageReference redirect = Page.BMSEmailPreview;
        redirect.getParameters().put('crmId', selectedMassEmailId);
        redirect.getParameters().put('view', 'EmailView');
        if (selectedMassEmailSendType == '' || selectedMassEmailSendType == 'email') {
            selectedMassEmailSendType = 'email';
            redirect.getParameters().put('etId', templates.get(selectedEmailTempId).Id);
        }
        redirect.getParameters().put('type', selectedMassEmailSendType);
        return redirect;
    }
    

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         7.Oct.2016         
    * @description  gives the list of Mass Email Record Status
    * @revision     
    *******************************************************************************/
    public List<SelectOption> getStatusOptions() {
        List<SelectOption> statusOptions = new List<SelectOption>();
        statusOptions.add(new SelectOption('NONE', '--None--'));
        statusOptions.add(new SelectOption('APPROVE', 'Approve'));
        statusOptions.add(new SelectOption('CANCEL', 'Cancel'));
        return statusOptions;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         31.Oct.2016         
    * @description  wrapper class for Contacts
    * @revision     
    *******************************************************************************/
    public class wCRMMassEmail {
        public Boolean isReadOnly {get;set;}
        public String selectedStatus {get;set;}
        public Campaign crmMassEmailRecord {get;set;}
        /*******************************************************************************
        * @author       Anterey Custodio
        * @date         31.Oct.2016         
        * @description  constructor class to create corresponding contacts 
                            with isReadOnly = false
        * @revision     
        *******************************************************************************/
        public wCRMMassEmail(Campaign crmMassEmailRecordInit) {
            isReadOnly = false;
            selectedStatus = 'NONE';
            crmMassEmailRecord = crmMassEmailRecordInit;
        }
    }
}