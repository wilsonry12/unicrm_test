/*******************************************************************************
* @author       Anterey Custodio
* @date         23.Jun.2017
* @description  Process Builder Class for Application. This contains all the 
					invoked methods that can be called on the Process Builder
* @revision     
*******************************************************************************/
public with sharing class StudFirst_ApplicationPBC {
	/*******************************************************************************
	* @author       Anterey Custodio
	* @date         23.Jun.2017
	* @description  method that creates and links one Assessment Plan for each
	*					Application Course Preference of an Application.
	*				Assessment Plans linked to Application Course Preferences were
	*					clones of an Assessment Plan template related to the Course
	*					Offering record
	* @revision     
	*******************************************************************************/
	@InvocableMethod
	public static void createAssessmentPlansForEachCoursePrefs(List<String> applicationIds) {
		CloneAssessmentPlanTempQueueable cloneQueueableJob = new CloneAssessmentPlanTempQueueable(applicationIds);
		System.enqueueJob(cloneQueueableJob);
	}
}