/**************************************************
* Author: Ryan Wilson
* Date: 07.APR.2017
* Description: This class maintains the response data model for Admission applications.
***************************************************/
public with sharing class StudFirst_AdmissionApplicationResponse {
    
    public List<Application> applications;

    public class Application {
        public boolean success;
        public boolean isNewApplicant;
        public String externalApplicantId;
        public String smsApplicantId;
        public List<SmsApplication> smsApplications;
        public List<String> info;
        public List<String> errors;
    }

    public class SmsApplication {
        public Integer applicationId;
        public String updatedDate;
        public boolean isNewApplication;
        public List<CoursePreference> coursePreferences;
    }

    public class CoursePreference {
        public String transferStatus;
        public Integer preferenceNumber;
        public String courseCode;
        public Integer courseVersionNumber;
        public String locationCode;
        public String attendanceMode;
        public String attendanceType;
        public String academicCalendarType;
        public Integer academicCiSequenceNumber;
        public String admissionCalendarType;
        public Integer admissionCiSequenceNumber;
        public String typeOfPlaceCode;
    }

}