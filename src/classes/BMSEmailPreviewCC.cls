/*******************************************************************************
* @author       Anterey Custodio
* @date         10.Nov.2016         
* @description  Controller class for BMSEmailPreview page
* @revision     
*******************************************************************************/
public class BMSEmailPreviewCC {
    //public variables used on page
    public String emailTemplateId {get;set;}
    public String massEmailRecordId {get;set;}
    public String htmlBody {get;set;}
    public String viewMode {get;set;}
    public Boolean showActions {get;set;}
    
    public EmailTemplate emailTemplateRecord {get;set;}
    public List<Campaign> massEmailRecord {get;set;}
    public Campaign crmCampaign {get;set;}
    public Boolean isBatchFinished {get;set;}

    //ACUSTODIO 02.Mar.2017 - String to determine if the user wants SMS or Email
    public String sendType {get;set;}
    public Boolean hasError {get;set;}

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         10.Nov.2016        
    * @description  Constructor class to initialise variables and methods
    * @revision     ACUSTODIO, 02.Mar.2017 - updated to retrieve sendType parameter
                        which is used to determine if the user wants to send SMS or email
    *******************************************************************************/
    public BMSEmailPreviewCC() {
        String errorMessage = '';
        hasError = false;
        massEmailRecordId = ApexPages.currentPage().getParameters().get('crmId');
        emailTemplateId = ApexPages.currentPage().getParameters().get('etId');
        viewMode = ApexPages.currentPage().getParameters().get('view');
        sendType = CommonUtilities.retrieveSendType();

        showActions = true;
        
        try {
            system.debug('@@@BMSEmailPreviewCC');
            //retrieve the mass email record
            massEmailRecord = retrieveMassEmailRecord(massEmailRecordId);
            //ACUSTODIO, 02.Mar.2017 - added checking if email record actually retrieved a value to prevent errors
            if (!massEmailRecord.isEmpty()) {
                if(massEmailRecord[0].Status == 'Approved') {
                    showActions = false;
                }
                
                //if sendtype is email, construct the email body
                if (sendType == 'sms') {
                    isBatchFinished = true;
                }
                else{
                    system.debug('@@@sendType email');
                    if (emailTemplateId != null) {
                       htmlBody = constructHTMLBody(massEmailRecord);
                    } else {
                        hasError = true;
                    }
                

                    isBatchFinished = checkIfBatchFinished(massEmailRecord[0]);
                    if (!isBatchFinished) {
                        String apexMessage = '"Save and Send" is disabled because we are currently adding recipients.';
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, apexMessage);
                        ApexPages.addMessage(myMsg);
                    }
                }
            } else {
                hasError = true;
            }
        } catch (Exception ex) {
            
        }

        if (hasError) {
            errorMessage = 'There seems to be a problem on the page. Please contact the administrator.';
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage);
            ApexPages.addMessage(myMsg);
        }
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         02.Mar.2017         
    * @description  constructs the HTML email body
    * @revision     
    *******************************************************************************/
    private String constructHTMLBody (List<Campaign> massEmailRecord) {
        system.debug('@@@constructHTMLBody');
        String emailHTMLBody ='';
        if(viewMode == 'EmailView'){
            //retrieve the email template
            emailTemplateRecord = retrieveEmailTemplateRecord(emailTemplateId);
            emailHTMLBody = emailTemplateRecord.HtmlValue;

            if (massEmailRecord[0].Name != null) {
                emailHTMLBody = emailHTMLBody.replace('[SUBJECT]', massEmailRecord[0].Name);
            }
            if (massEmailRecord[0].Message_Header__c != null) {
                emailHTMLBody = emailHTMLBody.replace('[MESSAGE_HEADER]', massEmailRecord[0].Message_Header__c);
            }
            if (massEmailRecord[0].Custom_Message__c != null) {
                emailHTMLBody = emailHTMLBody.replace('[CAMPAIGN_MESSAGE]', massEmailRecord[0].Custom_Message__c);
            } else {
                emailHTMLBody = emailHTMLBody.replace('[CAMPAIGN_MESSAGE]', '');
            }
            
            if(emailTemplateRecord.Name.contains('Student')){
                emailHTMLBody = emailHTMLBody.replace('[CONTACT_NAME]', 'Student');
            } else if (emailTemplateRecord.Name.contains('Staff')) {
                emailHTMLBody = emailHTMLBody.replace('[CONTACT_NAME]', 'Colleague');
            }
        }
        return emailHTMLBody;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         02.Mar.2017         
    * @description  retrieves the mass email record
    * @revision     
    *******************************************************************************/
    private List<Campaign> retrieveMassEmailRecord (String massEmailRecordId) {
        List<Campaign> massEmailRetrieved = new List<Campaign>();
        if (massEmailRecordId != null) {
            massEmailRetrieved = [  SELECT  Id, Batch_Finished__c, OwnerId, Send_Date_Time__c, Owner.Name, 
                                            Status, Custom_Message__c, 
                                            Email_Body__c, From__c, Notify__c, 
                                            Message_Header__c, Additional_Email__c, 
                                            Reply_To__c, Name, HTML_Email__c, 
                                            Email_Template__c, SMS_Body__c   
                                    FROM    Campaign
                                    WHERE   Id =: massEmailRecordId
                                    LIMIT   1   ];
        }
        return massEmailRetrieved;
    }

    
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         02.Mar.2017         
    * @description  retrieves the campaign record
    * @revision     
    *******************************************************************************/
    private EmailTemplate retrieveEmailTemplateRecord (String emailTemplateId) {
        EmailTemplate emailTemplateRecord = new EmailTemplate();
        List<EmailTemplate> templateRetrieved = new List<EmailTemplate>();
        if (emailTemplateId != null) {
            templateRetrieved = [   SELECT  Id, HtmlValue, Name 
                                    FROM    EmailTemplate 
                                    WHERE   Id =: emailTemplateId];
        }
        if (!templateRetrieved.isEmpty()) {
            emailTemplateRecord = templateRetrieved[0];
        }
        return emailTemplateRecord;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         17.Nov.2016         
    * @description  method to edit the email
    * @revision     
    *******************************************************************************/
    public PageReference editEmail() {
        PageReference redirect = Page.BMSEditEmailBody;
        redirect.getParameters().put('crmId',massEmailRecordId);
        redirect.getParameters().put('type',sendType);
        return redirect;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         15.Nov.2016         
    * @description  method to go back to page
    * @revision     
    *******************************************************************************/
    public PageReference cancel() {
        PageReference redirect = gotoBulkMessagingHome(false);
        return redirect;
    }

    public PageReference saveSend() {
        if(sendtype == 'sms'){
            massEmailRecord[0].Status = 'Approved';
            update massEmailRecord;
            
            Attachment attachmentRec = [SELECT Id, ParentId FROM Attachment WHERE ParentId =: massEmailRecord[0].Id];
            BMSSendSMSQueueable campaignMemberJob = new BMSSendSMSQueueable(attachmentRec.Id, massEmailRecord[0].Id);
            Id jobID = System.enqueueJob(campaignMemberJob);
        }
        else{
           massEmailRecord[0].HTML_Email__c = htmlBody;

            if(massEmailRecord[0].Custom_Message__c != null){
                String strEmailBody = massEmailRecord[0].Custom_Message__c;
                massEmailRecord[0].Custom_Message__c = strEmailBody;
            }

            if(massEmailRecord[0].Batch_Finished__c){
                massEmailRecord[0].Status = 'Approved';
            }
            update massEmailRecord;
            CommonUtilities.sendBMSEmail(massEmailRecord);

            List<String> campaignIds = new List<String>();
            campaignIds.add(massEmailRecord[0].Id);
            BMSUpdateCampaignMemStatusQueueable campaignMemberJob = new BMSUpdateCampaignMemStatusQueueable(campaignIds);
            Id jobID = System.enqueueJob(campaignMemberJob);
        }
        
        PageReference redirect = Page.BMSEmailReview;
        return redirect;
    }

    public PageReference saveHold() {
        massEmailRecord[0].Status = 'On Hold';
        massEmailRecord[0].HTML_Email__c = htmlBody;

        update massEmailRecord;

        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Saved Succesfully');
        ApexPages.addMessage(myMsg);

        return null;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         15.Nov.2016         
    * @description  method to redirect back to the home page
    * @revision     
    *******************************************************************************/
    private PageReference gotoBulkMessagingHome (Boolean isUpdateEnabled) {
        PageReference redirect = Page.BMSHome; 
        //only update when boolean is true
        if (isUpdateEnabled) {
            update massEmailRecord;
        }
        //only put id if mass email record is not null
        /*if (massEmailRecord.Id != null) {
            redirect.getParameters().put('crmId',massEmailRecord.Id);
        }*/
        return redirect;
    }
    
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         15.Nov.2016         
    * @description  method to check if the batch is finished or not
    * @revision     
    *******************************************************************************/
    private Boolean checkIfBatchFinished (Campaign campaignToCheck) {
        Boolean isFinished = true; 
        if (!campaignToCheck.Batch_Finished__c) {
            isFinished = false;
        }
        return isFinished;
    }
}