@isTest
private class CoursePreferenceCC_Test {
	private static Application__c appRecord;
	private static User communityUserRec;
	private static Contact contactRec;
	private static Application_Course_Preference__c coursePrefA;
	private static Application_Course_Preference__c coursePrefB;
	private static Course_Offering__c courseOfferingRec;
	private static Course_Offering__c courseOfferingRecNoUnit;

	@isTest static void test_SearchCourse() {
		//create a community user
		communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create required records
		createSampleData(communityUserRec.ContactId, 'Draft', 'TEST-STATUS', false);

		test.StartTest();
			//only run if there is a Domestic Applicant Profile User
			if (communityUserRec != null) {
				System.runAs(communityUserRec) {
					CoursePreferenceCC coursePref = new CoursePreferenceCC();

					List<String> attendanceType = CoursePreferenceCC.retrieveAttendanceType();
					System.assert(!attendanceType.isEmpty());

					List<String> location = CoursePreferenceCC.retrieveLocation('M00001');
					System.assert(!location.isEmpty());

					List<String> commencement = CoursePreferenceCC.retrieveCommencementPeriod('M00001');
					System.assert(!commencement.isEmpty());

					List<Course_Offering__c> courses = CoursePreferenceCC.findCourses('M00001', appRecord.Id, 'FULL TIME', 'CLAYTON', 'January-2019', 1, 1);

					Integer coursesCount = CoursePreferenceCC.retrieveCoursesTotalRecord('M00001', appRecord.Id, 'FULL TIME', 'CLAYTON', 'January-2019');

					List<Course_Offering__c> coursesWithUnits = CoursePreferenceCC.retrieveCoursesWithUnitSets('M00001', appRecord.Id, 'FULL TIME', 'CLAYTON', 'January-2019');

					//List<String> strParamA = new List<String>();
					//strParamA.add(courseOfferingRec.Id);

					String strParamA = '["'+courseOfferingRecNoUnit.Id+'"]';
					String strParamB = '["'+courseOfferingRec.Id+'"]';

					List<Application_Course_Preference__c> insertedAppCourses = CoursePreferenceCC.addCourse(strParamA, strParamA, appRecord.Id);
				
				}
			}
		test.StopTest();
	}
	
	@isTest static void test_ReorderCourse() {
		//create a community user
		communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create required records
		createSampleData(communityUserRec.ContactId, 'Draft', 'TEST-STATUS', true);

		test.StartTest();
			//only run if there is a Domestic Applicant Profile User
			if (communityUserRec != null) {
				System.runAs(communityUserRec) {
					CoursePreferenceCC coursePref = new CoursePreferenceCC();

					List<Application_Course_Preference__c> reorderCourse = CoursePreferenceCC.reOrderAppCourses(coursePrefA.Id, 'down');
					String returnValue = CoursePreferenceCC.deleteSelectedCourse(coursePrefB.Id, appRecord.Id);
				}
			}
		test.StopTest();
	}

	static void createSampleData (String contactId, String status, String outcome, Boolean addCourse) {
		//create a new draft application
		appRecord = StudFirst_TestHelper.createApplication(contactId);
		appRecord.OwnerId = communityUserRec.Id;
		appRecord.Status__c = status;
		insert appRecord;

		contactRec = [SELECT Id FROM Contact WHERE Id =: contactId];
		contactRec.Residency_Status__c = 'DOM-AUS';
		update contactRec;

		//Tertiary institution & qualification
		Institution__c tertInsRec = StudFirst_TestHelper.createTertiaryInstitution();
		insert tertInsRec;

		Callista_Outcome_Status__c outcomeStatus = StudFirst_TestHelper.createOutcomeStatus();
		insert outcomeStatus;

		Contact_Qualification__c conQualRec = new Contact_Qualification__c();
		conQualRec.Country__c = 'Australia';
		conQualRec.First_Year_Enrolled__c = String.valueOf(date.today().year()-4);
		conQualRec.Last_Year_Enrolled__c = String.valueOf(date.today().year());
		conQualRec.Other_Qualification__c = 'Certificate III';
		conQualRec.Institution_Code__c = tertInsRec.Institution_Code__c;
		conQualRec.Institution_Name__c = tertInsRec.Institution_Name__c;
		conQualRec.Status__c = 'CURRENTLY STUDYING';
		conQualRec.Contact__c = contactRec.Id;
		conQualRec.RecordTypeId = Schema.SObjectType.Contact_Qualification__c.getRecordTypeInfosByName().get('Tertiary Education').getRecordTypeId();
		insert conQualRec;

		Application_Qualification_Provided__c aqpRecord = new Application_Qualification_Provided__c();
		aqpRecord.Application__c = appRecord.Id;
		aqpRecord.Contact_Qualification__c = conQualRec.Id;
		insert aqpRecord;

		Work_Experience__c workExpRecord = new Work_Experience__c();
		workExpRecord = StudFirst_TestHelper.populateWorkExperienceRecord(workExpRecord);
		workExpRecord.Contact__c = contactRec.Id;
		insert workExpRecord;

		Contact_Document__c contactDocu = new Contact_Document__c();
		contactDocu = StudFirst_TestHelper.createContactDocument(contactRec.Id);
		insert contactDocu;

		Application_Work_Experience_Provided__c aweRecord = new Application_Work_Experience_Provided__c();
		aweRecord.Application__c = appRecord.Id;
		aweRecord.Work_Experience__c = workExpRecord.Id;
		insert aweRecord;

		Course__c courseRec = StudFirst_TestHelper.createCourse();
		insert courseRec;

		courseOfferingRec = StudFirst_TestHelper.createCourseOffering(courseRec);
		insert courseOfferingRec;

		courseOfferingRecNoUnit = StudFirst_TestHelper.createCourseOfferingNoUnitSet(courseRec);
		insert courseOfferingRecNoUnit;

		if(addCourse){
			coursePrefA = StudFirst_TestHelper.createCoursePreferences(appRecord, contactRec, courseOfferingRec, courseRec);
			coursePrefA.Outcome_Status__c = outcome;
			coursePrefA.Offer_Response_Status__c = outcome;
			coursePrefA.Preference_Number__c = '1';
			insert coursePrefA;

			coursePrefB = StudFirst_TestHelper.createCoursePreferences(appRecord, contactRec, courseOfferingRecNoUnit, courseRec);
			coursePrefB.Outcome_Status__c = outcome;
			coursePrefB.Offer_Response_Status__c = outcome;
			coursePrefB.Preference_Number__c = '2';
			insert coursePrefB;
		}
		
	}
	
}