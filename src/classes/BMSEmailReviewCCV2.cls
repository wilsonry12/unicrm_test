/**
* @author           Nick Guia
* @date             02/08/2017
* @description      - Apex Controller for BMSEmailReviewV2 page
* @revision
*
**/
global with sharing class BMSEmailReviewCCV2 {

    //public variables
    public List<String> campaignStatusList {
        get {
            return BMSUtils.getCampaignStatusList();
        }
    }

    public List<String> campaignTypeList {
        get {
            return BMSUtils.getCampaignTypeList();
        }
    }

    public Boolean isAdmin {
        get {
            return BMSUtils.checkIfAdmin(UserInfo.getProfileId());
        }
    }

    //CONSTRUCTOR
    public BMSEmailReviewCCV2() {}

    /**
    * @author       Nick Guia
    * @date         02/08/2017
    * @description  - Remote Action method for retrieving table data.
    *                 will be invoked by page's javascript
    * @return       - JSON string of wrapper class
    **/
    @RemoteAction
    global static String initializeTableData() {
        String sJSON;

        //build query string based on business rules
        String queryString = buildQueryString();
        //retrieve records and build the wrapper class to be displayed in the table
        List<BMSUtils.TableDataWrapper> tdwList = retrieveMassEmails(queryString);

        System.debug('*** json tdw : ' + JSON.serialize(tdwList));
        if(!tdwList.isEmpty()) {
            sJSON = JSON.serializePretty(tdwList);
        }
        return sJSON;
    }

    /**
    * @author       Nick Guia
    * @date         02/08/2017
    * @description  - Remote Action method for updating campaign status
    **/
    @RemoteAction
    global static void updateCampaignStatus(List<String> cmpIds, Boolean isApproved) {
        List<Campaign> cmpUpdateList = new List<Campaign>();
        String status = (isApproved) ? Label.Campaign_Status_Approved : Label.Campaign_Status_Cancelled;
        try {
            for(Campaign cmpLoop : [SELECT Id, Status
                                    FROM Campaign
                                    WHERE Id IN :cmpIds])
            {
                cmpLoop.Status = status;
                cmpUpdateList.add(cmpLoop);
            }
            update cmpUpdateList;
        } catch (Exception e) {
            //TO DO: INSERT EXCEPTION LOG
            System.debug('ERROR while updating campaign status : ' + e);
        }
    }

    /**
    * @author       Nick Guia
    * @date         02/08/2017
    * @description  - method for building query String based on business rules
    * @param        - profileId = Id of current user's profile
    * @return       - SOQL string
    **/
    private static List<BMSUtils.TableDataWrapper> retrieveMassEmails(String queryString) {
        List<BMSUtils.TableDataWrapper> tdwList = new List<BMSUtils.TableDataWrapper>();
        try {
            for (Campaign cmpLoop: Database.query(queryString)) {
                BMSUtils.TableDataWrapper tdw = new BMSUtils.TableDataWrapper(cmpLoop);
                tdwList.add(tdw);
            }   
        } catch (Exception e) {
            //TO DO: INSERT EXCEPTION LOG
            System.debug('ERROR ON : BMSEmailReviewCCV2.retrieveMassEmails() while trying to retrieve campaign records : ' +e);
        }
        System.debug('tdwList : ' + tdwList);
        return tdwList;
    }

    /**
    * @author       Nick Guia
    * @date         02/08/2017
    * @description  - method for building query String based on business rules
    * @return       - SOQL string
    **/
    private static String buildQueryString() {
        //set query string to dynamically pull records based on profile
        String sObjectName = 'Campaign';
        //add fields here as required
        //TO DO: CHECK IF WE CAN REMOVE SOME FIELDS
        String fields =  'Id, Name, SMS_Count__c, Status, Total_Members__c, ' + 
                        'Owner_Name__c, CreatedDate, Email_Template__c, ' +
                        'Batch_Finished__c, SMS_Body__c, Type ';

        /**add filters as required :
           - show only On Hold, Approved, or Cancelled
           - if not a BMS Admin or a System Administrator, display
             only records that the user own
        **/
        String filter = '(Status = \'' + Label.Campaign_Status_On_Hold + '\'' +
                        'OR Status = \'' + Label.Campaign_Status_Approved + '\'' +
                        'OR Status = \'' + Label.Campaign_Status_Cancelled + '\')';

        if(!BMSUtils.checkIfAdmin(UserInfo.getProfileId())) {
            filter += ' AND OwnerId = :UserInfo.getUserId()';
        }

        //add query limit based on 'rowLimit' variable (200 at the moment)
        filter += ' ORDER BY CreatedDate DESC LIMIT ' + BMSUtils.BMS_EMAIL_REVIEW_ROW_LIMIT;

        //build query string and return
        return String.format(
                    'SELECT {0} ' +
                    'FROM {1} ' +
                    'WHERE {2}',
            new List<String> {
                fields,
                sObjectName,
                filter
            });
    }
}