/**
 * Services class to provide CampaignMember sObject centric logical support
 * 
 * @author Carl Vescovi, PwC
 * @history
 * 		29/07/2017 	Nick Guia 	- created qualifyCreateSubscriptionMember and fieldHasChanged
 */


public class CampaignMemberServices {

	
	/**
	* MONTRACK specific logic
	* <p>this class removes tasks related to a now redundant cohort if a montrack campaign member changes cohorts
	* 
	*/
	public class manageChangesOfCohort implements Triggers.Handler {
		
		public void run(){
			
		  List <campaignmember> cmList = new list<campaignmember>();
          set<id> cohortSet = new set<id>();
          set<id> contactIds = new set<id>();
	          
          for(campaignmember cm : (List<CampaignMember>)Trigger.new){
            
            campaignmember oldValue = (CampaignMember)Trigger.oldMap.get(cm.id);
            
            if(oldValue.Cohort__c != null && cm.Cohort__c != null){
              if(cm.Cohort__c != oldValue.Cohort__c){
                  cmList.add(cm);
                  cohortSet.add(oldValue.cohort__c);
                  contactIds.add(cm.contactId);
              }
            }
          }
        
          // get all the round records for all the changing cohorts
          
          if(!cohortSet.isEmpty()){ // this condition handles that this is only done for Montrack campaigns
	          
	          list<Round__c> roundIds = new list<Round__c>([select id from Round__c where cohort__c in: cohortSet]);
	          
	          if(!roundIds.isEmpty()){
		          for(List<task> tasksToDelete : [select id, whoid, whatid from Task where whoid in:contactIds AND 
		                        whatid in: roundIds AND Subject='MonTRACK Call' AND status ='Not Started']){ 
	          
	          	  		delete tasksToDelete; 
		          }
	          }
      	  }
	           
        
		}
	}

	/*******************************************************************************
    * @author       Ryan Wilson
    * @date         31.Mar.2017       
    * @description  method to populate the email field being reference by the 
    *                Journey Builder in Marketing cloud
    * @revision     
    *******************************************************************************/

	public class populateEmailToField implements Triggers.Handler {
		public void run() {
			for(campaignmember cm : (List<CampaignMember>)Trigger.new){
            	if(cm.Email_To__c != null){
            		cm.Email_to_Send__c = cm.Email_To__c;
            	}
          	}
		}
	}

	/**
	* @author 	Nick Guia
	* @date 28/08/2017
	* @description For creating subscription member record based on business defined criteria
	**/
	public class qualifyCreateSubscriptionMember implements Triggers.Handler {
		public void run() {
			//map of subscription list IDs and campaign member id set
			Map<String, Set<Id>> cmIdsBySubscriptionList = new Map<String, Set<Id>>();
			Map<Id, CampaignMember> cmMap = new Map<Id, CampaignMember>(); //for collecting qualified CMs
			Set<Id> leadIds = new Set<Id>(); //for collecting lead ids related to CM
			Set<Id> contactIds = new Set<Id>(); //for collecting contact ids related to CM
			for(CampaignMember cm : (List<CampaignMember>) Trigger.new) {
				Boolean isQualified = false;
				if(Trigger.isInsert) {
					//qualify for after insert
					if(cm.Subscriptions_List__c != null && 
						cm.Subscriptions_List__c != '') 
					{
						isQualified = true;
					}
				} else if(Trigger.isUpdate) {
					//qualify for after update
					if(fieldHasChanged('Subscriptions_List__c', cm, Trigger.oldMap.get(cm.Id)) ||
					   fieldHasChanged('Channel_Preference__c', cm, Trigger.oldMap.get(cm.Id)))
					{
						isQualified = true;
					}
				}

				if(isQualified) {
					cmIdsBySubscriptionList = collectSubscriptionLists(cmIdsBySubscriptionList, cm);
					cmMap.put(cm.Id, cm);
					if(cm.ContactId != null){
						contactIds.add(cm.ContactId);
					} else if(cm.LeadId != null) {
						leadIds.add(cm.LeadId);
					}
				}
			}
			createSubscriptionMember(cmIdsBySubscriptionList, cmMap, leadIds, contactIds);
		}

		/**
		* @author 	Nick Guia
		* @date 28/08/2017
		* @description For collecting all Subscription Lists in the Campaign Member
		* @param cmIdsBySubscriptionList : <key> subscription list id
		*								   <val> set of Campaign Member ids
		**/
		private Map<String, Set<Id>> collectSubscriptionLists(Map<String, Set<Id>> cmIdsBySubscriptionList,
															CampaignMember cm) 
		{

			if(cm.Subscriptions_List__c != '' && cm.Subscriptions_List__c != null) {
				for(String sl : cm.Subscriptions_List__c.split(':')) {
					if(cmIdsBySubscriptionList.containsKey(sl)) { //check if subscription list id is already in the map
						cmIdsBySubscriptionList.get(sl).add(cm.Id);
					} else { //create a new key value pair
						cmIdsBySubscriptionList.put(sl, new Set<Id>{cm.Id});
					}
				}
			}
			System.debug('cmIdsBySubscriptionList : ' + cmIdsBySubscriptionList);
			return cmIdsBySubscriptionList;
		}

		/**
		* @author 	Nick Guia
		* @date 28/08/2017
		* @description For creating subscription member record on qualified Leads
		* @param cmIdsBySubscriptionList : <key> subscription list id
		*								   <val> set of Campaign Member ids
		* 		 cmMap : map of Campaign Members
		*		 leadIds : set of lead IDs related to CM
		*		 contactIds : set of contact IDs related to CM
		* @history
		*
		**/
		private void createSubscriptionMember(Map<String, Set<Id>> cmIdsBySubscriptionList,
											  Map<Id, CampaignMember> cmMap,
											  Set<Id> leadIds,
											  Set<Id> contactIds) 
		{
			/** <key> 	Campaign Member Id
				<value> map of:
					<key> 	subscription list Id
					<value> subscription member record
			**/
			Map<Id, Map<Id, Subscripton_Member__c>> existingSMbySL = new Map<Id, Map<Id, Subscripton_Member__c>>();
			/** query subscription member records to check if the lead is 
				already a member of the subscription list 
			**/ 
			for(Subscripton_Member__c sm : [SELECT Id, Lead__c, Subscription_List__c, Campaign__c, Contact__c
										FROM Subscripton_Member__c 
										WHERE Subscription_List__c IN :cmIdsBySubscriptionList.keySet()
										AND (Lead__c IN :leadIds
											OR Contact__c IN :contactIds)])
			{
				//check existing subscription member by subscription list ID
				if(sm.Lead__c != null) {
					//check by Lead
					if(existingSMbySL.containsKey(sm.Lead__c)) {
						existingSMbySL.get(sm.Lead__c)
								.put(sm.Subscription_List__c, sm);
					} else {
						existingSMbySL.put(sm.Lead__c,
							new Map<Id, Subscripton_Member__c> {
								sm.Subscription_List__c => sm
								});
					}
				} else if(sm.Contact__c != null) {
					//check by Contact
					if(existingSMbySL.containsKey(sm.Contact__c)) {
						existingSMbySL.get(sm.Contact__c)
								.put(sm.Subscription_List__c, sm);
					} else {
						existingSMbySL.put(sm.Contact__c,
							new Map<Id, Subscripton_Member__c> {
								sm.Subscription_List__c => sm
								});
					}
				}
			}

			List<Subscripton_Member__c> insertList = new List<Subscripton_Member__c>();
			//iterate through our map collection
			for(String slId : cmIdsBySubscriptionList.keySet()) {
				//iterate through campaign member IDs
				for(Id cmId : cmIdsBySubscriptionList.get(slId)) {
					//check if the lead of this campaign member is already subscribed
					CampaignMember cm = cmMap.get(cmId);
					//check by lead and subscription list
					if(existingSMbySL.containsKey(cm.LeadId)) {
						//lead has an existing subscription, check which subscription list it is
						if(!existingSMbySL.get(cm.LeadId).containsKey(slId)) {
							insertList.add(createSubscriptionMember(cm, slId));
						}
					} else if(existingSMbySL.containsKey(cm.ContactId)) { //check by contact and subscription list
						//contact has an existing subscription, check which subscription list it is
						if(!existingSMbySL.get(cm.ContactId).containsKey(slId)) {
							insertList.add(createSubscriptionMember(cm, slId));
						}
					} else {
						/** lead or contact doesn't have any existing subscription.
							create one for this subscription list
						**/
						insertList.add(createSubscriptionMember(cm, slId));
					}
				}
			}

			if(!insertList.isEmpty()) {
				insert insertList;
			}
		}

		/**
		* @author 	Nick Guia
		* @date 	29/08/2017
		* @description Method for generating Subscription Member record based on lead details
		**/
		private Subscripton_Member__c createSubscriptionMember(CampaignMember cm, String slId) {
			//prepopulate values
			Subscripton_Member__c sm = new Subscripton_Member__c();
			sm.Campaign__c = cm.CampaignId;
	        sm.Active__c = true;
	        sm.Subscribed_Date__c = System.today();
	        sm.Subscription_List__c = Id.valueOf(slId);
	        //assign parent. could be a lead or a contact
	        if(cm.ContactId != null) {
	        	sm.Contact__c = cm.ContactId;
	        } else if(cm.LeadId != null) {
	        	sm.Lead__c = cm.LeadId;
	        }

	        return updateChannelPreference(sm, cm);
		}

		/**
		* @author 	Nick Guia
		* @date 	29/08/2017
		* @description Method for parsing channel preferences and populating subscription member
		**/
		private Subscripton_Member__c updateChannelPreference(Subscripton_Member__c sm, 
																	CampaignMember cm)
		{
			if(cm.Channel_Preference__c != null) {
				//loop through the comma delimited field
				for(String pref : cm.Channel_Preference__c.split(':')) {
	                if(pref.equalsIgnoreCase('both')) {
	                    sm.Channel_Email__c = true;
	                    sm.Channel_SMS__c = true;
	                } else {
	                	if(pref.equalsIgnoreCase('sms')) {
	                		sm.Channel_SMS__c = true;
	                	}
	                	if(pref.equalsIgnoreCase('email')) {
		                	sm.Channel_Email__c = true;
		            	}
	                }
	            }
        	}
            return sm;
		}
	}

	/**
	* utility to confirm if field has changed
	* @param fieldname the API name of the field to look at
	* @param newObj the new version of the sObject to be considered
	* @param oldObj the previous or older version of the sObject to be considered
	* @return boolean confirming if change has been observed or not
	*/
	private static Boolean fieldHasChanged(String fieldName, SObject newObj, SObject oldObj) {
    	try {
        	return (newObj.get(fieldName) != oldObj.get(fieldName)) ? true : false;
        } catch (Exception e) {
            return false; // if passed a null array 
        }
	}
    
}