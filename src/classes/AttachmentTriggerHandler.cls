/**
* Handler Class to check/uncheck has_attachment checkbox on cases when an attachment is added/deleted on a case. 
* 
* @author Veera, PwC
* 
*/
public class AttachmentTriggerHandler {
    
    /*******************************************************************************
	* @author       Veera, PwC
	* @date         19.May.2017       
	* @description  method to handle after insert logic on Attachments
	* @revision     	
	*******************************************************************************/
    
    public static void handleAfterInsert(list<attachment> triggerNew) {      
        list<case> caseUpdateList =new list<case>();
        Map<id,case> caseRecMap = getCaseRecords(triggerNew);
        
        if(!caseRecMap.isEmpty()) {
            for(case cas: caseRecMap.values()) {
                if(cas.Has_Attachment__c==false) {
                    cas.Has_Attachment__c = true;
                }
                cas.Incoming_Email__c = true;
                caseUpdateList.add(cas);
            }  
        }
        
        if(!caseUpdateList.isEmpty()) {
            Database.SaveResult[] srList = database.update(caseUpdateList,false);
            
            for(database.SaveResult sr: srList) {
                if(!sr.issuccess()) {
                    for(database.Error err: sr.getErrors()){ 
                        system.debug('Exception Occured ::'+err.getMessage()+'::'+err.getfields());
                        ExLog.add('AttachmentTriggerHandler','Attachment','handleAfterInsert', err.getMessage());
                    }
                }
            }
        }
        
    }
    
    /*******************************************************************************
	* @author       Veera, PwC
	* @date         19.May.2017       
	* @description  method to handle after delete logic on Attachments
	* @revision     
	*******************************************************************************/
    public static void handleAfterDelete(list<attachment> triggerOld) {
        list<case> caseUpdateList =new list<case>();
        set<string> casRecordtypeSet = getCaseRecordtypes();
        set<id> caseIdSet = new set<id>();
        //Map<id,case> caseRecMap = getCaseRecords(triggerOld);
        //
        if(!triggerOld.isEmpty()) {
            for(attachment att: triggerOld) {
                if(string.valueOf(att.parentId.getSObjectType())=='Case') {
                    caseIdSet.add(att.ParentId);
                }
            }
        }
        
        if(!caseIdSet.isEmpty()) { 
            for(case cas:[select id,Has_Attachment__c,(select id from attachments) from case where id in :caseIdSet and recordtype.name in :casRecordtypeSet]) {
                if(cas.attachments.size() == 0 && cas.Has_Attachment__c == true) {
                    cas.Has_Attachment__c = false;
                    caseUpdateList.add(cas);
                }
            }
        }
        
        if(!caseUpdateList.isEmpty()) {
            Database.SaveResult[] srList = database.update(caseUpdateList,false);
            
            for(database.SaveResult sr: srList) {
                if(!sr.issuccess()){
                    for(database.Error err: sr.getErrors()) { 
                        system.debug('Exception Occured ::'+err.getMessage()+'::'+err.getfields());
                        ExLog.add('AttachmentTriggerHandler','Attachment','handleAfterDelete', err.getMessage());
                    }
                }
            }
        }
        
    }
    
    /*******************************************************************************
	* @author       Veera, PwC
	* @date         19.May.2017       
	* @description  method to return case records required to update check.
	* @revision     
	*******************************************************************************/
    static map<id,case> getCaseRecords(List<attachment> attList) {
        map<id,case> caseMap = new map<id,case>();
        set<id> caseIdSet = new set<id>();
        set<string> casRecordtypeSet = getCaseRecordtypes();
        
        for(Attachment att:attList) {
            if(string.valueOf(att.parentId.getSObjectType())=='Case') {
                caseIdSet.add(att.parentId);  
            }
        }
        
        if(!caseIdSet.isEmpty()) {
            for(case cas:[select id,Has_Attachment__c from case where id in :caseIdSet and recordtype.name in :casRecordtypeSet]) {
                caseMap.put(cas.id,cas);
            }
        }
        
        return caseMap;     
    }
    
    /*******************************************************************************
	* @author       Veera, PwC
	* @date         19.May.2017       
	* @description  method to return case records type developer name from custom setting.
	* @revision     
	*******************************************************************************/
    
    static set<string> getCaseRecordtypes() {
        set<string> casRecordtypeSet = new set<string>(); 
        for(Case_RecordType_CS__c hasAttach: Case_RecordType_CS__c.getAll().values()) {
            if(hasAttach.Has_Attachment_Trigger__c == true) {
                casRecordtypeSet.add(hasAttach.Recordtype_Name__c);
            }   
        }
        return casRecordtypeSet;
    }
    
}