@isTest
private class CourseLookupControllerTest {
	static testMethod void lookupCourseOffering() {
        Test.startTest();

       	Course__c course = PDTestHelper.createCourse();
       	Course_Offering__c co = PDTestHelper.createCourseOffering();
       	co.Course__c = course.Id;
       	update co;
       	System.debug('!@#$% Course Offering : ' + co);
       	List<Course_Offering__c> courseOfferings = CourseLookupController.getCourseOffering('PDD0001');
        Course__c course_details = CourseLookupController.getCourseDetails('PDD0001');
        
        Test.stopTest();
    }
}