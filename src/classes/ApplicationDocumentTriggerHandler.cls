public class ApplicationDocumentTriggerHandler {


	public static void handleAfterInsert(List<Application_Document_Provided__c> docsProvided) {	

		// Get Application Ids for all documents uploaded
	    Set<String> adpIds = new Set<String>();
	    for(Application_Document_Provided__c documentProvided : docsProvided) {
	        adpIds.add(documentProvided.Id);
	    }

	    //13.Sept.2017 Ant Custodio - Created a new web service StudFirst_SendDocumentsService to prevent damaged document errors
		if (!adpIds.isEmpty()) {
			//Send documents to Callista (Through MIX)
			StudFirst_SendDocumentsService.sendToCallistaByADPIds(adpIds);
		}
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         16.Oct.2017         
	* @description  tick the Document_submitted_successfully__c flag when
    *								the document is a duplicate from Callista SF-525
	*				This updates: 	Document_submitted_successfully__c flag
	* @revision     
	*******************************************************************************/
	public static void tickFlagIfDuplicate(Map<Id, Application_Document_Provided__c> oldMap, Map<Id, Application_Document_Provided__c> newMap) {	
		// Get Application Ids for all documents uploaded
	    for(Application_Document_Provided__c adpRec : newMap.values()) {
	    	//condition: if flag is false AND Response message is changed
	    	if (	!newMap.get(adpRec.Id).Document_submitted_successfully__c && 
	    			newMap.get(adpRec.Id).API_Response_Message__c != null && newMap.get(adpRec.Id).API_Response_Message__c != '' ) {
				//if Callista responds a duplicate message, tick the flag to true.
				StudFirst_Utilities.tickADPFlagIfDuplicate(adpRec, new Set<String>{System.Label.App_DocumentExistResponse}, false);
	    	}
	    }
	}
}