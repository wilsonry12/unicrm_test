/**************************************************************************************************   
Apex Controller Name :  LightningCreateCaseController 
Version              :  1.0                                                                          
Created Date         :                                                              
Function             :  controller class to create case from community ask a question page.                                                                             
*--------------------------------------------------------------------------------------------------  
Developer                        Date                        Description
Veera G                       3.July.2017                        Created.
***************************************************************************************************/
public without sharing class LightningCreateCaseController {
    
    /*******************************************************************************
* @author       Veera, PwC
* @date         3.July.2017       
* @description  method will return the picklist values of Community_Status__c picklist values.
* @InputParameter: string
* @return type  list<sobject>
* @revision     
*******************************************************************************/
    @AuraEnabled
    public static list<String> getReasonofEnquiry() {
        list<String> options = new list<String>();
        Schema.DescribeFieldResult fieldResult = Case.Community_Status__c.getDescribe();
        list<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        
        options.add('-- None --');
        for (Schema.PicklistEntry f: values) {
            options.add(f.getLabel());
        }
        
        return options;
    }
    
    /*******************************************************************************
* @author       Veera, PwC
* @date         3.July.2017       
* @description  method decides whether the logged in user is internal or guest.
* @InputParameter: string
* @return type  list<sobject>
* @revision     
*******************************************************************************/
    @AuraEnabled
    public static string getUserType() {
        
        string currentUserProfileName = [select id,name from Profile where Id =: userInfo.getProfileId() Limit 1].name;
        string usertype = '';
        
        if(currentUserProfileName == Label.Guest_User_Profile_Name) {
            usertype = 'Guest';
        } else {
            usertype = 'Internal';
        }
        
        system.debug('>>>++'+usertype);
        return usertype;        
    }
    
    /*******************************************************************************
* @author       Veera, PwC
* @date         3.July.2017       
* @description  method will create case which takes input from CreateCaseComponent.
* @InputParameter: case
* @return type  Id
* @revision     
*******************************************************************************/
    @AuraEnabled
    public static string createCase(Case casRec) {
        try { 
               casRec.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('HR').getRecordTypeId();
               AssignmentRule rule = new AssignmentRule();
               Database.DMLOptions dmlOpts = new Database.DMLOptions();
               dmlOpts.assignmentRuleHeader.useDefaultRule = true;
               Database.SaveResult[] sr= Database.Insert(new List<case> {casRec},dmlOpts);          
               return casRec.id;
               
               } catch (exception ex) {
                   ExLog.add('create Enquiry','LightningCreateCaseController','createCase', ex);
                   return null;
               }
               
               }
               
               /*******************************************************************************
* @author       Veera, PwC
* @date         3.July.2017       
* @description  method will take search text as input from description field in CreateCaseComponent and returns all the articles related to that search string.
* @InputParameter: string
* @return type  list<KnowledgeArticleVersion>
* @revision     
*******************************************************************************/
               @AuraEnabled
               public static list<KnowledgeArticleVersion> getArticalSuggestions(string searchText) {
                   list<KnowledgeArticleVersion> articleSuggestions = new list<KnowledgeArticleVersion>();
                   set<KnowledgeArticleVersion> articleSuggestionsSet = new set<KnowledgeArticleVersion>();
                   
                   if(searchText != '' && searchText.length()>3) {
                       list<string> searchTokens = searchText.split('[\\s]');
                       integer count = 0;
                       integer parseStringCount = searchTokens.size()>3?3:searchTokens.size();
                       while( count < parseStringCount) {
                           system.debug('>>>>>'+searchTokens[count]);
                           if(searchTokens[count].length()>3){
                               Search.SuggestionResults suggestionResults;
                               String objectType = 'KnowledgeArticleVersion';
                               Search.SuggestionOption options = new Search.SuggestionOption();
                               Search.KnowledgeSuggestionFilter filters = new Search.KnowledgeSuggestionFilter();
                               
                               if (objectType=='KnowledgeArticleVersion') {
                                   filters.setLanguage('en_US');
                                   filters.setPublishStatus('Online');
                               }
                               
                               options.setFilter(filters);
                               options.setLimit(5);
                               suggestionResults=Search.suggest(searchTokens[count], objectType, options);
                               list<Search.SuggestionResult> resultList= suggestionResults.getSuggestionResults();
                               
                               
                               for(integer i=0;i<resultList.size();i++) {
                                   articleSuggestionsSet.add((KnowledgeArticleVersion)resultList[i].getSObject());
                               }                    
                           }
                           
                           count++;
                       }
                       
                       if(!articleSuggestionsSet.isEmpty()) {
                           articleSuggestions.addAll(articleSuggestionsSet);
                       }
                       
                       return articleSuggestions;
                       
                   } else {
                       articleSuggestions = new list<KnowledgeArticleVersion>();
                       return articleSuggestions;
                   }
               }
               
               /*******************************************************************************
* @author       Veera, PwC
* @date         3.July.2017       
* @description  method is used to insert attachment from CreateCaseComponent.
* @InputParameter: string
* @return type  list<sobject>
* @revision     
*******************************************************************************/
               @AuraEnabled
               public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) { 
                   base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
                   
                   Attachment a = new Attachment();
                   a.parentId = parentId;
                   
                   a.Body = EncodingUtil.base64Decode(base64Data);
                   a.Name = fileName;
                   a.ContentType = contentType;
                   
                   insert a;
                   
                   return a.Id;
               }
               
               /*******************************************************************************
* @author       Veera, PwC
* @date         3.July.2017       
* @description  method will decide whether the attachment has to be attached in chunks or can be attached as attachment directly based on attachment size.
* @InputParameter: Id,string,string,string,string
* @return type  Id
* @revision     
*******************************************************************************/
               @AuraEnabled
               public static Id saveTheChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) { 
                   system.debug('before fileId is '+fileId);
                   if (fileId == '' || fileId ==null) {
                       fileId = saveTheFile(parentId, fileName, base64Data, contentType);
                   } else {
                       fileId=appendToFile(fileId, base64Data);
                   }
                   system.debug('after fileId is '+fileId);
                   return Id.valueOf(fileId);
               }
               
               /*******************************************************************************
* @author       Veera, PwC
* @date         3.July.2017       
* @description  method is used to update attachment body in chunks.
* @InputParameter: Id, String
* @return type  
* @revision     
*******************************************************************************/
               private static Id appendToFile(Id fileId, String base64Data) {
                   base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
                   
                   Attachment a = [
                       SELECT Id, Body
                       FROM Attachment
                       WHERE Id = :fileId
                   ];
                   
                   String existingBody = EncodingUtil.base64Encode(a.Body);
                   a.Body = EncodingUtil.base64Decode(existingBody + base64Data); 
                   
                   update a;
                   return a.Id;
               }
               
               /*******************************************************************************
* @author       Veera, PwC
* @date         3.July.2017       
* @description  method to return User First Name
* @Input Parameters: 
* @return type: string
* @revision         
*******************************************************************************/
               @AuraEnabled
               public static string getUserFirstName() {
                   
                   string firstName = ''; 
                   string currentUserProfile = [select id,name from Profile where Id =: userInfo.getProfileId() Limit 1].name;
                   
                   if(currentUserProfile != Label.Guest_User_Profile_Name){
                       firstName = ' '+[select id,firstname from user where id = :userinfo.getUserId()].firstname;
                   }
                   
                   return firstName;        
               }
               
               @AuraEnabled
               public static sobject getArticledetail(Id articleId){
                   list<sobject> articleSummary;
                   sobject suggestionRecord = null;
                   string articleType;
                   system.debug('>>>>>'+articleId);
                   
                   if(articleId != null) {
                       string kaArticletype = string.valueof(ArticleId.getSObjectType());
                       string articalNumberquery ='Select ArticleNumber from '+kaArticletype+' where id =\''+articleId+'\' LIMIT 1';
                       list<sobject> kaArticleRecord = database.query(articalNumberquery);
                       string articleNumber = string.valueof(kaArticleRecord[0].get('ArticleNumber'));
                       if(kaArticletype.right(3) != 'kav') {
                           articleType = kaArticletype+'v'; 
                       } else {
                           articleType = kaArticletype;
                       }
                       
                       string query ='Select Question__c,Summary,Answer__c,Australia_Student__c,Australia_Staff__c from '+articleType+' where ArticleNumber =\''+articleNumber+'\' LIMIT 1';
                       articleSummary = database.query(query);
                       if(!articleSummary.isEmpty()) {
                           suggestionRecord = articleSummary[0];
                       }   
                   }
                   
                   system.debug('>>>>>'+suggestionRecord);
                   return suggestionRecord;
               }
               
               }