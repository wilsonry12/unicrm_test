/*******************************************************************************
* @author       Ryan Wilson
* @date         03.May.2017         
* @description  controller class for appDeclarationForm.cmp
* @revision     
*******************************************************************************/
public with sharing class AppDeclarationFormCC {
	@AuraEnabled
    public static Application__c retrieveApplication (String applicationId) {
    	Application__c applicationRecord = [SELECT Id, Declaration_Accepted__c, Applicant__c, Applicant__r.FirstName, 
    										Applicant__r.LastName, Name, Status__c 
    										FROM Application__c 
    										WHERE Id =: applicationId];

        return applicationRecord;
    }

    @AuraEnabled
    public static Application__c acceptDeclaration (String applicationId) {
    	Application__c applicationRecord = [SELECT Id, Submitted_Date__c, Declaration_Accepted__c, Applicant__c, Applicant__r.FirstName, 
    										Applicant__r.LastName, Name, Status__c 
    										FROM Application__c 
    										WHERE Id =: applicationId];
		
        try {
            applicationRecord.Status__c = 'Sent for Submission';
            applicationRecord.Submitted_Date__c = System.today();
			applicationRecord.Declaration_Accepted__c = true;
            update applicationRecord; 

        } catch (Exception e) {
            throw new AuraHandledException('Unable to update your details: ' + e.getMessage());
        }
    	
		return applicationRecord;
    }
}