/**************************************************************************************************   
Apex Controller Name :  LightningSearchComponent 
Version              :  1.0                                                                          
Created Date         :                                                              
Function             :  Auto Complete functionality for community search component.                                                                             
*--------------------------------------------------------------------------------------------------  
    Developer                        Date                        Description
    Veera G                       3.July.2017                        Created.
***************************************************************************************************/
public with sharing class LightningSearchComponent {
    
    /*******************************************************************************
	* @author       Veera G, PwC
	* @date         3.July.2017       
	* @description  this method takes search Keyword as input from community search component on KEYUP and return a list of Knowledge Articles.
	* @Input Parameters: String
	* @return type  list<KnowledgeArticleVersion>
	* @revision     	
	*******************************************************************************/
    @AuraEnabled
    public static list<KnowledgeArticleVersion> getSearchArticles(string searchText) {
        list<KnowledgeArticleVersion> searchResults = new list<KnowledgeArticleVersion>();

        	Search.SuggestionResults suggestionResults;
            String objectType = 'KnowledgeArticleVersion';
            Search.SuggestionOption options = new Search.SuggestionOption();
            Search.KnowledgeSuggestionFilter filters = new Search.KnowledgeSuggestionFilter();
            
            if (objectType=='KnowledgeArticleVersion') {
                filters.setLanguage('en_US');
                filters.setPublishStatus('Online');
            }
            
            options.setFilter(filters);
            options.setLimit(6);
            suggestionResults=Search.suggest(searchText, objectType, options);
            list<Search.SuggestionResult> resultList= suggestionResults.getSuggestionResults();
            
            for(integer i=0;i<resultList.size();i++) {
                searchResults.add((KnowledgeArticleVersion)resultList[i].getSObject());
            }
        
        system.debug('>>>>'+searchResults);
        return searchResults;
    }
    
}