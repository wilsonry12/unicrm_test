/*******************************************************************************
    * @author       Majid Reisi Dehkordi
    * @date         04.Aug.2017         
    * @description  test for appInternationalStudentsInterimPage
    * @revision     
*******************************************************************************/
@isTest
private class appInternationalStudentsInterimPage_Test{
	
    /*******************************************************************************
    * @author       Majid Reisi Dehkordi
    * @date         01.Aug.2017         
    * @description  gives the list of countries
    *******************************************************************************/
    @isTest static void test_retrieveCountriesIncludedRiskyOnes() {
        createCountriesIncludedRiskyOnes();

        final List<Callista_Country_Code__c> countries = [SELECT Country__c, High_Risk__c FROM Callista_Country_Code__c ORDER BY Country__c];

        //System.assertEquals(true, applicationPortalCC.countries != null);

        List<String> options = new List<String>();

        for(Callista_Country_Code__c country: countries){
            if (string.valueOf(country.Country__c).toUpperCase() != 'AUSTRALIA'){
                if(country.High_Risk__c != FALSE){
                    options.add(string.valueOf(country.Country__c) + '   ');
                }
                else {
                    options.add(string.valueOf(country.Country__c));
                }
            }
        }

        for(String country: options) {
            if(country == 'AUSTRALIA')
                System.assertEquals(false, country == 'AUSTRALIA');
            if(country == 'Afghanistan   ')
                System.assertEquals(true , country == 'Afghanistan   ');
            if(country == 'Afghanistan')
                System.assertEquals(false , country == 'Afghanistan');
        }
    }

    /*******************************************************************************
    * @author       Majid Reisi Dehkordi
    * @date         01.Aug.2017         
    * @description  gives the list of countries
    *******************************************************************************/
    @isTest static void createCountriesIncludedRiskyOnes() {
        List<Callista_Country_Code__c> callistaCountryCodeList = new List<Callista_Country_Code__c>();

        Callista_Country_Code__c callistaCountryCode1 = new Callista_Country_Code__c();
        callistaCountryCode1.Name = 'Afghanistan';
        callistaCountryCode1.Code__c = '7201';
        callistaCountryCode1.Country__c = 'Afghanistan';
        callistaCountryCode1.High_Risk__c = true;

        Callista_Country_Code__c callistaCountryCode2 = new Callista_Country_Code__c();
        callistaCountryCode2.Name = 'Adelie Land (France)';
        callistaCountryCode2.Code__c = '1601';
        callistaCountryCode2.Country__c = 'Adelie Land (France)';
        callistaCountryCode2.High_Risk__c = false;

        Callista_Country_Code__c callistaCountryCode3 = new Callista_Country_Code__c();
        callistaCountryCode3.Name = 'Australia';
        callistaCountryCode3.Code__c = '1100';
        callistaCountryCode3.Country__c = 'Australia';
        callistaCountryCode3.High_Risk__c = false;

        Callista_Country_Code__c callistaCountryCode4 = new Callista_Country_Code__c();
        callistaCountryCode4.Name = 'AUSTRALIA';
        callistaCountryCode4.Code__c = '1100';
        callistaCountryCode4.Country__c = 'AUSTRALIA';
        callistaCountryCode4.High_Risk__c = false;

        callistaCountryCodeList.add(callistaCountryCode1);
        callistaCountryCodeList.add(callistaCountryCode2);
        callistaCountryCodeList.add(callistaCountryCode3);
        callistaCountryCodeList.add(callistaCountryCode4);

        insert callistaCountryCodeList;
    }
}