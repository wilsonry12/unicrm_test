public without sharing class ApplicationDocumentsCC {
	public ApplicationDocumentsCC() {
		
	}

	@AuraEnabled
    public static List<String> retrieveDocumentType() {
        List<String> options = new List<String>();

        Schema.DescribeFieldResult fieldResult = Contact_Document__c.Document_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getLabel());
        }

        //options.sort();

        return options;
    }

	@AuraEnabled
	public static List<Application_Document_Provided__c> retrieveAppDocuments(String appliId) {
		List<Application_Document_Provided__c> appDocuments = new List<Application_Document_Provided__c>();
		appDocuments = [SELECT Id, CreatedDate, Is_Submitted__c, Application__c, Application__r.Status__c, Contact_Document__c, Contact_Document__r.Name, 
						Contact_Document__r.Document_Type_Name__c, Contact_Document__r.Filename__c, 
                        Document_Id__c, Contact_Document__r.Comments__c   
						FROM Application_Document_Provided__c 
						WHERE Application__c =:appliId 
                        ORDER BY CreatedDate DESC 
                        LIMIT 20];
		return appDocuments;
	}

    @AuraEnabled
    public static Application_Document_Provided__c editApplicationDocument(String appDocId) {
        Application_Document_Provided__c appDoc = new Application_Document_Provided__c();
        appDoc = [SELECT Id, Contact_Document__c, Contact_Document__r.Document_Type_Name__c, Contact_Document__r.Filename__c, 
                    Document_Id__c, Contact_Document__r.Comments__c 
                    FROM Application_Document_Provided__c 
                    WHERE Id =:appDocId];
        return appDoc;
    }

	@AuraEnabled
	public static Id uploadDocument(String appliId, String fileName, String base64Data, String contentType, String docType, String comments, String existingDocument) {
		User userDetail = [SELECT Id, ContactId FROM User WHERE Id =:UserInfo.getUserId()];

        Contact_Document__c conDocu = new Contact_Document__c();
        Attachment attDocument = new Attachment();
        Application_Document_Provided__c appDocu = new Application_Document_Provided__c();

        //insert for new document
        System.debug('****EXISTING DOCUMENT: '+existingDocument);
        if(existingDocument != null && existingDocument != ''){

            appDocu = editApplicationDocument(existingDocument);

            conDocu = [SELECT Id, Filename__c, Contact__c, Document_Type_Name__c, Document_Type__c, Comments__c 
                        FROM Contact_Document__c 
                        WHERE Id =:appDocu.Contact_Document__c];

            attDocument = [SELECT Id, Body, Name, parentId, ContentType, Description 
                            FROM Attachment 
                            WHERE Id =:appDocu.Document_Id__c];

        }

        //update Attachment and Contact Document if new file has been uploaded. If no document replacement
        //don't update attachment
        if(conDocu.Contact__c == null){
           conDocu.Contact__c = userDetail.ContactId; 
        }
        conDocu.Document_Type_Name__c = getDocuTypeName(docType);
        if(docType != null && docType != ''){
            conDocu.Document_Type__c = getDocuTypeValue(docType);
        }
        else{
            conDocu.Document_Type__c = getDocuTypeValue('OTHER DOCUMENT TYPE NOT LISTED');
        }
        conDocu.Comments__c = comments;
        if(fileName != null && fileName != ''){
            conDocu.Filename__c = fileName;
        }
        upsert conDocu;

        System.debug('***FILENAME: '+fileName);
        if(fileName != null && fileName != ''){
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            if(attDocument.parentId == null){
               attDocument.parentId = conDocu.Id; 
            }
            attDocument.Body = EncodingUtil.base64Decode(base64Data);
            attDocument.Name = fileName;
            attDocument.ContentType = contentType;
            attDocument.Description = comments;
            upsert attDocument;
        }

        if(existingDocument == null || existingDocument == ''){
            appDocu.Application__c = appliId;
            appDocu.Contact_Document__c = conDocu.Id;
            appDocu.Document_Id__c = attDocument.Id;
            upsert appDocu;
        }

		return attDocument.Id;
	}

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         2.May.2017         
    * @description  Version 2 - instead of just Id, return the actual attachment
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static Attachment uploadDocumentv2(String appliId, String fileName, String base64Data, String contentType, String docType, String comments, String existingDocument) {
        User userDetail = [SELECT Id, ContactId FROM User WHERE Id =:UserInfo.getUserId()];

        Contact_Document__c conDocu = new Contact_Document__c();
        Attachment attDocument = new Attachment();
        Application_Document_Provided__c appDocu = new Application_Document_Provided__c();

        //insert for new document
        if(existingDocument != null && existingDocument != ''){
            appDocu = [ SELECT  Id, Contact_Document__c, Contact_Document__r.Document_Type_Name__c, Contact_Document__r.Filename__c, 
                                Document_Id__c, Contact_Document__r.Comments__c 
                        FROM    Application_Document_Provided__c 
                        WHERE   Application__c =: appliId AND Contact_Document__c =: existingDocument LIMIT 1];
            
            conDocu = [ SELECT   Id, Filename__c, Contact__c, Document_Type_Name__c, Document_Type__c, Comments__c 
                        FROM    Contact_Document__c 
                        WHERE   Id =:appDocu.Contact_Document__c LIMIT 1];

            attDocument = [ SELECT   Id, Body, Name, parentId, ContentType, Description 
                            FROM    Attachment 
                            WHERE   Id =:appDocu.Document_Id__c LIMIT 1];
        }

        //update Attachment and Contact Document if new file has been uploaded. If no document replacement
        //don't update attachment
        if(conDocu.Contact__c == null){
           conDocu.Contact__c = userDetail.ContactId; 
        }
        conDocu.Document_Type_Name__c = getDocuTypeName(docType);
        if(docType != null && docType != ''){
            conDocu.Document_Type__c = getDocuTypeValue(docType);
        }
        else{
            conDocu.Document_Type__c = getDocuTypeValue('OTHER DOCUMENT TYPE NOT LISTED');
        }
        conDocu.Comments__c = comments;
        if(fileName != null && fileName != ''){
            conDocu.Filename__c = fileName;
        }
        upsert conDocu;

        System.debug('***FILENAME: '+fileName);
        if(fileName != null && fileName != ''){
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            if(attDocument.parentId == null){
               attDocument.parentId = conDocu.Id; 
            }
            attDocument.Body = EncodingUtil.base64Decode(base64Data);
            attDocument.Name = fileName;
            attDocument.ContentType = contentType;
            attDocument.Description = comments;
            upsert attDocument;
        }

        if(existingDocument == null || existingDocument == ''){
            appDocu.Application__c = appliId;
            appDocu.Contact_Document__c = conDocu.Id;
            appDocu.Document_Id__c = attDocument.Id;
            upsert appDocu;
        }

        return attDocument;
    }

    private static String getDocuTypeName(String docType) {
        Schema.DescribeFieldResult fieldResult = Contact_Document__c.Document_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        Map<String, String> mapDocType = new Map<String, String>();
        for( Schema.PicklistEntry f : ple)
        {
            mapDocType.put(f.getLabel(), f.getLabel());
        }

        return mapDocType.get(docType);
    }

    private static String getDocuTypeValue(String docType) {
        Schema.DescribeFieldResult fieldResult = Contact_Document__c.Document_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        Map<String, String> mapDocType = new Map<String, String>();
        for( Schema.PicklistEntry f : ple)
        {
            mapDocType.put(f.getLabel(), f.getValue());
        }

        return mapDocType.get(docType);
    }

	@AuraEnabled
    public static Id saveTheChunk(String appliId, String fileName, String base64Data, String contentType, String docType, String fileId, String comments, String existingDocument) { 
        system.debug('fileId: ' + fileId);
        if (fileId == '' || fileId == null) {
            fileId = uploadDocument(appliId, fileName, base64Data, contentType, docType, comments, existingDocument);
        } else {
            appendToFile(fileId, base64Data);
        }
        
        return Id.valueOf(fileId);
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         2.May.2017         
    * @description  Version 2 - instead of just Id, return the actual attachment
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static String saveTheChunkv2(String appliId, String fileName, String base64Data, String contentType, String docType, String fileId, String comments, String existingDocument) { 
        String parentId = '';
        if (fileId == null) {
            Attachment attach = uploadDocumentv2(appliId, fileName, base64Data, contentType, docType, comments, existingDocument);
            fileId = attach.Id;
            parentId = attach.ParentId;
        } else {
            Attachment attach = appendToFilev2(fileId, base64Data);
            parentId = attach.ParentId;
        }
        
        return fileId+'-'+parentId;
    }
    
    private static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = [
            SELECT Id, Body,ParentId
            FROM Attachment
            WHERE Id = :fileId
        ];
        
     	String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data); 
        
        update a;
    }

    private static Attachment appendToFilev2(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        Attachment a = [
            SELECT Id, Body,ParentId
            FROM Attachment
            WHERE Id = :fileId
        ];
        
        String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + base64Data); 
        
        update a;

        return a;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         26.Apr.2017         
    * @description  deletes the attachment
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static String deleteSelectedAttachment (String attachmentId) {
        String contactDocToDelete = '';
        if (attachmentId != null && attachmentId != '') {
            List<Application_Document_Provided__c> attachToDelete = [ SELECT Id,Contact_Document__c FROM Application_Document_Provided__c WHERE Id =: attachmentId LIMIT 1];
            if (!attachToDelete.isEmpty()) {
                List<Contact_Document__c> conDocument = [ SELECT Id FROM Contact_Document__c WHERE Id =: attachToDelete[0].Contact_Document__c LIMIT 1];
                if(!conDocument.isEmpty()){
                    delete conDocument;
                }
            }
        }
        return contactDocToDelete;
    }
}