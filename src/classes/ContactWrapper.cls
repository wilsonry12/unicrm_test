/***********************************************
* Author: Ryan Wilson
* Date: 09.SEP.2017
* Revision: Wrapper Class for contact related fields and object
***********************************************/
public class ContactWrapper {
	public ContactRecord contactRecord {get;set;}

    public class ContactRecord {
        public Contact contact {get;set;}
		public List<Role> roles {get;set;}
		public List<Channel> channels {get;set;}
    }

    public class Role {
    	public String contactKey {get;set;}
    	public Roles__c role {get;set;}
    }

    public class Channel {
    	public String contactKey {get;set;}
    	public Channel__c channel {get;set;}
    }
}