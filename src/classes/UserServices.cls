/**
 * Services class to provide User sObject centric logical support
 * 
 * @author Carl Vescovi, PwC
 * 
 */
public without Sharing class UserServices {

    public static final String CURRENT_STUDENT = 'Current Student';
    public static final String CURRENT_STUDENT_VALUE = 'Current Course';
    public static final String FUTURE_STUDENT = 'Future Student';
    public static final String FUTURE_STUDENT_VALUE = 'Future Course';
    public static final String FACE_TO_FACE = 'Face to Face & Online';
    public static final String FACE_TO_FACE_ORIGIN = 'Face to Face';
    public static final String PHONE = 'Phone & Online';
    public static final String PHONE_ORIGIN = 'Phone';
    public static final String MONASH_CONNECT_OFFICER_PERM_SET = 'Monash Connect - Officer';
    
    /**
    * my info reset at login, called from a login flow
    */ 
    @InvocableMethod
    public static void resetMyInformation() {
          
          User usr = new User(id = UserInfo.getUserId(),
                              Location__c = null,
                              Enquiry_Role__c = null,
                              My_Information_LastUpdate__c = null,
                              Queue__c = null);
          update usr;
    }
    
    
    /**
    * build a map of users that have Monash Connect permission set applied
    */ 
    public static Set<id> MonashConnectOfficerUsers {
        get{
            if(MonashConnectOfficerUsers == null){
                MonashConnectOfficerUsers = new Set<id>();
                for(PermissionSetAssignment perm : [select id,
                                                            AssigneeId 
                                                            from PermissionSetAssignment
                                                            where PermissionSet.Label =:MONASH_CONNECT_OFFICER_PERM_SET])
                    MonashConnectOfficerUsers.add(perm.AssigneeId);
            }
            return MonashConnectOfficerUsers;
        }
        set;
    }
    
    /**
    * user details specific to console user settings
    */ 
    public static Map<Id,User> usersMap(Set<Id> userIds) {
        return new Map<Id,User>([select id,
                                 Enquiry_Role__c,
                                 Location__c,
                                 Profile.Name,
                                 My_Information_LastUpdate__c,
                                 Queue__c from User where id in:userIds]);
            
    }
    
    /** 
    * cached context user details
    */ 
    public static User contextUser {
        get{
            if(contextUser == null){
                contextUser = usersMap(new Set<Id> {UserInfo.getUserId()}).get(UserInfo.getUserId());
            }
            return contextUser;
        }
        private set;
    }
    
    /**
    * map of user and queue names to id
    * @return map of user or group id to name - so aggregates both into a single map
    */ 
    public static Map<id,string> userOrGroupName {
        get{
            if(userOrGroupName == null){
                userOrGroupName = new Map<id,string>();
                for(User u : [select id,firstname,lastname from User limit 1000]){
                    string fn = (u.firstname == null) ? '' : u.firstname;
                    userOrGroupName.put(u.id, fn+' '+u.lastname);
                }
                for(Group grp : [select id, name from Group where Type = 'Queue']){
                    userOrGroupName.put(grp.id, grp.name);
                }
            }
            return userOrGroupName;
        }
        set;
    }
    
    /** returns a map of role name by user id */
    public static Map<id,string> userRoleName {
        get{
            if(userRoleName == null){
                userRoleName = new Map<id,string>();
                for(list<user> uList : [select id, userrole.name from user where userrole.name != null limit 3000]){
                    for(user u : uList) userRoleName.put(u.id, u.userrole.name);
                }
            }
            return userRoleName;
        }
        set;
    }
    
    
    /**
     * update user records in the without sharing context
     * @param users list of user records to update
     */
    public static void updateUsers(list<user> users){
        update users;
    }
    
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         2.Apr.2017         
    * @description  updates the contact associated to the users depending on 
    *                   what the user updates on his own details
    * @revision     Ant Custodio, 30.May.2017 - disable for system admins
    *******************************************************************************/
    public class updateAssociatedContact implements Triggers.Handler {
        public void run(){
            User runningUser = [ SELECT Id, ProfileId FROM User WHERE Id =: userInfo.getUserId() ];
            List<User> communityUsersUpdated = new List<User>();
            Map<String, Profile> profileMap = new Map<String, Profile>();

            for (Profile profileRec: [  SELECT  Id,
                                                Name
                                        FROM    Profile
                                        WHERE   Name =: 'Domestic Applicants'
                                                OR Name =: 'System Administrator'
                                        LIMIT   2]) {
                profileMap.put(profileRec.Name, profileRec);
            }
            
            //only run if NOT system admin
            if (runningUser.ProfileId != profileMap.get('System Administrator').Id) {
                if (profileMap.containsKey('Domestic Applicants')) {
                    for(User userRec : (List<User>)Trigger.new) {
                        if (userRec.ProfileId == profileMap.get('Domestic Applicants').Id) {
                            communityUsersUpdated.add(userRec);
                        }
                    }
                    
                    List<Contact> contactToUpdate = new List<Contact>();
                    
                    if(!communityUsersUpdated.isEmpty()){
                        Map<String, User> contactUserMap = new Map<String, User>();
                        for (User userRec: communityUsersUpdated) {
                            contactUserMap.put(userRec.ContactId, userRec);
                        }
                        
                        //loop through Contacts
                        for (Contact contactRec: [  SELECT  Id, FirstName, LastName,
                                                            Email, Birthdate
                                                    FROM    Contact
                                                    WHERE   Id IN: contactUserMap.keySet()]) {
                            Contact contactRecord = contactRec;
                            contactRecord.FirstName = contactUserMap.get(contactRec.Id).FirstName;
                            contactRecord.LastName = contactUserMap.get(contactRec.Id).LastName;
                            contactRecord.Email = contactUserMap.get(contactRec.Id).Email;
                            contactRecord.Birthdate = contactUserMap.get(contactRec.Id).App_Birthdate__c;
                            contactRecord.Salutation = contactUserMap.get(contactRec.Id).App_Salutation__c;
                            contactRecord.Gender__c = contactUserMap.get(contactRec.Id).App_Gender__c;
                            contactRecord.MobilePhone = contactUserMap.get(contactRec.Id).MobilePhone;
                            contactRecord.Phone = contactUserMap.get(contactRec.Id).Phone;
                            contactRecord.Citizenship__c = contactUserMap.get(contactRec.Id).App_Citizenship__c;
                            contactRecord.Previous_Surname__c = contactUserMap.get(contactRec.Id).App_Previous_Surname__c;
                            contactRecord.Previous_Monash_ID__c = contactUserMap.get(contactRec.Id).App_Previous_Monash_ID__c;
                            contactRecord.Previous_Monash_Institution__c = contactUserMap.get(contactRec.Id).App_Previous_Monash_Institution__c;
                            contactRecord.Country_of_Residence__c = contactUserMap.get(contactRec.Id).App_Country_of_Residence__c;
                            contactRecord.Previously_Studied_at_Monash__c = contactUserMap.get(contactRec.Id).App_Previously_Studied_at_Monash__c;
                            contactRecord.Residency_Status__c = contactUserMap.get(contactRec.Id).App_Residency_Status__c;
                            contactRecord.HomePhone = contactUserMap.get(contactRec.Id).App_Home_Phone__c;
                            //contactRecord.HasDisabilities__c = contactUserMap.get(contactRec.Id).App_HasDisabilities__c;
                            contactRecord.Aboriginal_or_Torres_Strait_Islander__c = contactUserMap.get(contactRec.Id).App_Aboriginal_or_Torres_Strait_Islander__c;
                            contactRecord.MailingStreet = contactUserMap.get(contactRec.Id).Street;
                            contactRecord.MailingCity = contactUserMap.get(contactRec.Id).City;
                            contactRecord.MailingState = contactUserMap.get(contactRec.Id).State;
                            contactRecord.MailingPostalCode = contactUserMap.get(contactRec.Id).PostalCode;
                            contactRecord.MailingCountry = contactUserMap.get(contactRec.Id).Country;
                            contactRecord.Other_Given_Name__c = contactUserMap.get(contactRec.Id).Other_Given_Name__c;
                            contactRecord.Address_Type__c = contactUserMap.get(contactRec.Id).App_Address_Type__c;
                            contactToUpdate.add(contactRecord);
                        }
                    }
                    
                    if (!contactToUpdate.isEmpty()) {
                        update contactToUpdate;
                    }
                }
            }
        }
    }
        
}