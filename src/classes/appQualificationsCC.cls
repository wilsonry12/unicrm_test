/*******************************************************************************
* @author       Ant Custodio
* @date         7.Apr.2017         
* @description  controller class for appQualifications.cmp lightning component
* @revision     
*******************************************************************************/
public without sharing class appQualificationsCC {
    private static Contact_Qualification__c conQualRecord;
    private static List<Contact_Qualification__c> conQualList;
    private static final User currentUser = [   SELECT  ContactId, App_Previously_Studied_at_Monash__c
                                                FROM    User
                                                WHERE   Id =: userInfo.getUserId()];
    //this maps the qualifications for easier reference in saving
    private static final String admissionTestRecType = [SELECT Id, Name, sObjectType FROM RecordType WHERE DeveloperName = 'Admission_Test'].Id;
    
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static List<Contact_Qualification__c> retrieveConQualList() {
        conQualList = new List<Contact_Qualification__c>();

        if (currentUser.ContactId != null) {
            conQualList = [     SELECT  Id,
                                        Institution_Code__c,
                                        Institution_Name__c,
                                        Date_Achieved__c,
                                        Other_Institution__c,
                                        Qualification__c,
                                        Qualification__r.Qualification_Name__c,
                                        Other_Qualification__c,
                                        Qualification_Document__c,
                                        Qualification_Document__r.Filename__c,
                                        RecordType.Name
                                FROM    Contact_Qualification__c
                                WHERE   Contact__c =: currentUser.ContactId 
                                ORDER BY CreatedDate DESC];
        }
        
        return conQualList;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static List<Application_Qualification_Provided__c> retrieveAppQualProvidedList(String applicationId) {
        List<Application_Qualification_Provided__c> appQualList = new List<Application_Qualification_Provided__c>();

        if (currentUser.ContactId != null) {
            appQualList = [     SELECT  Id,
                                        Is_Submitted__c,
                                        Application__c,
                                        Contact_Qualification__c,
                                        Contact_Qualification__r.RecordType.Name,
                                        Contact_Qualification__r.Institution_Name__c,
                                        Contact_Qualification__r.Other_Institution__c,
                                        Contact_Qualification__r.Qualification__c,
                                        Contact_Qualification__r.Qualification__r.Qualification_Name__c,
                                        Contact_Qualification__r.Other_Qualification__c,
                                        Contact_Qualification__r.Qualification_Document__c,
                                        Contact_Qualification__r.Qualification_Document__r.Filename__c
                                FROM    Application_Qualification_Provided__c
                                WHERE   Application__c =: applicationId 
                                ORDER BY CreatedDate DESC];
        }
        
        return appQualList;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static Contact_Qualification__c retrieveConQualDetails() {
        conQualRecord = new Contact_Qualification__c();
        //TODO
        return conQualRecord;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  inserts a contact qualification record
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static Contact_Qualification__c upsertNewQualificationRecord (Contact_Qualification__c qualToUpsert, String qualDocId) {
        if (qualToUpsert != null) {
            if (qualToUpsert.Contact__c == null) {
                qualToUpsert.Contact__c = currentUser.ContactId;
            }
            if (qualDocId != null && qualDocId != '') {
                qualToUpsert.Qualification_Document__c = qualDocId;
            }
            upsert qualToUpsert;
        }
        return qualToUpsert;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  inserts a contact qualification with a selected 
                        qualification from lookup
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static Contact_Qualification__c upsertNewQualificationRecordWithSelQual (Contact_Qualification__c qualToUpsert, String qualificationSelected, String qualDocId) {
        if (qualToUpsert != null) {
            if (qualToUpsert.Contact__c == null) {
                qualToUpsert.Contact__c = currentUser.ContactId;
            }
            if (qualDocId != null && qualDocId != '') {
                qualToUpsert.Qualification_Document__c = qualDocId;
            }
            if (qualificationSelected != null) {
                List<Qualification__c> qualificationRec = [ SELECT  Qualification_Name__c 
                                                            FROM    Qualification__c 
                                                            WHERE   RecordTypeId =: admissionTestRecType
                                                            AND     Qualification_Name__c =: qualificationSelected
                                                            LIMIT   1 ];
                if (!qualificationRec.isEmpty()) {
                    qualToUpsert.Qualification__c = qualificationRec[0].Id;
                }
            }
            upsert qualToUpsert;
        }
        return qualToUpsert;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         2.May.2017         
    * @description  inserts a contact qualification record and application 
                        qualification provided
    * @revision     Ant Custodio, 14.Jul.2017 - populates the english test 
                        subscore fields
    *******************************************************************************/
    @AuraEnabled
    public static Contact_Qualification__c upsertNewQualificationRecordWithApp (Contact_Qualification__c qualToUpsert, 
                                                                                String applicationId, 
                                                                                String qualDocId) {
        if (qualToUpsert != null && applicationId != null) {
            if (qualToUpsert.Contact__c == null) {
                qualToUpsert.Contact__c = currentUser.ContactId;
            }
            if (qualDocId != null && qualDocId != '') {
                qualToUpsert.Qualification_Document__c = qualDocId;
            }

            upsert qualToUpsert;

            if (qualToUpsert.Reference_Count__c == 0 || qualToUpsert.Reference_Count__c == null) {
                Application_Qualification_Provided__c appQualificationToUpsert = new Application_Qualification_Provided__c();
                appQualificationToUpsert.Contact_Qualification__c = qualToUpsert.Id;
                appQualificationToUpsert.Application__c = applicationId;
                insert appQualificationToUpsert;
            }
        }
        return qualToUpsert;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         2.May.2017         
    * @description  inserts a contact qualification record and application 
                        qualification provided with a selected qualification 
                        from lookup
    * @revision     Ant Custodio, 14.Jul.2017 - populates the english test 
                        subscore fields
    *******************************************************************************/
    @AuraEnabled
    public static Contact_Qualification__c upsertNewQualificationRecordWithSelQualWithApp ( Contact_Qualification__c qualToUpsert, 
                                                                                            String qualificationSelected, 
                                                                                            String applicationId, 
                                                                                            String qualDocId) {
        if (qualToUpsert != null) {
            if (qualToUpsert.Contact__c == null) {
                qualToUpsert.Contact__c = currentUser.ContactId;
            }
            if (qualDocId != null && qualDocId != '') {
                qualToUpsert.Qualification_Document__c = qualDocId;
            }
            if (qualificationSelected != null) {
                List<Qualification__c> qualificationRec = [ SELECT  Qualification_Name__c 
                                                            FROM    Qualification__c 
                                                            WHERE   RecordTypeId =: admissionTestRecType
                                                            AND     Qualification_Name__c =: qualificationSelected
                                                            LIMIT   1 ];
                if (!qualificationRec.isEmpty()) {
                    qualToUpsert.Qualification__c = qualificationRec[0].Id;
                }
            }

            upsert qualToUpsert;

            if (qualToUpsert.Reference_Count__c == 0 || qualToUpsert.Reference_Count__c == null) {
                Application_Qualification_Provided__c appQualificationToInsert = new Application_Qualification_Provided__c();
                appQualificationToInsert.Contact_Qualification__c = qualToUpsert.Id;
                appQualificationToInsert.Application__c = applicationId;
                insert appQualificationToInsert;
            }
        }
        return qualToUpsert;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         26.Apr.2017         
    * @description  gives the list of countries
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static List<String> retrieveCountries() {
        List<String> options = new List<String>();

        Schema.DescribeFieldResult fieldResult = Contact_Qualification__c.Country__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getLabel());
        }       

        return options;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         26.Apr.2017         
    * @description  gives the list of completion years
    * @revision     13.May.2017 - updated rule: it should start from current year - 99 
                    26.Jul.2017 - updated the year formula to (current year +2) - 99
                    26.Jul.2017 - reverted it back - waiting for API to be updated
    *******************************************************************************/
    @AuraEnabled
    public static List<String> retrieveYearofCompletion() {
        List<String> options = new List<String>();
        
        //Change of rules (see revision)
        /*Schema.DescribeFieldResult fieldResult = Contact_Qualification__c.Year_of_Completion__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getLabel());
        }*/       
        
        for (Integer i=date.today().year(); i>=date.today().year()-99; i--) {
            options.add(String.valueOf(i));
        }

        return options;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         26.Apr.2017         
    * @description  gives the list of australian states
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static List<String> retrieveStates() {
        List<String> options = new List<String>();

        Schema.DescribeFieldResult fieldResult = Contact_Qualification__c.State__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getLabel());
        }       

        return options;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         26.Apr.2017         
    * @description  gives the list of australian states
    * @revision     13.May.2017 - updated rule: it should start from current year - 99 
    *******************************************************************************/
    @AuraEnabled
    public static List<String> retrievefirstYearComplete() {
        List<String> options = new List<String>();

        //Change of rules (see revision)
        /*Schema.DescribeFieldResult fieldResult = Contact_Qualification__c.First_Year_Enrolled__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getLabel());
        }*/   
        
        for (Integer i=date.today().year(); i>=date.today().year()-99; i--) {
            options.add(String.valueOf(i));
        }

        return options;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         26.Apr.2017         
    * @description  gives the list of australian states
    * @revision     13.May.2017 - updated rule: it should start from current year - 99 
    *******************************************************************************/
    @AuraEnabled
    public static List<String> retrievelastYearComplete() {
        List<String> options = new List<String>();

        //Change of rules (see revision)
        /*Schema.DescribeFieldResult fieldResult = Contact_Qualification__c.Last_Year_Enrolled__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getLabel());
        }*/       

        for (Integer i=date.today().year(); i>=date.today().year()-99; i--) {
            options.add(String.valueOf(i));
        }

        return options;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         26.Apr.2017         
    * @description  gives the list of level of completion
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static List<String> retrieveStatusOptions() {
        List<String> options = new List<String>();

        Schema.DescribeFieldResult fieldResult = Contact_Qualification__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getLabel());
        }       

        return options;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         26.Apr.2017         
    * @description  gives the admission type qualifications
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static List<String> retrieveAdmissionTestTypes() {
        List<String> options = new List<String>();
        for (Qualification__c qualificationRec: [   SELECT      Qualification_Name__c,
                                                                Available_on_Portal__c
                                                    FROM        Qualification__c 
                                                    WHERE       RecordTypeId =: admissionTestRecType
                                                                AND Available_on_Portal__c = true
                                                    ORDER BY    Qualification_Name__c ]) {
            options.add(qualificationRec.Qualification_Name__c);
        }
        return options;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         26.Apr.2017         
    * @description  gets the list of contact qualification record types
    * @revision     removed the Tertiary restriction
    *******************************************************************************/
    @AuraEnabled
    public static List<String> retrieveRecordTypeNames() {
        List<String> options = new List<String>();

        for( RecordType recType: [  SELECT      Id, Name, sObjectType 
                                    FROM        RecordType 
                                    WHERE       sObjectType = 'Contact_Qualification__c'
                                    ORDER BY    Name DESC])
        {
            //Tertiary education is not required for MUFY (previously studied at monash)
            //if (    (recType.Name != 'Tertiary Education' && recType.Name != 'Other Qualification') ||
            //        ((recType.Name == 'Tertiary Education' || recType.Name == 'Other Qualification') && !currentUser.App_Previously_Studied_at_Monash__c) ){
                options.add(recType.Name);
            //}
        }
        return options;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         26.Apr.2017         
    * @description  returns the selected record type's Id
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static String retrieveRecordTypeByName(String recordTypeName) {
        String recordTypeId = '';

        for( RecordType recType: [  SELECT  Id, Name, sObjectType 
                                    FROM    RecordType 
                                    WHERE   sObjectType = 'Contact_Qualification__c'
                                    AND     Name =: recordTypeName 
                                    LIMIT   1 ])
        {
            recordTypeId = recType.Id;
        }

        return recordTypeId;
    }    

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         10.Apr.2017         
    * @description  saves the new record
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static String deleteSelectedQualification (String qualificationId) {
        String deletedId = '';
        if (qualificationId != null && qualificationId != '') {
            List<Contact_Qualification__c> qualificationToDelete = [ SELECT Id FROM Contact_Qualification__c WHERE Id =: qualificationId LIMIT 1];
            if (!qualificationToDelete.isEmpty()) {
                deletedId = qualificationToDelete[0].Id;
                delete qualificationToDelete;
            }
        }
        return deletedId;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         10.Apr.2017         
    * @description  saves the new record
    * @revision     clears the 'Applying for Credit' field
    *******************************************************************************/
    @AuraEnabled
    public static String deleteSelectedAppQualificationProvided (String appQualProvidedId) {
        String deletedId = '';
        if (appQualProvidedId != null && appQualProvidedId != '') {
            List<Application_Qualification_Provided__c> appQualToDelete = [   SELECT  Id, Application__c,
                                                                                            Contact_Qualification__c,
                                                                                            Contact_Qualification__r.Reference_Count__c
                                                                                    FROM    Application_Qualification_Provided__c 
                                                                                    WHERE   Id =: appQualProvidedId 
                                                                                    LIMIT 1 ];
            if (!appQualToDelete.isEmpty()) {
                deletedId = appQualToDelete[0].Id;
                //if the reference count is 1, this means that this qualification is on a draft application
                //if it is more than 1, that means it has been prepopulated from a previously submitted application
                if (appQualToDelete[0].Contact_Qualification__r.Reference_Count__c < 2) {
                    //delete the parent
                    Contact_Qualification__c conQualToDelete = [  SELECT  Id, Qualification_Document__c
                                                                        FROM    Contact_Qualification__c
                                                                        WHERE   Id =: appQualToDelete[0].Contact_Qualification__c 
                                                                        LIMIT   1 ];
                    //delete the attachments if there's any before deleting the qualification
                    if (conQualToDelete.Qualification_Document__c != null) {
                        Contact_Document__c contDocToDelete = [ SELECT  Id
                                                                FROM    Contact_Document__c
                                                                WHERE   Id =: conQualToDelete.Qualification_Document__c];
                        delete contDocToDelete;
                    }

                    //finally, delete the parent contact qualification
                    delete conQualToDelete;
                } else {
                    //if it's been used on other applications, only delete the junction
                    delete appQualToDelete;
                }

                //revalidate the apply for credit flag
                validateApplyForCreditFlag(appQualToDelete[0].Application__c);
                
            }
        }
        return deletedId;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Jul.2017         
    * @description  updates the apply for credit flag everytime the AQP is deleted
    * @revision     
    *******************************************************************************/
    private static void validateApplyForCreditFlag (String appId) {
        //get the application record
        List<Application__c> applicationRec = [ SELECT  Id, Applying_for_Credit__c,
                                                        Applicant__c
                                                FROM    Application__c
                                                WHERE   Id =: appId
                                                LIMIT   1 ];
        if (!applicationRec.isEmpty()) {
            //clear picklist if there are no Tertiary Education left
            Integer tertiaryCount = [   SELECT  COUNT() 
                                        FROM    Application_Qualification_Provided__c 
                                        WHERE   Contact_Qualification__r.RecordType.Name = 'Tertiary Education' 
                                                AND Application__c =: applicationRec[0].Id];
            if (tertiaryCount == 0) {
                applicationRec[0].Applying_for_Credit__c = '';
                update applicationRec;
            }
        }
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         10.Apr.2017         
    * @description  returns the institution prefix
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static String retrieveInstitutionPrefix () {
        return Institution__c.sobjecttype.getDescribe().getKeyPrefix();
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         27.Apr.2017         
    * @description  returns the Institution__c record using 
                        the provided institution Id
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static Institution__c retrieveCustomSettingById (String institutionId) {
        Institution__c callistaInst = new Institution__c ();
        if (institutionId != null && institutionId != '') {
            List<Institution__c> callistaInstList = [ SELECT  Id, Institution_Code__c, Institution_Name__c
                                                                FROM    Institution__c
                                                                WHERE   Id =: institutionId ];
            if (!callistaInstList.isEmpty()) {
                callistaInst = callistaInstList[0];
            }
        }
        return callistaInst;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         11.May.2017         
    * @description  
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static Application_Qualification_Provided__c retrieveAppQualificationById (String qualId) {
        Application_Qualification_Provided__c appQualRecord = new Application_Qualification_Provided__c();

        if (qualId != null && qualId != '') {
            for (Application_Qualification_Provided__c aqpRecord: [ SELECT  Id,
                                                                            Is_Submitted__c,
                                                                            Application__c,
                                                                            Application__r.Credit_Intention__c,
                                                                            Contact_Qualification__c,
                                                                            Contact_Qualification__r.RecordType.Name,
                                                                            Contact_Qualification__r.Country__c,
                                                                            Contact_Qualification__r.Instruction_in_English__c,
                                                                            Contact_Qualification__r.First_Year_Enrolled__c,
                                                                            Contact_Qualification__r.Last_Year_Enrolled__c,
                                                                            Contact_Qualification__r.Institution_Name__c,
                                                                            Contact_Qualification__r.Other_Institution__c,
                                                                            Contact_Qualification__r.Qualification__c,
                                                                            Contact_Qualification__r.Qualification__r.Qualification_Name__c,
                                                                            Contact_Qualification__r.Other_Qualification__c,
                                                                            Contact_Qualification__r.Status__c,
                                                                            Contact_Qualification__r.Year_of_Completion__c,
                                                                            Contact_Qualification__r.State__c,
                                                                            Contact_Qualification__r.Score__c,
                                                                            Contact_Qualification__r.Qualification_Document__c,
                                                                            Contact_Qualification__r.Qualification_Document__r.Filename__c,
                                                                            Contact_Qualification__r.Date_Achieved__c,
                                                                            Contact_Qualification__r.Other_Qualification_Comments__c,
                                                                            Contact_Qualification__r.isTestCompleted__c,
                                                                            Contact_Qualification__r.Reference_Count__c,
                                                                            Contact_Qualification__r.Essay_Rating__c,
                                                                            Contact_Qualification__r.Listening__c,
                                                                            Contact_Qualification__r.Reading__c,
                                                                            Contact_Qualification__r.Speaking__c,
                                                                            Contact_Qualification__r.Test_of_Written_English__c,
                                                                            Contact_Qualification__r.Writing__c
                                                                    FROM    Application_Qualification_Provided__c
                                                                    WHERE   Id =: qualId
                                                                    LIMIT   1 ] ) {
                appQualRecord = aqpRecord;
            }
        }
        return appQualRecord;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         11.May.2017         
    * @description  
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static Contact_Qualification__c retrieveQualificationById (String qualId) {
        Contact_Qualification__c qualRecord = new Contact_Qualification__c();

        if (qualId != null && qualId != '') {
            for (Contact_Qualification__c qualList : [  SELECT  Id,
                                                                Name,
                                                                Contact__c,
                                                                RecordType.Name,
                                                                Country__c,
                                                                Instruction_in_English__c,
                                                                First_Year_Enrolled__c,
                                                                Last_Year_Enrolled__c,
                                                                Institution_Name__c,
                                                                Institution_Code__c,
                                                                Other_Institution__c,
                                                                Qualification__c,
                                                                Qualification__r.Qualification_Name__c,
                                                                Other_Qualification__c,
                                                                Status__c,
                                                                Year_of_Completion__c,
                                                                State__c,
                                                                Score__c,
                                                                Qualification_Document__c,
                                                                Qualification_Document__r.Filename__c,
                                                                Qualification_Document__r.Comments__c,
                                                                Qualification_Document__r.Document_Type_Name__c,
                                                                Date_Achieved__c,
                                                                Other_Qualification_Comments__c,
                                                                isTestCompleted__c,
                                                                Reference_Count__c,
                                                                Essay_Rating__c,
                                                                Listening__c,
                                                                Reading__c,
                                                                Speaking__c,
                                                                Test_of_Written_English__c,
                                                                Writing__c
                                                        FROM    Contact_Qualification__c
                                                        WHERE   Id =: qualId
                                                                    LIMIT   1 ] ) {
                qualRecord = qualList ;
            }
        }
        return qualRecord;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         6.Jul.2017         
    * @description  retrieve Institution record using the institution code
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static String retrieveInstitutionIdByCode (String insCode, String insName) {
        Institution__c instRecord = new Institution__c();
        for (Institution__c instRec: [  SELECT  Id
                                        FROM    Institution__c 
                                        WHERE   Institution_Code__c =: insCode
                                        AND     Institution_Name__c =: insName
                                        LIMIT   1 ] ) {
            instRecord = instRec;
        }
        return instRecord.Id;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         14.Jul.2017         
    * @description  retrieve english score fields depending on which admission 
                        test type the user selected
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static List<String> displayEnglishScoreFields (String selTestType) {
        List<String> fieldsToShow = new List<String>();
        if (selTestType != null && selTestType != '') {
            List<Qualification__c> qualificationRec = [ SELECT  Id,
                                                                Qualification_Name__c,
                                                                Available_on_Portal__c,
                                                                Sub_score_Type__c
                                                        FROM    Qualification__c 
                                                        WHERE   Qualification_Name__c =: selTestType
                                                        LIMIT   1 ];

            if (!qualificationRec.isEmpty()) {
                //convert multi select field to Set<String>
                Set<String> multiSelectFieldSet = appQualificationsCC.convertMultiSelectToSet(qualificationRec[0].Sub_score_Type__c);
                List<Schema.FieldSetMember> fieldSetMemberList = SObjectType.Contact_Qualification__c.FieldSets.Sub_Score_Field_Set.getFields();
                for (Schema.FieldSetMember fsmRec : fieldSetMemberList) {
                    if (multiSelectFieldSet.contains(fsmRec.getLabel())) {
                        fieldsToShow.add(fsmRec.getFieldPath());
                    }
                }
            }
        }
        return fieldsToShow;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         14.Jul.2017         
    * @description  converts a multi select field to a set of String
    * @revision     
    *******************************************************************************/
    public static Set<String> convertMultiSelectToSet (String multiSelectField) {
        Set<String> multiSelectSet = new Set<String>();
        if (multiSelectField != null && multiSelectField != '') {
            multiSelectSet.addAll(multiSelectField.split(';'));
        }
        return multiSelectSet;
    }
}