/*
 * A services class specific to EmailMessage sObject
 * @author PwC
 * 
 */
public class EmailMessageServices {
    
    //Anterey Custodio, 23.Jan.2017 - only fire trigger if it is one of these record types
    private static Map<String,String> allowedRecordTypesMap = retrieveAllowedRecordTypes();
    
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         23.Jan.2017       
    * @description  method to retrieve allowed record types
    * @revision     
    *******************************************************************************/
    private static Map<String,String> retrieveAllowedRecordTypes() {
        Map<String,String> allowedRTQueried = new Map<String,String>();
        for (RecordTypes_Allowed_on_CaseTrigger__c allowedRecordType: [SELECT API_Name__c FROM RecordTypes_Allowed_on_CaseTrigger__c]) {
            String recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(allowedRecordType.API_Name__c).getRecordTypeId();
            allowedRTQueried.put(recTypeId, allowedRecordType.API_Name__c);
        }
        
        return allowedRTQueried;
    }
    
    /**
    * purpose is to map the 'To' address on email message to the case that resulted from this email message, if required
    * this in turn allows case assignment rules engine to know which inbound email service the case originated from
    */ 
    public class mapEmailToAddressToCases implements Triggers.Handler {
        public void run(){
            
            Map<id,case> cases = new Map<id,case>(); // for bulk update later if required
            Set<id> caseIds = new Set<Id>();
            
            for(EmailMessage email : (List<EmailMessage>)Trigger.New){
                if(email.ToAddress != null && email.ParentId != null) caseIds.add(email.ParentId); // cull to only those we have info required
            }
            
            if(!caseIds.isEmpty()){
                // only want the cases that need the email field populated i.e. new 'email to salesforce' cases
                cases = new Map<id,Case>([select id, 
                                          Email_To__c,
                                          Latest_Inbound_Email__c
                                          from Case 
                                          where id in:caseIds 
                                          and (Email_To__c = null or Email_To__c = '')
                                          and (Origin = 'Email' or Origin like 'Email%')
                                          AND RecordTypeId IN: allowedRecordTypesMap.keySet()]); // cull to empty only
                if(!cases.isEmpty()){
                    
                    for(EmailMessage email : (List<EmailMessage>)Trigger.New){
                        if(email.ToAddress != null && email.ParentId != null && cases.containsKey(email.ParentId)) {
                         cases.put(email.ParentId, new Case(id = email.ParentId, Email_To__c = email.ToAddress)); // update email field 
                        }
                    }
                    try{
                        update cases.values(); // push new email values into affected cases
                    } catch (exception ex){
                        ExLog.write('EmailMessageServices','mapEmailToAddressToCases','updating case Email_To__c field',ex.getMessage());
                    }
                }
                
            }
        }
    }
    
    /** when inbound email received, flag the receipt on affected cases, so that customer service can see inbound activity */
    
    public class flagInboundEmailResponse implements Triggers.Handler {
        public void run(){
            
            Map<id,case> cases = new Map<id,case>(); // for bulk update later if required
            Set<id> caseIds = new Set<Id>();
            
            for(EmailMessage email : (List<EmailMessage>)Trigger.New){
                
                if( (email.Incoming==true || Test.isRunningTest()) && email.ParentId != null) caseIds.add(email.ParentId); // cull to only those we have info required
            }
            if(!caseIds.isEmpty()){
                // only want the cases that need the email field populated i.e. new 'email to salesforce' cases
                maintainCaseFields(caseIds);
                
            }
        }
    }
    
    /** updates cases that have an inbound email logged against them.
     * If is being reopened, then tries to reassign back to the most recent 'unassigned' queue that it belonged to.
     * @param caseIds the ids of the affected cases
     * @param emails the list of inbound email messages
     * @return void
     */
    public static void maintainCaseFields(Set<id> caseIds){
        Map<id,case> cases = new Map<id,Case>([select id, 
                                          Latest_Inbound_Email__c,
                                          Latest_Unassigned_Queue_OwnerId__c,
                                          Case_Queue_Name__c, 
                                          Status,
                                          CreatedDate,
                                          Origin,
                                          (select id,MessageDate from emailMessages where Incoming = true order by MessageDate DESC limit 2),
                                          IsClosed
                                          from Case 
                                          where id in:caseIds 
                                          AND RecordTypeId IN: allowedRecordTypesMap.keySet() ]); 
        
        Set<String> enquiryQueues = new Set<String>();
        for(QueueList__c qlist: [SELECT Name, Retain_Owner__c FROM QueueList__c WHERE Retain_Owner__c = false]){
          enquiryQueues.add(qList.Name);
        }

        if(!cases.isEmpty()){
            for(case c : cases.values()){
                // need to consider Email - System value and Email - Airport Pickup value
                if(!(c.Origin != null && c.Origin.indexOf('Email - ') != -1  && c.emailMessages.size() == 1) && !c.emailMessages.isEmpty()){ // ignores the email2case channel initial email
                    system.debug('** met criteria so reopening');
                    datetime msgDate = (c.emailMessages[0].MessageDate != null) ? c.emailMessages[0].MessageDate : datetime.now();
                    case updatedCase = new Case(id = c.id, Latest_Inbound_Email__c = msgDate);
                    if (c.IsClosed) updatedCase.status = 'Open';
                    
                    if (c.IsClosed && c.Latest_Unassigned_Queue_OwnerId__c > '' && enquiryQueues.contains(c.Case_Queue_Name__c)) 
                    {
                      updatedCase.ownerId = c.Latest_Unassigned_Queue_OwnerId__c;
                    }

                    cases.put(c.id, updatedCase);  // update email field and status
                }
            }
            try{
                update cases.values(); // push new email values into affected cases
            } catch (exception ex){
                ExLog.write('EmailMessageServices','flagInboundEmailResponse','updating Latest_Inbound_Email field',ex.getMessage());
            }
        }
    }
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         28.Jul.2017        
    * @description  method that sends email to the agent cloning the provided
                      list of emailmessage records
    * @revision     
    *******************************************************************************/
    @InvocableMethod
    public static void sendEmailToAgent (List<String> emailMsgIds) {
		//key = EmailMessage.Id, value = Case.Id
		Set<String> caseIds = new Set<String>();
		//get all the parent Ids
		for(EmailMessage email : [SELECT Id, ParentId 
		                        FROM EmailMessage 
		                        WHERE Id IN: emailMsgIds]){
		  caseIds.add(email.ParentId);
		}

		//Only include Admissions Record Type and Notify Agent = true
		String admissionsRecType = CommonServices.recordTypeId('Case','Admissions');
		Map<String, Case> casesFoundMap = new Map<String, Case> ();
		for (Case caseRec: [  SELECT  Id, Application_Course_Preference__c,
		                            Application_Course_Preference__r.Application__r.Agent_Email__c
		                    FROM    Case
		                    WHERE   Id IN: caseIds AND
		                            Notify_Agent__c = true AND
		                            RecordTypeId =: admissionsRecType AND
		                            Application_Course_Preference__r.Application__r.Agent_Email__c != '' ] ) {
			casesFoundMap.put(caseRec.Id, caseRec);
		}

		List<Messaging.SingleEmailMessage> messagesToSend = new List<Messaging.SingleEmailMessage>();
		for (EmailMessage email : [SELECT  Id,
                                        HtmlBody, 
                                        HasAttachment, 
                                        Subject,
                                        ToAddress,
                                        ParentId
                                FROM    EmailMessage
                                WHERE   Id IN: emailMsgIds]) {
          
			if (casesFoundMap.containsKey(email.ParentId) ) {
				Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
				//get the attachment
				List<String> attIds = new List<String>();
				for (Attachment attRec: [ SELECT  Id 
				                          FROM    Attachment 
				                          WHERE   ParentId =: email.Id ]) {
				  attIds.add(attRec.Id);
				}

				
				if (!attIds.isEmpty()) {
			  		message.setEntityAttachments(attIds);
			    }
			    message.setHtmlBody(email.HtmlBody);
			    message.setSubject(email.Subject);
			    List<String> emailAddresses = new List<String>();
			    emailAddresses.add(casesFoundMap.get(email.ParentId).Application_Course_Preference__r.Application__r.Agent_Email__c);
			    message.setToAddresses(emailAddresses);
			    messagesToSend.add(message);
			}
      	}

      if (!messagesToSend.isEmpty()) {

        Messaging.SendEmailResult[] results = Messaging.sendEmail(messagesToSend);
        if (results[0].success) {
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: '
                  + results[0].errors[0].message);
        }
      }
    }
}