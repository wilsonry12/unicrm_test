/*
    AUTHOR : Vineeth Batreddy
    DESCRIPTION : This class is used to create records for unit tests
*/
public without sharing class StudFirst_TestHelper {
	
    public static Application__c createApplication(Contact applicant) {
        Application__c application = new Application__c();
        application.Applicant__c = applicant.Id;
        return application;
    }
    
    public static Contact createApplicant() {
        Contact applicant = new Contact();
        applicant.FirstName = 'Joe';
        applicant.LastName = 'Bloggs';
        applicant.Salutation = 'Mr';
        applicant.Gender__c = 'M';
        applicant.Preferred_Name__c = 'RD';
        applicant.Previous_Surname__c = 'Something';
        applicant.Previously_Studied_at_Monash__c = false;
        applicant.Previous_Monash_Institution__c = '';
        applicant.Previous_Monash_ID__c = '';
        applicant.Birthdate = System.today();
        applicant.Email = 'applicant@mailinator.com';
        applicant.MailingStreet = '1 smile street';
        applicant.MailingCity = 'Melbourne';
        applicant.MailingState = 'Victoria';
        applicant.MailingCountry = 'Australia';
        applicant.MailingPostalCode = '3000';

        return applicant;
    }

    public static Application_Course_Preference__c createCoursePreferences(Application__c application, Contact applicant, Course_Offering__c courseOffering, Course__c course) {
        Application_Course_Preference__c acp = new Application_Course_Preference__c();
        acp.Academic_CI_Sequence_Number__c = 1;
        acp.Academic_Calendar_Type__c = '';
        acp.Admission_CI_Sequence_Number__c = 1;
        acp.Admission_Calendar_Type__c = '';
        acp.Applicant__c = applicant.Id;
        acp.Application__c = application.Id;
        acp.Attendance_Mode__c = '';
        acp.Attendance_Type__c = 'FT';
        acp.Commencement_Period__c = '';
        acp.Course_Code__c = 'M00001';
        //acp.Course_Offering__c = courseOffering.Id;
        acp.Course_Title__c = 'Master Of Something';
        acp.Course_Version_Number__c = 1;
        acp.Course__c = course.Id;
        acp.Location_Code__c = '';
        acp.Location__c = '';
        acp.Order__c = 1;
        acp.Outcome_Status__c = '';
        acp.Preference_Number__c = '';
        acp.Status__c = '';
        acp.Attendance_Mode_Description__c = 'On Campus';
        acp.Attendance_Type_Description__c = 'FULL TIME';
        acp.Location_Code__c = 'CLAYTON';
        acp.Type_of_Place_Description__c = 'Test type of place';
        acp.Unit_Set_Code__c = 'MM00000';
        acp.Unit_Set_Description__c = 'set description';

        return acp;
    }

    public static Contact_Qualification__c createContactQualification(Contact applicant, Qualification__c q, String rType) {
        Contact_Qualification__c cq = new Contact_Qualification__c();
        cq.Contact__c = applicant.Id;
        cq.Country__c = 'Australia';
        cq.Date_Achieved__c = System.today();
        cq.First_Year_Enrolled__c = '';
        cq.Institution_Code__c = '';
        cq.Institution_Name__c = 'Institution';
        cq.Instruction_in_English__c = true;
        cq.Last_Year_Enrolled__c = '';
        cq.Other_Institution__c = '';
        cq.Other_Qualification_Comments__c = '';
        cq.Other_Qualification__c = '';
        cq.Qualification__c = q.Id;
        cq.Score__c = '';
        cq.State__c = '';
        cq.State__c = '';
        cq.Status__c = '';
        cq.Year_of_Completion__c = '';
        cq.isTestCompleted__c = true;
        Map<String,RecordType> recordTypes = getRecordTypes('Contact_Qualification__c');
        cq.RecordType = recordTypes.get(rType);
        cq.RecordTypeId = recordTypes.get(rType).Id;
        //for Admission Test
        cq.Listening__c = '8';
        cq.Reading__c = '8';
        cq.Writing__c = '8';
        cq.Speaking__c = '8';
        return cq;
    }



    public static Work_Experience__c createWorkExperience(Contact applicant) {
        Work_Experience__c we = new Work_Experience__c();
        we.Contact__c = applicant.Id;
        we.Position__c = '';
        we.Employer_Name__c = '';
        we.Start_Date__c = System.today();
        we.End_Date__c = System.today();
        we.Contact_Person_First_Name__c = '';
        we.Contact_Person_Last_Name__c = '';
        
        return we;
    }

    public static Course_Offering__c createCourseOffering(Course__c course) {
        Course_Offering__c courseOffering = new Course_Offering__c();
        courseOffering.Course__c = course.Id;
        courseOffering.Academic_CI_Sequence_Number__c = 1;
        courseOffering.Academic_Calendar_Type__c = '2019';
        courseOffering.Admission_CI_Sequence_Number__c = 1;
        courseOffering.Admission_Calendar_Type__c = 'ADM';
        courseOffering.Attendance_Mode__c = 'SCG';
        courseOffering.Attendance_Type__c = 'PT';
        courseOffering.Course_Code__c = 'M00001';
        courseOffering.Course_Title__c = 'Master Of Something';
        courseOffering.Course_Version_Number__c = 1;
        courseOffering.Admission_Calendar_Description__c = 'July-2019';
        courseOffering.Attendance_Mode_Description__c = 'On Campus';
        courseOffering.Attendance_Type_Description__c = 'FULL TIME';
        courseOffering.Citizenship_Type__c = 'DOM-AUS';
        courseOffering.Course_Type_Group__c = 'OPG';
        courseOffering.Location_Code__c = 'CLAYTON';
        courseOffering.Type_of_Place_Description__c = 'Test type of place';
        courseOffering.Type_of_Place_Code__c = 'PC001';
        courseOffering.Unit_Set_Code__c = 'MM00000';
        courseOffering.Unit_Set_Description__c = 'set description';
        courseOffering.Active__c = true;
        return courseOffering;
    }

    public static Course_Offering__c createCourseOfferingNoUnitSet(Course__c course) {
        Course_Offering__c courseOffering = new Course_Offering__c();
        courseOffering.Course__c = course.Id;
        courseOffering.Academic_CI_Sequence_Number__c = 1;
        courseOffering.Academic_Calendar_Type__c = '2019';
        courseOffering.Admission_CI_Sequence_Number__c = 1;
        courseOffering.Admission_Calendar_Type__c = 'ADM';
        courseOffering.Attendance_Mode__c = 'SCD';
        courseOffering.Attendance_Type__c = 'FT';
        courseOffering.Course_Code__c = 'M00002';
        courseOffering.Course_Title__c = 'Master Of Something Too';
        courseOffering.Course_Version_Number__c = 1;
        courseOffering.Admission_Calendar_Description__c = 'January-2019';
        courseOffering.Attendance_Mode_Description__c = 'On Campus';
        courseOffering.Attendance_Type_Description__c = 'FULL TIME';
        courseOffering.Citizenship_Type__c = 'DOM-AUS';
        courseOffering.Course_Type_Group__c = 'OPG';
        courseOffering.Location_Code__c = 'CLAYTON';
        courseOffering.Type_of_Place_Code__c = 'PC002';
        courseOffering.Active__c = true;
        return courseOffering;
    }

    public static Course__c createCourse() {
        Course__c course = new Course__c();
        course.Name = '12345';

        return course; 
    }

    public static Qualification__c createQualification(String name) {
        Qualification__c q = new Qualification__c();
        q.Qualification_Name__c = name;
        q.Overseas__c = false;
        q.Callista_Code__c = '123';
        //for Admission Test
        q.Sub_score_Type__c = 'Listening; Reading; Speaking; Writing';

        return q;
    }
	
    public static Application_Document_Provided__c createApplicationDocument(String applicationId, String contactDocumentId) {
        Application_Document_Provided__c adp = new Application_Document_Provided__c();
        adp.Contact_Document__c = contactDocumentId;
        adp.Application__c = applicationId;
        
        return adp;
    }
    
    public static Contact_Document__c createContactDocument(String contactId, String cqId) {
        Contact_Document__c cd = new Contact_Document__c();
        cd.Contact__c = contactId;
        cd.Contact_Qualification__c = cqId;
        cd.Comments__c = 'My comments';
        cd.Filename__c = 'Test File';
        cd.Document_Type__c = 'OTHERDCTYP';
        
        return cd;
    }
    
    public static Attachment createAttachment(String parentId) {
        Attachment attachment = new Attachment();
        attachment.Name = 'Test File';
        attachment.ParentId = parentId;
        attachment.ContentType = 'PDF';
        attachment.Description = 'Test File';
        attachment.Body = Blob.valueOf('Hello');
        
        return attachment;
    }
    
    public static Map<String,RecordType> getRecordTypes(String obj) {
        //Query for the record types of an object
        List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType= :obj and isActive=true];
     
        //Create a map between the RecordType Name and RecordType
        Map<String,RecordType> recordTypes = new Map<String,RecordType>{};
        for(RecordType rt: rtypes)
            recordTypes.put(rt.Name,rt);
     
        return recordTypes;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         24.May.2017        
    * @description  creates and returns a domestic applicant profile community user
    * @revision     
    *******************************************************************************/
    public static User createCommunityUser () {
        User userRec;
        try {
            String profileId = [SELECT Id FROM Profile WHERE Name = 'Domestic Applicants' LIMIT 1].Id;

            Account accountRecord = new Account(name ='AccountTest') ;
            insert accountRecord;

            Contact contactRecord = new Contact();
            contactRecord.FirstName = 'First';
            contactRecord.LastName = 'Last';
            contactRecord.AccountId = accountRecord.Id;
            insert contactRecord;

            Integer rnd = Integer.valueOf(Math.random()*9999);
            userRec = new User( Alias = String.valueOf(rnd)+ 'usr', 
                                Email = String.valueOf(rnd) +'testuser@mutesting.edu', 
                                EmailEncodingKey = 'UTF-8', 
                                LastName = 'TestingUser', 
                                LanguageLocaleKey = 'en_US', 
                                LocaleSidKey = 'en_GB', 
                                ProfileId = profileId,
                                TimeZoneSidKey = 'Australia/Sydney', 
                                UserName = String.valueOf(rnd) +'testuser@mutesting.edu',
                                ContactId = contactRecord.Id,
                                App_Salutation__c = '',
                                App_Gender__c = '',
                                App_Birthdate__c = null,
                                App_Residency_Status__c = '',
                                Street = '',
                                App_Address_Type__c = 'POSTAL');
            insert userRec;

        } catch (Exception ex) {
            System.debug('@@ex: '+ ex.getMessage());
            ExLog.add('ApplicationPortalCC Test Class', 'ApplicationPortalCC_Test', 'createCommunityUser' , ex.getMessage());
        }

        return userRec;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         24.May.2017        
    * @description  returns an tertiary institution record
    * @revision     
    *******************************************************************************/
    public static Institution__c createTertiaryInstitution () {
        Institution__c tertInsRec = new Institution__c();
        tertInsRec.Country__c = 'Australia';
        tertInsRec.Institution_Code__c = '834';
        tertInsRec.Institution_Name__c = 'CANNING COLLEGE, AUSTRALIA';
        tertInsRec.Type__c = 'Tertiary';

        return tertInsRec;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         24.May.2017        
    * @description  returns a tertiary qualification record
    * @revision     
    *******************************************************************************/
    public static Qualification__c createTertiaryQualification () {
        String TERTIARY_RECORDTYPE = Schema.SObjectType.Qualification__c.getRecordTypeInfosByName().get('Tertiary Qualification').getRecordTypeId();
        Qualification__c tertqualRec = new Qualification__c();
        tertqualRec.Callista_Code__c = 'AP';
        tertqualRec.Overseas__c = false;
        tertqualRec.Qualification_Name__c = 'APPRENTICESHIP';
        tertqualRec.RecordTypeId = TERTIARY_RECORDTYPE;

        return tertqualRec;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         24.May.2017        
    * @description  returns an tertiary institution record
    * @revision     
    *******************************************************************************/
    public static Institution__c createSecondaryInstitution () {
        Institution__c secInsRec = new Institution__c();
        secInsRec.Country__c = 'Australia';
        secInsRec.Institution_Code__c = '196';
        secInsRec.Institution_Name__c = 'ST LEONARD\'S COLLEGE - IB';
        secInsRec.State__c = 'VIC';
        secInsRec.Type__c = 'Secondary';
        
        return secInsRec;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         24.May.2017        
    * @description  returns a Secondary qualification record
    * @revision     
    *******************************************************************************/
    public static Qualification__c createSecondaryQualification () {
        
        String SECONDARY_RECORDTYPE = Schema.SObjectType.Qualification__c.getRecordTypeInfosByName().get('Secondary Qualification').getRecordTypeId();
        Qualification__c secondQualRec = new Qualification__c();
        secondQualRec.Callista_Code__c = 'FY-TRINITY';
        secondQualRec.Overseas__c = false;
        secondQualRec.Qualification_Name__c = 'TRINITY FOUNDATION YEAR';
        secondQualRec.State__c = 'VIC';
        secondQualRec.RecordTypeId = SECONDARY_RECORDTYPE;

        return secondQualRec;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         24.May.2017        
    * @description  returns a English Test record
    * @revision     
    *******************************************************************************/
    public static Qualification__c createEnglishTest () {
        
        String ENGLISH_TEST_RECORDTYPE = Schema.SObjectType.Qualification__c.getRecordTypeInfosByName().get('Admission Test').getRecordTypeId();
        Qualification__c englishTest = new Qualification__c();
        englishTest.Available_on_Portal__c = true;
        englishTest.Callista_Code__c = 'IELTS';
        englishTest.Overseas__c = false;
        englishTest.Qualification_Name__c = 'IELTS (INTERNATIONAL ENGLISH LANGUAGE TESTING SERVICE)';
        englishTest.RecordTypeId = ENGLISH_TEST_RECORDTYPE;
        
        return englishTest;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         24.May.2017        
    * @description  returns a contact document record
    * @revision     
    *******************************************************************************/
    public static Contact_Document__c createContactDocument (String contactId) {
        
        Contact_Document__c docRec = new Contact_Document__c();
        docRec.Comments__c = 'certificate';
        docRec.Contact__c = contactId;
        docRec.Document_Type__c = 'ACCEPT-ADM';
        docRec.Filename__c = 'certificate.pdf';

        return docRec;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         24.May.2017        
    * @description  populates an existing work experience record
    * @revision     
    *******************************************************************************/
    public static Work_Experience__c populateWorkExperienceRecord (Work_Experience__c workExpRecord) {

        workExpRecord.Position__c = 'Developer';
        workExpRecord.Employer_Name__c = 'Descenture';
        workExpRecord.Start_Date__c = date.today().addMonths(-12);
        workExpRecord.End_Date__c = date.today().addMonths(-1);
        workExpRecord.Contact_Person_Phone__c = '123456';
        workExpRecord.Contact_Person_First_Name__c = 'Steph';
        workExpRecord.Contact_Person_Last_Name__c = 'Curry';
        workExpRecord.Contact_Person_Email__c = 'scurry30@gmail.com.test';
        workExpRecord.Contact_Person_Street__c = '7000 Coliseum Way';
        workExpRecord.Contact_Person_City__c = 'Oakland';
        workExpRecord.Contact_Person_State__c = 'California';
        workExpRecord.Contact_Person_PostalCode__c = '94621';

        return workExpRecord;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         24.May.2017        
    * @description  returns a citizenship type custom setting
    * @revision     
    *******************************************************************************/
    public static Callista_Citizenship_Type__c createCitizenshipTypeRecord () {
        
        Callista_Citizenship_Type__c citizenshipType = new Callista_Citizenship_Type__c();
        citizenshipType.Name = 'DOM-AUS';
        citizenshipType.Description__c = 'AUSTRALIAN CITIZEN (INCL AUS CITIZENS WITH DUAL CITIZENSHIP)';
        citizenshipType.Explanation__c = 'I am an Australian citizen (with or without dual citizenship)';
        citizenshipType.Type__c = 'DOMESTIC';
        citizenshipType.Campus_of_Study__c = 'Australian campus and any location not listed below';
        
        return citizenshipType;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         24.May.2017        
    * @description  returns a Australian Postcodes custom setting
    * @revision     
    *******************************************************************************/
    public static Australian_Postcodes__c createAustralicanPostcodeRecord () {
        
        Australian_Postcodes__c ausPostal = new Australian_Postcodes__c();
        ausPostal.Name = '3000';
        
        return ausPostal;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         24.May.2017        
    * @description  returns an application record 
    * @notes        [OVERLOADED TO RETRIEVE IDS INSTEAD OF ACTUAL CONTACT]
    * @revision     
    *******************************************************************************/
    public static Application__c createApplication(String applicantId) {
        Application__c application = new Application__c();
        application.Applicant__c = applicantId;
        return application;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         25.May.2017        
    * @description  creates an Application qualification record
    * @revision     
    *******************************************************************************/
    public static Application_Qualification_Provided__c createAppQualProvided(String appId, String conQualId) {
        Application_Qualification_Provided__c aqpRecord = new Application_Qualification_Provided__c();
        aqpRecord.Application__c = appId;
        aqpRecord.Contact_Qualification__c = conQualId;
        return aqpRecord;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         25.May.2017        
    * @description  creates an Application qualification record
    * @revision     
    *******************************************************************************/
    public static Application_Work_Experience_Provided__c createAppWorkExpProvided(String appId, String workExpRecordId) {
        Application_Work_Experience_Provided__c aweRecord = new Application_Work_Experience_Provided__c();
        aweRecord.Application__c = appId;
        aweRecord.Work_Experience__c = workExpRecordId;
        return aweRecord;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         30.May.2017        
    * @description  creates an Application qualification record
    * @revision     
    *******************************************************************************/
    public static Document_Requested__c  createDocRequestRecord(String contactId, String appId, String acpId) {
        Document_Requested__c docReqRecord = new Document_Requested__c();
        docReqRecord.Additional_Documents__c = 'TEST';
        docReqRecord.Applicant__c = contactId;
        docReqRecord.Application__c = appId;
        docReqRecord.Application_Course_Preference__c = acpId;
        return docReqRecord;
    }

    public static Callista_Outcome_Status__c createOutcomeStatus() {
        Callista_Outcome_Status__c outcomeStatus = new Callista_Outcome_Status__c();
        outcomeStatus.Name = 'TEST-STATUS';
        outcomeStatus.FSPortal_Status__c = 'Testing';
        outcomeStatus.Explanation__c = 'This is a test';
        outcomeStatus.Status__c = 'Test';
        return outcomeStatus;
    }

    /*******************************************************************************
                Ryan Wilson - Helper methods for the Assessment plan    
    *******************************************************************************/

    public static List<Course_Offering__c> createCourseOfferingForAssessment() {
        List<Course_Offering__c> courseOffers = new List<Course_Offering__c>();

        for(Integer i=1; i<=2; i++){
            Course_Offering__c courseOffering = new Course_Offering__c();
            courseOffering.Academic_CI_Sequence_Number__c = 1;
            courseOffering.Academic_Calendar_Type__c = '2019';
            courseOffering.Admission_CI_Sequence_Number__c = 1;
            courseOffering.Admission_Calendar_Type__c = 'ADM';
            courseOffering.Attendance_Mode__c = 'IN';
            courseOffering.Attendance_Type__c = 'FT';
            courseOffering.Course_Code__c = 'M0000'+i;
            courseOffering.Course_Title__c = 'Master Of Something Too';
            courseOffering.Course_Version_Number__c = 1;
            courseOffering.Admission_Calendar_Description__c = 'January-2019';
            courseOffering.Attendance_Mode_Description__c = 'On Campus';
            courseOffering.Attendance_Type_Description__c = 'FULL TIME';
            courseOffering.Citizenship_Type__c = 'DOM-AUS';
            courseOffering.Course_Type_Group__c = 'OPG';
            courseOffering.Location_Code__c = 'CITY';
            courseOffering.Type_of_Place_Code__c = 'FULL-FEE';
            courseOffering.Faculty__c = 'Faculty of Law';
            courseOffering.Active__c = true;
            courseOffers.add(courseOffering);
        }

        return courseOffers;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         26.Jun.2017        
    * @description  Create Assessor group 
    * @revision     
    *******************************************************************************/
    public static Assessor_Group__c createAssessorGroup() {
        Assessor_Group__c assessorGroup = new Assessor_Group__c();
        assessorGroup.Name = 'Test Assessor Group';
        assessorGroup.Faculty__c = 'Faculty of Law';
        return assessorGroup;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         26.Jun.2017        
    * @description  Create Assessment plan 
    * @revision     
    *******************************************************************************/
    public static Assessment_Plan__c createAssessmentPlan() {
        Assessment_Plan__c assessmentPlan = new Assessment_Plan__c();
        assessmentPlan.Name = 'Test Assessment Plan';
        assessmentPlan.Plan_Faculty__c = 'Faculty of Law'; 
        assessmentPlan.Plan_Description__c = 'Test description';
        return assessmentPlan;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         29.Jun.2017        
    * @description  Create Assessment plan task
    * @revision     
    *******************************************************************************/
    public static Assessment_Plan_Task__c createAssessmentPlanTask(String apId, String arId) {
        Assessment_Plan_Task__c aptRecord = new Assessment_Plan_Task__c();
        aptRecord.Assessment_Plan__c = apId;
        aptRecord.Assessor_Role__c = arId;
        aptRecord.Description__c = 'DESC 1';
        aptRecord.Status__c = 'Active';
        aptRecord.Step__c = 1;
        aptRecord.Task_Title__c = 'Assess Application';
        aptRecord.Type__c = 'Assess';
        return aptRecord;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         29.Jun.2017        
    * @description  Create Assessor Role 
    * @revision     
    *******************************************************************************/
    public static Assessor_Role__c createAssessorRole(String agId) {
        Assessor_Role__c arRecord = new Assessor_Role__c();
        arRecord.Name = 'ROLE1';
        arRecord.Assesor_Group__c = agId;
        arRecord.Role_Description__c = 'ROLE 1';
        return arRecord;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         29.Jun.2017        
    * @description  Create Assessment selection rule 
    * @revision     
    *******************************************************************************/
    public static Assessment_Selection_Rule__c createAssessmentSelectionRule(String agId, String apId) {
        Assessment_Selection_Rule__c asrRecord = new Assessment_Selection_Rule__c();
        asrRecord.Name = 'ASR 1';
        asrRecord.Assesor_Group__c = agId;
        asrRecord.Assessment_Plan__c = apId;
        asrRecord.Course_Filter__c = 'All';
        asrRecord.Course_Codes__c = 'M00001';
        asrRecord.Location__c = 'CLAYTON';
        return asrRecord;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         29.Jun.2017        
    * @description  Create Queue with members
    * @revision     
    *******************************************************************************/
    public static Group createGroupWithMembers(String arName, Boolean allowQueueInsert) {
        Group assessorGroupRec = new Group();
        assessorGroupRec.Name = arName;
        assessorGroupRec.Type = 'Queue';
        insert assessorGroupRec;

        GroupMember groupMemberRec = new GroupMember();
        groupMemberRec.GroupId = assessorGroupRec.Id;
        groupMemberRec.UserOrGroupId = UserInfo.getUserId();
        insert groupMemberRec;

        //if true, add queue to the group
        if (allowQueueInsert) {
            List<QueueSobject> queueList = new List<QueueSobject>();
            QueueSobject queueRec = new QueueSobject();
            queueRec.QueueId = assessorGroupRec.Id;
            queueRec.SobjectType = 'Assessment_Plan_Task__c';
            queueList.add(queueRec);
            QueueSobject queueRec2 = new QueueSobject();
            queueRec2.QueueId = assessorGroupRec.Id;
            queueRec2.SobjectType = 'Assessment_Plan__c';
            queueList.add(queueRec2);
            insert queueList;
        }

        return assessorGroupRec;
    }
}