/*******************************************************************************
* @author       Ryan Wilson
* @date         22.Nov.2016         
* @description  test class for BMSEmailPreviewCC
* @revision     
*******************************************************************************/
@isTest
private class BMSEmailPreviewCC_Test {
        
        @isTest static void test_EmailPreview() {
                List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
                insert contactRecordList;
                
                EmailTemplate template = BMS_TestDataHelper.buildTemplate();
                Campaign massEmail = BMS_TestDataHelper.buildSampleCRMMassEmail(template.Name);
                massEmail.Additional_Email__c = 'ryan.wilson@monash.edu';
                massEmail.Notify__c = 'ryan.wilson@monash.edu';
                massEmail.Batch_Finished__c = true;
                insert massEmail;

                test.startTest();
                PageReference pageRef = Page.BMSEmailPreview;
                Test.setCurrentPage(pageRef);

                apexpages.currentpage().getparameters().put('crmId',massEmail.Id);
                apexpages.currentpage().getparameters().put('etId',template.Id);
                apexpages.currentpage().getparameters().put('view','EmailView');

                BMSEmailPreviewCC bmsEmailPreview = new BMSEmailPreviewCC();
                bmsEmailPreview.editEmail();
                bmsEmailPreview.saveSend();
                bmsEmailPreview.saveHold();
                bmsEmailPreview.cancel();

                test.stopTest();
        }
        /*******************************************************************************
        * @author       Anterey Custodio
        * @date         07.Mar.2017       
        * @description  view when batch is not yet finished
        *******************************************************************************/
        @isTest static void test_EmailPreview_BatchNotFinished() {
                List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
                insert contactRecordList;
                
                EmailTemplate template = BMS_TestDataHelper.buildTemplate();
                Campaign massEmail = BMS_TestDataHelper.buildSampleCRMMassEmail(template.Name);
                massEmail.Additional_Email__c = 'ryan.wilson@monash.edu';
                massEmail.Notify__c = 'ryan.wilson@monash.edu';
                                massEmail.Batch_Finished__c = true;
                insert massEmail;

                test.startTest();
                PageReference pageRef = Page.BMSEmailPreview;
                Test.setCurrentPage(pageRef);

                apexpages.currentpage().getparameters().put('crmId',massEmail.Id);
                apexpages.currentpage().getparameters().put('etId',template.Id);
                apexpages.currentpage().getparameters().put('view','EmailView');

                BMSEmailPreviewCC bmsEmailPreview = new BMSEmailPreviewCC();
                bmsEmailPreview.editEmail();
                bmsEmailPreview.saveSend();
                bmsEmailPreview.saveHold();
                bmsEmailPreview.cancel();

                test.stopTest();
        }

        /*******************************************************************************
        * @author       Anterey Custodio
        * @date         07.Mar.2017       
        * @description  for staff
        *******************************************************************************/
        @isTest static void test_EmailPreview_Staff() {
                List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
                insert contactRecordList;

                Folder bmsFolder = [SELECT DeveloperName,Id,Name,Type FROM Folder WHERE Name = 'BMS Emails'];
                EmailTemplate template =  [SELECT Id, HtmlValue, FolderId, Name FROM EmailTemplate WHERE FolderId =:bmsFolder.Id AND Name = 'Staff Communication' LIMIT 1];
                Campaign massEmail = BMS_TestDataHelper.buildSampleCRMMassEmail(template.Name);
                massEmail.Additional_Email__c = 'ryan.wilson@monash.edu';
                massEmail.Notify__c = 'ryan.wilson@monash.edu';
                                massEmail.Batch_Finished__c = true;
                insert massEmail;

                test.startTest();
                PageReference pageRef = Page.BMSEmailPreview;
                Test.setCurrentPage(pageRef);

                apexpages.currentpage().getparameters().put('crmId',massEmail.Id);
                apexpages.currentpage().getparameters().put('etId',template.Id);
                apexpages.currentpage().getparameters().put('view','EmailView');

                BMSEmailPreviewCC bmsEmailPreview = new BMSEmailPreviewCC();
                bmsEmailPreview.editEmail();
                bmsEmailPreview.saveSend();
                bmsEmailPreview.saveHold();
                bmsEmailPreview.cancel();

                test.stopTest();
        }

        @isTest static void test_SMSPreview() {
                Campaign crmSMS = BMS_TestDataHelper.buildSampleCRMMassEmail('Sample Template');
                crmSMS.Type = 'SMS';
                insert crmSMS;

                String strCsvContent = 'MobilePhone'+'\r\n'+'61234567890'+'\r\n'+'61324567890'+'\r\n'+'61474567890'+'\r\n';
                Blob csvBlob = Blob.valueOf(strCsvContent);

                Attachment attachmentRec = new Attachment();
                attachmentRec.Body = csvBlob;
                attachmentRec.Name = 'fileName';
                attachmentRec.ParentId = crmSMS.Id;
                insert attachmentRec;

                MarketingCloudApp__c mcApp = new MarketingCloudApp__c();
                mcApp.Name = 'Credentials';
                mcApp.Client_ID__c = 'ssjhdsakhd882793879';
                mcApp.Client_Secret__c = 'sdhsaldhlhd7263535336';
                insert mcApp;

                Test.startTest();
                //Test SMS Token mock response
                SingleRequestMock fakeSMSSendResponse = new SingleRequestMock(202,
                                                         'Complete',
                                                         '{"tokenId": "ZUROcEpwLWJCMFNCRXRYZnFkb3lYZzo3Njox"}',
                                                         null);
                Test.setMock(HttpCalloutMock.class, fakeSMSSendResponse);

                PageReference pageRef = Page.BMSEmailPreview;
                Test.setCurrentPage(pageRef);

                apexpages.currentpage().getparameters().put('crmId',crmSMS.Id);
                apexpages.currentpage().getparameters().put('type','sms');

                BMSEmailPreviewCC bmsEmailPreview = new BMSEmailPreviewCC();
                bmsEmailPreview.editEmail();
                bmsEmailPreview.saveSend();
                bmsEmailPreview.saveHold();
                bmsEmailPreview.cancel();

                Test.stopTest();
        }
        
}