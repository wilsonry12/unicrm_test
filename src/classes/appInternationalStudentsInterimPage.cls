/*******************************************************************************
    * @author       Majid Reisi Dehkordi
    * @date         01.Aug.2017         
    * @description  Controller for the lightning component of InternationalStudentsInterim
*******************************************************************************/
public with sharing class appInternationalStudentsInterimPage {
	public static final List<Callista_Country_Code__c> countries = [SELECT Country__c, High_Risk__c FROM Callista_Country_Code__c ORDER BY Country__c];

    /*******************************************************************************
    * @author       Majid Reisi Dehkordi
    * @date         01.Aug.2017         
    * @description  gives the list of countries
    *******************************************************************************/
    @AuraEnabled
    public static List<String> retrieveCountries() {

    	Map<String, Callista_Country_Code__c> mapCountriesAndStatus = new Map<String, Callista_Country_Code__c>();

        List<String> options = new List<String>();

        for(Callista_Country_Code__c country: countries){
            if (string.valueOf(country.Country__c).toUpperCase() != 'AUSTRALIA'){
                if(country.High_Risk__c != FALSE){
                    options.add(string.valueOf(country.Country__c) + '   ');
                }
                else{
                	options.add(string.valueOf(country.Country__c));
                }
            }
        }

        return options;
    }
}