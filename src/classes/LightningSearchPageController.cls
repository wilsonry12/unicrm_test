/**************************************************************************************************   
Apex Controller Name :  LightningSearchPageController 
Version              :  1.0                                                                          
Created Date         :                                                              
Function             :  this class is display search results in community with pagination.                                                                             
*--------------------------------------------------------------------------------------------------  
    Developer                        Date                        Description
    Veera G                       3.July.2017                        Created.
***************************************************************************************************/
public with sharing class LightningSearchPageController {
    
    /*******************************************************************************
    * @author       Veera, PwC
    * @date         3.July.2017       
    * @description  method will take search text as input from Search page URL and returns all the articles related to that search string.
    * @InputParameter: string
	* @return type  list<sobject>
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static list<KnowledgeArticleVersion> getSearchResults(string searchText) {
        list<KnowledgeArticleVersion> articleSuggestions;
        set<KnowledgeArticleVersion> articleSet = new set<KnowledgeArticleVersion>();
        
        if(string.valueOf(CustomURLs__c.getInstance().Exclude_Token__c)!='' && string.valueOf(CustomURLs__c.getInstance().Exclude_Token__c)!=NULL) {
            string excludeString = string.valueOf(CustomURLs__c.getInstance().Exclude_Token__c);
            searchText = searchText.remove(excludeString);
        }
        
        if(searchText != '' && searchText.length()>=3) {
            list<string> searchTextList = searchText.split('[\\s]');
            system.debug('>>>>>>searchTextList'+searchTextList);
            Search.SuggestionResults suggestionResults;
            String objectType = 'KnowledgeArticleVersion';
            Search.SuggestionOption options = new Search.SuggestionOption();
            Search.KnowledgeSuggestionFilter filters = new Search.KnowledgeSuggestionFilter();
            
            if (objectType=='KnowledgeArticleVersion') {
                filters.setLanguage('en_US');
                filters.setPublishStatus('Online');
            }
            
            options.setFilter(filters);
            options.setLimit(2000);
            suggestionResults=Search.suggest(searchText, objectType, options);
            list<Search.SuggestionResult> resultList= suggestionResults.getSuggestionResults();
            articleSuggestions = new list<KnowledgeArticleVersion>();
            
            for(integer i=0;i<resultList.size();i++) {
                articleSuggestions.add((KnowledgeArticleVersion)resultList[i].getSObject());
            }

            list<KnowledgeArticleVersion> ArticlebyTopic = getSearchArticlesbyTopic(searchTextList);

            if(!articleSuggestions.isEmpty() && !ArticlebyTopic.isEmpty()){
                articleSet.addAll(articleSuggestions);
                for(KnowledgeArticleVersion ArticlebyTopicResult : ArticlebyTopic) {
                    ArticlebyTopicResult.id = string.valueof(ArticlebyTopicResult.id).length()==18?string.valueof(ArticlebyTopicResult.id).substring(0, 15):string.valueof(ArticlebyTopicResult.id);
                    articleSet.add(ArticlebyTopicResult);
                }
                
                if(!articleSet.isEmpty()){
                    articleSuggestions.clear();
                    articleSuggestions.addAll(articleSet);
                }
                
            } else if(!ArticlebyTopic.isEmpty()) {
                articleSuggestions.addAll(ArticlebyTopic);
            } 
            
            
            System.debug('articleSuggestions'+articleSuggestions);
            return articleSuggestions;
        } else {
            articleSuggestions = new list<sobject>();
            return articleSuggestions;
        }
    }
    
    /*******************************************************************************
    * @author       Veera, PwC
    * @date         3.July.2017       
    * @description  method returns page size from CustomURLs__c custom setting.
    * @InputParameter: 
	* @return type  : Integer
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static integer getPageSize() {
        integer pageSize = integer.valueOf(CustomURLs__c.getInstance().SearchResultsPageSize__c)!=NULL?integer.valueOf(CustomURLs__c.getInstance().SearchResultsPageSize__c):10;
        return pageSize;
    }
    
    /*******************************************************************************
    * @author       Veera, PwC
    * @date         3.July.2017       
    * @description  method will return a list of articles related to the topic matching the search string.
    * @InputParameter: string
	* @return type  list<sobject>
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static list<KnowledgeArticleVersion> getSearchArticlesbyTopic(list<string> searchList){
        list<KnowledgeArticleVersion> searchArticleByTopicList =new list<KnowledgeArticleVersion>();
        list<topic> topicList = new list<topic>();
        list<TopicAssignment> topicAssignmentList = new list<TopicAssignment>();
        Map<String,Set<Id>> articleTypeMap=new Map<String,Set<Id>>();
        
        string topicQuery = 'select id,name from topic where';
        for(string searchToken: searchList){
            topicQuery = topicQuery + ' Name Like \'%'+searchToken+'%\' OR';
        }
        topicQuery = topicQuery.removeEnd('OR');
        topicList = database.query(topicQuery);
        
        if(!topicList.isEmpty()) {
            string topicAssignmentQuery = 'SELECT EntityId,EntityType,Id,TopicId FROM TopicAssignment where';
            for(topic topicId:topicList){
                topicAssignmentQuery = topicAssignmentQuery + ' TopicId =\''+topicId.Id+'\' OR';
            }
            topicAssignmentQuery = topicAssignmentQuery.removeEnd('OR');
            topicAssignmentQuery = topicAssignmentQuery +' LIMIT 1000';
            topicAssignmentList = database.query(topicAssignmentQuery);
        }
        
        if(!topicAssignmentList.isEmpty()) {
           for(TopicAssignment assign: topicAssignmentList) {
               set<id> idSet = new set<id>();
               idSet.add(assign.EntityId);
            }
            
            String query='select id,title FROM KnowledgeArticleVersion where language=\'en_US\' and Publishstatus=\'Online\' and id in :idSet LIMIT 1000';
            searchArticleByTopicList= Database.query(query);
        }
        return searchArticleByTopicList;
    }
    
    /*******************************************************************************
    * @author       Veera, PwC
    * @date         3.July.2017       
    * @description  method returns the search token in UpperCase.
    * @InputParameter: string
	* @return type  string	
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static string getSearchToken(string searchText){
        if(string.valueOf(CustomURLs__c.getInstance().Exclude_Token__c)!='' && string.valueOf(CustomURLs__c.getInstance().Exclude_Token__c)!=NULL) {
            string excludeString = string.valueOf(CustomURLs__c.getInstance().Exclude_Token__c);
            system.debug('>>>>SearchText::'+excludeString);
            searchText = '"'+searchText.remove(excludeString).toUpperCase()+'"';
        } else {
            searchText = '"'+searchText.toUpperCase()+'"'; 
        }
        
        return searchText;
    }
    
    
}