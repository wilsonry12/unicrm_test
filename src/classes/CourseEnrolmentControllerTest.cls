@isTest
private class CourseEnrolmentControllerTest {

    static testMethod void registerCandidate_NonConcession() {
        Course__c course = PDTestHelper.createCourse();
        Course_Offering__c co = PDTestHelper.createCourseOffering();
        co.Course__c = course.Id;
        update co;

        String givenName = 'Mario';
        String familyName = 'Bro';
        String address = '738 Blackburn Road'; 
        String suburb = 'Notting Hill';
        String state = 'Victoria'; 
        String postCode = '3168';
        String homePhoneNumber = '0389768976';
        String workPhoneNumber = '0389761122'; 
        String mobileNumber = '0433433455';
        String email = 'mario.bro@mailinator.com';
        String jobTitle = 'Student';
        String selectedCourseOfferingId = co.Id; 
        String howDidyouHearAboutUs = 'Age'; 
        String organisation = 'Awesome University';
        String orgStreetAddress = '700 Blackburn Road';
        String orgSuburb = 'Notting Hill';
        String orgState = 'Victoria';
        String orgPostcode = '3168';
        Boolean isConcession = false;

        // Enrol
        List<String> enrol = CourseEnrolmentController.register(givenName, familyName, address, suburb, state, postCode, homePhoneNumber, workPhoneNumber, mobileNumber, email, jobTitle, selectedCourseOfferingId, howDidyouHearAboutUs, organisation, orgStreetAddress, orgSuburb, orgState, orgPostcode, isConcession,true,'testCourse');
        
    }

    static testMethod void registerCandidate_Concession() {
        Course__c course = PDTestHelper.createCourse();
        Course_Offering__c co = PDTestHelper.createCourseOffering();
        co.Course__c = course.Id;
        update co;

        String givenName = 'Mario';
        String familyName = 'Bro';
        String address = '738 Blackburn Road'; 
        String suburb = 'Notting Hill';
        String state = 'Victoria'; 
        String postCode = '3168';
        String homePhoneNumber = '0389768976';
        String workPhoneNumber = '0389761122'; 
        String mobileNumber = '0433433455';
        String email = 'mario.bro@mailinator.com';
        String jobTitle = 'Student';
        String selectedCourseOfferingId = co.Id; 
        String howDidyouHearAboutUs = 'Age'; 
        String organisation = '';
        String orgStreetAddress = '';
        String orgSuburb = '';
        String orgState = '';
        String orgPostcode = '';
        Boolean isConcession = true;

        List<String> enrol = CourseEnrolmentController.register(givenName, familyName, address, suburb, state, postCode, homePhoneNumber, workPhoneNumber, mobileNumber, email, jobTitle, selectedCourseOfferingId, howDidyouHearAboutUs, organisation, orgStreetAddress, orgSuburb, orgState, orgPostcode, isConcession,true,'testCourse');

    }

    static testMethod void waitlistCandidate_NonConcession() {
        Course__c course = PDTestHelper.createCourse();
        Course_Offering__c co = PDTestHelper.createCourseOffering();
        co.Course__c = course.Id;
        update co;

        String givenName = 'Mario';
        String familyName = 'Bro';
        String address = '738 Blackburn Road'; 
        String suburb = 'Notting Hill';
        String state = 'Victoria'; 
        String postCode = '3168';
        String homePhoneNumber = '0389768976';
        String workPhoneNumber = '0389761122'; 
        String mobileNumber = '0433433455';
        String email = 'mario.bro@mailinator.com';
        String jobTitle = 'Student';
        String selectedCourseOfferingId = co.Id; 
        String howDidyouHearAboutUs = 'Age'; 
        String organisation = 'Awesome University';
        String orgStreetAddress = '700 Blackburn Road';
        String orgSuburb = 'Notting Hill';
        String orgState = 'Victoria';
        String orgPostcode = '3168';
        Boolean isConcession = false;

        CourseEnrolmentController.waitlist(givenName, familyName, address, suburb, state, postCode, homePhoneNumber, workPhoneNumber, mobileNumber, email, jobTitle, selectedCourseOfferingId, howDidyouHearAboutUs, organisation, orgStreetAddress, orgSuburb, orgState, orgPostcode, isConcession,true,'testCourse');
    }

    static testMethod void waitlistCandidate_Concession() {
        Course__c course = PDTestHelper.createCourse();
        Course_Offering__c co = PDTestHelper.createCourseOffering();
        co.Course__c = course.Id;
        update co;

        String givenName = 'Mario';
        String familyName = 'Bro';
        String address = '738 Blackburn Road'; 
        String suburb = 'Notting Hill';
        String state = 'Victoria'; 
        String postCode = '3168';
        String homePhoneNumber = '0389768976';
        String workPhoneNumber = '0389761122'; 
        String mobileNumber = '0433433455';
        String email = 'mario.bro@mailinator.com';
        String jobTitle = 'Student';
        String selectedCourseOfferingId = co.Id; 
        String howDidyouHearAboutUs = 'Age'; 
        String organisation = 'Awesome University';
        String orgStreetAddress = '700 Blackburn Road';
        String orgSuburb = 'Notting Hill';
        String orgState = 'Victoria';
        String orgPostcode = '3168';
        Boolean isConcession = true;

        CourseEnrolmentController.waitlist(givenName, familyName, address, suburb, state, postCode, homePhoneNumber, workPhoneNumber, mobileNumber, email, jobTitle, selectedCourseOfferingId, howDidyouHearAboutUs, organisation, orgStreetAddress, orgSuburb, orgState, orgPostcode, isConcession,true,'testCourse');
    }

    static testMethod void retrieveHAFValues() {
        CourseEnrolmentController.retrieveHAFValues();
    }

    static testMethod void payment() {
        Course__c course = PDTestHelper.createCourse();
        Course_Offering__c co = PDTestHelper.createCourseOffering();    
        co.Course__c = course.Id;
        update co;
        Contact contact = PDTestHelper.createContact();
        Case enquiry = PDTestHelper.createEnquiry();
        Application__c application = PDTestHelper.createApplication(contact, enquiry);
        Payment__c payment = PDTestHelper.createPayment(co, contact, application);
        ChargentOrders__ChargentOrder__c chOrder = PDTestHelper.createChargentOrder(payment);
        ChargentOrders__Payment_Request__c paymentRequest = PDTestHelper.createPaymentRequest(chOrder);

        payment.Send_Receipt__c = true;
        update payment;
    }

    static testMethod void paymentRequest() {
        Course__c course = PDTestHelper.createCourse();
        Course_Offering__c co = PDTestHelper.createCourseOffering();    
        co.Course__c = course.Id;
        update co;
        Contact contact = PDTestHelper.createContact();
        Case enquiry = PDTestHelper.createEnquiry();
        Application__c application = PDTestHelper.createApplication(contact, enquiry);
        Payment__c payment = PDTestHelper.createPayment(co, contact, application);
        ChargentOrders__ChargentOrder__c chOrder = PDTestHelper.createChargentOrder(payment);
        ChargentOrders__Payment_Request__c paymentRequest = PDTestHelper.createPaymentRequest(chOrder);
        PDTestHelper.makePayment(paymentRequest, chOrder);

        payment.Send_Receipt__c = true;
        update payment;
    }

    static testMethod void uploadDocument() {
        Case enquiry = PDTestHelper.createEnquiry();
        String content = 'Something';
        Id docId = CourseEnrolmentController.saveTheChunk('Test File', content, '.txt', null, String.valueOf(enquiry.Id));
        CourseEnrolmentController.saveTheChunk('Test File', content, '.txt', String.valueOf(docId), String.valueOf(enquiry.Id));
    }

    static testMethod void deleteDocument() {
        Case enquiry = PDTestHelper.createEnquiry();
        Attachment attachment = PDTestHelper.createAttachment(enquiry.Id);
        CourseEnrolmentController.deleteDocument(attachment.Id);
    }
}