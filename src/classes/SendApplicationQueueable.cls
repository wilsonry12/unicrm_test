/*******************************************************************************
* @author       Ant Custodio
* @date         2.Nov.2017
* @description  Class used to send the application to Callista
* @revision     
*******************************************************************************/
public class SendApplicationQueueable implements Queueable, Database.AllowsCallouts {
	private String requestStr;
	private String applicationId;
	private static final String APPLICATION_SUBMIT_SUCCESS = 'Submitted';
    private static final String APPLICATION_SUBMIT_UNSUCCESSFUL = 'Review';

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         2.Nov.2017
	* @description  
	* @parameters	
	* @revision     
	*******************************************************************************/
	public SendApplicationQueueable(String reqStr, String appId) {
		requestStr = reqStr;
		applicationId = appId;
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         2.Nov.2017
	* @description  Queue Execute
	*******************************************************************************/
	public void execute(QueueableContext context) {
        try {
        	requestStr = StudFirst_Utilities.convertTo7BitAcceptedString(requestStr, '?');
	        // Prepare request
	        HttpRequest request = new HttpRequest();

	        // Retrieve integration settings
	        IntegrationSettings_SubmitApplication__c is = IntegrationSettings_SubmitApplication__c.getInstance();
	        
	        // Endpoint
	        if(is.Enable_Mock__c) {
	            request.setEndpoint(is.Mock_Endpoint_Applications__c);
	        } else {
	            request.setEndpoint(is.Base_Url__c + is.Path__c);    
	        }
	        
	        // HTTP method
	        request.setMethod(is.Method__c);    
	        
	        // HTTP request
	        request.setBody(requestStr);
	        
	        // HTTP timeout
	        request.setTimeout(Integer.valueOf(is.Timeout__c));
	        
	        // HTTP headers
	        request.setHeader('Content-Type', is.Header_ContentType__c);
	        request.setHeader('client_id', is.Header_ClientId__c);
	        request.setHeader('client_secret', is.Header_ClientSecret__c);
	        
	        System.debug('!@#$% HTTP Request : ' + request);
	        System.debug('!@#$% HTTP Request Body : ' + request.getBody());
	        
	        // Send Request
	        Http httpClient = new Http();
	        HttpResponse resp = httpClient.send(request);
	        System.debug('!@#$% HTTP Response Response : ' + resp.getBody());
	        
	        // Update the record with response from Callista
	        parseResponse(requestStr, resp, applicationId);

		} catch (Exception ex) {
			ExLog.add('Error on Queueable Class', 'SendApplicationQueueable', 'execute' , ex);
		}
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         2.Nov.2017
	* @description  Parse the response message
	* @revision     
	*******************************************************************************/
	private static void parseResponse(String req, HttpResponse res, String applicationId) {

        String resp = '';
        String respStatus = '';
        Integer respStatusCode = 1;
        String callistaApplicationId = '';
        String callistaApplicantId = '';
        StudFirst_AdmissionApplicationResponse response = null;

        if(res != null) {
            resp = res.getBody();
            respStatus = res.getStatus();
            respStatusCode = res.getStatusCode();

            if(String.isNotBlank(resp)) {
                response = (StudFirst_AdmissionApplicationResponse)JSON.deserialize(resp, StudFirst_AdmissionApplicationResponse.class);    
            } 
        }

        Application__c appl = [SELECT Id, Status__c, Callista_Application_Id__c, Callista_Applicant_Id__c, API_Response_Message__c FROM Application__c WHERE Id = :applicationId];

        if(response != null && response.applications != null) {
            if(response.applications.size() > 0) {
                callistaApplicantId = response.applications.get(0).smsApplicantId;
                
                if(response.applications.get(0).smsApplications != null) {
                    if(response.applications.get(0).smsApplications.size() > 0) {
                        callistaApplicationId = String.valueOf(response.applications.get(0).smsApplications.get(0).applicationId);
                    }   
                }
                        
                if(String.isNotBlank(callistaApplicationId)) {
                    appl.Callista_Application_Id__c = callistaApplicationId;
                }
                if(String.isNotBlank(callistaApplicantId)) {
                    appl.Callista_Applicant_Id__c = callistaApplicantId;    
                }
                    

            }
        }

        if(respStatusCode == 200) {   
            appl.Status__c = APPLICATION_SUBMIT_SUCCESS; 
            Id batchJobId = Database.executeBatch(new SendDocumentsBatch(new Set<String> {applicationId}), 200);   
        } else {
            appl.Status__c = APPLICATION_SUBMIT_UNSUCCESSFUL;
        }
                
        appl.API_Request_Message__c = req;
        appl.API_Response_Message__c = 'HTTP Status Code : ' + respStatusCode + '\n HTTP Status : ' + respStatus + '\n Response : \n--------------\n\n' + resp;
    
        update appl;

    }
}