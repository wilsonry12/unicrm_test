/**
 * Services class to provide Task sObject centric logical support
 * 
 * @author Carl Vescovi, PwC
 * 
 */
public without sharing class TaskServices {
	
	public static final String FIRST_ATTEMPT = 'First Attempt';
	public static final String SECOND_ATTEMPT = 'Second Attempt';
	public static final String THIRD_ATTEMPT = 'Third Attempt';
	public static final String MONTRACK_CALL_SUBJECT = 'MonTRACK Call Attempt';
	public static final String COMPLETED = 'Completed';
	public static final String NOT_APPLICABLE = 'Not Applicable';
	public static final String NOT_STARTED = 'Not Started';
	
	/** update task records without sharing restrictions 
	 * @param tsks list of task records to update
	 */
	
	public static void updateTaskRecords(List<Task> tsks){
		update tsks;
	}
	
	/** delete task records without sharing restrictions 
	 * @param tsks list of tasks to delete
	 */
	
	public static void deleteTaskRecords(List<Task> tsks){
		delete tsks;
	}
	
	/** copy fields from contact record over to Task, if Task is of a specific record type. 
	 * This is used specifically where standard reporting cannot aggregate information from multiple objects i.e.
	 * MonTrack Round - to Task - to Contact.
	*/
	public class considerTaskFieldMappings implements Triggers.Handler {
		public void run(){
			// copy mapped fields from Contact to task record depending on task record type
			List<Contact_to_Task_Field_Mapping__mdt> fieldMappings = 
				new List<Contact_to_Task_Field_Mapping__mdt>([ SELECT Id, 
																	  Contact_field_API_name__c, 
																	  Task_field_API_name__c,
																	  Task_Record_Type__c
																	  FROM Contact_to_Task_Field_Mapping__mdt limit 400]);
			
			// build a map of task record type ids 
			Map<id,string> recordTypeIds = new Map<id,string>();
			for(Recordtype rt : [select id, developerName from RecordType where SObjectType = 'Task' limit 50]) 
				recordTypeIds.put(rt.id,rt.developername.tolowercase());
			
			// build a query string to ensure retrieval of all fields that may be wanted from contact if mapping to be done
			string qry = 'select ';
			for(Contact_to_Task_Field_Mapping__mdt fld : fieldMappings) qry += fld.Contact_field_API_name__c + ',';
			qry += 'Id from contact where id in:contactIds limit 200';    
			
			
			// retrieve contacts linked to tasks via whoId
			Set<id> contactIds = new Set<id>();
			for(task tsk : (List<Task>)Trigger.new) { if(tsk.whoId != null) contactIds.add(tsk.whoId); }
			
			Map<id,Contact> contacts = new Map<id,contact>();
			for(Contact c : Database.query(qry)) contacts.put(c.id,c);
			
			// loop through tasks, consider record type and if there are mappings then map
			for(Task tsk : (List<Task>)Trigger.new){
				if(tsk.whoId != null && contacts.containsKey(tsk.whoId) ){
					// check mappings for possibles
					for(Contact_to_Task_Field_Mapping__mdt mapping : fieldMappings){
						if(recordTypeIds.get(tsk.recordTypeId) == mapping.Task_Record_Type__c.tolowercase()) 
							tsk.put(mapping.Task_field_API_Name__c,contacts.get(tsk.whoId).get(mapping.Contact_field_API_Name__c));
						
					}
				}
			}
		}
	}	
	
	/** if is a wrap up task (inserted by workflow action) then set the Type to 'Wrap up' 
	 * This is done via trigger so that can insert a normally unavailable value into picklist field - cannot be achieved via config
	*/
	public class considerWrapUpTasks implements Triggers.Handler {
		public void run(){
			for(Task tsk : (List<task>)Trigger.new){
				if(tsk.Subject == 'Wrap Up Task' && tsk.Description == 'Enquiry wrapped up') tsk.Type = 'Wrap Up';
			}
		}
	}

	/*******************************************************************************
	* @author       Ryan Wilson
	* @date         16/09/2016       
	* @description  populates the First response Datetime based on the first task created  
	* @revision     
	*******************************************************************************/

	public class updateFirstResponseDateTime implements Triggers.Handler {
		public void run(){
			Set<Id> caseIds = new Set<Id>();
			List<Case> caseForUpdate = new List<Case>();

			for(Task tsk: (List<task>)Trigger.new){
				caseIds.add(tsk.WhatId);
			}

			Map<id,case> cases = new Map<id,Case>([SELECT id, 
										  First_Response_Date_Time__c,
										  Latest_Unassigned_Queue_OwnerId__c,
										  Case_Queue_Name__c, 
										  Status,
										  CreatedDate,
										  Origin,
										  (SELECT Id, CreatedDate, CreatedById, CreatedBy.Name, User_Role__c FROM Tasks  
												WHERE CreatedBy.Name != 'System Admin' ORDER BY CreatedDate ASC LIMIT 2),
										  IsClosed
										  FROM Case 
										  WHERE id IN:caseIds 
										  ]);

			for(Case cse: cases.values()){
				if(cse.Tasks.size() == 1 || !cse.Tasks.isEmpty()){
					if(cse.First_Response_Date_Time__c == null){
						cse.First_Response_Date_Time__c = cse.Tasks[0].CreatedDate;
						caseForUpdate.add(cse);
					}
				}
			}

			try{
				if(caseForUpdate.size() > 0){
					update caseForUpdate; // update first response date time
				}
			} catch (exception ex){
				ExLog.write('TaskServices','updateFirstResponseDateTime','updating First_Response_Date_Time__c field',ex.getMessage());
			}
		}
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         09.Aug.2017       
	* @description  creates an Outbound Communications Plaform Event after creating
	*					an email.
	* @triggered 	On Insert of Task
	* @revision     
	*******************************************************************************/
	public class generatePlatformEvent implements Triggers.Handler {
		public void run () {
			Map<String, Task> taskMap = new Map<String, Task>();
			Map<String, Agent_Portal_Event__e> apEventMap = new Map<String, Agent_Portal_Event__e>();
			String admissionsRecType = CommonServices.recordTypeId('Case','Admissions');
			
			for(Task taskRec: (List<Task>)Trigger.new) {
				taskMap.put(taskRec.WhatId, taskRec);
			}

			Set<String> taskIdsToUse = new Set<String>();
			//get the outbound email message of the related case
			for (Case caseRec: [	SELECT 	Id,
											Application_Course_Preference__r.Application__r.Callista_Application_Id__c,
											Application_Course_Preference__r.Preference_Number__c,
											Application_Course_Preference__r.Application__r.Callista_Student_Id__c,
											Application_Course_Preference__r.Application__r.Agent_Org_Unit_Code__c
									FROM 	Case
									WHERE 	Id IN: taskMap.KeySet() AND 
											Notify_Agent__c = true AND
											RecordtypeId =: admissionsRecType AND
											Application_Course_Preference__c != null AND 
											Application_Course_Preference__r.Application__c != null ]) {

				Agent_Portal_Event__e apEvent = new Agent_Portal_Event__e();
				apEvent.Application_Id__c = caseRec.Application_Course_Preference__r.Application__r.Callista_Application_Id__c;
				apEvent.Course_Preference_Id__c = caseRec.Application_Course_Preference__r.Preference_Number__c;
				apEvent.Person_Id__c = caseRec.Application_Course_Preference__r.Application__r.Callista_Student_Id__c;
				apEvent.Agent_Org_Unit_Id__c = caseRec.Application_Course_Preference__r.Application__r.Agent_Org_Unit_Code__c;
				apEventMap.put(caseRec.Id, apEvent);

				taskIdsToUse.add(taskMap.get(caseRec.Id).Id);
			}

			for (Task taskRec: [	SELECT 	Id,
											Description,
											WhatId,
											Subject
									FROM 	Task
									WHERE 	Id IN: taskIdsToUse AND
											Type = 'Internal Email' ]) {
				Agent_Portal_Event__e apEvent = apEventMap.get(taskRec.WhatId);
				//splits the content to get the fields
				Map<String, String> contentMap = splitContent(taskRec.Description);
				//TODO - Way of putting attachment links(?) to the Platform Event instance
				apEvent.Attachment_Names__c = contentMap.get('Attachment');
				apEvent.Email_Body__c = contentMap.get('Body');
				apEvent.Enquiry_Id__c = taskRec.WhatId;
				apEvent.Subject__c = contentMap.get('Subject');
				apEvent.To_Addresses__c = contentMap.get('Additional To');
				apEventMap.put(taskRec.WhatId, apEvent);
			}

			try {
				if(!apEventMap.isEmpty()){
					EventBus.publish(apEventMap.values());
				}
			} catch (exception ex) {
				ExLog.write('TaskServices','generatePlatformEvent','creating an Outbound Communications Platform Event',ex.getMessage());
			}
		}
	}
	/*******************************************************************************
	* @author       Ant Custodio
	* @date         11.Aug.2017       
	* @description  splits the content of the email
	* @triggered 	On Insert of Task
	* @revision     
	*******************************************************************************/
	private static Map<String, String> splitContent (String content) {
		Map<String, String> contentMap = new Map<String, String>();
		
		//split to key value pairs. New key is defined by new line (\n) and key|value is separated by colon (:)
		String[] descSplit = content.split('\n');

		for (Integer i=0; descSplit.size()>i; i++) {
		    String[] keyValStr = descSplit[i].split(':');
		    
		    if (descSplit[i] != '') {
		        if (keyValStr.size() > 1) {
		            contentMap.put(keyValStr[0], keyValStr[1]); 
		        } else if (keyValStr[0] == 'Body') {
		            String bodyStr = '';
		            for (Integer j=(i+1); descSplit.size()>j; j++) {
		               	//put a new line if not the last character
		                if (j != (descSplit.size()-1)) {
		                    bodyStr += descSplit[j] + '\n';
		                } else {
		                    bodyStr += descSplit[j];
		                }
		            }
		            contentMap.put(keyValStr[0], bodyStr); 
		            break;
		        }
		    }
		}

		return contentMap;
	}
}