/*******************************************************************************
* @author       Ryan Wilson
* @date         22.Nov.2016         
* @description  test class for BMSEditEmailBodyCC
* @revision     
*******************************************************************************/
@isTest
private class BMSEditEmailBodyCC_Test {
        
        @isTest static void test_EditEmail() {
                List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
                insert contactRecordList;

                EmailTemplate template = BMS_TestDataHelper.buildTemplate();
                Campaign massEmail = BMS_TestDataHelper.buildSampleCRMMassEmail(template.Name);
                massEmail.Batch_Finished__c = true;
                insert massEmail;

                test.startTest();
                PageReference pageRef = Page.BMSEditEmailBody;
                Test.setCurrentPage(pageRef);

                apexpages.currentpage().getparameters().put('crmId',massEmail.Id);

                BMSEditEmailBodyCC bmsEditEmail = new BMSEditEmailBodyCC();
                bmsEditEmail.getEmailTemplateOptions();
                bmsEditEmail.selectedTemplateId = template.Id;
                bmsEditEmail.gotoEmailTemplate();
                bmsEditEmail.saveMassEmailRecord();
                bmsEditEmail.cancelEdit();
                test.stopTest();
        }
        
        /*******************************************************************************
        * @author       Anterey Custodio
        * @date         31.Jan.2017        
        * @description  Method to test the negative path
        *******************************************************************************/
        @isTest static void test_EditEmail_Negative() {
                List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
                insert contactRecordList;
                
                EmailTemplate template = BMS_TestDataHelper.buildTemplate();
                Campaign massEmail = BMS_TestDataHelper.buildSampleCRMMassEmail(template.Name);
                massEmail.Name = 'test Name';
                massEmail.Message_Header__c = '';
                massEmail.Add_CRICOS__c = true;
                massEmail.Add_Base_Signature__c = true;
                massEmail.Batch_Finished__c = true;
                insert massEmail;

                test.startTest();
                PageReference pageRef = Page.BMSEditEmailBody;
                Test.setCurrentPage(pageRef);

                apexpages.currentpage().getparameters().put('crmId',massEmail.Id);

                BMSEditEmailBodyCC bmsEditEmail = new BMSEditEmailBodyCC();
                bmsEditEmail.getEmailTemplateOptions();
                bmsEditEmail.selectedTemplateId = template.Id;
                bmsEditEmail.gotoEmailTemplate();
                //no subject/header
                system.assertEquals(null, bmsEditEmail.massEmailRecord.Message_Header__c);
                bmsEditEmail.saveMassEmailRecord();
                bmsEditEmail.cancelEdit();
                test.stopTest();
        }
        /*******************************************************************************
        * @author       Anterey Custodio
        * @date         07.Mar.2017          
        * @description  Testing the SMS functionality
        * @revision     
        *******************************************************************************/
        @isTest static void test_SavingSMS() {
                List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
                insert contactRecordList;

                EmailTemplate template = BMS_TestDataHelper.buildTemplate();
                Campaign massEmail = BMS_TestDataHelper.buildSampleCRMMassEmail(template.Name);
                massEmail.Name = 'testName';
                massEmail.Type = 'SMS';
                massEmail.SMS_Body__c = 'SMS';
                massEmail.Batch_Finished__c = true;
                insert massEmail;

                PageReference pageRef = Page.BMSEditEmailBody;
                pageRef.getParameters().put('crmId',massEmail.Id);
                pageRef.getParameters().put('type','sms');
                Test.setCurrentPage(pageRef);

                test.startTest();
                        BMSEditEmailBodyCC bmsEditEmail = new BMSEditEmailBodyCC();
                        system.assertEquals('sms', Apexpages.currentpage().getparameters().get('type'));
                        system.assertEquals('sms', bmsEditEmail.sendType);
                        bmsEditEmail.getEmailTemplateOptions();
                        bmsEditEmail.selectedTemplateId = template.Id;
                        bmsEditEmail.gotoEmailTemplate();
                        bmsEditEmail.saveMassEmailRecord();
                        bmsEditEmail.cancelEdit();
                test.stopTest();
        }    

        /*******************************************************************************
        * @author       Anterey Custodio
        * @date         07.Mar.2017          
        * @description  Testing the SMS validation rules
        * @revision     
        *******************************************************************************/
        @isTest static void test_SMS_Validation_Errors() {
                List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
                insert contactRecordList;

                EmailTemplate template = BMS_TestDataHelper.buildTemplate();
                Campaign massEmail = BMS_TestDataHelper.buildSampleCRMMassEmail(template.Name);
                massEmail.Name = 'testName';
                massEmail.Type = 'SMS';
                massEmail.SMS_Body__c = '';
                massEmail.Batch_Finished__c = true;
                insert massEmail;

                
                PageReference pageRef = Page.BMSEditEmailBody;
                pageRef.getParameters().put('crmId',massEmail.Id);
                pageRef.getParameters().put('type','sms');
                Test.setCurrentPage(pageRef);

                test.startTest();
                        BMSEditEmailBodyCC bmsEditEmail = new BMSEditEmailBodyCC();
                        system.assertEquals('sms', Apexpages.currentpage().getparameters().get('type'));
                        system.assertEquals('sms', bmsEditEmail.sendType);
                        //no subject/header
                        system.assertEquals(null, bmsEditEmail.massEmailRecord.SMS_Body__c);
                        bmsEditEmail.saveMassEmailRecord();
                        bmsEditEmail.cancelEdit();
                test.stopTest();
        }     
}