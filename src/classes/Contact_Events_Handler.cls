/***********************************************
* Author: Ryan Wilson
* Date: 15.SEP.2017
* Revision: Class for handling inbound events related to contact
***********************************************/

public class Contact_Events_Handler {
	
	public static void createUpdateStaffContact(List<Contact__e> contactEvents) {
		Map<String, Contact__e> mapStaffContactEvtsByID = new Map<String, Contact__e>(); //for Staff contacts from PE
		Map<String, Contact__e> mapStaffContactEvtsByName = new Map<String, Contact__e>();
		Map<String, Contact__e> mapNewStaffContact = new Map<String, Contact__e>();
		Map<String, Contact__e> mapUpdateStaffContact = new Map<String, Contact__e>();

		Map<String, Contact> mapExistingStaffContacts = new Map<String, Contact>();

		Map<String, Account> mapExistingAccount = new Map<String, Account>();

		List<Account> newAccounts = new List<Account>();
		
		//Search filters for existing contacts
		Set<String> contactNames = new Set<String>();
		Set<String> emailAdds = new Set<String>();
		Set<String> sapIds = new Set<String>();
		Set<String> facultyPortfolios = new Set<String>(); //for searching accounts

		//collect staff data on PE
		for(Contact__e cEvent: contactEvents){
			mapStaffContactEvtsByID.put(cEvent.SAP_Staff_Id__c, cEvent); // for SAP matching with existing contacts
			mapStaffContactEvtsByName.put(cEvent.First_Name__c+'-'+cEvent.Last_Name__c+'-'+cEvent.Email__c, cEvent); //for contacts that only match by First Name, LastName, Email
			//use for the query string searching contacts
			contactNames.add(cEvent.First_Name__c+' '+cEvent.Last_Name__c);
			emailAdds.add(cEvent.Email__c);
			sapIds.add(cEvent.SAP_Staff_Id__c);
			//use Account matching and creation
			facultyPortfolios.add(cEvent.Faculty_Portfolio__c); // searching accounts
		}

		//Query existing Accounts. If found get the Id, if not found, create a faculty account
		System.debug('****FACULTY/PORTFOLIO: '+facultyPortfolios);
		String commaSeparatedFields_account = CommonUtilities.getAllFields('Account');
		String queryString_account = 'SELECT '+commaSeparatedFields_account+' FROM Account WHERE Name IN:facultyPortfolios';

		List<Account> matchingAccounts = Database.Query(queryString_account);

		//Build map for new and existing accounts
		if(matchingAccounts.size() > 0){
			List<String> nonExistingAccts = new List<String>();

			for(Account acct: matchingAccounts){
				mapExistingAccount.put(acct.Name, acct);
			}

			for(String acctName: facultyPortfolios){
				if(!mapExistingAccount.containsKey(acctName)){
					nonExistingAccts.add(acctName);
				}
			}

			//Create new accounts - faculty
			if(nonExistingAccts.size() >0){
				for(String acctName: nonExistingAccts){
					Account insert_account = new Account();
					insert_account.Name = acctName;
					insert_account.RecordTypeId = CommonServices.recordTypeId('Account', 'Faculty');
					newAccounts.add(insert_account);
				}
			}
		}else{
			for(String acctName: facultyPortfolios){
					Account insert_account = new Account();
					insert_account.Name = acctName;
					insert_account.RecordTypeId = CommonServices.recordTypeId('Account', 'Faculty');
					newAccounts.add(insert_account);
				}
		}

		if(newAccounts.size() >0){
			insert newAccounts;

			//Insert new accounts in the existing map
			for(Account acct: newAccounts){
				mapExistingAccount.put(acct.Name, acct);
			}
		}
		System.debug('****MAP EXIST ACCOUNT: '+mapExistingAccount);

		//search for existing contact records
		//CRITERIA: (FirstName, LastName and Email) or SAP Id
		String commaSeparatedFields_contact = CommonUtilities.getAllFields('Contact');
		String commaSeparatedFields_role = CommonUtilities.getAllFields('Roles__c');
		String commaSeparatedFields_channel = CommonUtilities.getAllFields('Channel__c');
		String queryString_contact = 'SELECT '+commaSeparatedFields_contact+', (SELECT '+commaSeparatedFields_channel+' FROM Channels__r), (SELECT '+commaSeparatedFields_role+' FROM Roles__r WHERE Status__c = \'Current\') FROM Contact WHERE (SAP_Staff_Id__c IN:sapIds) OR (Name IN:contactNames AND Email IN:emailAdds)';
		
		List<Contact> matchingContacts = Database.Query(queryString_contact);

		//Build map for new and existing contacts
		if(matchingContacts.size() > 0){
			Set<String> matchingSAPIds = new Set<String>();

			for(Contact con: matchingContacts){
				String strFNameLnameEmail = con.FirstName+'-'+con.LastName+'-'+con.Email;
				if(con.SAP_Staff_Id__c == null && mapStaffContactEvtsByName.containsKey(strFNameLnameEmail)){
					
					mapUpdateStaffContact.put(con.Id, mapStaffContactEvtsByName.get(strFNameLnameEmail));
					matchingSAPIds.add(mapStaffContactEvtsByName.get(strFNameLnameEmail).SAP_Staff_Id__c);
				}
				else if(mapStaffContactEvtsByID.containsKey(con.SAP_Staff_Id__c)){
					mapUpdateStaffContact.put(con.Id, mapStaffContactEvtsByID.get(con.SAP_Staff_Id__c));
					matchingSAPIds.add(con.SAP_Staff_Id__c);
				}
				
				mapExistingStaffContacts.put(con.Id, con);
			}

			for(Contact__e conEvt: mapStaffContactEvtsByID.values()){
				if(!matchingSAPIds.contains(conEvt.SAP_Staff_Id__c)){
					mapNewStaffContact.put(conEvt.SAP_Staff_Id__c, conEvt);
				}
			}

		}else{
			mapNewStaffContact.putAll(mapStaffContactEvtsByID);
		}

		/*System.debug('****MAP EXISTING CONTACT: '+mapExistingStaffContacts);

		System.debug('****MAP NEW CONTACT: '+mapNewStaffContact);
		System.debug('****MAP NEW CONTACT SIZE: '+mapNewStaffContact.size());
		
		System.debug('****MAP UPDATE CONTACT: '+mapUpdateStaffContact);
		System.debug('****MAP UPDATE CONTACT SIZE: '+mapUpdateStaffContact.size());*/

		List<ContactWrapper> conWrapList = new List<ContactWrapper>();

		if(mapNewStaffContact.size() > 0){
			List<ContactWrapper> forInsert_ConWrapList = CommonUtilities.insertNewStaffContact(mapNewStaffContact, mapExistingAccount);
			conWrapList.addAll(forInsert_ConWrapList);
		}

		/****UPDATE EXISTING CONTACT ****/
		if(mapUpdateStaffContact.size() > 0){
			List<ContactWrapper> forUpdate_ConWrapList = CommonUtilities.updateStaffContact(mapExistingStaffContacts, mapUpdateStaffContact, mapExistingAccount);
			conWrapList.addAll(forUpdate_ConWrapList);
		}
		/****END: UPDATE EXISTING CONTACT****/

		insertUpdateContactRecord(conWrapList);
	}

	private static void insertUpdateContactRecord(List<ContactWrapper> conWrapList) {
		List<Contact> contactList_forUpsert = new List<Contact>();
        List<Channel__c> channelList = new List<Channel__c>();
        List<Roles__c> roleList = new List<Roles__c>();
		
		try{
			//Wrapper - iterate contacts for insert
            for(ContactWrapper conwrp: conWrapList){
                Contact con = new Contact();
                con = conwrp.contactRecord.contact;
                contactList_forUpsert.add(con);
            }
            //upsert contactList;

            Schema.SObjectField fKey = Contact.Fields.Id;
            List<Database.UpsertResult> upr = Database.upsert(contactList_forUpsert, fKey, false);

            Set<Id> successIds = new Set<Id>();
            List<Database.Error> conErrors = new List<Database.Error>();
            
            for(Database.UpsertResult upResult: upr){
            	if(upResult.isSuccess()){
            		successIds.add(upResult.getId());
            	}
            	else{
            		conErrors.addAll(upResult.getErrors());
            	}
            }

            List<Contact> contactSuccess = new List<Contact>([SELECT Id, Name, SAP_Staff_Id__c 
            													FROM Contact WHERE Id IN:successIds]);

            if(contactSuccess.size() > 0){
            	//Iterate inserted contacts and update channel wrapper
	            for(Contact con: contactSuccess){
	                for(ContactWrapper conwrp: conWrapList){
	                    if(con.SAP_Staff_Id__c == conwrp.contactRecord.contact.SAP_Staff_Id__c){
	                        for(ContactWrapper.Channel chnlwrp: conwrp.contactRecord.channels){
	                            chnlwrp.channel.Contact__c = con.Id;
	                            Channel__c chan = new Channel__c();
	                            chan = chnlwrp.channel;
	                            channelList.add(chan);
	                        }
	                    }
	                }
	            }
	            //System.debug('****CHANNEL LIST: '+channelList);
	            upsert channelList;

	            //Iterate inserted channels and update role wrapper
	            for(Channel__c chan: channelList){
	                for(ContactWrapper conwrp: conWrapList){
	                    if(chan.Unique_Id__c == conwrp.contactRecord.contact.SAP_Staff_Id__c){
	                        for(ContactWrapper.Role rolwrp: conwrp.contactRecord.roles){
	                            rolwrp.role.Contact__c = chan.Contact__c;
	                            rolwrp.role.Channel__c = chan.Id;
	                            Roles__c rol = new Roles__c();
	                            rol = rolwrp.role;
	                            roleList.add(rol);
	                        }
	                    }
	                }
	            }
	            //System.debug('****ROLE LIST: '+roleList);
	            upsert roleList;
            }

            //Insert exception logs if DML statement has errors
            if(conErrors.size() > 0){
            	List<Exception_Log__c> exlogs = new List<Exception_Log__c>();
            	for(Database.Error err: conErrors){
            		exlogs.add(ExLog.writeExlog('Contact Events Handler', 'Contact_Events_Handler.cls', 'insertUpdateContactRecord', err));
            	}
            	insert exlogs;
            }
            
			
        }catch(Exception ex){
            //create an exception log for errors
            ExLog.add('Contact Events Handler', 'Contact_Events_Handler.cls', 'insertUpdateContactRecord' , 'Exception: '+ex.getMessage());
        }
	}

}