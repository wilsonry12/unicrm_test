/*******************************************************************************
* @author       Ant Custodio
* @date         10.Apr.2017         
* @description  controller class for application Header lightning component
* @revision     
*******************************************************************************/
public without sharing class ApplicationHeaderCC {
	private static Application__c applicationRecord;
	/*******************************************************************************
    * @author       Ant Custodio
    * @date         10.Apr.2017         
    * @description  update the application status and submit
    * @revision     
    *******************************************************************************/
    @AuraEnabled
	public static Application__c retrieveApplicationRecord (String appIdParam) {
		applicationRecord = [SELECT Id, Status__c, Citizenship_Type_Filter__c, 
                                Access_and_Equity_Question__c, Refused_Visa_Question__c, Breached_Visa_Question__c, 
                                Convicted_Any_Crime_Question__c, Issued_Protection_Visa_Question__c, 
                                Medical_Health_Visa_Question__c, Physical_Mental_Health_Question__c 
    							FROM Application__c
    							WHERE Id =: appIdParam ];
		return applicationRecord;
	}

    @AuraEnabled
    public static String checkApplicationRecord (String appIdParam, Application__c appForUpdate, Boolean isIntApplicant) {
        String validationMessage = 'Valid';
        applicationRecord = [   SELECT  Id, Status__c, Applying_for_Credit__c, 
                                Access_and_Equity_Question__c, Refused_Visa_Question__c, Breached_Visa_Question__c, 
                                Convicted_Any_Crime_Question__c, Issued_Protection_Visa_Question__c, 
                                Medical_Health_Visa_Question__c, Physical_Mental_Health_Question__c 
                                FROM    Application__c
                                WHERE   Id =: appIdParam ];

        List<Application_Qualification_Provided__c> appQualifications = 
                                new List<Application_Qualification_Provided__c>([SELECT Id, Application__c, Name, Qualification_Type__c 
                                                                                FROM Application_Qualification_Provided__c 
                                                                                WHERE Application__c =:appIdParam 
                                                                                AND (Qualification_Type__c = 'Tertiary_Education' OR Qualification_Type__c = 'Secondary_Education' OR Qualification_Type__c = 'Other_Qualification')]);
        
        List<Application_Work_Experience_Provided__c> appWorkExperiences = 
                                new List<Application_Work_Experience_Provided__c>([SELECT Id FROM Application_Work_Experience_Provided__c 
                                                                                    WHERE Application__c =:appIdParam]);

        List<Application_Course_Preference__c> appCourses = 
                                new List<Application_Course_Preference__c>([SELECT Id FROM Application_Course_Preference__c 
                                                                        WHERE Application__c =:appIdParam]);

        Integer tertiaryCount = [   SELECT  COUNT() 
                                    FROM    Application_Qualification_Provided__c 
                                    WHERE   Contact_Qualification__r.RecordType.Name = 'Tertiary Education' 
                                            AND Application__c =: appIdParam];
        //if there's a tertiary education existing but 'Applying for Credit' picklist hasn't been selected
        if  (   tertiaryCount > 0   && 
            (   applicationRecord.Applying_for_Credit__c == '' || 
                applicationRecord.Applying_for_Credit__c == null) ) {
            validationMessage = System.Label.App_HasTertiaryButNoCreditIntention;
        }

        if(appCourses.isEmpty()){
            validationMessage = 'Please add the courses you want to apply to continue.';
        }
        else if(appQualifications.isEmpty() && appWorkExperiences.isEmpty()) {  
            validationMessage = 'Please add your qualifications or work experiences to continue.';
        }

        //validate question for international applicants
        else if(isIntApplicant){ 
            applicationRecord.Access_and_Equity_Question__c = appForUpdate.Access_and_Equity_Question__c; 
            applicationRecord.Refused_Visa_Question__c = appForUpdate.Refused_Visa_Question__c;
            applicationRecord.Breached_Visa_Question__c = appForUpdate.Breached_Visa_Question__c; 
            applicationRecord.Convicted_Any_Crime_Question__c = appForUpdate.Convicted_Any_Crime_Question__c;
            applicationRecord.Issued_Protection_Visa_Question__c = appForUpdate.Issued_Protection_Visa_Question__c;
            applicationRecord.Medical_Health_Visa_Question__c = appForUpdate.Medical_Health_Visa_Question__c;
            applicationRecord.Physical_Mental_Health_Question__c = appForUpdate.Physical_Mental_Health_Question__c;
            
            if(applicationRecord.Access_and_Equity_Question__c == null || 
                    applicationRecord.Refused_Visa_Question__c == null ||  
                    applicationRecord.Breached_Visa_Question__c == null || 
                    applicationRecord.Convicted_Any_Crime_Question__c == null ||
                    applicationRecord.Issued_Protection_Visa_Question__c == null ||
                    applicationRecord.Medical_Health_Visa_Question__c == null ||
                    applicationRecord.Physical_Mental_Health_Question__c == null) {

                validationMessage = System.Label.App_Questions_Error;
            }
        }

        if(validationMessage == 'Valid'){
            update applicationRecord;
        }
        

        return validationMessage;
    }
	
    /*******************************************************************************
    * @author       Ant Custodio
    * @date         10.Apr.2017         
    * @description  update the application status and submit
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static void deleteApplicationRecord (String appIdParam) {
        try {
            applicationRecord = [   SELECT  Id, Status__c, Applicant__c 
                                    FROM    Application__c
                                    WHERE   Id =: appIdParam ];
            applicationRecord.Status__c = 'Cancelled';
            update applicationRecord;

            List<Work_Experience__c> workExp = new List<Work_Experience__c>([SELECT Id, Contact__c, Reference_Count__c 
                                                                            FROM Work_Experience__c 
                                                                            WHERE Contact__c =:applicationRecord.Applicant__c 
                                                                            AND Reference_Count__c = 1]);
            if(workExp.size() >0){
                delete workExp;
            }

            List<Contact_Qualification__c> conQualification = new List<Contact_Qualification__c>([SELECT Id, Contact__c, Reference_Count__c 
                                                                            FROM Contact_Qualification__c 
                                                                            WHERE Contact__c =:applicationRecord.Applicant__c 
                                                                            AND Reference_Count__c = 1]);
            if(conQualification.size() >0){
                delete conQualification;
            }

            List<Contact_Document__c> conDocument = new List<Contact_Document__c>([SELECT Id, Contact__c, Reference_Count__c 
                                                                            FROM Contact_Document__c 
                                                                            WHERE Contact__c =:applicationRecord.Applicant__c 
                                                                            AND Reference_Count__c = 1]);
            if(conDocument.size() >0){
                delete conDocument;
            }

            //delete applicationRecord;
        } catch (Exception e) {
            throw new AuraHandledException('Unable to cancel your application: ' + e.getMessage());
        }
    }
}