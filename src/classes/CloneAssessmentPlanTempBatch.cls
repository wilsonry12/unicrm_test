/*******************************************************************************
* @author       Ant Custodio
* @date         22.Jun.2017
* @description  Batch class that clones the assessment plan template for the
					Application Course Preference
* @revision     
*******************************************************************************/
global class CloneAssessmentPlanTempBatch implements Database.Batchable<sObject> {
	
	private String query;
	private Map<String, Set<String>> acpMap;
	private String CP_PLAN_RECTYPEID;
	
	/*******************************************************************************
	* @author       Ant Custodio
	* @date         22.Jun.2017
	* @description  Constructor - initialise variables
	* @revision     
	*******************************************************************************/
	global CloneAssessmentPlanTempBatch(Map<String, Set<String>> acpRetrievedMap) {
		//populates the Application Course Preference Ids to be used on the batch
		acpMap = acpRetrievedMap;
		CP_PLAN_RECTYPEID = [	SELECT 	Id
								FROM 	RecordType
								WHERE 	DeveloperName = 'CP_Plan' ].Id;
	}
	
	/*******************************************************************************
	* @author       Ant Custodio
	* @date         22.Jun.2017
	* @description  Start - retrieve all Application Course Preference using the
						given Ids
	* @revision     
	*******************************************************************************/
	global Database.QueryLocator start(Database.BatchableContext BC) {
		Set<String> acpIds = acpMap.keySet();
		query = 'SELECT		Id,	' +
				'			Preference_Number__c, ' +
				'			Applicant__c, ' +
				'			Application__c, ' +
				'			Course_Offering__c, ' +
				'			Course_Offering__r.Selection_Rule_Assessment_Plan_Id__c '  +
				'FROM		Application_Course_Preference__c ' +
				'WHERE 		Application__c IN: acpIds ' +
				'ORDER BY 	Preference_Number__c';
		return Database.getQueryLocator(query);
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         22.Jun.2017
	* @description  Execute - clone the Selection Rule Assessment Plan Template
	* @revision     
	*******************************************************************************/
   	global void execute(Database.BatchableContext BC, List<Application_Course_Preference__c> scope) {
   		//key = Application_Course_Preference__c.Course_Offering__r.Selection_Rule_Assessment_Plan_Id__c
   		//value = Application_Course_Preference__c
   		Map<String, List<Application_Course_Preference__c>> aptIdMap = new Map<String, List<Application_Course_Preference__c>>();
   		
		for (Application_Course_Preference__c acpRecord: scope) {
			if (acpRecord.Course_Offering__r.Selection_Rule_Assessment_Plan_Id__c != null) {
				if (aptIdMap.containsKey(acpRecord.Course_Offering__r.Selection_Rule_Assessment_Plan_Id__c)) {
					aptIdMap.get(acpRecord.Course_Offering__r.Selection_Rule_Assessment_Plan_Id__c).add(acpRecord);
				} else {
					aptIdMap.put(acpRecord.Course_Offering__r.Selection_Rule_Assessment_Plan_Id__c, new List<Application_Course_Preference__c>{acpRecord});
				}
				
			} else {
				//TODO - logic for records with no selection rule
			}	
		}

		try {
			//deep clone the assessment plan
			cloneAssessmentPlanWithChildren(aptIdMap);
		} catch (Exception ex) {
			ExLog.add('CloneAssessmentPlanTempBatch', 'deep cloning Assessment_Plan__c', 'execute' , ex);
		}
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         22.Jun.2017
	* @description  Clones the Assessment plan given
	* @revision     
	*******************************************************************************/
	private void cloneAssessmentPlanWithChildren (Map<String, List<Application_Course_Preference__c>> aptIdMap) {
		List<Assessment_Plan__c> apToInsertList = new List<Assessment_Plan__c>();
		Map<String, List<Assessment_Plan_Task__c>> aptMap = new Map<String, List<Assessment_Plan_Task__c>>();
		Set<string> arNames = new Set<String>();
		//key = Application-Faculty
		//value = Number Course Preferences on this Faculty
		Map<String, Integer> facultyMap = new Map<String, Integer>();
		Set<String> facultySet = new Set<String>();

		for (Assessment_Plan__c apRecord: [	SELECT 	Id, Name,
													Applicant__c,
													Application__c,
													Application_Course_Preference__c,
													Plan_Description__c,
													Plan_Faculty__c,
													Plan_Status__c,
													UniquePlanId__c,
													OwnerId,
													(	SELECT 	Assessor_Role__c,
																Assessor_Role__r.Name,
																Assessment_Plan__c,
																Assessment_Plan__r.UniquePlanId__c,
																Description__c,
																Status__c,
																Step__c,
																Task_Title__c,
																Type__c
														FROM 	Assessment_Plan_Tasks__r)
											FROM 	Assessment_Plan__c 
											WHERE 	Id IN: aptIdMap.keySet()]) {

			//convert to 15 digit Id to match the external unique Id 
			String apRecordId = String.valueOf(apRecord.Id).substring(0, 15);

			//put 1 assessment plan for each application course preference
			for (Application_Course_Preference__c acpRecord: aptIdMap.get(apRecordId)) {
				Assessment_Plan__c apToClone = apRecord.clone();
				apToClone.RecordTypeId = CP_PLAN_RECTYPEID;
				
				//set the Application Course Preference as a unique Id so
				//we can add child records
				apToClone.UniquePlanId__c = acpRecord.Id;
				apToClone.Application_Course_Preference__c = acpRecord.Id;
				apToClone.Applicant__c = acpRecord.Applicant__c;
				apToClone.Application__c = acpRecord.Application__c;
				//checkwhether to set it to Active or On-Hold
				if (facultySet.contains(acpRecord.Application__c+'-'+apToClone.Plan_Faculty__c)) {
					apToClone.Plan_Status__c = 'On-Hold';
				} else {
					apToClone.Plan_Status__c = 'Active';
					facultySet.add(acpRecord.Application__c+'-'+apToClone.Plan_Faculty__c);
				}
				apToInsertList.add(apToClone);

				aptMap.put(apToClone.UniquePlanId__c, apRecord.Assessment_Plan_Tasks__r.clone());

				
			}
			
			//get all assessor role and use it in assigning to plan tasks
			for (Assessment_Plan_Task__c aptRecord: apRecord.Assessment_Plan_Tasks__r) {
				arNames.add(aptRecord.Assessor_Role__r.Name);
			}
		}

		if (!apToInsertList.isEmpty()) {
			//Insert the cloned assessment plan
			insert apToInsertList;
		}

		//create a map of the queue using the assessor Role
		//TODO - revisit next sprint to use auto-number(or similar) to the name
		Map<String, String> arToQueueMap = new Map<String, String> ();
		for (Group grpRecord: [	SELECT	Id,
										Name 
								FROM	Group 
								WHERE 	Type = 'Queue' 
										AND Name IN: arNames ]) {
			arToQueueMap.put(grpRecord.Name, grpRecord.Id);
		}

		List<Assessment_Plan_Task__c> aptToCloneList = new List<Assessment_Plan_Task__c>();
		for (Assessment_Plan__c apRecord: apToInsertList) {
			for (Assessment_Plan_Task__c aptRecord: aptMap.get(apRecord.UniquePlanId__c)) {
				Assessment_Plan_Task__c aptClonedRecord = aptRecord.clone();
				//remove the Id
				aptClonedRecord.Id = null;
				if (arToQueueMap.containsKey(aptClonedRecord.Assessor_Role__r.Name)) {
					aptClonedRecord.OwnerId = arToQueueMap.get(aptClonedRecord.Assessor_Role__r.Name);
				} else { //if not found, assign to SPM Manager
					aptClonedRecord.OwnerId = apRecord.OwnerId;
				}
				//set the assessment plan parent
				aptClonedRecord.Assessment_Plan__c = apRecord.Id;

				aptToCloneList.add(aptClonedRecord);
			}
		}
		//insert the cloned assessment plan tasks
		if (!aptToCloneList.isEmpty()) {
			insert aptToCloneList;
		}
	}
	
	/*******************************************************************************
	* @author       Ant Custodio
	* @date         22.Jun.2017
	* @description  Finish - TODO
	* @revision     
	*******************************************************************************/
	global void finish(Database.BatchableContext BC) {
		
	}
}