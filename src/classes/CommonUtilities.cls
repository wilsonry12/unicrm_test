/*******************************************************************************
* @author       Ryan Wilson
* @date         22.Nov.2016         
* @description  Common Utility class 
* @revision     ACUSTODIO, 28.Feb.2017 - requeried the parameter list to retrieve 
                    the merge fields used on sending emails to the email sende
*******************************************************************************/
public class CommonUtilities {
    public static final string BMS_ADMIN = 'BMS Admin';
    public static final string SYSTEM_ADMIN = 'System Administrator';
    public static final string EVENT_UPSERT = 'upsert';
    public static final string EVENT_DELETE = 'delete';

    public CommonUtilities() {
        
    }

    public static void sendBMSEmail(List<Campaign> massEmailList){
        String admStudentEmail = System.label.StudentCommsEmail;
        System.debug('****ADM Student:'+admStudentEmail);
        
        //requery the mass email list to re-reference the fields being used in the class
        List<Campaign> requeriedMassEmailList = [  SELECT  Id, 
                                                    Email_Template__c, 
                                                    Reply_To__c, 
                                                    Additional_Email__c, 
                                                    Notify__c,
                                                    Name,
                                                    Message_Header__c,
                                                    Custom_Message__c,
                                                    Type,
                                                    SMS_Body__c
                                                    FROM Campaign
                                                    WHERE Id IN: massEmailList];
        

        OrgWideEmailAddress senderEmail = [SELECT Address,DisplayName,Id FROM OrgWideEmailAddress WHERE DisplayName = 'Monash Student Global'];
        Id studentGlobalId = senderEmail.Id;

        Map<String, EmailTemplate> mapTemplates = new Map<String, EmailTemplate>(); 

        Folder bmsFolder = [SELECT DeveloperName,Id,Name,Type FROM Folder WHERE Name = 'BMS Emails'];
        
        for(EmailTemplate template: [SELECT Id, HtmlValue, FolderId, Name FROM EmailTemplate WHERE FolderId =:bmsFolder.Id]){
            mapTemplates.put(template.Name, template);
        }

        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();

        for(Campaign cme: requeriedMassEmailList){
            Messaging.SingleEmailMessage additionalMail = new Messaging.SingleEmailMessage();
            additionalMail.setSaveAsActivity(false);
            if (cme.Type != 'SMS') {
                additionalMail.setTemplateId(mapTemplates.get(cme.Email_Template__c).Id);
            }
            additionalMail.setOrgWideEmailAddressId(studentGlobalId);

            if(cme.Reply_To__c != null){
                additionalMail.setReplyTo(cme.Reply_To__c);
            }

            if(cme.Additional_Email__c != null){
                String tempStr = cme.Additional_Email__c;
                String[] strAddEmails = tempStr.split(';');
                additionalMail.setToAddresses(strAddEmails);
            }
            
            List<String> strCCEmails = new List<String>();
            if(cme.Notify__c != null){
                String tempCC = cme.Notify__c;
                strCCEmails = tempCC.split(';');
            }
            strCCEmails.add(admStudentEmail);
            //strCCEmails.add(userInfo.getUserEmail());
            //System.debug('****CC Address:'+strCCEmails);
            additionalMail.setBccAddresses(strCCEmails);

            String htmlBody = '';
            if (cme.Type != 'SMS') {
                htmlBody = mapTemplates.get(cme.Email_Template__c).HtmlValue;
                if (cme.Name != null) {
                    htmlBody = htmlBody.replace('[SUBJECT]', cme.Name);
                }
                if (cme.Message_Header__c != null) {
                    htmlBody = htmlBody.replace('[MESSAGE_HEADER]', cme.Message_Header__c);
                }
                if (cme.Custom_Message__c != null) {
                    htmlBody = htmlBody.replace('[CAMPAIGN_MESSAGE]', cme.Custom_Message__c);
                } else {
                    htmlBody = htmlBody.replace('[CAMPAIGN_MESSAGE]', '');
                }
                
                if(mapTemplates.get(cme.Email_Template__c).Name.contains('Student')){
                    htmlBody = htmlBody.replace('[CONTACT_NAME]', 'Student');
                } else if (mapTemplates.get(cme.Email_Template__c).Name.contains('Staff')){
                    htmlBody = htmlBody.replace('[CONTACT_NAME]', 'Colleague');
                }
                
            } else if (cme.Type == 'SMS') {
                htmlBody = cme.SMS_Body__c;
            }
            additionalMail.setHtmlBody(htmlBody);
            additionalMail.setSubject(cme.Name);
            messages.add(additionalMail);
        }

        try{
            if(messages.size() >0){ Messaging.sendEmail(messages); }
        }
        catch(Exception ex){
            System.debug('Error encountered while sending email: '+ex.getMessage());
        }
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         02.Mar.2017         
    * @description  retrieves the send type (Email or SMS)
    * @revision     
    *******************************************************************************/
    public static String retrieveSendType () {
        String sendType = Apexpages.currentpage().getparameters().get('type');
        if (sendType != 'email' && sendType != 'sms') {
            //auto set to email when none specified
            sendType = 'email';
        }
        return sendType;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         17.Mar.2017         
    * @description  retrieves profile name
    * @revision     
    *******************************************************************************/
    public static String getProfileName () {
        String currentUserProfile = [SELECT Id, Name FROM Profile WHERE Id =: userinfo.getProfileId() ].Name;
        return currentUserProfile;
    }

    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         17.Mar.2017         
    * @description  validates the email address 
    * @return       returns false if invalid and true if valid
    * @revision     
    *******************************************************************************/
    public static Boolean validateEmail(String email) {
        Boolean res = true;
            
        
        String emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);

        if (!MyMatcher.matches()) 
            res = false;
        return res; 
    }

    public static void assignCampaignMemberStatus(Id campaignId){
        List<CampaignMemberStatus> campMemStatus = new List<CampaignMemberStatus>();
        CampaignMemberStatus cms1 = new CampaignMemberStatus(CampaignId=campaignId, Label='Pending Approval');
        campMemStatus.add(cms1);
        
        CampaignMemberStatus cms2 = new CampaignMemberStatus(CampaignId=campaignId, Label='Ready to Send');
        campMemStatus.add(cms2);

        insert campMemStatus;
    }

    /***********************************************
    * Ryan Wilson
    * 29.AUG.2017
    * Build Contact event record
    ***********************************************/
    public static Contact__e createContactEvent(Contact contact, String eventType) {
        Contact__e contactEvent = new Contact__e();

        // only map CRM contact id (used for callback by MIX)
        contactEvent.CRM_Contact_Id__c = contact.CRM_Id__c;
        
        if(contact.Aboriginal_or_Torres_Strait_Islander__c != null){
            contactEvent.Aboriginal_or_Torres_Strait_Islander__c = contact.Aboriginal_or_Torres_Strait_Islander__c.equalsIgnoreCase('yes') ? true : false;
        }
        
        contactEvent.Birthdate__c = contact.Birthdate;
        contactEvent.Deceased__c = contact.Deceased__c;
        contactEvent.DoNotCall__c = contact.DoNotCall;
        contactEvent.Email__c = contact.Email;
        contactEvent.First_Name__c = contact.First_Name__c;
        contactEvent.Gender__c = contact.Gender__c;
        contactEvent.Highest_level_of_education_to_date__c = contact.Highest_level_of_education_to_date__c;
        contactEvent.Impairments__c = contact.Impairments__c;
        contactEvent.Last_Name__c = contact.Last_Name__c;
        contactEvent.MobilePhone__c = contact.MobilePhone;
        contactEvent.Nationality__c = contact.Nationality__c;
        contactEvent.Other_Given_Name__c = contact.Other_Given_Name__c;
        contactEvent.OtherPhone__c = contact.OtherPhone;
        contactEvent.Person_ID__c = contact.Person_ID__c;
        contactEvent.Phone__c = contact.Phone;
        contactEvent.Preferred_Language__c = contact.Preferred_Language__c;
        contactEvent.Preferred_Method_Of_Contact__c = contact.Preferred_Method_Of_Contact__c;
        contactEvent.Preferred_Name__c = contact.Preferred_Name__c;
        contactEvent.Previous_Surname__c = contact.Previous_Surname__c;
        contactEvent.SAP_Staff_Id__c = contact.SAP_Staff_Id__c;
        contactEvent.Spoken_Languages__c = contact.Spoken_Languages__c;
        contactEvent.Title__c = contact.Title;
        contactEvent.Under_18__c = contact.Under_18__c;
        contactEvent.Last_Modified_By__c = (UserInfo.getName() == 'Automated Process')? 'MIX API': UserInfo.getName();
        contactEvent.Last_Modified_Date__c = System.now();
        contactEvent.Event_Type__c = eventType;
        return contactEvent;
    }

    /***********************************************
    * Ryan Wilson
    * 29.AUG.2017
    * Build Role event record
    ***********************************************/
    public static Role__e createRoleEvent(Roles__c role, String eventType) {
        Role__e roleEvent = new Role__e();
        roleEvent.Affiliation_Key__c = role.Name;
        roleEvent.Alias__c = role.Alias__c;
        roleEvent.Channel_Id__c = role.Channel__r.Name;
        roleEvent.CRM_Contact_Id__c = role.Contact__r.CRM_Id__c;
        roleEvent.Person_ID__c = role.Contact__r.Person_ID__c;
        roleEvent.Title__c = role.Contact__r.Title;
        roleEvent.First_Name__c = role.Contact__r.First_Name__c;
        roleEvent.Last_Name__c = role.Contact__r.Last_Name__c;
        roleEvent.Preferred_Name__c = role.Contact__r.Preferred_Name__c;
        roleEvent.Gender__c = role.Contact__r.Gender__c;
        roleEvent.Start_Date__c = role.Start_Date__c;
        roleEvent.End_Date__c = role.End_Date__c;
        roleEvent.Status__c = role.Status__c;
        roleEvent.Last_Modified_By__c = (UserInfo.getName() == 'Automated Process')? 'MIX API': UserInfo.getName();
        roleEvent.Last_Modified_Date__c = System.now();
        roleEvent.Event_Type__c = eventType;
        return roleEvent;
    }

    /***********************************************
    * Ryan Wilson
    * 29.AUG.2017
    * Build Channel event record
    ***********************************************/
    public static Channels__e createChannelEvent(Channel__c channel, String eventType) {
        Channels__e channelEvent = new Channels__e();
        channelEvent.CRM_Contact_Id__c = channel.Contact__r.CRM_Id__c;
        channelEvent.Person_ID__c = channel.Contact__r.Person_ID__c;
        channelEvent.Birthdate__c = channel.Contact__r.Birthdate;
        channelEvent.Title__c = channel.Contact__r.Title;
        channelEvent.First_Name__c = channel.Contact__r.First_Name__c;
        channelEvent.Last_Name__c = channel.Contact__r.Last_Name__c;
        channelEvent.Preferred_Name__c = channel.Contact__r.Preferred_Name__c;
        channelEvent.Gender__c = channel.Contact__r.Gender__c;
        channelEvent.City__c = channel.City__c;
        channelEvent.Country__c = channel.Country__c;
        channelEvent.Email__c = channel.Email__c;
        channelEvent.Start_Date__c = channel.Start_Date__c;
        channelEvent.End_Date__c = channel.End_Date__c;
        channelEvent.Facebook__c = channel.Facebook__c;
        channelEvent.Line_1__c = channel.Line_1__c;
        channelEvent.Line_2__c = channel.Line_2__c;
        channelEvent.Post_Code__c = channel.Post_Code__c;
        channelEvent.State__c = channel.State__c;
        channelEvent.Type__c = channel.Type__c;
        channelEvent.Mobile_Phone__c = channel.Phone__c;
        channelEvent.Phone__c = channel.Phone__c;
        channelEvent.Lat__c = String.valueOf(channel.Lat__c);
        channelEvent.Long__c = String.valueOf(channel.Long__c);
        channelEvent.Event_Type__c = eventType;
        channelEvent.Last_Modified_By__c = (UserInfo.getName() == 'Automated Process')? 'MIX API': UserInfo.getName();
        channelEvent.Last_Modified_Date__c = System.now();
        return channelEvent;
    }

    /***********************************************
    * Ryan Wilson
    * 09.SEP.2017
    * Build Contact wrapper
    ***********************************************/
    private static ContactWrapper build_ContactWrapper(Contact__e conEvnt, Contact con) {
        ContactWrapper conWrap = new ContactWrapper();
        conWrap.contactRecord = new ContactWrapper.ContactRecord();
        conWrap.contactRecord.channels = new List<ContactWrapper.Channel>();
        conWrap.contactRecord.roles = new List<ContactWrapper.Role>();

        conWrap.contactRecord.contact = new Contact();

        if(con != null){
            conWrap.contactRecord.contact = con;
        }

        conWrap.contactRecord.contact.FirstName = conEvnt.First_Name__c;
        conWrap.contactRecord.contact.First_Name__c = conEvnt.First_Name__c;
        conWrap.contactRecord.contact.LastName = conEvnt.Last_Name__c;
        conWrap.contactRecord.contact.Last_Name__c = conEvnt.Last_Name__c;
        conWrap.contactRecord.contact.Preferred_Name__c = conEvnt.Preferred_Name__c;
        conWrap.contactRecord.contact.Email = conEvnt.Email__c;
        conWrap.contactRecord.contact.Phone = conEvnt.Phone__c;
        conWrap.contactRecord.contact.MobilePhone = conEvnt.MobilePhone__c;
        conWrap.contactRecord.contact.Title = conEvnt.Title__c;
        conWrap.contactRecord.contact.SAP_Staff_Id__c = conEvnt.SAP_Staff_Id__c;
        conWrap.contactRecord.contact.Salutation = conEvnt.Salutation__c;
        conWrap.contactRecord.contact.Status__c = conEvnt.Status__c;
        conWrap.contactRecord.contact.Authcate__c = conEvnt.Authcate__c;
        conWrap.contactRecord.contact.Contact_Profile__c = conEvnt.Contact_Profile__c;
        conWrap.contactRecord.contact.Organisation_Unit__c = conEvnt.Organisation_Unit__c;
        conWrap.contactRecord.contact.Organisation_Unit_Code__c = conEvnt.Organisation_Unit_Code__c;
        conWrap.contactRecord.contact.Faculty_Portfolio__c = conEvnt.Faculty_Portfolio__c;
        conWrap.contactRecord.contact.Faculty_Portfolio_Code__c = conEvnt.Faculty_Portfolio_Code__c;
        conWrap.contactRecord.contact.School_Division_Name__c = conEvnt.School_Division_Name__c;
        conWrap.contactRecord.contact.School_Division_Code__c = conEvnt.School_Division_Code__c;
        conWrap.contactRecord.contact.Contract_Type__c = conEvnt.Contract_Type__c;
        conWrap.contactRecord.contact.Personnel_Area__c = conEvnt.Personnel_Area__c;
        conWrap.contactRecord.contact.Personnel_Area_Code__c = conEvnt.Personnel_Area_Code__c;
        conWrap.contactRecord.contact.Personnel_Sub_Area__c = conEvnt.Personnel_Sub_Area__c;
        conWrap.contactRecord.contact.Personnel_Sub_Area_Code__c = conEvnt.Personnel_Sub_Area_Code__c;
        conWrap.contactRecord.contact.PDO_Supervisor__c = conEvnt.PDO_Supervisor__c;
        conWrap.contactRecord.contact.IT_account_disabled__c = conEvnt.IT_account_disabled__c;
        conWrap.contactRecord.contact.Source__c = conEvnt.Source__c;
        conWrap.contactRecord.contact.LastModifiedDate_API__c = conEvnt.Last_Modified_Date__c;
        conWrap.contactRecord.contact.LastModifiedBy_API__c = conEvnt.Last_Modified_By__c;

        return conWrap;
    }

    /***********************************************
    * Ryan Wilson
    * 09.SEP.2017
    * Build Role wrapper
    ***********************************************/
    private static ContactWrapper.Role build_RoleWrapper(Contact__e conEvnt, Roles__c role, String accountId) {
        ContactWrapper.Role roleWrp = new ContactWrapper.Role();
        roleWrp.role = new Roles__c();
        
        if(role != null){
            //update previous role
            roleWrp.role = role;
            roleWrp.contactKey = conEvnt.SAP_Staff_Id__c;
            roleWrp.role.Status__c = 'Former'; 
        }else{
            //create new role
            roleWrp.contactKey = conEvnt.SAP_Staff_Id__c;
            roleWrp.role.Account__c = accountId;
            roleWrp.role.Role__c = conEvnt.Title__c;
            roleWrp.role.Status__c = (conEvnt.Status__c != 'Inactive')? 'Current': 'Former';
            roleWrp.role.Start_Date__c = conEvnt.Start_Date__c;
            roleWrp.role.End_Date__c = conEvnt.End_Date__c;
        }

        return roleWrp;
    }

    /***********************************************
    * Ryan Wilson
    * 09.SEP.2017
    * Build Channel wrapper
    ***********************************************/
    private static ContactWrapper.Channel build_ChannelWrapper(Contact__e conEvnt, Channel__c chan) {
        ContactWrapper.Channel channelWrp = new ContactWrapper.Channel();
        channelWrp.channel = new Channel__c();

        if(chan != null){
            channelWrp.channel = chan;
        }

        channelWrp.contactKey = conEvnt.SAP_Staff_Id__c;
        channelWrp.channel.Email__c = conEvnt.Email__c;
        channelWrp.channel.Mobile_Phone__c = conEvnt.MobilePhone__c;
        channelWrp.channel.Phone__c = conEvnt.Phone__c;
        channelWrp.channel.Unique_Id__c = conEvnt.SAP_Staff_Id__c;
        
        return channelWrp;
    }

    /***********************************************
    * Ryan Wilson
    * 09.SEP.2017
    * Build a list of Contact wrapper for new Contacts, Role and Channel to be created
    ***********************************************/
    public static List<ContactWrapper> insertNewStaffContact(Map<String, Contact__e> mapNewStaffContact, Map<String, Account> mapExistingAccount) {
        //Handle new Staff contacts - insert contact, role and channel
        List<ContactWrapper> conWrapList = new List<ContactWrapper>();

        for(Contact__e conEvnt: mapNewStaffContact.values()){
            //initialize individual contact wrapper
            ContactWrapper conWrap = build_ContactWrapper(conEvnt, null);

            ContactWrapper.Channel channelWrp = build_ChannelWrapper(conEvnt, null);
            conWrap.contactRecord.channels.add(channelWrp);

            ContactWrapper.Role roleWrp = build_RoleWrapper(conEvnt, null, mapExistingAccount.get(conEvnt.Faculty_Portfolio__c).Id);
            conWrap.contactRecord.roles.add(roleWrp);

            conWrapList.add(conWrap);
        }

        return conWrapList;
    }

    /***********************************************
    * Ryan Wilson
    * 09.SEP.2017
    * Build a list of Contact wrapper for existing Contacts, Role and Channel to be updated
    ***********************************************/
    public static List<ContactWrapper> updateStaffContact(Map<String, Contact> mapExistingStaffContacts, Map<String, Contact__e> mapUpdateStaffContact, Map<String, Account> mapExistingAccount) {
        List<ContactWrapper> conWrapList = new List<ContactWrapper>();

        for(Contact con: mapExistingStaffContacts.values()){
            Contact__e conEvnt = mapUpdateStaffContact.get(con.Id);

            if(conEvnt != null){
                //initialize individual contact wrapper
                ContactWrapper conWrap = build_ContactWrapper(conEvnt, con);

                if(con.Roles__r.size() > 0){
                    for(Roles__c role: con.Roles__r){
                        String staffStatus = (conEvnt.Status__c != 'Inactive')? 'Current': 'Former';
                        //If Role or Status change, create new Role and deactivate old role....
                        if(role.Role__c != conEvnt.Title__c){
                            ContactWrapper.Role roleWrp = build_RoleWrapper(conEvnt, null, mapExistingAccount.get(conEvnt.Faculty_Portfolio__c).Id);
                            conWrap.contactRecord.roles.add(roleWrp);

                            ContactWrapper.Role roleWrp_deactivate = build_RoleWrapper(conEvnt, role, mapExistingAccount.get(conEvnt.Faculty_Portfolio__c).Id);
                            conWrap.contactRecord.roles.add(roleWrp_deactivate);
                        }
                        else if(role.Status__c != staffStatus){
                            ContactWrapper.Role roleWrp_deactivate = build_RoleWrapper(conEvnt, role, mapExistingAccount.get(conEvnt.Faculty_Portfolio__c).Id);
                            conWrap.contactRecord.roles.add(roleWrp_deactivate);
                        }
                    }
                }
                else{
                    ContactWrapper.Role roleWrp = build_RoleWrapper(conEvnt, null, mapExistingAccount.get(conEvnt.Faculty_Portfolio__c).Id);
                    conWrap.contactRecord.roles.add(roleWrp);
                }
                
                if(con.Channels__r.size() > 0){
                    for(Channel__c chan: con.Channels__r){
                        ContactWrapper.Channel channelWrp = build_ChannelWrapper(conEvnt, chan);
                        conWrap.contactRecord.channels.add(channelWrp);
                    }
                }
                else{
                    ContactWrapper.Channel channelWrp = build_ChannelWrapper(conEvnt, null);
                    conWrap.contactRecord.channels.add(channelWrp);
                }

                conWrapList.add(conWrap);
            }
        }

        return conWrapList;
    }

    /***********************************************
    * Ryan Wilson
    * 29.AUG.2017
    * retrieve Platform event settings
    ***********************************************/
    public static Boolean allowPublishEvent() {
        Boolean allowPublish = false;
        try{
            Platform_Events_Setting__mdt eventSettings = [SELECT Label, Publish_Events__c 
                                                        FROM Platform_Events_Setting__mdt 
                                                        WHERE Label = 'Event Settings' limit 1 ];

            if(eventSettings != null){
                allowPublish = eventSettings.Publish_Events__c;
            }
        }
        catch(Exception ex){

        }
        
        return allowPublish;
    }

    /***********************************************
    * Ryan Wilson
    * 29.AUG.2017
    * Retrieve inserted or updated Role record
    ***********************************************/
    public static List<Roles__c> getRoles(Set<Id> roleIds) {
        String strQueryString = queryBuilder('Roles__c');
        String commaSepratedFields = getAllFields('Roles__c');
        String contactFields = ', Contact__r.CRM_Id__c, '
                            +'Contact__r.Person_ID__c, '
                            +'Contact__r.Title, '
                            +'Contact__r.Birthdate, '
                            +'Contact__r.First_Name__c, '
                            +'Contact__r.Last_Name__c, '
                            +'Contact__r.Gender__c, '
                            +'Contact__r.Preferred_Name__c, '
                            +'Channel__r.Name';

        String queryString = 'SELECT ' + commaSepratedFields + contactFields +' FROM Roles__c WHERE Id IN:roleIds';
        
        List<Roles__c> roleList = Database.Query(queryString);
        return roleList;
    }

    /***********************************************
    * Ryan Wilson
    * 29.AUG.2017
    * Retrieve inserted or updated Role record
    ***********************************************/
    public static List<Channel__c> getChannels(Set<Id> channelIds) {
        String commaSepratedFields = getAllFields('Channel__c');
        String contactFields = ', Contact__r.CRM_Id__c, '
                            +'Contact__r.Person_ID__c, '
                            +'Contact__r.Title, '
                            +'Contact__r.Birthdate, '
                            +'Contact__r.First_Name__c, '
                            +'Contact__r.Last_Name__c, '
                            +'Contact__r.Gender__c, '
                            +'Contact__r.Preferred_Name__c';
        
        String queryString = 'SELECT ' + commaSepratedFields + contactFields +' FROM Channel__c WHERE Id IN:channelIds';
        
        List<Channel__c> roleList = Database.Query(queryString);
        return roleList;
    }

    /***********************************************
    * Ryan Wilson
    * 29.AUG.2017
    * Get all fields for a specific object
    ***********************************************/
    public static String getAllFields(String sobjName) {
        if(sobjName != null && sobjName != ''){
            String SobjectApiName = sobjName;
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
            String commaSepratedFields = '';
            for(Schema.SObjectField val : fieldMap.values()){
                if(val.getDescribe().isAccessible()){                    
                    if(commaSepratedFields == null || commaSepratedFields == ''){
                        commaSepratedFields = val.getDescribe().getName();
                    }else{
                        commaSepratedFields = commaSepratedFields + ', ' + val.getDescribe().getName();
                    }
                }                
            }
            
            return commaSepratedFields;
        }        
        return null;
    }

    /***********************************************
    * Ryan Wilson
    * 29.AUG.2017
    * Generic method that generates a query for all fields on any object
    ***********************************************/
    public static String queryBuilder(String sobjName) {
        if(sobjName != null && sobjName != ''){
            String SobjectApiName = sobjName;
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
            String commaSepratedFields = '';
            for(Schema.SObjectField val : fieldMap.values()){
                if(val.getDescribe().isAccessible()){                    
                    if(commaSepratedFields == null || commaSepratedFields == ''){
                        commaSepratedFields = val.getDescribe().getName();
                    }else{
                        commaSepratedFields = commaSepratedFields + ', ' + val.getDescribe().getName();
                    }
                }                
            }
            
            String query = 'select ' + commaSepratedFields + ' from ' + SobjectApiName;
            return query;
        }        
        return null;
    }

}