/**
 * This class contains unit tests for validating the behavior of the MonTrack custom controller
 * these methods are part of the testing framework implemented throughout UniCRM
 * @revision Anterey Custodio, 27.Jan.2017 - rewritten the whole test class because it is hitting the
 *           'Too Many SOQL query' error. The reason for this is because a method with a 
 *               lot of queries has been called multiple times in a single testMethod
 */
@isTest
private class mt_mainControllerCC_Test {
    
    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_Error1() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);

                montrack.taskClicked();
                montrack.saveCall(); //Error  
            Test.stopTest();             
        }    
    }    

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_Error2() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);

                montrack.taskClicked();
                montrack.currenttask.status = 'Call Back Requested by Student/Other';
                montrack.saveCall(); //Error
            Test.stopTest();             
        }    
    }  

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_Error3() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);

                montrack.taskClicked();
                montrack.currenttask.status = 'No Answer';
                montrack.currentContact.MonTrack_Opt_Out__c = true;
                montrack.saveCall(); //Error
            Test.stopTest();             
        }    
    }  

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_Pass1() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);

                montrack.taskClicked();
                montrack.currenttask.status = 'No Answer'; //I
                montrack.currentTask.Call_Back_Date_Time__c= System.Now(); 
                montrack.currentContact.MonTrack_Opt_Out__c = true;
                montrack.currentContact.MonTrack_Opt_Out_Reason__c='Reason';
                montrack.saveCall();

                montrack.deleteTask();    //undo
            Test.stopTest();             
        }    
    } 

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_Complete() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);

                montrack.taskClicked();
                montrack.currenttask.status = 'No Answer'; //I
                montrack.currentTask.Call_Back_Date_Time__c= System.Now(); 
                montrack.currentContact.MonTrack_Opt_Out__c = true;
                montrack.currentContact.MonTrack_Opt_Out_Reason__c='Reason';
                montrack.saveCall();

                montrack.currenttask.status = 'Completed';                
                montrack.saveCall(); //Error-Complete your call  
            Test.stopTest();             
        }    
    } 

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_SaveAndDeleteTopic() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);

                montrack.saveTopicRecord();    //Exception //Both are mandatory                 
                montrack.topic.type__c = 'Montrack';
                montrack.saveTopicRecord();    //Exception
                montrack.topic.description__c = 'Academic - Exam timetable'; 
                montrack.saveTopicRecord();

                Id topicId=montrack.topic.id;
                apexpages.currentPage().getParameters().put('tId',topicId);
                montrack.deleteTopicRecord();
            Test.stopTest();             
        }    
    }

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_SaveAndDeleteReferral() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);

                //Referral
                montrack.saveReferralRecord(); //Exception
                montrack.referral.referral__c = 'Childcare';
                montrack.saveReferralRecord();


                Id referralId=montrack.referral.id;
                apexpages.currentPage().getParameters().put('rId',referralId);
                montrack.deleteReferralRecord();
            Test.stopTest();             
        }    
    }

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_SaveCallError() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);

                montrack.currentTask.Reaction_to_Call__c = '';
                montrack.currentTask.Duration_mins__c = '';
                montrack.saveCall(); //Error

                montrack.currentTask.Reaction_to_Call__c = 'Well Received';
                montrack.currentTask.Duration_mins__c = '5';
                montrack.saveCall(); //Save

                montrack.onChangeRefreshList(); // just to run the logic
            Test.stopTest();             
        }    
    }

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_DeleteTask() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);

                //Topic Insert after Complete
                montrack.topic.description__c = 'Academic - Exam timetable'; 
                montrack.topic.type__c = 'Montrack';
                montrack.saveTopicRecord();    //Error
                //Referral after complete
                montrack.referral.referral__c = 'Childcare';
                montrack.saveReferralRecord();

            Test.stopTest();             
        }    
    }

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_Reset() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);

                montrack.resetChildObjects();
                montrack.revertToListView();
                montrack.dummyAction();

            Test.stopTest();             
        }    
    }

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_InitialPageLoad() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);
                montrack.taskClicked();

                taskDetail td = montrack.tdetail; // simulate getter by VF page
                montrack.testRoundTasks = montrack.otherRounds; // simulate get by VF page 
                montrack.testTaskHistory = montrack.taskHistories; // simulate get by VF page
                mt_mainControllerCC.TaskHistory th = new mt_mainControllerCC.TaskHistory();
                Boolean rendered = montrack.renderTable;

                // confirm input fields 
                system.assertEquals('Not Started',montrack.currenttask.status);

            Test.stopTest();             
        }    
    }

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_Error4() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);
                montrack.taskClicked();

                // test the save call validation
                montrack.resetChildObjects(); // simulate onchange action and then save
                montrack.currenttask.status = 'Not Started';
                //montrack.currentTask.Call_Back_Date_Time__c= System.Now();                
                montrack.saveCall();
                system.assertEquals('msg2',montrack.errorMsg); // should have prevented save

            Test.stopTest();             
        }    
    }

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_Error5() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);
                montrack.taskClicked();

                boolean b=montrack.renderTaskTable;               
                mt_mainControllerCC.roundTask rt=new mt_mainControllerCC.roundTask();
                rt.cohort=null;
                rt.round=null;
                rt.task=null;
                rt.SSAName=null;
                rt.createdDate=null;
                montrack.editOnlyMode=true;

                montrack.resetChildObjects(); // simulate onchange action and then save
                montrack.currenttask.status = 'Call Back Requested by Student/Other';
                //montrack.currentTask.Call_Back_Date_Time__c= System.Now();                
                montrack.saveCall();
                system.assertEquals('msg3',montrack.errorMsg); // should have prevented save

            Test.stopTest();             
        }    
    }

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_SaveTopic() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);
                montrack.taskClicked();

                id tmpId = montrack.currentTask.id; // store this for delete action later in test method

                // now add a topic and referral, then try to save as completed again

                montrack.topic.type__c = 'Montrack';
                montrack.topic.description__c = 'Academic - Exam timetable';
                montrack.saveTopicRecord();
                system.assertEquals(1,[ select count() from topic__c where round__c =:montrack.round and student__c =:montrack.contactId]); // confirm insert success

            Test.stopTest();             
        }    
    }

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  
    *******************************************************************************/
    static testMethod void testmt_mainControllerCC_resetUser() {
        
        //create user
        
        User u= TestHelper.ssaUser();
        
        createSampleData (u);
            
        
        System.runAs(u) {
            Test.startTest();        
                mt_mainControllerCC montrack = initialiseController (u);
                montrack.taskClicked();

                id tmpId = montrack.currentTask.id; // store this for delete action later in test method

                // now add a topic and referral, then try to save as completed again

                montrack.topic.type__c = 'Montrack';
                montrack.topic.description__c = 'Academic - Exam timetable';
                montrack.saveTopicRecord();
                system.assertEquals(1,[ select count() from topic__c where round__c =:montrack.round and student__c =:montrack.contactId]); // confirm insert success

                montrack.referral.referral__c = 'Childcare';
                montrack.saveReferralRecord();
                system.assertEquals(1,[select count() from referrals__c where round__c =:montrack.round and student__c =:montrack.contactId]); // confirm insert success

                montrack.saveCall(); // expect a successful save this time
                montrack.clearContextStudent(); // cover off, and going to reset in next step

                // now change cohort and round vals to cover the reset functions
                montrack.resetRoundAndUser(); // this called on cohort picklist change
                montrack.cohort = cohort[0].id;
                montrack.resetUser();
            Test.stopTest();             
        }    
    }

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  Public Variables
    *******************************************************************************/
    static set<ID> contactID;
    static List<Cohort__c> cohort;
    static list<Round__c> rList;

    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  method to create test data
    *******************************************************************************/
    static void createSampleData (User u) {
        //create account
        
        List<Account>acct= TestHelper.createStudent(10);
        insert acct;
        
        List<Contact> conList= TestHelper.createStudentContacts(10, acct);

        /*Anterey Custodio (OAKTON), 28/09/2016 - populate the Person ID field used by Callista*/
        for (Integer i=0; i<conList.size() ; i++) {
            conList[i].Person_ID_unique__c = '1111111'+i;
        }
        /*Anterey Custodio (OAKTON) - END*/

        insert conList;
        
        Set<ID>accountID = new Set<ID>();
        
        for(Account a:acct){
            accountID.add(a.ID);
        }
        
        //create Campaign
        
        List<Campaign> camp=  TestHelper.createCampaign(1);
        insert camp;
        
        //create Cohort
        
        cohort = TestHelper.createCohort(2);
        insert cohort;
        
        contactID= new Set<ID> ();
        for(Contact con : conList){
            contactID.add(con.Id);
        }
        list<CampaignMember> cm= TestHelper.createCampaignMember(u.Id, cohort[0].id, camp[0].Id,contactID );
        insert cm;
        
        list<CampaignMember> tmpCM= new List<CampaignMember>();
        tmpCM=[SELECT ID,contactID,cohort__c,CampaignID FROM CampaignMember WHERE Cohort__c=:cohort[0].ID AND CampaignID=:camp[0].Id ];
        
        rList= TestHelper.createRounds(tmpCM[0].Cohort__c, tmpCM[0].CampaignId, 4); 
        insert rList;
    }
    /*******************************************************************************
    * @author       Anterey Custodio (OAKTON)
    * @date         27.Jan.2017         
    * @description  method to initialise Controller values
    *******************************************************************************/
    static mt_mainControllerCC initialiseController (User u) {
        // confirming can add tasks
        list<Task> taskList = new list<Task>();
        boolean callbackAdded = false;
        for(ID conID:contactID){
        Task newtask= new Task( whatId= rlist[3].id,
                             whoId=conID,
                             Subject='MonTRACK Call');
                             if(!callbackAdded){
                                newTask.Status = 'Call Back Requested by Student/Other';
                                newTask.Call_Back_Date_Time__c = datetime.now().addHours(1);
                                callbackAdded = true;
                             }
            taskList.add(newtask);
        }
        insert taskList;
        List<Task> task=[Select Id, WhatID FROM Task where WhatID =:rlist[3].id];
        
        
 
         PageReference ref = Page.mt_main;
         Test.setCurrentPage(ref);
         mt_mainControllerCC montrack = new mt_mainControllerCC();                 
         List<selectOption> cOptions = montrack.cohortOptions;
         montrack.cohort = cohort[0].id;
         List<selectOption> rOptions = montrack.roundOptions;
         montrack.round = rList[3].id;
         List<selectOption> uOptions = montrack.ssaUserOptions;                 
         montrack.ssaUser = u.id;
         montrack.retrieveCalls();
         List<selectOption> fOptions = montrack.filterOptions;
         montrack.listFilter= 'Not Started';                 
         montrack.retrieveCalls();
         montrack.listFilter= 'Incomplete';
         montrack.retrieveCalls();
         montrack.listFilter= 'Completed';
         montrack.retrieveCalls();
         montrack.listFilter= 'All';
         montrack.retrieveCalls();
         
         string msg = montrack.nextCallbackMessage;                 
         system.assertEquals(10,montrack.taskDetailList.size());
         
         id tskId = montrack.taskDetailList[0].taskId;
         id conId = montrack.taskDetailList[0].contactId;
         
         apexpages.currentpage().getparameters().put('tId',tskId);
         apexpages.currentpage().getparameters().put('cId',conId);

         return montrack;
    }
}