/*******************************************************************************
* @author       Ryan Wilson
* @date         6.OCT.2017
* @description  Test class for ApplicationToContactFix_Batch
* @revision     
*******************************************************************************/
@isTest
private class ApplicationToContactFix_Batch_Test {
    private static Contact contactRecord;

    @isTest static void test_method_one() {
        //create a community user
        User communityUserRec = createCommunityUser();
        System.assertNotEquals(null, contactRecord.Id, 'Contact not found');

        Contact con_student = StudFirst_TestHelper.createApplicant();
        insert con_student;

        //create a new draft application
        Application__c appRecord = createCompletedApplication(true);
        appRecord.Applicant__c = con_student.Id;
        appRecord.OwnerId = communityUserRec.Id;
        appRecord.Status__c = 'Submitted';
        appRecord.From_Portal__c = true;
        update appRecord;

        Test.startTest();
        Database.executeBatch(new ApplicationToContactFix_Batch(), 200);
        Test.stopTest();
    }

    static User createCommunityUser () {
        User userRec;
        try {
            String profileId = [SELECT Id FROM Profile WHERE Name = 'Domestic Applicants' LIMIT 1].Id;

            Account accountRecord = new Account(name ='Domestic Applicants') ;
            insert accountRecord;

            contactRecord = new Contact();
            contactRecord.FirstName = 'Joe';
            contactRecord.LastName = 'Bloggs';
            contactRecord.AccountId = accountRecord.Id;
            contactRecord.Birthdate = System.today();
            contactRecord.Email = 'applicant@mailinator.com';
            insert contactRecord;

            Integer rnd = Integer.valueOf(Math.random()*9999);
            userRec = new User( Alias = String.valueOf(rnd)+ 'usr', 
                                Email = 'applicant@mailinator.com', 
                                EmailEncodingKey = 'UTF-8', 
                                FirstName = 'Joe',
                                LastName = 'Bloggs', 
                                LanguageLocaleKey = 'en_US', 
                                LocaleSidKey = 'en_GB',
                                App_Birthdate__c = System.today(), 
                                ProfileId = profileId,
                                TimeZoneSidKey = 'Australia/Sydney', 
                                UserName = String.valueOf(rnd) +'testuser@monash.edu',
                                ContactId = contactRecord.Id);
            insert userRec;

        } catch (Exception ex) {
            System.debug('@@ex: '+ ex.getMessage());
        }

        return userRec;
    }

    private static Application__c createCompletedApplication(Boolean isComplete) {
        // Create application
        Application__c application = StudFirst_TestHelper.createApplication(contactRecord);
        application.Status__c = 'Draft';
        insert application;  

        // Create Qualifications
        Qualification__c qual1 = StudFirst_TestHelper.createQualification('Secondary');
        insert qual1;

        Qualification__c qual2 = StudFirst_TestHelper.createQualification('Tertiary');
        insert qual2;

        Qualification__c qual3 = StudFirst_TestHelper.createQualification('Other');
        insert qual3;

        Qualification__c qual4 = StudFirst_TestHelper.createQualification('English');
        insert qual4;

        // Create Contact Qualifications
        Contact_Qualification__c contactQualification1 = StudFirst_TestHelper.createContactQualification(contactRecord, qual1, 'Secondary Education');
        insert contactQualification1;

        Contact_Qualification__c contactQualification2 = StudFirst_TestHelper.createContactQualification(contactRecord, qual2, 'Tertiary Education');
        insert contactQualification2;

        Contact_Qualification__c contactQualification3 = StudFirst_TestHelper.createContactQualification(contactRecord, qual3, 'Other Qualification');
        insert contactQualification3;

        Contact_Qualification__c contactQualification4 = StudFirst_TestHelper.createContactQualification(contactRecord, qual4, 'English Test');
        insert contactQualification4;

        // Create Work Experience
        Work_Experience__c workExperience = StudFirst_TestHelper.createWorkExperience(contactRecord);
        insert workExperience;

        // Create Course
        Course__c course = StudFirst_TestHelper.createCourse();
        insert course;

        // Creare Course Offering
        Course_Offering__c courseOffering = StudFirst_TestHelper.createCourseOffering(course);
        insert courseOffering;

        // Create Course Preferences
        if(isComplete){
            Application_Course_Preference__c coursePreference = StudFirst_TestHelper.createCoursePreferences(application, contactRecord, courseOffering, course);
            insert coursePreference;
        }
        

        return application;
    }
    
}