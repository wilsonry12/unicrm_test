global class BatchDeleteRecords implements Database.Batchable<sObject> {

    global final String query;

    global BatchDeleteRecords(String q) {
        query=q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC,List<sObject> scope) {
        delete scope;        
    }

    global void finish(Database.BatchableContext BC) {
        //Send an email to the User after your batch completes
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'vineeth.batreddy@monash.edu'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Apex Batch Job is done');
        mail.setPlainTextBody('The batch Apex job processed : BatchDeleteRecords');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

}