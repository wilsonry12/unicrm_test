public with sharing class BMSLandingController {
	
	public String optionParam {get;set;}
	public String BMS_EMAIL {get;set;}
    public String BMS_SMS {get;set;}
    public String currentUserProfile {get;set;}
    
	public BMSLandingController() {
		BMS_EMAIL = 'email';
		BMS_SMS = 'sms';
		currentUserProfile = CommonUtilities.getProfileName();
	}

	public PageReference gotoAddRecepients(){
		PageReference redirect = Page.BMSOptions;
        redirect.getParameters().put('type',optionParam);
        return redirect;
	}

	public PageReference redirectUser() {
		PageReference redirect = null;
		if(currentUserProfile != CommonUtilities.BMS_ADMIN && currentUserProfile != CommonUtilities.SYSTEM_ADMIN){
			redirect = Page.BMSOptions;
			redirect.getParameters().put('type',BMS_EMAIL);
		}
		return redirect;
	}
}