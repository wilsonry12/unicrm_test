/*******************************************************************************
* @author		Ant Custodio
* @date         19.Jun.2017     
* @description  test class for ApplicationHandler
* @revision     
*******************************************************************************/
@isTest
public with sharing class ApplicationHandler_Test {
	/*******************************************************************************
	* @author       Ant Custodio
	* @date         19.Jun.2017     
	* @description  try and insert 2 draft applications
	* @revision     
	*******************************************************************************/
	static testMethod void test_InsertingTwoDrafts() {
		
		//create a community user
		User communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');
		String profileId = [SELECT Id FROM Profile WHERE Name = 'Domestic Applicants' LIMIT 1].Id;
		//only run if there is a Domestic Applicant Profile User
		System.assertEquals(profileId, communityUserRec.ProfileId);
		
		Account acc = new Account();
		acc.Name = 'Kangaroo';
		acc.Callista_Org_Unit_Code__c = 'KANGA-ED';
		insert acc;

		Account acc2 = new Account();
		acc2.Name = 'Koala';
		acc2.Callista_Org_Unit_Code__c = 'KOALA-ED';
		insert acc2;
		
		test.startTest();
		System.runAs(communityUserRec) {
			//create a new draft application
			Application__c appRecord = StudFirst_TestHelper.createApplication(communityUserRec.ContactId);
			appRecord.Status__c = 'Draft';
			appRecord.Agent_Org_Unit_Code__c = 'KANGA-ED';
			insert appRecord;
			System.assertNotEquals(null, appRecord.Id, 'No application created');
			
			//the trigger should fire and populate this lookup
			System.assertEquals(acc.Id, [SELECT Agent__c FROM Application__c WHERE Id =: appRecord.Id].Agent__c);

			Boolean exceptionCaught = false;
			try {
				Application__c appRecord2 = StudFirst_TestHelper.createApplication(communityUserRec.ContactId);
				appRecord2.Status__c = 'Draft';
				insert appRecord2;
			} catch (Exception ex) {
				exceptionCaught = true;
			}
			System.assert(exceptionCaught, 'Another draft application has been inserted');

			//scenario 2, update the org unit code, the trigger should fire and update this lookup
			appRecord.Agent_Org_Unit_Code__c = 'KOALA-ED';
			update appRecord;
			System.assertEquals(acc2.Id, [SELECT Agent__c FROM Application__c WHERE Id =: appRecord.Id].Agent__c);

		}
		test.stopTest();
	}
}