/**
 * This class contains unit tests for validating the behavior of EmailMessageServices methods
 * these methods are part of the testing framework implemented throughout UniCRM
 * 
 */
 @isTest
 private class EmailMessageServices_Test {
	private static Application__c appRecord;
	private static User communityUserRec;
	private static Contact contactRec;
	private static Application_Course_Preference__c coursePrefA;
	private static Application_Course_Preference__c coursePrefB;
	private static Course_Offering__c courseOfferingRec;
	private static Course_Offering__c courseOfferingRecNoUnit;

	static testMethod void confirmEmailMapping(){
			TestHelper.calabrioSetUp();
			
			List<Contact> testContacts = TestHelper.createStudentContacts(2, null);

			insert testContacts;
			
			system.assertEquals(2,[select COUNT() from Contact],'test contacts not inserted');
			
			Set<Id> testContactIds = new Set<Id>();
			for(Contact testContact : testContacts) testContactIds.add(testContact.Id);
			//Anterey Custodio, 23.Jan.2017 - added recordtype to sample data
			RecordTypes_Allowed_on_CaseTrigger__c recTypesAllowed = new RecordTypes_Allowed_on_CaseTrigger__c();
			recTypesAllowed.Name = 'Standard Enquiry';
			recTypesAllowed.API_Name__c = 'Standard Enquiry';
			insert recTypesAllowed;
			
			QueueList__c queueListRec = new QueueList__c();
			queueListRec.Name = 'Schools Engagement';
			queueListRec.Retain_Owner__c = true;
			insert queueListRec;
			
			String enquiryRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Standard Enquiry').getRecordTypeId();
			List<case> cases = new List<case>();
			for(integer i=0; i<2; i++){
					cases.add(new case(SuppliedName = 'testName',
						SuppliedPhone = '0400000000',
						SuppliedEmail = 'testformEmail@monash.edu',
						Type = 'Web',
						Origin = 'Email - System',
						Case_type__c = 'Standard Enquiry',
						ContactId = null,
						AccountId = null,
						Course_code__c = '0',
						Status = 'Closed',
						RecordTypeId = enquiryRecordType,
						Case_Queue_Name__c = 'Schools Engagement'));
			}
			
			insert cases;
			cases[0].Origin = 'Email'; // telephony forces default to phone. need to use update
			cases[1].Origin = 'Email - System'; // telephony forces default to phone. need to use update

			Test.startTest();

			update cases;
			
			List<EmailMessage> emails = new List<EmailMessage>();
			
			For(case c : cases)
			emails.add(new EmailMessage( ParentId = c.id,
			 FromAddress = 'testfrom@testmonash.edu',
			 FromName = 'testSender',
			 Subject = 'testSubject',
			 TextBody = 'test body',
			 ToAddress = 'testEndpoint@monash.edu',
			 Incoming = true));    
			
			insert emails;
			
			system.assertEquals(false,[select id,IsClosed from case where id =:cases[0].id limit 1].IsClosed,'inbound should have opened case'); // non system
			system.assertEquals(true,[select id,IsClosed from case where id =:cases[1].id limit 1].IsClosed,'inbound should not have reopened case'); // system 
			
			EmailMessage em = new EmailMessage(ParentId = cases[1].id,
			 FromAddress = 'testfrom@testmonash.edu',
			 FromName = 'testSender',
			 Subject = 'testSubject',
			 TextBody = 'test body',
			 ToAddress = 'testEndpoint@monash.edu',
			 Incoming = true);
			insert em;
			system.assertEquals(false,[select id,IsClosed from case where id =:cases[1].id limit 1].IsClosed,'inbound should have opened case'); // system 2nd inbound email
			Set<id> caseIds = new Set<id>();
			
			Test.stopTest();
	}
	
	/*******************************************************************************
	* @author       Anterey Custodio
	* @date         28.Jul.2017        
	* @description  tests the Invocable method that sends emails to agent when 
										certain criteria was met
	* @revision     
	*******************************************************************************/
	static testMethod void test_sendEmailToAgent() {
		//create data
		String admissionsRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Admissions').getRecordTypeId();
		//create a community user
		communityUserRec = StudFirst_TestHelper.createCommunityUser();
		//create required records
		createSampleData(communityUserRec.ContactId, 'Draft', 'TEST-STATUS', true);
		
		Test.startTest();
		Case caseRec = new  Case(SuppliedName = 'testName',
								SuppliedPhone = '0400000000',
								SuppliedEmail = 'testformEmail@monash.edu',
								ContactId = null,
								AccountId = null,
								RecordTypeId = admissionsRecType,
								Notify_Agent__c = true,
								Application_Course_Preference__c = coursePrefA.Id );
		insert caseRec;

		System.assert(caseRec.Notify_Agent__c, 'Notify Agent should be set to true');
		System.assert(caseRec.RecordTypeId == admissionsRecType, 'Record Type should be Admissions');

		EmailMessage emlMsg = new EmailMessage( ParentId = caseRec.id,
												FromAddress = 'testfrom@testmonash.edu',
												FromName = 'testSender',
												Subject = 'testSubject',
												HtmlBody = '<b>test body</b>',
												ToAddress = 'testEndpoint@monash.edu'); 
		insert emlMsg;

		System.assertEquals(caseRec.Id, emlMsg.ParentId, 'Email Message ParentId should be the Case created');

		EmailMessageServices.sendEmailToAgent (new List<String> {emlMsg.Id});

		Test.stopTest();
	}

	/*******************************************************************************
	* @author       Anterey Custodio
	* @date         1.Aug.2017        
	* @description  creates sample data
	* @revision     
	*******************************************************************************/
	static void createSampleData (String contactId, String status, String outcome, Boolean addCourse) {
		//create a new draft application
		appRecord = StudFirst_TestHelper.createApplication(contactId);
		appRecord.OwnerId = communityUserRec.Id;
		appRecord.Status__c = status;
		appRecord.Agent_Email__c = 'agentEmail@agents.com';
		insert appRecord;

		contactRec = [SELECT Id FROM Contact WHERE Id =: contactId];
		contactRec.Residency_Status__c = 'DOM-AUS';
		update contactRec;

		//Tertiary institution & qualification
		Institution__c tertInsRec = StudFirst_TestHelper.createTertiaryInstitution();
		insert tertInsRec;

		Callista_Outcome_Status__c outcomeStatus = StudFirst_TestHelper.createOutcomeStatus();
		insert outcomeStatus;

		Contact_Qualification__c conQualRec = new Contact_Qualification__c();
		conQualRec.Country__c = 'Australia';
		conQualRec.First_Year_Enrolled__c = String.valueOf(date.today().year()-4);
		conQualRec.Last_Year_Enrolled__c = String.valueOf(date.today().year());
		conQualRec.Other_Qualification__c = 'Certificate III';
		conQualRec.Institution_Code__c = tertInsRec.Institution_Code__c;
		conQualRec.Institution_Name__c = tertInsRec.Institution_Name__c;
		conQualRec.Status__c = 'CURRENTLY STUDYING';
		conQualRec.Contact__c = contactRec.Id;
		conQualRec.RecordTypeId = Schema.SObjectType.Contact_Qualification__c.getRecordTypeInfosByName().get('Tertiary Education').getRecordTypeId();
		insert conQualRec;

		Application_Qualification_Provided__c aqpRecord = new Application_Qualification_Provided__c();
		aqpRecord.Application__c = appRecord.Id;
		aqpRecord.Contact_Qualification__c = conQualRec.Id;
		insert aqpRecord;

		Work_Experience__c workExpRecord = new Work_Experience__c();
		workExpRecord = StudFirst_TestHelper.populateWorkExperienceRecord(workExpRecord);
		workExpRecord.Contact__c = contactRec.Id;
		insert workExpRecord;

		Contact_Document__c contactDocu = new Contact_Document__c();
		contactDocu = StudFirst_TestHelper.createContactDocument(contactRec.Id);
		insert contactDocu;

		Application_Work_Experience_Provided__c aweRecord = new Application_Work_Experience_Provided__c();
		aweRecord.Application__c = appRecord.Id;
		aweRecord.Work_Experience__c = workExpRecord.Id;
		insert aweRecord;

		Course__c courseRec = StudFirst_TestHelper.createCourse();
		insert courseRec;

		courseOfferingRec = StudFirst_TestHelper.createCourseOffering(courseRec);
		insert courseOfferingRec;

		courseOfferingRecNoUnit = StudFirst_TestHelper.createCourseOfferingNoUnitSet(courseRec);
		insert courseOfferingRecNoUnit;

		if(addCourse){
			coursePrefA = StudFirst_TestHelper.createCoursePreferences(appRecord, contactRec, courseOfferingRec, courseRec);
			coursePrefA.Outcome_Status__c = outcome;
			coursePrefA.Offer_Response_Status__c = outcome;
			coursePrefA.Preference_Number__c = '1';
			insert coursePrefA;

			coursePrefB = StudFirst_TestHelper.createCoursePreferences(appRecord, contactRec, courseOfferingRecNoUnit, courseRec);
			coursePrefB.Outcome_Status__c = outcome;
			coursePrefB.Offer_Response_Status__c = outcome;
			coursePrefB.Preference_Number__c = '2';
			insert coursePrefB;
		}
	}
}