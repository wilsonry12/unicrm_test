//This class provides logic for inbound just-in-time provisioning of single sign-on users in your Salesforce organization.
global class AskMonash_JITHandler implements Auth.SamlJitHandler {
    private class JitException extends Exception{}
   
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        try{
              //create account
               String firstName = '';
               String lastName = '';
               String email = '';
               String fedId = '';
               String staffProfileId = [SELECT Id FROM Profile where name =:label.AskMonashStaffProfileName].id;
               if (staffProfileId == null || staffProfileId == ''){
                    throw new JitException();
               } 
               String studentProfileId = '';
              if(attributes.containsKey('http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname')) {
                  firstName  = attributes.get('http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname');
              }
              if(attributes.containsKey('http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname')) {
                  lastName  = attributes.get('http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname');
              }
              if(attributes.containsKey('http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress')) {
                  email = attributes.get('http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress');
              }
              if(attributes.containsKey('http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname')){
                  fedId = attributes.get('http://schemas.microsoft.com/ws/2008/06/identity/claims/windowsaccountname');
              }
              
              //try to find a matching contact
              list<Contact> mtachingContacts =[select id from Contact where Authcate__c =:fedId];
              if(mtachingContacts  == null ||mtachingContacts.size()==0 ){
                  //create a new account if not found
                   Account myAccount = new Account();
                   myAccount.Name = firstName + lastName; 
                   insert myAccount ;
                   //create Contact
                   Contact myContact = new Contact();
                   myContact.firstName = firstName;
                   myContact.lastName = lastName;
                   myContact.email = email ;
                   myContact.accountid = myAccount.Id;
                   myContact.Authcate__c  = fedId;
                   insert myContact;
                   //insert user
                   String uid = UserInfo.getUserId();
                   User currentUser = [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id=:uid];
                   User u = new User();
                   u.ContactId = myContact.Id;
                   u.userName = email;
                   u.email = email ;
                   u.firstName = firstName;
                   u.lastName = lastName;
                   u.LocaleSidKey = currentUser.LocaleSidKey;
                   u.LanguageLocaleKey = currentUser.LanguageLocaleKey;
                   u.EmailEncodingKey  = currentUser.EmailEncodingKey ;
                   u.TimeZoneSidKey = currentUser.TimeZoneSidKey;
                   String alias = '';
                    if(u.FirstName == null) {
                        alias = u.LastName;
                    } else {
                        alias = u.FirstName.charAt(0) + u.LastName;
                    }
                    if(alias.length() > 5) {
                        alias = alias.substring(0, 5);
                    } 
                    u.Alias = alias;
                    u.CommunityNickname = alias;
                    u.FederationIdentifier = fedId;
                    u.ProfileId = staffProfileId ;
                    u.IsActive = true;
                    return u;
              }
              
              if(mtachingContacts.size() > 1){
                  throw new JitException();
              }
              else if(mtachingContacts.size() == 1){
                  String uid = UserInfo.getUserId();
                   User currentUser = [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id=:uid];
                   User u = new User();
                   u.ContactId = mtachingContacts[0].Id;
                   u.userName = email;
                   u.email = email;
                   u.firstName = FirstName;
                   u.lastName = LastName;
                   u.LocaleSidKey = currentUser.LocaleSidKey;
                   u.LanguageLocaleKey = currentUser.LanguageLocaleKey;
                   u.EmailEncodingKey  = currentUser.EmailEncodingKey ;
                   u.TimeZoneSidKey = currentUser.TimeZoneSidKey;
                   String alias = '';
                    if(u.FirstName == null) {
                        alias = u.LastName;
                    } else {
                        alias = u.FirstName.charAt(0) + u.LastName;
                    }
                    if(alias.length() > 5) {
                        alias = alias.substring(0, 5);
                    }
                    u.Alias = alias;
                    u.FederationIdentifier = fedId ;
                    u.ProfileId = staffProfileId ;
                    u.IsActive = true;
                    return u;
              }
        }
        Catch(Exception e){
            throw new JitException();
        }
     
        return null;
      
    }

    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
    }
}