/*
    AUTHOR : Vineeth Batreddy
    DESCRIPTION : This class is used to initiate the submission of admission application
*/

global class StudFirst_SubmitAdmissionApplication {

    private static final String AGENT_PERSON_ID = Label.CALLISTA_AGENT_PERSON_ID;
    private static final String DOMESTIC_COUNTRY = 'AUSTRALIA';
    private static final String DEFAULT_DOMESTIC_ADDRESS_TYPE = 'POSTAL';
    private static final String DEFAULT_INTERNATIONAL_ADDRESS_TYPE = 'OS-POSTAL';
    private static final String QUAL_TYPE_SECONDARY = 'Secondary Education';
    private static final String QUAL_TYPE_TERTIARY = 'Tertiary Education';
    private static final String QUAL_TYPE_OTHER = 'Other Qualification';
    private static final String QUAL_TYPE_ENGLISH = 'English Test';
    private static final String STANDARD_APPLICATION_TYPE = 'STANDARD';
    private static final String DEFAULT_APPLICATION_NUMBER = '123';
    private static final String APPLICATION_SUBMIT_SUCCESS = 'Submitted';
    private static final String APPLICATION_SUBMIT_UNSUCCESSFUL = 'Review';
    private static final String INDIGENOUS = 'INDIGENOUS';

    // This is invoked from Submit Application button on Application detail page
    WebService static void send(String applicationId) {
        System.debug('!@#$% Invoking application submission');

        List<String> applicationIds = new List<String>();
        
        if(String.isNotBlank(applicationId)) {
            applicationIds.add(applicationId);
            sendApplication(applicationIds);    
        }
            
    }

    // This is invoked from Submit Application flow when the application status is set to Sent for Submission
    @InvocableMethod
    public static void sendApplication(List<String> applicationIds) {

        String applicationId = applicationIds.get(0);

        // Retrieve data
        Application__c application = StudFirst_SubmitApplicationHelper.getApplicationById(applicationId);
        Contact applicant = StudFirst_SubmitApplicationHelper.getApplicantById(application.Applicant__c);
        List<Contact_Qualification__c> contactQualifications = StudFirst_SubmitApplicationHelper.getContactQualifications(applicant.Id);
        List<Work_Experience__c> workExperience = StudFirst_SubmitApplicationHelper.getWorkExperience(applicant.Id);
        List<Application_Course_Preference__c> coursePreferences = StudFirst_SubmitApplicationHelper.getCoursePreferences(applicationId);
        
        // Split qualifications by qualification types
        List<Contact_Qualification__c> secondary = new List<Contact_Qualification__c>();
        List<Contact_Qualification__c> tertiary = new List<Contact_Qualification__c>();
        List<Contact_Qualification__c> other = new List<Contact_Qualification__c>();
        List<Contact_Qualification__c> english = new List<Contact_Qualification__c>();

        System.debug('!@#$% application : ' + application);
        System.debug('!@#$% applicant : ' + applicant);
        System.debug('!@#$% contactQualifications : ' + contactQualifications);
        System.debug('!@#$% workExperience : ' + workExperience);
        System.debug('!@#$% coursePreferences : ' + coursePreferences);
        System.debug('!@#$% Secondary : ' + secondary);
        System.debug('!@#$% Tertiary : ' + tertiary);
        System.debug('!@#$% Other : ' + other);
        System.debug('!@#$% English : ' + english);

        for(Contact_Qualification__c cq : contactQualifications) {
            if(cq.Qualification_Type__c.equalsIgnoreCase(QUAL_TYPE_SECONDARY)) {
                secondary.add(cq);
            } else if(cq.Qualification_Type__c.equalsIgnoreCase(QUAL_TYPE_TERTIARY)) {
                tertiary.add(cq);    
            } else if(cq.Qualification_Type__c.equalsIgnoreCase(QUAL_TYPE_ENGLISH)) {
                english.add(cq);    
            } else {
                other.add(cq);    
            }
        }

        String req = generateRequest(application, applicant, secondary, tertiary, other, english, workExperience, coursePreferences);
        SendApplicationQueueable sendAppJob = new SendApplicationQueueable(req, application.Id);
        Id jobID = System.enqueueJob(sendAppJob);

    }

    private static String generateRequest(Application__c app, Contact applicant, List<Contact_Qualification__c> secondary, List<Contact_Qualification__c> tertiary, List<Contact_Qualification__c> other, List<Contact_Qualification__c> english, List<Work_Experience__c> wrkExp, List<Application_Course_Preference__c> coursePreferences) {

        StudFirst_AdmissionApplicationRequest aar = new StudFirst_AdmissionApplicationRequest();
        aar.admissionApplication = new StudFirst_AdmissionApplicationRequest.AdmissionApplication();
        aar.admissionApplication.agent = new StudFirst_AdmissionApplicationRequest.Agent();
        aar.admissionApplication.agent.agentPersonId = AGENT_PERSON_ID;

        // Applications
        aar.admissionApplication.applications = new List<StudFirst_AdmissionApplicationRequest.Application>();
        
        // Application
        StudFirst_AdmissionApplicationRequest.Application application  = new StudFirst_AdmissionApplicationRequest.Application();
        // Applicant
        application.applicant = new StudFirst_AdmissionApplicationRequest.Applicant();
        application.applicant.externalApplicantId = applicant.Id;
        application.applicant.surname = applicant.LastName;
        application.applicant.firstName = applicant.FirstName;
        application.applicant.otherName = applicant.Other_Given_Name__c;
        application.applicant.title = applicant.Salutation;
        application.applicant.sex = applicant.Gender__c;
        application.applicant.preferredGivenName = applicant.Preferred_Name__c;
        application.applicant.previousSurname = applicant.Previous_Surname__c;
        application.applicant.previousStudentIndicator = applicant.Previously_Studied_at_Monash__c;
        application.applicant.previousInstitutionCode = StudFirst_SubmitApplicationHelper.getInstitutionCode(applicant.Previous_Monash_Institution__c);
        application.applicant.previousPersonId = applicant.Previous_Monash_ID__c;
        application.applicant.birthDate = StudFirst_SubmitApplicationHelper.convertDate(applicant.Birthdate);
        application.applicant.emailAddress = applicant.Email;
        
        application.applicant.addressLine1 = applicant.MailingStreet;
        application.applicant.addressLine2 = applicant.MailingCity;
        application.applicant.addressLine3 = applicant.MailingState;
        application.applicant.addressLine4 = '';
        application.applicant.addressLine5 = '';
        application.applicant.postcode = '';
        application.applicant.osCode = '';

        if(applicant.MailingCountry != null) { 
            // Domestic          
            if(applicant.MailingCountry.equalsIgnoreCase(DOMESTIC_COUNTRY)) {
                application.applicant.addressType = DEFAULT_DOMESTIC_ADDRESS_TYPE;  
                application.applicant.postcode = applicant.MailingPostalCode;  
            } 
            // International
            if(!applicant.MailingCountry.equalsIgnoreCase(DOMESTIC_COUNTRY)) {
                application.applicant.addressLine4 = applicant.MailingCountry; 
                application.applicant.osCode = applicant.MailingPostalCode; 
                application.applicant.addressType = DEFAULT_INTERNATIONAL_ADDRESS_TYPE;  
            }   
        }           
        
        application.applicant.phone1 = applicant.HomePhone;
        application.applicant.phone2 = applicant.Phone;
        application.applicant.phone3 = applicant.MobilePhone;
        application.applicant.addressOtherDetail = applicant.Email;
        application.applicant.citizenshipType = app.Citizenship_Type_Filter__c;
        application.applicant.readOnlyAccessIndicator = false;
        application.applicant.prApplicationIndicator = false;
        application.applicant.password = 'CRM';

        application.applicant.chessNumber = '';
        application.applicant.birthCountryCode = '';
        application.applicant.citizenCountryCode = '';
        application.applicant.passportNumber = '';
        application.applicant.prApplicationDate = '';
        application.applicant.nameFormatCode = '';
        application.applicant.addressSubmissionDate = '';
        application.applicant.registrationPhoneNumber = '';
        application.applicant.applicantId = '';

        // Visa
        application.applicant.visa = new StudFirst_AdmissionApplicationRequest.Visa();
        application.applicant.visa.startDate = '';
        application.applicant.visa.endDate = '';
        application.applicant.visa.type = '';
        application.applicant.visa.visaNumber = '';

        // Secondary
        application.applicant.secondary = new List<StudFirst_AdmissionApplicationRequest.Secondary>();
        for(Contact_Qualification__c sec : secondary) {
            application.applicant.secondary.add(StudFirst_SubmitApplicationHelper.addSecondary(sec));    
        }
        

        // Tertiary
        application.applicant.tertiary = new List<StudFirst_AdmissionApplicationRequest.Tertiary>();
        for(Contact_Qualification__c ter : tertiary) {
            application.applicant.tertiary.add(StudFirst_SubmitApplicationHelper.addTertiary(ter));    
        }

        // Other Qualification
        application.applicant.otherQualification = new List<StudFirst_AdmissionApplicationRequest.OtherQualification>();
        for(Contact_Qualification__c oq : other) {
            application.applicant.otherQualification.add(StudFirst_SubmitApplicationHelper.addOther(oq));    
        }

        // Admission Test
        application.applicant.admissionTest = new List<StudFirst_AdmissionApplicationRequest.AdmissionTest>();
        for(Contact_Qualification__c eng : english) {
            application.applicant.admissionTest.add(StudFirst_SubmitApplicationHelper.addAdmissionTest(eng));    
        }

        // Work Experience
        application.applicant.workExperience = new List<StudFirst_AdmissionApplicationRequest.WorkExperience>();
        for(Work_Experience__c we : wrkExp) {
            //Ant Custodio, 21.Jul.2017 - only send work experience records that are either new or not on the previous application
            if (we.Reference_Count__c == 1) {
                application.applicant.workExperience.add(StudFirst_SubmitApplicationHelper.addWorkExperience(we));  
            }
            //END - Ant Custodio
        }

        // Course Application
        application.courseApplication = new StudFirst_AdmissionApplicationRequest.CourseApplication();
        application.courseApplication.applicationNumber = DEFAULT_APPLICATION_NUMBER;
        application.courseApplication.standardApplicationType = STANDARD_APPLICATION_TYPE;
        application.courseApplication.accessEquityComments = '';
        //application.courseApplication.requestForAdvancedStandingIndicator = app.Credit_Intention__c;
        
        if(app.Applying_for_Credit__c != null && app.Applying_for_Credit__c.equalsIgnoreCase('yes')) {
            application.courseApplication.requestForAdvancedStandingIndicator = true;
        } else {
            application.courseApplication.requestForAdvancedStandingIndicator = false;
        }
        
        application.courseApplication.requestForScholarshipIndicator = false;
        application.courseApplication.accessEquityCategories = new List<String>();
        if(applicant.Aboriginal_or_Torres_Strait_Islander__c != null && applicant.Aboriginal_or_Torres_Strait_Islander__c.equalsIgnoreCase('yes'))
            application.courseApplication.accessEquityCategories.add(INDIGENOUS);
        

        // Course Data
        application.courseApplication.courseData = new List<StudFirst_AdmissionApplicationRequest.CourseData>();
        for(Application_Course_Preference__c coursePreference : coursePreferences) {
            application.courseApplication.courseData.add(StudFirst_SubmitApplicationHelper.addCourseData(coursePreference));    
        }
        
        aar.admissionApplication.applications.add(application);
        
        return JSON.serializePretty(aar);
    
    }
}