/*******************************************************************************
* @author		Ant Custodio
* @date         12.Sept.2017     
* @description  Request structure for sending notification to MIX to retrieve the
					document
* @revision     
*******************************************************************************/
public class StudFirst_SendDocumentsServiceRequest {
	
	public String source;
    public String sourceTime;
    public String type;
    public List<AppDocument> events;
    
    public class AppDocument {
        public String agentPersonId;
        public String applicantId;
        public String externalApplicantId;
        public String contentType;
        public String filename;
        public String id;
        public String applicationDocumentId;
        public String contactDocumentId;
        public String documentType;
        public String comments;
    }
}