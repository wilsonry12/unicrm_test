/*******************************************************************************
* @author       Anterey Custodio
* @date         21.Nov.2016         
* @description  test class for BMSDeleteFlaggedCampaign
* @revision     
*******************************************************************************/
@isTest
public class BMSDeleteFlaggedCampaign_Test {
    /*******************************************************************************
    * @author       Anterey Custodio
    * @date         21.Nov.2016          
    * @description  Page Navigation: idle view - user just accessing the page
    * @revision     
    *******************************************************************************/
    static testMethod void test_ScheduleBatch() {
		//insert sample Contacts
        /*String KnownToMonashRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Known to Monash systems').getRecordTypeId();
        List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
        insert contactRecordList;

        List<Contact> changeContactRecordType = new List<Contact>();
        for (Contact contactRecord: [SELECT Id, RecordTypeId FROM Contact WHERE Id IN: contactRecordList]) {
            contactRecord.RecordTypeId = KnownToMonashRecTypeId;
            changeContactRecordType.add(contactRecord);
        }

        if (!changeContactRecordType.isEmpty()) {
            update changeContactRecordType;
        }
        
        Campaign campaignRecord = BMS_TestDataHelper.buildSampleCampaign();
        campaignRecord.Status = 'Delete';
        campaignRecord.RecordTypeId = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('BMS').getRecordTypeId();
        insert campaignRecord;
        
        Test.startTest();
            String CRON_EXP = '0 0 0 15 3 ? 2022';
            
            // Schedule the test job
            String jobId = System.schedule('BMSDeleteFlaggedCampaign',
                            CRON_EXP, 
                            new BMSDeleteFlaggedCampaignSC());
            
            // Get the information from the CronTrigger API object
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
            NextFireTime
            FROM CronTrigger WHERE id = :jobId];
            
            // Verify the expressions are the same
            System.assertEquals(CRON_EXP, 
            ct.CronExpression);
            
            // Verify the job has not run
            System.assertEquals(0, ct.TimesTriggered);
            
            // Verify the next time the job will run
            System.assertEquals('2022-03-15 00:00:00', 
            String.valueOf(ct.NextFireTime));
        Test.stopTest();
            
        // Now that the scheduled job has executed after Test.stopTest(),
        //   fetch the new merchandise that got added.
        List<CRM_Mass_Email__c> crmMassEmailToDelete = [SELECT Id FROM CRM_Mass_Email__c WHERE Status__c = 'Delete'];
        System.assert(crmMassEmailToDelete.isEmpty());
        */
    }
}