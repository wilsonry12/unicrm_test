/*******************************************************************************
* @author       Ant Custodio
* @date         7.Apr.2017         
* @description  controller class for ApplicationSuccess.cmp
* @revision     
*******************************************************************************/
public without sharing class ApplicationSuccessCC {
	/*******************************************************************************
    * @author       Ant Custodio
    * @date         7.Apr.2017         
    * @description  retrieves the application using the URL parameters
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static Application__c retrieveApplication (String applicationId) {
    	Application__c applicationRecord = [SELECT Id, Name, Status__c FROM Application__c WHERE Id =: applicationId];

        return applicationRecord;
    }
}