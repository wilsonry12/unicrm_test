/*******************************************************************************
* @author       Ryan Wilson
* @date         07.Apr.2017         
* @description  controller class for CoursePreference.cmp lightning component
* @revision     
*******************************************************************************/
public without sharing class CoursePreferenceCC {
    public static final User userRecord = [ SELECT  Id, ContactId  
                                            FROM    User 
                                            WHERE   Id =: userInfo.getUserId()];
    
    public static final Contact applicant = [SELECT Id, Residency_Status__c FROM Contact WHERE Id =:userRecord.ContactId];

    public static final String POST_GRADUATE_FILTER = 'OPG';

    @AuraEnabled
    public static List<String> retrieveAttendanceType() {
        List<String> options = new List<String>();

        Schema.DescribeFieldResult fieldResult = Course_Offering__c.Attendance_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getLabel());
        }       

        return options;
    }

    @AuraEnabled
    public static List<String> retrieveLocation(String searchStr) {
        List<String> options = new List<String>();

        String newSearchText = '%'+searchStr+'%';
        Map<String, String> mapLocations = new Map<String, String>();
        
        for(Course_Offering__c co : [SELECT Id, Location_Code__c, Course_Code__c, Course_Title__c  
                                    FROM Course_Offering__c 
                                    WHERE (Course_Code__c LIKE :newSearchText OR Course_Title__c LIKE :newSearchText) 
                                    ORDER BY Location_Code__c])
        {
            if(!mapLocations.containsKey(co.Location_Code__c)){
                mapLocations.put(co.Location_Code__c, co.Location_Code__c);
            }
        }       

        return mapLocations.values();
    }

    @AuraEnabled
    public static List<String> retrieveCommencementPeriod(String searchStr) {
        List<String> options = new List<String>();
        String newSearchText = '%'+searchStr+'%';
        Map<String, String> mapPeriods = new Map<String, String>();
        
        for(Course_Offering__c co : [SELECT Id, Admission_Calendar_Description__c, Course_Code__c, Course_Title__c  
                                    FROM Course_Offering__c 
                                    WHERE (Course_Code__c LIKE :newSearchText OR Course_Title__c LIKE :newSearchText) 
                                    ORDER BY Admission_Calendar_Description__c])
        {
            if(!mapPeriods.containsKey(co.Admission_Calendar_Description__c)){
                mapPeriods.put(co.Admission_Calendar_Description__c, co.Admission_Calendar_Description__c);
            }
        }       

        return mapPeriods.values();
    }

    private static List<Course_Offering__c> retrieveCourseOfferings(String searchStr, String strCode, String strAttType, String strLocation, String strPeriod, Boolean hasUnitSets) {
        //Search filters;
        Integer maxSize = 2000;
        String newSearchText = '';
        String searchByCode = '';
        String searchByLocation = '';
        String searchByAttendanceType = '';
        String searchByCommencePeriod = '';

        List<Application_Course_Preference__c> appCourses = new List<Application_Course_Preference__c>();
        Set<Id> appCCIds = new Set<Id>();
        appCourses = CoursePreferenceCC.retrieveAppCourses(strCode);
        for(Application_Course_Preference__c appCC: appCourses){
            appCCIds.add(appCC.Course_Offering__c);
        }

        String citizenFilter = applicant.Residency_Status__c;

        String queryString = 'SELECT Academic_Calendar_Type__c,Academic_CI_Sequence_Number__c,Academic_Year__c,Active__c,'
                            +'Admission_Calendar_Description__c,Admission_Calendar_Type__c,Admission_Category__c,Admission_CI_Sequence_Number__c,'
                            +'Attendance_Mode_Description__c,Attendance_Mode__c,Attendance_Type_Description__c,Attendance_Type__c,'
                            +'Citizenship_Type__c,Course_Code__c,Course_Title__c,Course_Version_Number__c,Course_Type__c,'
                            +'Course_Type_Group__c,Location_Description__c,Type_of_Place_Code__c,Type_of_Place_Description__c,'
                            +'Faculty__c,Id,Location_Code__c,Name,Unit_Set_Code__c,Unit_Set_Description__c,Unit_Set_Version__c,Unique_Code__c ' 
                            +'FROM Course_Offering__c '
                            +'WHERE Active__c = True AND Citizenship_Type__c =:citizenFilter';

        if(appCCIds.size() >0){
            queryString += ' AND Id NOT IN:appCCIds';
        }

        if(hasUnitSets){
            queryString += ' AND Unit_Set_Code__c != null';
        }

        if(searchStr != null && searchStr != ''){
            newSearchText = '%'+searchStr+'%';
            queryString += ' AND (Course_Code__c LIKE :newSearchText OR Course_Title__c LIKE :newSearchText)';
        }

        if(strAttType != null && strAttType != ''){
            queryString += ' AND Attendance_Type_Description__c =\''+strAttType+'\'';
        }

        if(strLocation != null && strLocation != ''){
            queryString += ' AND Location_Code__c =\''+strLocation+'\'';
        }

        if(strPeriod != null && strPeriod != ''){
            searchByCommencePeriod = '%'+strPeriod+'%';
            queryString += ' AND Admission_Calendar_Description__c LIKE :searchByCommencePeriod';
        }

        queryString += ' ORDER BY Course_Code__c, Attendance_Type_Description__c, Admission_Calendar_Description__c,Unit_Set_Description__c';

        if(!hasUnitSets){
            queryString += ' LIMIT '+maxSize;
        }

        List<Course_Offering__c> courses = new List<Course_Offering__c>();
        courses = Database.query(queryString);

        return courses;
    }

    @AuraEnabled
    public static Integer retrieveCoursesTotalRecord(String searchStr, String strCode, String strAttType, String strLocation, String strPeriod) {
        List<Course_Offering__c> courses = retrieveCourseOfferings(searchStr, strCode, strAttType, strLocation, strPeriod, false);

        Map<String, Course_Offering__c> mapCourse = new Map<String, Course_Offering__c>();
        for(Course_Offering__c course: courses){
            if(!mapCourse.containsKey(course.Unique_Code__c)){
                mapCourse.put(course.Unique_Code__c, course);
            }
        }

        return mapCourse.size();
    }

    @AuraEnabled
    public static List<Course_Offering__c> findCourses(String searchStr, String strCode, String strAttType, String strLocation, String strPeriod, Integer pagesize, Integer pagenumber) {
        //pagination variables
        Integer StartIndex, EndIndex ;
        StartIndex = (Integer.valueof(pagenumber) * Integer.valueof(pagesize)) + 1;
        EndIndex =  (Integer.valueof(pagesize) * (Integer.valueof(pagenumber) + 1));

        List<Course_Offering__c> courses = retrieveCourseOfferings(searchStr, strCode, strAttType, strLocation, strPeriod, false);

        Map<String, Course_Offering__c> mapCourse = new Map<String, Course_Offering__c>();
        for(Course_Offering__c course: courses){
            if(!mapCourse.containsKey(course.Unique_Code__c)){
                mapCourse.put(course.Unique_Code__c, course);
            }
        }

        List<Course_Offering__c> groupCourses = new List<Course_Offering__c>();
        if(courses != NULL && courses.size() > 0){
            Integer i = 1;
            for(Course_Offering__c temp : mapCourse.values()){
                if(i >= StartIndex && i <= EndIndex){
                    groupCourses.add(temp);
                    if(i==EndIndex){
                        break;
                    }
                }
                i++;
            }
        }

        System.debug('****COURSES: '+groupCourses);
        return groupCourses;
    }

    @AuraEnabled
    public static List<Course_Offering__c> retrieveCoursesWithUnitSets(String searchStr, String strCode, String strAttType, String strLocation, String strPeriod) {
        List<Course_Offering__c> courses = retrieveCourseOfferings(searchStr, strCode, strAttType, strLocation, strPeriod, true);

        System.debug('****COURSES WITH UNITS: '+courses);
        System.debug('****COURSES WITH UNITS: '+courses.size());
        return courses;
    }

    @AuraEnabled
    public static List<Application_Course_Preference__c> retrieveAppCourses(String appId) {
        List<Application_Course_Preference__c> appCourses = new List<Application_Course_Preference__c>();
        if(appId != null){
            appCourses = [SELECT Academic_Calendar_Type__c,Academic_CI_Sequence_Number__c,Admission_Calendar_Type__c,
                        Admission_CI_Sequence_Number__c,Applicant__c,Application__c,Attendance_Type__c,Attendance_Mode_Description__c,
                        Commencement_Period__c,Attendance_Type_Description__c,Attendance_Mode__c,Type_of_Place_Description__c,
                        Course_Code__c,Course_Offering__c,Course_Title__c,Course_Version_Number__c,Course__c,CreatedDate,Id,
                        Location_Code__c,Name,Outcome_Status__c,Preference_Number__c,Status__c,Unit_Set_Code__c, Unit_Set_Description__c,
                        Offer_Response_Status__c,Offer_Condition__c, Offer_Date__c, Offer_Response_Date__c   
                        FROM Application_Course_Preference__c 
                        WHERE Application__c =:appId 
                        ORDER BY Preference_Number__c ASC];
        }
        
        return appCourses;
    }

    @AuraEnabled
    public static List<Application_Course_Preference__c> reOrderAppCourses(String courseId, String direction) {
        List<Application_Course_Preference__c> appCourses = new List<Application_Course_Preference__c>();

        if(courseId != null){
            Application_Course_Preference__c appA = new Application_Course_Preference__c();
            appA = [SELECT Id, Preference_Number__c, Application__c FROM Application_Course_Preference__c WHERE Id=:courseId];
            Integer orderA = Integer.valueOf(appA.Preference_Number__c);
            Id appId = appA.Application__c;
            System.debug('****APP A: '+appA.Preference_Number__c);

            Application_Course_Preference__c appB = new Application_Course_Preference__c();
            Integer item = (direction == 'down')? orderA+1: orderA-1;
            appB = [SELECT Id, Preference_Number__c, Application__c FROM Application_Course_Preference__c WHERE Application__c=:appId AND Preference_Number__c=:String.valueof(item)];
            Integer orderB = Integer.valueOf(appB.Preference_Number__c);
            System.debug('****APP B: '+appB.Preference_Number__c);

            Integer updatedOrderA = (direction == 'down')? orderA+1: orderA-1;
            Integer updatedOrderB = (direction == 'down')? orderB-1: orderB+1;

            appA.Preference_Number__c = String.valueOf(updatedOrderA);
            appB.Preference_Number__c = String.valueOf(updatedOrderB);

            update appA;
            update appB;

            appCourses = CoursePreferenceCC.retrieveAppCourses(appId);
        }

        return appCourses;
    }

    @AuraEnabled
    public static List<Application_Course_Preference__c> addCourse(String jsonStr, String jsonStrUnitSets, String applicationId) {
        List<Application_Course_Preference__c> appCourses = new List<Application_Course_Preference__c>();

        //Deserialize to set of Ids
        Type idArrType = Type.forName('List<Id>');
        List<Id> courseIds = (List<Id>) JSON.deserialize(jsonStr, idArrType);
        System.debug('****SELECTED IDS: '+courseIds);

        Type idArrType_UnitSets = Type.forName('List<Id>');
        List<Id> courseIds_UnitSets = (List<Id>) JSON.deserialize(jsonStrUnitSets, idArrType_UnitSets);
        System.debug('****SELECTED IDS WITH UNIT SETS: '+courseIds_UnitSets);

        if(courseIds.size() >0 || courseIds_UnitSets.size() >0){
            List<Course_Offering__c> coursesToAdd = new List<Course_Offering__c>();
            
            Map<String,Course_Offering__c> selectedCourses_NoUnitSets = new Map<String,Course_Offering__c>();
            for(Course_Offering__c crs_NoSets: [SELECT Academic_Calendar_Type__c,Academic_CI_Sequence_Number__c,Academic_Year__c,Active__c,
                            Admission_Calendar_Description__c,Admission_Calendar_Type__c,Admission_Category__c,Admission_CI_Sequence_Number__c,
                            Attendance_Mode_Description__c,Attendance_Mode__c,Attendance_Type_Description__c,Attendance_Type__c,
                            Citizenship_Type__c,Commencement_Period__c,Course_Code__c,Course_Title__c,Course_Version_Number__c, Course_Type_Group__c,
                            Location_Description__c,Type_of_Place_Code__c,Type_of_Place_Description__c,Unique_Code__c,
                            Faculty__c,Id,Location_Code__c,Name,Unit_Set_Code__c,Unit_Set_Description__c,Unit_Set_Version__c 
                            FROM Course_Offering__c 
                            WHERE Id IN: courseIds]){

                selectedCourses_NoUnitSets.put(crs_NoSets.Unique_Code__c, crs_NoSets);

            }

            Map<String,Course_Offering__c> selectedCourses_WithUnitSets = new Map<String,Course_Offering__c>();
            for(Course_Offering__c crs_hasSets: [SELECT Academic_Calendar_Type__c,Academic_CI_Sequence_Number__c,Academic_Year__c,Active__c,
                            Admission_Calendar_Description__c,Admission_Calendar_Type__c,Admission_Category__c,Admission_CI_Sequence_Number__c,
                            Attendance_Mode_Description__c,Attendance_Mode__c,Attendance_Type_Description__c,Attendance_Type__c,
                            Citizenship_Type__c,Commencement_Period__c,Course_Code__c,Course_Title__c,Course_Version_Number__c, Course_Type_Group__c,
                            Location_Description__c,Type_of_Place_Code__c,Type_of_Place_Description__c,Unique_Code__c,
                            Faculty__c,Id,Location_Code__c,Name,Unit_Set_Code__c,Unit_Set_Description__c,Unit_Set_Version__c 
                            FROM Course_Offering__c 
                            WHERE Id IN: courseIds_UnitSets]){

                selectedCourses_WithUnitSets.put(crs_hasSets.Unique_Code__c, crs_hasSets);

            }

            //remove courses with no unit sets
            if(selectedCourses_WithUnitSets.size() >0){
                for(String crsKey: selectedCourses_WithUnitSets.keySet()){
                    if(selectedCourses_NoUnitSets.containsKey(crsKey)){
                        selectedCourses_NoUnitSets.remove(crsKey);
                    }
                }

                for(String key: selectedCourses_NoUnitSets.keySet()){
                    Course_Offering__c course = selectedCourses_NoUnitSets.get(key);
                    insertSelectedCourses(course, applicationId, false);
                }

                for(String key: selectedCourses_WithUnitSets.keySet()){
                    Course_Offering__c course = selectedCourses_WithUnitSets.get(key);
                    insertSelectedCourses(course, applicationId, true);
                }

            }

            else{
                for(String key: selectedCourses_NoUnitSets.keySet()){
                    Course_Offering__c course = selectedCourses_NoUnitSets.get(key);
                    insertSelectedCourses(course, applicationId, false);
                }
            }
            
            
            
        }
         
        appCourses = CoursePreferenceCC.retrieveAppCourses(applicationId);
        return appCourses;
    }

    private static void insertSelectedCourses(Course_Offering__c selectedCourse, String applicationId, Boolean hasUnitSet) {
        List<Application_Course_Preference__c> appCourses = new List<Application_Course_Preference__c>();

        Integer orderNumber = 1;
        List<Application_Course_Preference__c> currentAppCourses = new List<Application_Course_Preference__c>();
        currentAppCourses = CoursePreferenceCC.retrieveAppCourses(applicationId);
        if(currentAppCourses.size() >0){
            Integer listSize = currentAppCourses.size();
            orderNumber = Integer.valueOf(currentAppCourses[listSize-1].Preference_Number__c)+1;
        }

        Application_Course_Preference__c appCourse = new Application_Course_Preference__c();
        appCourse.Application__c = applicationId;
        appCourse.Applicant__c = userRecord.ContactId;
        appCourse.Status__c = 'Open';
        appCourse.Preference_Number__c = String.valueOf(orderNumber);
        appCourse.Academic_Calendar_Type__c = selectedCourse.Academic_Calendar_Type__c;
        appCourse.Academic_CI_Sequence_Number__c = selectedCourse.Academic_CI_Sequence_Number__c;
        appCourse.Admission_Calendar_Type__c = selectedCourse.Admission_Calendar_Type__c;
        appCourse.Admission_CI_Sequence_Number__c = selectedCourse.Admission_CI_Sequence_Number__c;
        appCourse.Attendance_Type__c = selectedCourse.Attendance_Type__c;
        appCourse.Attendance_Type_Description__c = selectedCourse.Attendance_Type_Description__c;
        appCourse.Commencement_Period__c = selectedCourse.Admission_Calendar_Description__c;
        appCourse.Course_Code__c = selectedCourse.Course_Code__c;
        appCourse.Course_Title__c = selectedCourse.Course_Title__c;
        appCourse.Course_Version_Number__c = selectedCourse.Course_Version_Number__c;
        appCourse.Location_Code__c = selectedCourse.Location_Code__c;
        appCourse.Location_Description__c = selectedCourse.Location_Description__c;
        appCourse.Attendance_Mode__c = selectedCourse.Attendance_Mode__c;
        appCourse.Attendance_Mode_Description__c = selectedCourse.Attendance_Mode_Description__c;
        appCourse.Type_of_Place_Code__c = selectedCourse.Type_of_Place_Code__c;
        appCourse.Type_of_Place_Description__c = selectedCourse.Type_of_Place_Description__c;

        appCourse.Course_Offering__c = selectedCourse.Id;
        appCourse.Unit_Set_Code__c = (hasUnitSet)? selectedCourse.Unit_Set_Code__c: '';
        appCourse.Unit_Set_Description__c = (hasUnitSet)? selectedCourse.Unit_Set_Description__c: '';
        appCourse.Unit_Set_Version__c = (hasUnitSet)? selectedCourse.Unit_Set_Version__c: 0.0;
        //appCourses.add(appCourse);
        //orderNumber++;
        insert appCourse;
    }

    @AuraEnabled
    public static String deleteSelectedCourse (String deleteThisCourse, String applicationId) {
        String forDelete = '';
        if (deleteThisCourse != null && deleteThisCourse != '') {
            List<Application_Course_Preference__c> courseToDelete = [ SELECT Id FROM Application_Course_Preference__c WHERE Id =: deleteThisCourse LIMIT 1];
            if (!courseToDelete.isEmpty()) {
                delete courseToDelete;
            }
        }

        //Arrange the courses if 1 is deleted
        List<Application_Course_Preference__c> appCourses = new List<Application_Course_Preference__c>();
        if(applicationId != null){
            appCourses = [SELECT Id, Preference_Number__c  
                        FROM Application_Course_Preference__c 
                        WHERE Application__c =:applicationId 
                        ORDER BY Preference_Number__c ASC];
        }

        if(appCourses.size() >0){
            Integer orderNumber = 1;
            for(Application_Course_Preference__c appcrs: appCourses){
                appcrs.Preference_Number__c = String.valueOf(orderNumber);
                orderNumber++;
            }
            update appCourses;
        }
         
        return forDelete;
    }

}