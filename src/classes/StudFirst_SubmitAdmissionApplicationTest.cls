@isTest
public class StudFirst_SubmitAdmissionApplicationTest
{

    @testSetup static void setup() {
        IntegrationSettings_SubmitApplication__c is = new IntegrationSettings_SubmitApplication__c();
        is.Base_Url__c = 'https://mix-dev.monash.edu';
        is.Path__c = '/v1/admissions/applications';
        is.Method__c = 'POST';
        is.Header_ClientId__c = 'c8411f0ca73a4ae6a0c7081a1336630f';
        is.Header_ClientSecret__c = 'cd2d5b9e26fa4999A33FF6A7F12D65AD';
        is.Header_ContentType__c = 'application/json';
        is.Mock_Endpoint_Applications__c = 'http://monash.getsandbox.com/v1/admissions/applications';
        is.Enable_Mock__c = false;
        is.Timeout__c = '12000';

        insert is;

    }
    
    static testMethod void sendApplication() {
        Test.startTest();

        String applicationId = createCompletedApplication();
        Test.setMock(HttpCalloutMock.class, new StudFirst_MockHttpResponseGenerator());
        StudFirst_SubmitAdmissionApplication.send(applicationId);

        Test.stopTest();

        Application__c application = StudFirst_SubmitApplicationHelper.getApplicationById(applicationId);
        System.assertEquals(application.Status__c == 'Submitted', true);
        System.assertEquals(application.Callista_Application_Id__c != null, true);
        System.assertEquals(application.Callista_Applicant_Id__c != null, true);
        
    }

    private static String createCompletedApplication() {
        // Create applicant
        Contact applicant = StudFirst_TestHelper.createApplicant();
        insert applicant;

        // Create application
        Application__c application = StudFirst_TestHelper.createApplication(applicant);
        insert application;  

        // Create Qualifications
        Qualification__c qual1 = StudFirst_TestHelper.createQualification('Secondary');
        insert qual1;

        Qualification__c qual2 = StudFirst_TestHelper.createQualification('Tertiary');
        insert qual2;

        Qualification__c qual3 = StudFirst_TestHelper.createQualification('Other');
        insert qual3;

        Qualification__c qual4 = StudFirst_TestHelper.createQualification('English');
        insert qual4;

        // Create Contact Qualifications
        Contact_Qualification__c contactQualification1 = StudFirst_TestHelper.createContactQualification(applicant, qual1, 'Secondary Education');
        insert contactQualification1;

        Contact_Qualification__c contactQualification2 = StudFirst_TestHelper.createContactQualification(applicant, qual2, 'Tertiary Education');
        insert contactQualification2;

        Contact_Qualification__c contactQualification3 = StudFirst_TestHelper.createContactQualification(applicant, qual3, 'Other Qualification');
        insert contactQualification3;

        Contact_Qualification__c contactQualification4 = StudFirst_TestHelper.createContactQualification(applicant, qual4, 'English Test');
        insert contactQualification4;

        // Create Work Experience
        Work_Experience__c workExperience = StudFirst_TestHelper.createWorkExperience(applicant);
        insert workExperience;

        // Create Course
        Course__c course = StudFirst_TestHelper.createCourse();
        insert course;

        // Creare Course Offering
        Course_Offering__c courseOffering = StudFirst_TestHelper.createCourseOffering(course);
        insert courseOffering;

        // Create Course Preferences
        Application_Course_Preference__c coursePreference = StudFirst_TestHelper.createCoursePreferences(application, applicant, courseOffering, course);
        insert coursePreference;

        return application.Id;
    }

}