/*
    AUTHOR : Vineeth Batreddy
    DESCRIPTION : This class is used to initiate the submission of application documents
*/
global class StudFirst_SubmitApplicationDocuments {

    private static final Integer SLEEP_TIME = 1001;    
    private static final String AGENT_PERSON_ID = Label.CALLISTA_AGENT_PERSON_ID;
    
    WebService static void send(String applicationId) {
        System.debug('!@#$% Invoking document submission');

        List<String> applicationIds = new List<String>();
        
        if(String.isNotBlank(applicationId)) {
            applicationIds.add(applicationId);
            sendDocuments(applicationIds);    
        }
            
    }

    @InvocableMethod
    public static void sendDocuments(List<String> applicationIds) {

        String applicationId = applicationIds.get(0);
        Application__c application = StudFirst_SubmitApplicationHelper.getApplicationById(applicationId);
        Map<String, Application_Document_Provided__c> allApplicationDocuments = StudFirst_SubmitApplicationHelper.getDocumentsForApplication(applicationId);
        List<String> contactDocumentIds = new List<String>();
        
        //Ant Custodio, 21.Jul.2017 - added checking for documents that were already submitted to Callista to prevent duplicates
        Map<String, Application_Document_Provided__c> applicationDocuments = new Map<String, Application_Document_Provided__c>();
        for (Application_Document_Provided__c docProvided: allApplicationDocuments.values()) {
            if (!docProvided.Is_Submitted__c) {
                applicationDocuments.put(docProvided.Contact_Document__c, docProvided);
            }
        }
        //END - Ant Custodio

        for(Integer a=0; a<applicationDocuments.size(); a++) {
            if(!applicationDocuments.values()[a].Document_submitted_successfully__c)
                contactDocumentIds.add(applicationDocuments.values()[a].Contact_Document__c);
        }
        Map<String, Attachment> attachments = StudFirst_SubmitApplicationHelper.getAttachments(contactDocumentIds);
        
        System.debug('!@#$% applicationDocuments : ' + applicationDocuments);
        System.debug('!@#$% attachments : ' + attachments);
        
        //Added check to only send request if Callista_Applicant_Id__c is not blank R.Wilson 14/07/2017
        if(String.isNotBlank(application.Callista_Applicant_Id__c)) {
           if(applicationDocuments.size()>0 && attachments.size()>0) {
                Map<String, String> requests = generateRequests(applicationDocuments, attachments, application.Applicant__c);
                Set<String> appDocIds = requests.keySet();

                for(String appDocId : appDocIds) {
                    sendRequest(requests.get(appDocId), application.Callista_Applicant_Id__c, appDocId);    
                }
            } 
        }
    }
    
    private static Map<String, String> generateRequests(Map<String, Application_Document_Provided__c> applicationDocuments, Map<String, Attachment> attachments, String applicantId) {
        Map<String, String> requests = new Map<String, String>();
        
        for(Integer i=0; i<applicationDocuments.size(); i++) {
            Application_Document_Provided__c currentDoc = new Application_Document_Provided__c();
            currentDoc = applicationDocuments.values()[i];
            
            StudFirst_DocumentDTO doc = new StudFirst_DocumentDTO();
            // External Applicant Id
            doc.externalApplicantId = applicantId;
            // Agent
            doc.Agent = new StudFirst_DocumentDTO.Agent();
            doc.Agent.agentPersonId = AGENT_PERSON_ID;
            // Document
            doc.document = new StudFirst_DocumentDTO.Document();
            doc.document.type = currentDoc.Contact_Document__r.Document_Type__c;
            doc.document.filename = currentDoc.Contact_Document__r.Filename__c;
            
            // Attachment
            Attachment currentAttachment = attachments.get(currentDoc.Contact_Document__c);
            if(currentAttachment != null) {
                doc.document.contentType = currentAttachment.ContentType;
                doc.document.comments = currentAttachment.Description;

                Blob documentBlob = currentAttachment.Body;
                doc.document.binary = EncodingUtil.base64Encode(documentBlob);    
            
                requests.put(currentDoc.Id, JSON.serialize(doc));
            }    
        }
        
        return requests;
    }
    
    @future(callout=true)
    private static void sendRequest(String req, String callistaApplicantId, String applicationDocumentId) {
        
        
        // Prepare request
        HttpRequest request = new HttpRequest();

        // Retrieve integration settings
        IntegrationSettings_SubmitApplication__c is = IntegrationSettings_SubmitApplication__c.getInstance();
        
        // Endpoint
        String endpoint = '';
        if(is.Enable_Mock__c) {
            endpoint = is.Mock_Endpoint_Documents__c;
        } else {
            endpoint = is.Base_Url__c + is.Documents_Path__c;    
        }
        endpoint = endpoint + '/'+callistaApplicantId+'/documents';
        request.setEndpoint(endpoint);
        
        // HTTP method
        request.setMethod(is.Method__c);    
        
        // HTTP request
        request.setBody(req);
        
        // HTTP timeout
        request.setTimeout(Integer.valueOf(is.Timeout__c));
        
        // HTTP headers
        request.setHeader('Content-Type', is.Header_ContentType__c);
        request.setHeader('client_id', is.Header_ClientId__c);
        request.setHeader('client_secret', is.Header_ClientSecret__c);
        
        System.debug('!@#$% HTTP Request : ' + request);
        System.debug('!@#$% HTTP Request Body : ' + request.getBody());
        System.debug('!@#$% Zzzzzzzzz Sleeping for ' + SLEEP_TIME + ' milliseconds');
        
        // Used to make the transaction sleep for a set milliseconds - INTERIM SOLUTION DUE TO CONSTRAINTS IN CALLISTA
        StudFirst_SubmitApplicationHelper.sleep(SLEEP_TIME);


        // Send Request
        Http httpClient = new Http();
        HttpResponse resp = httpClient.send(request);
        System.debug('!@#$% HTTP Response : ' + resp.getBody());

        // Update Document with request & response messages
        Integer respStatusCode = 1;
        String respStatus = '';
        String respBody = '';
        if(resp != null) {
            respStatusCode = resp.getStatusCode();
            respStatus = resp.getStatus();
            respBody = resp.getBody();
        }

        Application_Document_Provided__c adp = [SELECT Id, Document_submitted_successfully__c, API_Request_Message__c, API_Response_Message__c FROM Application_Document_Provided__c WHERE Id = :applicationDocumentId];    
            
        //adp.API_Request_Message__c = req;
        adp.API_Response_Message__c = 'HTTP Status Code : ' + respStatusCode + '\n HTTP Status : ' + respStatus + '\n Response : \n--------------\n\n' + respBody;
    
        if(resp != null && resp.getStatusCode() == 201) {
            adp.Document_submitted_successfully__c = true;   
        }
        
        update adp;
        
    }
}