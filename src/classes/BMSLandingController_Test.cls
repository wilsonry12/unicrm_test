@isTest
private class BMSLandingController_Test
{
        @isTest
        static void BMSHomeTest()
        {
                test.startTest();
                PageReference pageRef = Page.BMSHome;
                Test.setCurrentPage(pageRef);

                BMSLandingController bmsctrl = new BMSLandingController();
                bmsctrl.gotoAddRecepients();
                bmsctrl.currentUserProfile = 'BMS User';
                bmsctrl.redirectUser();
                test.stopTest();
        }

    @isTest
    static void BMSOptionsTest()
    {
        test.startTest();
                PageReference pageRef = Page.BMSOptions;
                Test.setCurrentPage(pageRef);

                apexpages.currentpage().getparameters().put('type','Email');

                BMSOptionsController bmsctrl = new BMSOptionsController();
                bmsctrl.currentUserProfile = 'BMS User';
                Boolean bParam = bmsctrl.showBackButton;
                String strParam = bmsctrl.toggleIcon;
                bmsctrl.optionParam = bmsctrl.BMS_PERSON_ID;
                bmsctrl.gotoSelectedOption();
                bmsctrl.optionParam = bmsctrl.BMS_EMAIL_ADD;
                bmsctrl.gotoSelectedOption();
                bmsctrl.optionParam = bmsctrl.BMS_SEARCH;
                bmsctrl.gotoSelectedOption();
                bmsctrl.gotoPrevious();
                test.stopTest();
    }
}