/*******************************************************************************
* @author		Ant Custodio
* @date         23.Jun.2017
* @description  test class for StudFirst_ApplicationPBC
* @revision     
*******************************************************************************/
@isTest
public with sharing class StudFirst_ApplicationPBC_Test {
	private static Application__c appRecord;
	private static Course_Offering__c courseOfferingRec;
	private static Assessor_Role__c arRecord;
	private static Assessor_Group__c agRecord;
	private static Assessment_Plan__c apRecord;
	private static Assessment_Selection_Rule__c asrRecord;
	private static final String CP_PLAN_RECTYPEID = [	SELECT 	Id
														FROM 	RecordType
														WHERE 	DeveloperName = 'CP_Plan' ].Id;
	/*******************************************************************************
	* @author       Ant Custodio
	* @date         29.Jun.2017
	* @description  test the createAssessmentPlansForEachCoursePrefs method.
					EXPECTED: All Application Course Preferences should have 
					an Assessment Plan
	* @revision     
	*******************************************************************************/
	static testMethod void test_createAssessmentPlansForEachCoursePrefs() {
		//create a community user
		User communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create required records
		createSampleData(communityUserRec.ContactId, communityUserRec.Id);

		//create assessor base profile user
		String profileId = [SELECT Id FROM Profile WHERE Name = 'Assessor Base Profile' LIMIT 1].Id;
		Integer rnd = Integer.valueOf(Math.random()*9999);
        User userRec = new User( Alias = String.valueOf(rnd)+ 'asu', 
                            Email = String.valueOf(rnd) +'testassessor@monash.edu', 
                            EmailEncodingKey = 'UTF-8', 
                            LastName = 'Assessor', 
                            LanguageLocaleKey = 'en_US', 
                            LocaleSidKey = 'en_GB', 
                            ProfileId = profileId,
                            TimeZoneSidKey = 'Australia/Sydney', 
                            UserName = String.valueOf(rnd) +'testassessor@monash.edu');
        insert userRec;

        //assessor creates a new assessor role
		System.runAs(userRec) {
			test.startTest();
				Group grpRecord = StudFirst_TestHelper.createGroupWithMembers(arRecord.Name, true);
				//this will run the batch for the assessment selection rule
				asrRecord = StudFirst_TestHelper.createAssessmentSelectionRule(agRecord.Id, apRecord.Id);
				insert asrRecord;
			test.stopTest();

			//assert that the course offering selected is part of an assessment selection rule
			System.assertEquals(asrRecord.Id, 
								[	SELECT	Assessment_Plan_Selection_Rule__c 
									FROM 	Course_Offering__c 
									WHERE 	Id =: courseOfferingRec.Id].Assessment_Plan_Selection_Rule__c,
								'Batch did not execute');
			//only run if there is a Domestic Applicant Profile User
			if (communityUserRec != null) {
				System.runAs(communityUserRec) {
					appRecord.Status__c = 'Submitted';
					update appRecord;
				}
			}
		}
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         29.Jun.2017
	* @description  Scenario 2, remove the creation of group and queue
	* @revision     
	*******************************************************************************/
	static testMethod void test_createAssessmentPlansForEachCoursePrefs_NoQueue() {
		//create a community user
		User communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create required records
		createSampleData(communityUserRec.ContactId, communityUserRec.Id);

		//create assessor base profile user
		String profileId = [SELECT Id FROM Profile WHERE Name = 'Assessor Base Profile' LIMIT 1].Id;
		Integer rnd = Integer.valueOf(Math.random()*9999);
        User userRec = new User( Alias = String.valueOf(rnd)+ 'asu', 
                            Email = String.valueOf(rnd) +'testassessor@monash.edu', 
                            EmailEncodingKey = 'UTF-8', 
                            LastName = 'Assessor', 
                            LanguageLocaleKey = 'en_US', 
                            LocaleSidKey = 'en_GB', 
                            ProfileId = profileId,
                            TimeZoneSidKey = 'Australia/Sydney', 
                            UserName = String.valueOf(rnd) +'testassessor@monash.edu');
        insert userRec;

        //assessor creates a new assessor role
		System.runAs(userRec) {
			test.startTest();
				//this will run the batch for the assessment selection rule
				asrRecord = StudFirst_TestHelper.createAssessmentSelectionRule(agRecord.Id, apRecord.Id);
				insert asrRecord;
			test.stopTest();

			//assert that the course offering selected is part of an assessment selection rule
			System.assertEquals(asrRecord.Id, 
								[	SELECT	Assessment_Plan_Selection_Rule__c 
									FROM 	Course_Offering__c 
									WHERE 	Id =: courseOfferingRec.Id].Assessment_Plan_Selection_Rule__c,
								'Batch did not execute');
			//only run if there is a Domestic Applicant Profile User
			if (communityUserRec != null) {
				System.runAs(communityUserRec) {
					appRecord.Status__c = 'Submitted';
					update appRecord;
				}
			}
		}
	}
	
	/*******************************************************************************
	* @author       Ant Custodio
	* @date         29.Jun.2017
	* @description  Scenario 3, force error - has group but no queue created
	* @revision     
	*******************************************************************************/
	static testMethod void test_createAssessmentPlansForEachCoursePrefs_ForceError() {
		///create a community user
		User communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create required records
		createSampleData(communityUserRec.ContactId, communityUserRec.Id);

		//create assessor base profile user
		String profileId = [SELECT Id FROM Profile WHERE Name = 'Assessor Base Profile' LIMIT 1].Id;
		Integer rnd = Integer.valueOf(Math.random()*9999);
        User userRec = new User( Alias = String.valueOf(rnd)+ 'asu', 
                            Email = String.valueOf(rnd) +'testassessor@monash.edu', 
                            EmailEncodingKey = 'UTF-8', 
                            LastName = 'Assessor', 
                            LanguageLocaleKey = 'en_US', 
                            LocaleSidKey = 'en_GB', 
                            ProfileId = profileId,
                            TimeZoneSidKey = 'Australia/Sydney', 
                            UserName = String.valueOf(rnd) +'testassessor@monash.edu');
        insert userRec;

        //assessor creates a new assessor role
		System.runAs(userRec) {
			test.startTest();
				Group grpRecord = StudFirst_TestHelper.createGroupWithMembers(arRecord.Name, false);
				//this will run the batch for the assessment selection rule
				asrRecord = StudFirst_TestHelper.createAssessmentSelectionRule(agRecord.Id, apRecord.Id);
				insert asrRecord;
			test.stopTest();

			//assert that the course offering selected is part of an assessment selection rule
			System.assertEquals(asrRecord.Id, 
								[	SELECT	Assessment_Plan_Selection_Rule__c 
									FROM 	Course_Offering__c 
									WHERE 	Id =: courseOfferingRec.Id].Assessment_Plan_Selection_Rule__c,
								'Batch did not execute');
			//only run if there is a Domestic Applicant Profile User
			if (communityUserRec != null) {
				System.runAs(communityUserRec) {
					appRecord.Status__c = 'Submitted';
					update appRecord;
				}
			}
		}
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         29.Jun.2017
	* @description  creates all sample data required
	* @revision     
	*******************************************************************************/
	static void createSampleData (String contactId, String ownerId) {
		//create a new draft application
		appRecord = StudFirst_TestHelper.createApplication(contactId);
		appRecord.OwnerId = ownerId;
		insert appRecord;

		//Tertiary institution & qualification
		Institution__c tertInsRec = StudFirst_TestHelper.createTertiaryInstitution();
		tertInsRec.OwnerId = ownerId;
		insert tertInsRec;

		Contact_Qualification__c conQualRec = new Contact_Qualification__c();
		conQualRec.Country__c = 'Australia';
		conQualRec.First_Year_Enrolled__c = String.valueOf(date.today().year()-4);
		conQualRec.Last_Year_Enrolled__c = String.valueOf(date.today().year());
		conQualRec.Other_Qualification__c = 'Certificate III';
		conQualRec.Institution_Code__c = tertInsRec.Institution_Code__c;
		conQualRec.Institution_Name__c = tertInsRec.Institution_Name__c;
		conQualRec.Status__c = 'CURRENTLY STUDYING';
		conQualRec.RecordTypeId = Schema.SObjectType.Contact_Qualification__c.getRecordTypeInfosByName().get('Tertiary Education').getRecordTypeId();
		conQualRec.Contact__c = contactId;
		insert conQualRec;

		Work_Experience__c workExpRecord = StudFirst_TestHelper.populateWorkExperienceRecord(new Work_Experience__c());
		workExpRecord.Contact__c = contactId;
		insert workExpRecord;

		Course__c courseRec = StudFirst_TestHelper.createCourse();
		insert courseRec;

		List<Course_Offering__c> coList = new List<Course_Offering__c>();
		courseOfferingRec = StudFirst_TestHelper.createCourseOffering(courseRec);
		courseOfferingRec.Faculty__c = 'Faculty of Law';
		coList.add(courseOfferingRec);

		coList.add(courseOfferingRec.clone());
		insert coList;

		agRecord = StudFirst_TestHelper.createAssessorGroup();
		insert agRecord;

		arRecord = StudFirst_TestHelper.createAssessorRole(agRecord.Id);
		insert arRecord;

		apRecord = StudFirst_TestHelper.createAssessmentPlan();
		apRecord.RecordTypeId = [	SELECT	Id 
									FROM 	RecordType 
									WHERE 	DeveloperName = 'Template' ].Id;
		insert apRecord;

		Assessment_Plan_Task__c aptRecord = StudFirst_TestHelper.createAssessmentPlanTask(apRecord.Id, arRecord.Id);
		insert aptRecord;

		Contact contactRec = [	SELECT 	Id 
								FROM 	Contact 
								WHERE 	Id =: contactId ];

		List<Application_Course_Preference__c> cpList = new List<Application_Course_Preference__c>();
		Application_Course_Preference__c acpRecord = StudFirst_TestHelper.createCoursePreferences(appRecord, contactRec, coList[0], courseRec);
		acpRecord.Course_Offering__c = coList[0].Id;
		cpList.add(acpRecord);
		Application_Course_Preference__c acpRecord2 = StudFirst_TestHelper.createCoursePreferences(appRecord, contactRec, coList[1], courseRec);
		acpRecord2.Course_Offering__c = coList[1].Id;
		cpList.add(acpRecord2);
		insert cpList;

		Document_Requested__c docRequest = StudFirst_TestHelper.createDocRequestRecord(contactRec.Id, appRecord.Id, cpList[0].Id);
		insert docRequest;

		Application_Qualification_Provided__c aqpRecord = StudFirst_TestHelper.createAppQualProvided(appRecord.Id, conQualRec.Id);
		insert aqpRecord;

		Application_Work_Experience_Provided__c aweRecord = StudFirst_TestHelper.createAppWorkExpProvided(appRecord.Id, workExpRecord.Id);
		insert aweRecord;
	}
}