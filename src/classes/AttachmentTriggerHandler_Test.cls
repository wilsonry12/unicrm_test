@istest
public class AttachmentTriggerHandler_Test {
    
    static void setupdata() {
        
        Case_RecordType_CS__c customSettingRecord = new Case_RecordType_CS__c(Has_Attachment_Trigger__c = true,Recordtype_Name__c = 'HR',name='testCS1');
        insert customSettingRecord;
        
        Map<String,Schema.RecordTypeInfo> recordTypeInfoByName = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName();
        Id recTypeID = recordTypeInfoByName.get('HR').getRecordTypeId();
        list<case> caseRec = TestHelper.webToCaseForms(1);
        list<case> caseList = new list<case>();
        
        for(case cas:caseRec) {
            cas.Origin = 'Web';
            cas.recordtypeId = recTypeID;
            caseList.add(cas);
        }
        insert caseList;
    }
    
    @istest
    static void afterInsertTest(){
        
        setupdata();
        list<case> caseList = [select id,has_attachment__c from case];
        list<attachment> attachmentList = TestHelper.createAttachments(1, caseList[0].id);
        insert attachmentList;
        system.assertEquals(1, attachmentList.size());
        system.assertEquals([select has_attachment__c from case where id =:caseList[0].id].has_attachment__c, true);
        
    }
    
    @istest
    static void afterdeleteTest(){
        
        setupdata(); 
        list<case> caseList = [select id,has_attachment__c from case];
        list<attachment> attachmentList = TestHelper.createAttachments(1, caseList[0].id);
        insert attachmentList;
        system.assertEquals(1, attachmentList.size());
        system.assertEquals([select has_attachment__c from case where id =:caseList[0].id].has_attachment__c, true);
        delete attachmentList;
        system.assertEquals([select has_attachment__c from case where id =:caseList[0].id].has_attachment__c, false);        
    }
    
}