/*******************************************************************************
* @author		Ant Custodio
* @date         19.Sept.2017     
* @description  contains all the generic methods by Student First (AA) 
					for Email Message object
* @revision     
*******************************************************************************/
global class  StudFirst_GlobalEmailServices {
	/*******************************************************************************
	* @author		Ant Custodio
	* @date         19.Sept.2017     
	* @description  Method called by button on Email Message to resend the same email
						to the same recipient
	* @revision     16.Oct.2017 - Ant Custodio removed cloning of emailmessages 
						and attachments and used setTargetObjectId instead
	*******************************************************************************/
	WebService static void resendEmail(String emlMsgId) {
		if (emlMsgId != '') {
			List<Attachment> attList = [	SELECT 	Id, Body,
													ContentType, Description,
													Name, OwnerId,
													ParentId
											FROM 	Attachment
											WHERE 	ParentId =: emlMsgId	];

			List<EmailMessage> emlMsgRecList = [SELECT 	Id, BccAddress, CcAddress,
														FromAddress, FromName, 
														Headers, HtmlBody,
														Incoming, ParentId, Status,
														Subject, TextBody, ToAddress
												FROM 	EmailMessage
												WHERE 	Id =: emlMsgId
												LIMIT 	1	];

			if (!emlMsgRecList.isEmpty()) {
				try {
					// Modification by Majid Reisi Dehkordi 12/10/207
					// Read the emails from the App_AdmissionEmails (which is a custom label)
					// Seperate them by a comma and allocate those admissions emalis to 
					// admissionEmails field
					Set<String> admissionEmails = new Set<String>(((String)System.Label.App_AdmissionEmails).split(','));
					// End of Modification by Majid Reisi Dehkordi 12/10/2017

					String owaStr = '';
					//get the From Address from the Org wide email address
					for(OrgWideEmailAddress owa : [	SELECT	Id, Address, DisplayName 
													FROM	OrgWideEmailAddress 
													WHERE 	Address IN: admissionEmails	]){
						if (owa.Address == emlMsgRecList[0].FromAddress) {
		                	owaStr = owa.Id;
		                }
		            }
					//build the email message
					Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
					
					if (emlMsgRecList[0].CcAddress != null) {
			        	semail.setCcAddresses(splitEmailsBySemiColon(emlMsgRecList[0].CcAddress));
			        }
			        if (emlMsgRecList[0].BccAddress != null) {
			        	semail.setBccAddresses(splitEmailsBySemiColon(emlMsgRecList[0].BccAddress));
			        }
			        semail.setBccSender(false);
			        semail.setSaveAsActivity(true);
			        semail.setSubject(emlMsgRecList[0].Subject);
			        semail.setPlainTextBody(emlMsgRecList[0].TextBody);
			        semail.setHtmlBody(emlMsgRecList[0].HtmlBody);
			        semail.setWhatId(emlMsgRecList[0].ParentId);
			        if (owaStr != '') {
			        	semail.setOrgWideEmailAddressId(owaStr);
			        }
			        
			        Messaging.EmailFileAttachment[] files = new Messaging.EmailFileAttachment[]{};
	        		for(attachment attRec : attList) {
						Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
						efa.setBody(attRec.Body);
						efa.setContentType(attRec.ContentType);
						efa.setFileName(attRec.Name);
						files.add(efa);
					}
		            
					if(!files.isEmpty()) {
						semail.setFileAttachments(files);
					}
					
					//ACUSTODIO, 26.Sept.2017 - Sends the Email to the Agent Portal as well
					List<Case> caseRecList = [	SELECT 	Id,
														Notify_Agent__c,
														Application_Course_Preference__r.Applicant__c,
														//Application_Course_Preference__r.Application__r.Agent__c,
														//Application_Course_Preference__r.Application__r.Agent__r.Callista_Org_Unit_Code__c,
														Application_Course_Preference__r.Application__r.Agent_Org_Unit_Code__c,
														Application_Course_Preference__r.Application__r.Callista_Applicant_Id__c,
														Application_Course_Preference__r.Application__r.Callista_Student_Id__c,
														Subject,
														Description
												FROM 	Case 
												WHERE 	Id =: emlMsgRecList[0].ParentId
												LIMIT	1	];
					
					if (!caseRecList.isEmpty()) {
						if (caseRecList[0].Notify_Agent__c) {
							//send the email to Agent Portal if notify agent flag is true
							sendToAgentPortal(caseRecList);
						}

						String applicantId = caseRecList[0].Application_Course_Preference__r.Applicant__c;
						//send an email if applicant (from address) is not blank
						if(!Test.isRunningTest() && applicantId != null) {
							//set the to address to the related contact
							semail.setTargetObjectId(applicantId);
							Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ semail });
						}
					}

				} catch (Exception ex) {
					Exlog.add('Apex Class Line Number: '+ex.getLineNumber(),'EmailMessageServices','resendEmail',ex);
				}
			}
		}
	}

	/*******************************************************************************
	* @author		Ant Custodio
	* @date         19.Sept.2017     
	* @description  Splits emails by semicolon
	* @revision     
	*******************************************************************************/
	private static List<String> splitEmailsBySemiColon(String stringToSplit) {
		List<String> emailSplit = new List<String>();
		if (stringToSplit != '') {
			stringToSplit = stringToSplit.trim();
			for (String emailStr: stringToSplit.split(';')) {
				emailSplit.add(emailStr);
			}
		}

		return emailSplit;
	}

	/*******************************************************************************
	* @author		Ant Custodio
	* @date         26.Sept.2017     
	* @description  send a request to Agent Portal
	* @revision     
	*******************************************************************************/
	public static void sendToAgentPortal(List<Case> caseList) {
		List<StudFirst_CRMInitiatedCommsToAP_Request> requestList = new 
			List<StudFirst_CRMInitiatedCommsToAP_Request>();
		for (Case caseRec: caseList) {
			StudFirst_CRMInitiatedCommsToAP_Request crmToApReq = new StudFirst_CRMInitiatedCommsToAP_Request();
			crmToApReq.enquiryRecordId = caseRec.Id;
			crmToApReq.agentOrgUnitId = caseRec.Application_Course_Preference__r.Application__r.Agent_Org_Unit_Code__c;
			crmToApReq.applicantId = caseRec.Application_Course_Preference__r.Application__r.Callista_Applicant_Id__c;
			crmToApReq.personId = caseRec.Application_Course_Preference__r.Application__r.Callista_Student_Id__c;
			crmToApReq.subject = caseRec.Subject;
			crmToApReq.messageBody = caseRec.Description;
			requestList.add(crmToApReq);
		}

		//send a request to Agent Portal so that the Agents will be notified
		if (!requestList.isEmpty()) {
			for (StudFirst_CRMInitiatedCommsToAP_Request request: requestList) {
				System.debug('@@@ request: ' + JSON.serialize(request));
				StudFirst_CRMToAPQueueable campaignMemberJob = new StudFirst_CRMToAPQueueable(JSON.serialize(request));
		        Id jobID = System.enqueueJob(campaignMemberJob);
			}
		}
	}
}