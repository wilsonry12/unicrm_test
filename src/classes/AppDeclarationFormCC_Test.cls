@isTest
private class AppDeclarationFormCC_Test {
	private static Contact contactRecord;

	@testSetup static void setup() {
        IntegrationSettings_SubmitApplication__c is = new IntegrationSettings_SubmitApplication__c();
        is.Base_Url__c = 'https://mix-dev.monash.edu';
        is.Path__c = '/v1/admissions/applications';
        is.Method__c = 'POST';
        is.Header_ClientId__c = 'c8411f0ca73a4ae6a0c7081a1336630f';
        is.Header_ClientSecret__c = 'cd2d5b9e26fa4999A33FF6A7F12D65AD';
        is.Header_ContentType__c = 'application/json';
        is.Mock_Endpoint_Applications__c = 'http://monash.getsandbox.com/v1/admissions/applications';
        is.Enable_Mock__c = false;
        is.Timeout__c = '12000';

        insert is;

    }

	@isTest static void test_acceptDeclarationForm() {
		//create a community user
		User communityUserRec = createCommunityUser();
		System.assertNotEquals(null, contactRecord.Id, 'Contact not found');

		//create a new draft application
		Application__c appRecord = createCompletedApplication(true);
		appRecord.OwnerId = communityUserRec.Id;
		update appRecord;

		test.startTest();
		if (communityUserRec != null) {
			System.runAs(communityUserRec) {
				Test.setMock(HttpCalloutMock.class, new StudFirst_MockHttpResponseGenerator());

				System.assertNotEquals(null, appRecord.Id, 'No application created');
				Application__c retrieveApplicationValue = AppDeclarationFormCC.retrieveApplication(appRecord.Id);
				Application__c acceptApplicationValue = AppDeclarationFormCC.acceptDeclaration(appRecord.Id);
			}
		}
		test.stopTest();
	}

	@isTest static void test_acceptDeclarationFormError() {
		//create a community user
		User communityUserRec = createCommunityUser();
		System.assertNotEquals(null, contactRecord.Id, 'Contact not found');

		//create a new draft application
		Application__c appRecord = createCompletedApplication(false);
		appRecord.Applicant__c = null;
		appRecord.OwnerId = communityUserRec.Id;
		update appRecord;

		test.startTest();
		if (communityUserRec != null) {
			System.runAs(communityUserRec) {
				Test.setMock(HttpCalloutMock.class, new StudFirst_MockHttpResponseGenerator());

				Boolean hasError = false;
	           	try {
	           		Application__c returnError = AppDeclarationFormCC.acceptDeclaration(appRecord.Id);
		        } catch (Exception ex) {
	           		hasError = true;
	           	}
			}
		}
		test.stopTest();
	}
	
	static User createCommunityUser () {
		User userRec;
		try {
			String profileId = [SELECT Id FROM Profile WHERE Name = 'Domestic Applicants' LIMIT 1].Id;

			Account accountRecord = new Account(name ='AccountTest') ;
        	insert accountRecord;

			contactRecord = new Contact();
			contactRecord.FirstName = 'First';
			contactRecord.LastName = 'Last';
			contactRecord.AccountId = accountRecord.Id;
			insert contactRecord;

			Integer rnd = Integer.valueOf(Math.random()*9999);
			userRec = new User(	Alias = String.valueOf(rnd)+ 'usr', 
								Email = String.valueOf(rnd) +'testuser@monash.edu', 
								EmailEncodingKey = 'UTF-8', 
								LastName = 'TestingUser', 
								LanguageLocaleKey = 'en_US', 
								LocaleSidKey = 'en_GB', 
								ProfileId = profileId,
								TimeZoneSidKey = 'Australia/Sydney', 
								UserName = String.valueOf(rnd) +'testuser@monash.edu',
								ContactId = contactRecord.Id);
			insert userRec;

		} catch (Exception ex) {
			System.debug('@@ex: '+ ex.getMessage());
			ExLog.add('ApplicationPortalCC Test Class', 'ApplicationPortalCC_Test', 'createCommunityUser' , ex.getMessage());
		}

		return userRec;
	}

	private static Application__c createCompletedApplication(Boolean isComplete) {
        // Create application
        Application__c application = StudFirst_TestHelper.createApplication(contactRecord);
        application.Status__c = 'Draft';
        insert application;  

        // Create Qualifications
        Qualification__c qual1 = StudFirst_TestHelper.createQualification('Secondary');
        insert qual1;

        Qualification__c qual2 = StudFirst_TestHelper.createQualification('Tertiary');
        insert qual2;

        Qualification__c qual3 = StudFirst_TestHelper.createQualification('Other');
        insert qual3;

        Qualification__c qual4 = StudFirst_TestHelper.createQualification('English');
        insert qual4;

        // Create Contact Qualifications
        Contact_Qualification__c contactQualification1 = StudFirst_TestHelper.createContactQualification(contactRecord, qual1, 'Secondary Education');
        insert contactQualification1;

        Contact_Qualification__c contactQualification2 = StudFirst_TestHelper.createContactQualification(contactRecord, qual2, 'Tertiary Education');
        insert contactQualification2;

        Contact_Qualification__c contactQualification3 = StudFirst_TestHelper.createContactQualification(contactRecord, qual3, 'Other Qualification');
        insert contactQualification3;

        Contact_Qualification__c contactQualification4 = StudFirst_TestHelper.createContactQualification(contactRecord, qual4, 'English Test');
        insert contactQualification4;

        // Create Work Experience
        Work_Experience__c workExperience = StudFirst_TestHelper.createWorkExperience(contactRecord);
        insert workExperience;

        // Create Course
        Course__c course = StudFirst_TestHelper.createCourse();
        insert course;

        // Creare Course Offering
        Course_Offering__c courseOffering = StudFirst_TestHelper.createCourseOffering(course);
        insert courseOffering;

        // Create Course Preferences
        if(isComplete){
        	Application_Course_Preference__c coursePreference = StudFirst_TestHelper.createCoursePreferences(application, contactRecord, courseOffering, course);
        	insert coursePreference;
        }
        

        return application;
    }
	
}