/***********************************************************************
** Author: Ryan Wilson (OAKTON)
** Created Date: 13.AUG.2017
** Description: Request wrapper class for Admissions_AgentPortalCommsService
** History:
***********************************************************************/
global without sharing class Admissions_AgentPortalRequest {
	global class CommsWrapper {
		public String enquiryRecordId {get; set;}
		public String callistaPersonId {get; set;}
		public String agentOrgUnitId {get; set;}
		public String agencyName {get; set;}
		public String staffName {get; set;}
		public String phoneNumber {get; set;}
		public String messageSubject {get; set;}
		public String messageBody {get; set;}
		public String category {get; set;}
		public String subCategory {get; set;}
		public String applicantId {get; set;}
	}
}