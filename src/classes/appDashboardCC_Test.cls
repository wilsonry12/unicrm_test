@isTest
private class appDashboardCC_Test {
	private static Application__c appRecord;
	private static User communityUserRec;
	private static Contact contactRec;
	private static Application_Course_Preference__c coursePref;

	@isTest static void test_retrieveDraftApplication() {
		//create a community user
		communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create required records
		createSampleData(communityUserRec.ContactId, 'Draft', 'TEST-STATUS');

		test.StartTest();
			//only run if there is a Domestic Applicant Profile User
			if (communityUserRec != null) {
				System.runAs(communityUserRec) {
					appDashboardCC app = new appDashboardCC();
					Boolean toggleCreateButton = appDashboardCC.toggleCreateNewApplicationButton();
					
					communityUserRec.App_Salutation__c = 'Miss';
					update communityUserRec;
					Boolean toggleCreateButton1 = appDashboardCC.toggleCreateNewApplicationButton();

					communityUserRec.App_Gender__c = 'Female';
					update communityUserRec;
					appDashboardCC app3 = new appDashboardCC();
					Boolean toggleCreateButton2 = appDashboardCC.toggleCreateNewApplicationButton();

					communityUserRec.App_Birthdate__c = System.today();
					update communityUserRec;
					Boolean toggleCreateButton3 = appDashboardCC.toggleCreateNewApplicationButton();

					communityUserRec.App_Residency_Status__c = 'Australian Citizen';
					update communityUserRec;
					Boolean toggleCreateButton4 = appDashboardCC.toggleCreateNewApplicationButton();

					communityUserRec.Street = 'test street';
					update communityUserRec;
					Boolean toggleCreateButton5 = appDashboardCC.toggleCreateNewApplicationButton();

					communityUserRec.City = 'test street';
					update communityUserRec;
					Boolean toggleCreateButton6 = appDashboardCC.toggleCreateNewApplicationButton();

					communityUserRec.State = 'test street';
					update communityUserRec;
					Boolean toggleCreateButton7 = appDashboardCC.toggleCreateNewApplicationButton();

					communityUserRec.PostalCode = 'test street';
					update communityUserRec;
					Boolean toggleCreateButton8 = appDashboardCC.toggleCreateNewApplicationButton();


					List<appDashboardCC.ApplicationWrapper> appDraft = appDashboardCC.retrieveDraftApplication();

					String returnString = appDashboardCC.cancelDraftApplication(appRecord.Id);

					Boolean hasError = false;
		           	try {
		           		String returnError = appDashboardCC.cancelDraftApplication(communityUserRec.Id);
			        } catch (Exception ex) {
		           		hasError = true;
		           	}
				}
			}
		test.StopTest();

	}

	@isTest static void test_retrieveSubmittedApplication() {
		//create a community user
		communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create required records
		createSampleData(communityUserRec.ContactId, 'Submitted', 'TEST-STATUS');

		test.StartTest();
			//only run if there is a Domestic Applicant Profile User
			if (communityUserRec != null) {
				System.runAs(communityUserRec) {
					List<appDashboardCC.ApplicationWrapper> appDraft = appDashboardCC.retrieveSubmittedApplication();

					appDashboardCC.CourseWrapper crsWrap = appDashboardCC.viewOutcomeStatus(coursePref.Id);

					String returnString = appDashboardCC.insertNewApplication();
				}
			}
		test.StopTest();

	}
	
	static void createSampleData (String contactId, String status, String outcome) {
		//create a new draft application
		appRecord = StudFirst_TestHelper.createApplication(contactId);
		appRecord.OwnerId = communityUserRec.Id;
		appRecord.Status__c = status;
		insert appRecord;

		contactRec = [SELECT Id FROM Contact WHERE Id =: contactId];
		contactRec.Residency_Status__c = 'DOM-AUS';
		update contactRec;

		//Tertiary institution & qualification
		Institution__c tertInsRec = StudFirst_TestHelper.createTertiaryInstitution();
		insert tertInsRec;

		Callista_Outcome_Status__c outcomeStatus = StudFirst_TestHelper.createOutcomeStatus();
		insert outcomeStatus;

		Contact_Qualification__c conQualRec = new Contact_Qualification__c();
		conQualRec.Country__c = 'Australia';
		conQualRec.First_Year_Enrolled__c = String.valueOf(date.today().year()-4);
		conQualRec.Last_Year_Enrolled__c = String.valueOf(date.today().year());
		conQualRec.Other_Qualification__c = 'Certificate III';
		conQualRec.Institution_Code__c = tertInsRec.Institution_Code__c;
		conQualRec.Institution_Name__c = tertInsRec.Institution_Name__c;
		conQualRec.Status__c = 'CURRENTLY STUDYING';
		conQualRec.Contact__c = contactRec.Id;
		conQualRec.RecordTypeId = Schema.SObjectType.Contact_Qualification__c.getRecordTypeInfosByName().get('Tertiary Education').getRecordTypeId();
		insert conQualRec;

		Application_Qualification_Provided__c aqpRecord = new Application_Qualification_Provided__c();
		aqpRecord.Application__c = appRecord.Id;
		aqpRecord.Contact_Qualification__c = conQualRec.Id;
		insert aqpRecord;

		Work_Experience__c workExpRecord = new Work_Experience__c();
		workExpRecord = StudFirst_TestHelper.populateWorkExperienceRecord(workExpRecord);
		workExpRecord.Contact__c = contactRec.Id;
		insert workExpRecord;

		Contact_Document__c contactDocu = new Contact_Document__c();
		contactDocu = StudFirst_TestHelper.createContactDocument(contactRec.Id);
		insert contactDocu;

		Application_Work_Experience_Provided__c aweRecord = new Application_Work_Experience_Provided__c();
		aweRecord.Application__c = appRecord.Id;
		aweRecord.Work_Experience__c = workExpRecord.Id;
		insert aweRecord;

		Course__c courseRec = StudFirst_TestHelper.createCourse();
		insert courseRec;

		Course_Offering__c courseOfferingRec = StudFirst_TestHelper.createCourseOffering(courseRec);
		insert courseOfferingRec;

		coursePref = StudFirst_TestHelper.createCoursePreferences(appRecord, contactRec, courseOfferingRec, courseRec);
		coursePref.Outcome_Status__c = outcome;
		coursePref.Offer_Response_Status__c = outcome;
		insert coursePref;
	}
	
}