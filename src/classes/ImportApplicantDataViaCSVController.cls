/**
 * Upload Bulk Registration CSV file, read. 
 * Create Contact if not exists or update existing, Create Application Course Preference record
 *
 * @author Nadula Karunaratna, PD
 * 
 */

public class ImportApplicantDataViaCSVController {

    public String documentName {get;set;}
    public Blob csvFileBody{get;set;}
    public String[] contactDataLines {get;set;}
    public List <Contact> lstContacts {get;set;}
    public boolean readSuccess {get;set;}
    public List<String> lstFieldNames{get;set;}
    Contact conObj;
    public String contactDataAsString;
    String fieldValue;
    Integer fieldNumber;
    Map <String, Integer> fieldNumberMap = new Map < String, Integer > ();

    //Integer recordCount = 0;
    public boolean showDataPanel {get;set;}  

    //use to generate the data table
    public List <Contact> lstContactsData {get;set;}
    public List<String> lstFieldNamesData {get;set;}
      

    //Define incoming paramters 
    public String applicationID {get; set;} 
    public String courseOfferingID {get; set;} 
    //public String paymentType {get; set;}

    public String courseCode {get; set;}
    public String courseName {get; set;}
    
    public String howManyApplicants {get; set;}    

    //display file upload buttons
    public boolean displayUploadBlock {get;set;}

    //refresh data panel
    //public boolean reRenderTableDataPanel {get;set;}

    //showMessagePanel_1
    public boolean showMessagePanel_1 {get;set;}
    

  public ImportApplicantDataViaCSVController () 
  {
    documentName = '';
    readSuccess = FALSE;
    contactDataLines = new String[] {};
    lstContacts = new List <Contact> ();
    lstFieldNames = new List<String>();
    
    showDataPanel =  FALSE;
    displayUploadBlock = TRUE;
    showMessagePanel_1 = FALSE;
  }

  public void readFromFile(){
    lstContacts.clear();    
    try{
        contactDataAsString = csvFileBody.toString();
        system.debug('contactDataAsString ' +contactDataAsString );          
        readCSVFile();
    }
    catch(exception e){
        readSuccess = FALSE;
        ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR,'Error reading CSV file');
        ApexPages.addMessage(errorMessage);
        showDataPanel =  FALSE;
    }
  }

  public void enableReUploadCSV() {
    showMessagePanel_1 = FALSE;
    displayUploadBlock = TRUE;    
  } 

  public void readCSVFile() {
    //reRenderTableDataPanel = TRUE;
    lstContacts.clear();
    contactDataLines = contactDataAsString.split('\n');
    string[] csvFieldNames = contactDataLines[0].split(',');
    for (Integer i = 0; i < csvFieldNames.size(); i++) {
        fieldNumberMap.put(csvFieldNames[i], i);
        lstFieldNames.add(csvFieldNames[i].trim());
    }

    system.debug('contactDataLines ' +contactDataLines);

    for (Integer i = 1; i < contactDataLines.size(); i++) {
        conObj = new Contact();
        string[] csvRecordData = contactDataLines[i].split(',');
        for (String fieldName: csvFieldNames) {
            fieldNumber = fieldNumberMap.get(fieldName);
            fieldValue = csvRecordData[fieldNumber];
            conObj.put(fieldName.trim(), fieldValue.trim());
        }

        //Check if contact exists and display message [out of scope]
        /*Contact contact = getExistingContact(conObj.FirstName, conObj.LastName, conObj.Email);
        if(contact != null) {
          //ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.INFO, conObj.Salutation + ' '+ conObj.FirstName + ' '+ conObj.LastName + ' ['+ conObj.Email + '] Already exists in the system.');
          //ApexPages.addMessage(errorMessage);
        } else {
          ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.INFO, conObj.Salutation + ' '+ conObj.FirstName + ' '+ conObj.LastName + ' ['+ conObj.Email + '] New Student.');
          ApexPages.addMessage(errorMessage);              
        }*/            

        lstContacts.add(conObj);                
    }
    Integer applicantsCount;
    applicantsCount = integer.valueOf(howManyApplicants.trim());
    if((lstContacts.size() > 0) && (lstContacts.size() == applicantsCount)){
        readSuccess = TRUE;
        showDataPanel = TRUE;   
        lstContactsData = lstContacts;
        lstFieldNamesData = lstFieldNames;

    } else {
        //do something to stop registering, but remember user can not go back to select intended number
        ApexPages.Message errorMessage2 = new ApexPages.Message(ApexPages.Severity.ERROR,'Number of applicants in file does not match to the intended number selected!');
        ApexPages.addMessage(errorMessage2);
        displayUploadBlock = FALSE;
        showMessagePanel_1 = TRUE;
        showDataPanel = FALSE;

        lstContacts.clear();
        lstFieldNames.clear();
        //readSuccess = TRUE;
        
    }            
  }

  //check if Contact exists, then use existing contct
  public static Contact getExistingContact(String givenName, String familyName, String email) {
    List<Contact> contacts = [SELECT FirstName, LastName, Title, Email, HomePhone, Phone, MobilePhone, MailingStreet, MailingCity, MailingState, MailingPostalCode FROM Contact WHERE FirstName = :givenName AND LastName = :familyName AND Email = :email];
    if(contacts.size()>0)
      return contacts.get(0);
    else
      return null;
  }


  public void saveData() {
    saveBulkApplicantData(lstContacts,courseOfferingID,applicationID,courseCode,courseName,readSuccess,displayUploadBlock);
  }

  public static void saveBulkApplicantData(List <Contact> lstContacts, String courseOfferingID, String applicationID, String courseCode, String courseName, Boolean readSuccess, Boolean displayUploadBlock){

    Integer recordCount = 0;
    
    for (Integer i = 0; i < lstContacts.size(); i++) {

      //Get existing contact is exists
      Contact contact = getExistingContact(lstContacts[i].FirstName, lstContacts[i].LastName, lstContacts[i].Email);

      //Create new contact if not exists
      if(contact == null) {
        contact = new Contact(); 
      }

      try {

        contact.Salutation = lstContacts[i].Salutation; 
        contact.FirstName = lstContacts[i].FirstName;
        contact.LastName = lstContacts[i].LastName;
        contact.Email = lstContacts[i].Email;
        contact.MailingStreet = lstContacts[i].MailingStreet;
        contact.MailingCity = lstContacts[i].MailingCity;
        contact.MailingState = lstContacts[i].MailingState;
        contact.MailingPostalCode = lstContacts[i].MailingPostalCode;
        contact.MailingCountry = lstContacts[i].MailingCountry;
        upsert contact; //insert new or update existing         

        //Get Course Code and Course Title
        List<Course__c> courseDataList = [SELECT Name, Course_Description__c, Id FROM Course__c WHERE Id IN (SELECT Course__c FROM Course_Offering__c WHERE Id = :courseOfferingID)];
        Course__c courseData = courseDataList[0];                
        //ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.INFO, 'name - ' + courseData.Name + '| title - ' + courseData.Course_Description__c );
        //ApexPages.addMessage(errorMessage);        

        //Create ACP record
        Application_Course_Preference__c acp = new Application_Course_Preference__c();
        acp.Applicant__c = contact.Id;
        acp.Application__c = applicationID;
        acp.Course_Offering__c = courseOfferingID;
        acp.Course_Code__c = courseCode; //courseData.Name;
        acp.Course_Title__c = courseName; //courseData.Course_Description__c;
        //if(paymentType == 'Credit_Card'){ acp.Status__c = 'Registered'; } else { acp.Status__c = 'Registered - Not paid'; }
        acp.Status__c = 'Registered';
        acp.Course__c = courseData.Id;

        upsert acp; //insert new         

        recordCount = recordCount + 1;

      } catch (Exception e) {
        ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'An error has occured while saving data');
        ApexPages.addMessage(errorMessage);
      } 


    } //end for

    //Display summery
    if(recordCount > 0){
      ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.INFO, 'Total of [ ' + recordCount + ' ] student (s) registered.');
      ApexPages.addMessage(errorMessage);
      readSuccess = FALSE;
      displayUploadBlock = FALSE;
    }

  } //end savedata


}