/*******************************************************************************
* @author       Ryan Wilson
* @date         06.04.2017         
* @description  Test class for updating campaign member status
* @revision     
*******************************************************************************/
@isTest
private class BMSUpdateCampaignMemStatusBatch_Test {
	
	@isTest(SeeAllData=true) 
	static void UpdateCampaignMemberTest() {
		//insert sample Contacts
        String KnownToMonashRecTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Known to Monash systems').getRecordTypeId();
        List<Contact> contactRecordList = BMS_TestDataHelper.buildSampleContacts();
        insert contactRecordList;

        List<Contact> changeContactRecordType = new List<Contact>();
        for (Contact contactRecord: [SELECT Id, RecordTypeId FROM Contact WHERE Id IN: contactRecordList]) {
            contactRecord.RecordTypeId = KnownToMonashRecTypeId;
            changeContactRecordType.add(contactRecord);
        }

        if (!changeContactRecordType.isEmpty()) {
            update changeContactRecordType;
        }

        Campaign crmEmail = BMS_TestDataHelper.buildSampleCRMMassEmail('Sample Template');
        crmEmail.Type = 'Email';
        insert crmEmail;

        CommonUtilities.assignCampaignMemberStatus(crmEmail.Id);

        List<CampaignMember> campaignMemberToInsertList = new List<CampaignMember>();
		for (Contact contactRecord: contactRecordList) {
			CampaignMember campaignMemberRec = new CampaignMember();
			campaignMemberRec.ContactId = contactRecord.Id;
			campaignMemberRec.Status = 'Pending Approval';
			campaignMemberRec.CampaignId = crmEmail.id;
			campaignMemberToInsertList.add(campaignMemberRec);
		}
		if (!campaignMemberToInsertList.isEmpty()) {
			insert campaignMemberToInsertList;
		}

        test.startTest();
        crmEmail.Status = 'Approved';
        update crmEmail;

        List<String> campaignIds = new List<String>();
        campaignIds.add(crmEmail.Id);

        BMSUpdateCampaignMemStatusQueueable campaignMemberJob = new BMSUpdateCampaignMemStatusQueueable(campaignIds);
        Id jobID = System.enqueueJob(campaignMemberJob);
		test.stopTest();
	}
	
}