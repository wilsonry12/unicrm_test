/*******************************************************************************
	* @author       Ant Custodio
	* @date         3.Aug.2017         
	* @description  AA Class that contains all operations related to Attachments
	* @revision     
	*******************************************************************************/
public with sharing class AttachmentServices {
	private static final String AGENT_PERSON_ID = Label.CALLISTA_AGENT_PERSON_ID;
    private static final String NOTEDOCTYPE = 'CRM_EMAIL';
    private static final String admissionsRecType = CommonServices.recordTypeId('Case','Admissions');
    public static Boolean runOnce = false;

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         3.Aug.2017         
	* @description  Clones the Email Attachment to the related Contact whenever the 
	*				applicant replied to the email. 
	*				This creates:
	*					- Contact Document record
	*					- Application Documents Provided record
	*					- Attachment record
	* @revision     Ant Custodio - 15.Aug.2017 - sends a summary of the email message
						to Callista as a text file
	*******************************************************************************/
	public class cloneEmailAttachmentToContactDocument implements Triggers.Handler {
		public void run(){
			List<Attachment> attList = new List<Attachment>();
			String emailMsgPrefix = EmailMessage.sobjecttype.getDescribe().getKeyPrefix();
			for (Attachment attRec: (List<Attachment>)Trigger.new) {
				if (attRec.ParentId != null) {
					//only include attachments that are linked to an EmailMessage
					String attParentIdPrefix = String.valueOf(attRec.ParentId).subString(0,3);
					if (emailMsgPrefix == attParentIdPrefix) {
						attList.add(attRec);
					}
				}
			}

			if (!attList.isEmpty()) {
				cloneEmailAttachmentsToContactDocument(attList, new Map<String, String>(), true);
			}
			
		}
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         19.Oct.2017         
	* @description  when a case is related to ACP, link the attachment related to it
						as well.
	* @revision     
	*******************************************************************************/
	public static void cloneEmailAttachmentToContactDocumentUsingCases(Map<Id, Case> oldMap, Map<Id, Case> newMap) {
		Map<String, String> caseIdMap = new Map<String, String>();
		for(Case caseInstance : newMap.values()) {
        	if(	oldMap.get(caseInstance.Id).Application_Course_Preference__c == null && 
        		newMap.get(caseInstance.Id).Application_Course_Preference__c != null &&
        		newMap.get(caseInstance.Id).RecordTypeId == admissionsRecType) {
        		caseIdMap.put(caseInstance.Id, caseInstance.Application_Course_Preference__c);
        	}
        }

        if (!caseIdMap.isEmpty() && !runOnce) {
        	Set<String> emlMsgIds = new Set<String>();
        	//get all the emailmessage related to this Case
	        for (EmailMessage emlMsg: [	SELECT	Id 
	        							FROM	EmailMessage
	        							WHERE	ParentId IN: caseIdMap.keySet()
	        									AND Incoming = true]) {
	        	emlMsgIds.add(emlMsg.Id);
	        }

	        if (!emlMsgIds.isEmpty()) {
	        	//get all the attachments for this email message
	        	List<Attachment> attToCloneList = new List<Attachment>();
	        	for (Attachment attRec: [	SELECT 	Id, Name, Body, 
			    									ContentType, ParentId, BodyLength 
			    							FROM 	Attachment 
			    							WHERE 	ParentId IN: emlMsgIds ]) {
	        		attToCloneList.add(attRec);
	        	}

	        	if (!attToCloneList.isEmpty()) {
	        		cloneEmailAttachmentsToContactDocument(attToCloneList, caseIdMap, false);
		        }
	        }
	        runOnce = true;
        }
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         19.Oct.2017         
	* @description  Creates corresponding Contact_Document__c and Application_Document_Provided__c
						for each inserted Attachment
	* @revision     
	*******************************************************************************/
	public static void cloneEmailAttachmentsToContactDocument(List<Attachment> emlMsgAttachments, Map<String, String> caseIdMap, Boolean updateTheCase) {
		//call method that exclude: attachments that were already inserted AND attached to Email Message
		Map<String, List<Attachment>> emlMsgAttMap = generateEmailMsgMap(emlMsgAttachments, !updateTheCase);
		if (!emlMsgAttMap.isEmpty()) {
			//get parent email message so that the parent fields can be used
			Map<String, EmailMessage> emlMsgMap = retrieveApplicantEmailMessageMap (emlMsgAttMap);
			//set for collecting case IDs if the attachment is too big / a small jpg image
			Set<String> caseIds = new Set<String>();
			Map<String, Contact_Document__c> cdToInsertMap = new Map<String, Contact_Document__c>();
			Map<String, Application_Course_Preference__c> acpAppMap = new Map<String, Application_Course_Preference__c>();
			try {
				List<EmailMessage> emlMsgList = emlMsgMap.values();
				if (emlMsgList.isEmpty()) {
					if (!caseIdMap.isEmpty()) {
						for (Application_Course_Preference__c acpRec: [	SELECT 	Id,
																				Application__c,
																				Applicant__c,
																				Application__r.Callista_Applicant_Id__c
																		FROM 	Application_Course_Preference__c
																		WHERE 	Id IN: caseIdMap.values()]) {
							acpAppMap.put(acpRec.Id, acpRec);
						}
					}
					emlMsgList = [	SELECT 	Id, ParentId, 
											Parent.ContactId, 
											Parent.CaseNumber,
											FromAddress,
											MessageDate,
											ToAddress,
											CcAddress,
											Subject,
											TextBody,
											Parent.Application_Course_Preference__c,
											Parent.Application_Course_Preference__r.Application__c,
											Parent.Application_Course_Preference__r.Application__r.Callista_Applicant_Id__c
									FROM 	EmailMessage 
									WHERE 	ParentId IN: caseIdMap.keySet()
											AND Incoming = true ];
				}
				//if this method is called from attachment trigger and the Parent Case has an ACP, it will go here
				if (!emlMsgList.isEmpty()) {
					//for each attachment, create one Contact Document
					for (EmailMessage emlMsg: emlMsgList) {
						String contactId = emlMsg.Parent.ContactId;
						if (caseIdMap.containsKey(emlMsg.ParentId)) {
							contactId = acpAppMap.get(caseIdMap.get(emlMsg.ParentId)).Applicant__c;
						}
						if (emlMsgAttMap.containsKey(emlMsg.Id)) {
							for (Attachment attRec: emlMsgAttMap.get(emlMsg.Id)) {
								//ONLY RUN if the file size is less than 5MB and if the file size is less than 50KB it shouldn't be image
								Boolean isFileBigOrSmall = true;
								if(theFileIsQualifiedToBeSentToCallista(attRec.BodyLength, attRec.ContentType)) {
									isFileBigOrSmall = false;
									Contact_Document__c cdRec = new Contact_Document__c();
									cdRec.Comments__c = 'Enquiry Source: ' + emlMsg.Parent.CaseNumber;
									cdRec.Contact__c = contactId;
									cdRec.Document_Type__c = 'OTHERDCTYP';
									cdRec.Filename__c = attRec.Name;
									cdToInsertMap.put(emlMsg.Id+'-'+attRec.Name+'-'+attRec.BodyLength, cdRec);
								}

								if (isFileBigOrSmall || Test.isRunningTest()) {
									//adding caseIds to be used for update
									caseIds.add(emlMsg.ParentId);
								}
							}
						}
					}
				}

				//updates the case if there is a large document or a jpg file smaller than 50KB
				if(!caseIds.isEmpty() && updateTheCase) {	
					updateCaseFlagForDocs(caseIds);
				}

				if (!cdToInsertMap.isEmpty()) {
					insert cdToInsertMap.values();

					//insert ADPs And Attachment Records to Contact Document
					Set<String> adpIds = insertADPsAndAttachments(emlMsgList, emlMsgAttMap, cdToInsertMap, caseIdMap, acpAppMap);
					/*if (!adpIds.isEmpty()) {
						//method that sends a notification to Callista to retrieve the document
						StudFirst_SendDocumentsService.sendToCallistaByADPIds(adpIds);
					}*/
				}
			} catch (Exception ex) {
				Exlog.add('Apex Class','AttachmentServices','cloneEmailAttachmentsToContactDocument', ex + ' - ' + ex.getLineNumber() +' -- emlMsgAttachments:' + emlMsgAttachments);
			}
		}
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         20.Oct.2017
	* @description  updates the case if there is a large document or a 
						jpg file smaller than 50KB
	* @revision     
	*******************************************************************************/
	private static Set<String> insertADPsAndAttachments(List<EmailMessage> emlMsgList, Map<String, List<Attachment>> emlMsgAttMap, 
														Map<String, Contact_Document__c> cdToInsertMap, Map<String, String> caseIdMap, 
														Map<String, Application_Course_Preference__c> acpAppMap) {
		Set<String> adpIds = new Set<String>();
		List<Attachment> attToInsertList = new List<Attachment>();
		List<Application_Document_Provided__c> adpToInsertList = new List<Application_Document_Provided__c>();

		if (!emlMsgList.isEmpty()) {
			for (EmailMessage emlMsg: emlMsgList) {
				String attFileNames = '';
				String appId = emlMsg.Parent.Application_Course_Preference__r.Application__c;
				String applicantId = emlMsg.Parent.Application_Course_Preference__r.Application__r.Callista_Applicant_Id__c;

				if (emlMsgAttMap.containsKey(emlMsg.Id)) {
					for (Attachment attRec: emlMsgAttMap.get(emlMsg.Id)) {
						if (caseIdMap.containsKey(emlMsg.ParentId)) {
							String acpId = caseIdMap.get(emlMsg.ParentId);
							if (emlMsg.Parent.Application_Course_Preference__c == null && acpAppMap.containsKey(acpId)) {
								appId = acpAppMap.get(acpId).Application__c;
								applicantId = acpAppMap.get(acpId).Application__r.Callista_Applicant_Id__c;
							}
						}

						String keyToUse = emlMsg.Id+'-'+attRec.Name+'-'+attRec.BodyLength;
						if (cdToInsertMap.containsKey(keyToUse)) {
							//next, add the attachments to the created contact documents 
							Attachment attToInsert = attRec.clone();
							attToInsert.ParentId = cdToInsertMap.get(keyToUse).Id;
							attToInsertList.add(attToInsert);

							//lastly, create a junction object between the contact document and the application
							Application_Document_Provided__c adpRec = new Application_Document_Provided__c();
							adpRec.Application__c = appId;
							adpRec.Contact_Document__c = attToInsert.ParentId;
							adpToInsertList.add(adpRec);
							
							//collect the file names (to be used on generating email summary file)
							attFileNames += attRec.Name + '\n';
						}
					}
					//submit the Email Message TextBody to Callista as a text file
					submitEmailMessageToCallista(emlMsg, applicantId, attFileNames);
				}
			}
		}

		//insert the attachment and the ADP junction object
		if (!attToInsertList.isEmpty()) {
			insert attToInsertList;
		}

		if (!adpToInsertList.isEmpty()) {
			insert adpToInsertList;
			//get the Ids and use it to send documents to callista
			for (Application_Document_Provided__c adpRec: adpToInsertList) {
				adpIds.add(adpRec.Id);
			}
		}

		return adpIds;
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         20.Oct.2017
	* @description  updates the case if there is a large document or a 
						jpg file smaller than 50KB
	* @revision     Ant Custodio (TEMP FIX - Revisit when fixing SF-479)
	*******************************************************************************/
	private static void updateCaseFlagForDocs(Set<String> caseIds) {
		//Here the cases are queried from the database and then they get updated.
		List<Case> cases = new List<Case>();
		/*for (Case caseRec: [Select id, status from Case where id IN: caseIds AND LargeOrSmallDocReceived__c = false]) {
			caseRec.Status = 'Large/Small Document';
			caseRec.LargeOrSmallDocReceived__c = true;
			cases.add(caseRec);
		}*/
		for (Case caseRec: [Select id, status from Case where id IN: caseIds AND Large_Doc_Received__c = false]) {
			caseRec.Status = 'Large Document';
			caseRec.Large_Doc_Received__c = true;
			cases.add(caseRec);
		}
		if (!cases.isEmpty()) {
			update cases;
		}
	}

	/*******************************************************************************
	* @author       Majid Reisi Dehkordi
	* @date         18.Oct.2017
	* @description  Checks if the file is bigger than 5 Mb or if less than 50 Kb
	*	 			checks if the file type is picture. If picture then not qualified
	*				otherwise qualified.
	* @revision     
	*******************************************************************************/
	private static boolean theFileIsQualifiedToBeSentToCallista(Integer bodyLengthOfFile, String contentTypeOfFile) {
		if (bodyLengthOfFile > Integer.valueOf(System.Label.App_MaxFileSizeInBytes))
			return false;
		if (bodyLengthOfFile <= Integer.valueOf(System.Label.App_MinFileSizeInBytes) && contentTypeOfFile == 'image/jpeg')
			return false;
		return true;
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         15.Aug.2017         
	* @description  submits the Email Message TextBody to Callista as a text file
	* @revision     
	*******************************************************************************/
	private static void submitEmailMessageToCallista (EmailMessage emlMsg, String externalApplicantId, String attachmentFileNames) {
		StudFirst_DocumentDTO doc = new StudFirst_DocumentDTO();
		// External Applicant Id
        doc.externalApplicantId = externalApplicantId;
        // Agent
        doc.Agent = new StudFirst_DocumentDTO.Agent();
        doc.Agent.agentPersonId = AGENT_PERSON_ID;
        // Document
        doc.document = new StudFirst_DocumentDTO.Document();
        doc.document.type = NOTEDOCTYPE;
        doc.document.filename = emlMsg.Parent.CaseNumber + '_Email_' + emlMsg.Id;
		doc.document.contentType = 'text/plain';
        doc.document.comments = 'Enquiry Source: ' + emlMsg.Parent.CaseNumber;
        

        String docBody = 'From: ' + emlMsg.FromAddress + '\n';
		docBody += 'Sent: ' + emlMsg.MessageDate.format() + '\n';
		docBody += 'To: ' + emlMsg.ToAddress + '\n';
		if (emlMsg.CcAddress != null) {
			docBody += 'CC: ' + emlMsg.CcAddress + '\n';
		}
		if (emlMsg.Subject != null) {
			docBody += 'Subject: ' + emlMsg.Subject + '\n';
		}
		docBody += '\n' + emlMsg.TextBody;

        if (attachmentFileNames != '') {
        	docBody += '\n\nAttachments:\n' + attachmentFileNames;
        }
        Blob documentBlob = Blob.valueOf(docBody);
        doc.document.binary = EncodingUtil.base64Encode(documentBlob);

        //Submit Summary text file to Callista
		StudFirst_SubmitEnquirySummary.sendRequest(JSON.serialize(doc), doc.externalApplicantId);
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         4.Aug.2017         
	* @description  Returns a Map using the List of Attachments
	* 				KEY = EmailMessage.Id
	*				VALUE = List<Attachment>
	* @revision     
	*******************************************************************************/
	private static Map<String, List<Attachment>> generateEmailMsgMap (List<Attachment> attToMapList, Boolean overrideFlag) {
		String emailMsgPrefix = EmailMessage.sobjecttype.getDescribe().getKeyPrefix();
		Map<String, List<Attachment>> emlMsgAttMap = new Map<String, List<Attachment>>();

		for (Attachment attRec: attToMapList) {
			//only include attachments that are about to be inserted (before insert)	
			if (attRec.Id == null || overrideFlag) {
				//only include attachments that are linked to an EmailMessage
				String attParentIdPrefix = String.valueOf(attRec.ParentId).subString(0,3);
				if (emailMsgPrefix == attParentIdPrefix) {
					if (emlMsgAttMap.containsKey(attRec.ParentId)) {
						List<Attachment> attList = emlMsgAttMap.get(attRec.ParentId);
						attList.add(attRec);
						emlMsgAttMap.put(attRec.ParentId, attList);
					} else {
						emlMsgAttMap.put(attRec.ParentId, new List<Attachment> {attRec});
					}
				}
			}
		}

		return emlMsgAttMap;
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         4.Aug.2017         
	* @description  Retrieves the Applicant's EmailMessage
	*				Criteria:
	* 					- if the Email Message Parent Case = 'Admissions'
	* 					- it only works on Inbound Emails
	* 					- it only works for direct Applicants (Applicants without agents)
	* @revision     
	*******************************************************************************/
	private static Map<String, EmailMessage> retrieveApplicantEmailMessageMap (Map<String, List<Attachment>> emlMsgAttMap) {
		Map<String, EmailMessage> emlMsgMap = new Map<String, EmailMessage>();

		for ( EmailMessage emlMsgRec: [	SELECT 	Id, ParentId,
												Subject,
												TextBody,
												FromAddress,
												ToAddress,
												CcAddress,
												MessageDate,
												Parent.CaseNumber,
												Parent.Status,
												Parent.ContactId,
												Parent.RecordTypeId,
												Parent.Application_Course_Preference__c,
												Parent.Application_Course_Preference__r.Application__c,
												Parent.Application_Course_Preference__r.Application__r.Applicant__c,
												Parent.Application_Course_Preference__r.Application__r.Status__c,
												Parent.Application_Course_Preference__r.Application__r.Callista_Applicant_Id__c,
												Incoming
										FROM 	EmailMessage
										WHERE 	Id IN: emlMsgAttMap.KeySet() AND
												Incoming = true AND
												Parent.RecordTypeId =: admissionsRecType AND 
												Parent.ContactId != null AND
												Parent.Application_Course_Preference__c != null] ) {
			emlMsgMap.put(emlMsgRec.Id, emlMsgRec);
		}

		return emlMsgMap;
	}
}