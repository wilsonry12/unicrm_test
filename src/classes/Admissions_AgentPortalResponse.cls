/***********************************************************************
** Author: Ryan Wilson (OAKTON)
** Created Date: 13.AUG.2017
** Description: Response wrapper class for Admissions_AgentPortalCommsService
** History:
***********************************************************************/
global without sharing class Admissions_AgentPortalResponse {
	global string status;
	global string enquiryRecordId;
	global string error;
}