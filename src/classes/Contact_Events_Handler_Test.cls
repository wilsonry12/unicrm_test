/***********************************************
* Author: Ryan Wilson
* Date: 26.SEP.2017
* Revision: Test class for Contact_Events_Handler class
***********************************************/
@isTest
private class Contact_Events_Handler_Test {
	
	@isTest static void createStaffRecord() {

		Contact__e contactEvent_new = new Contact__e(
						First_Name__c = 'Testing User',
						Last_Name__c = 'Staff 001',
						Email__c = 'staff0001@monash.edu',
						MobilePhone__c = '0412318615',
						Title__c = 'Contractor',
						SAP_Staff_Id__c = '01118264',
						Status__c = 'Active',
						Authcate__c = 'tsta0006',
						Contract_Type__c = 'External - Unpaid',
						Faculty_Portfolio__c = 'Chief Information Officer',
						Organisation_Unit__c = 'Portfolio Management',
						School_Division_Name__c = 'eSolutions',
						Source__c = 'SAP',
						Event_Type__c = 'inbound',
						Last_Modified_Date__c = System.now());
						
		Test.startTest();

			Database.SaveResult sr_new = EventBus.publish(contactEvent_new);

		Test.stopTest();

		List<Contact> contactsCreated = new List<Contact>([SELECT Id, SAP_Staff_Id__c, Name, Email 
															FROM Contact 
															WHERE SAP_Staff_Id__c = '01118264']);

		System.assertEquals(1, contactsCreated.size());

	}

	@isTest static void updateStaffRecord() {
		//create a staff contact
		Account acc = new Account(Name = 'Monash Connect Test', RecordTypeId = CommonServices.recordTypeId('Account', 'Faculty'));
		insert acc;

		Contact con = new Contact(FirstName = 'Testing User',
						LastName = 'Staff 001',
						Email = 'staff0013@monash.edu',
						MobilePhone = '0412314400',
						Title = 'Consultant',
						SAP_Staff_Id__c = '01118264',
						Status__c = 'Active',
						Authcate__c = 'tsta0006',
						Contract_Type__c = 'External - Unpaid',
						Faculty_Portfolio__c = 'Monash Connect Test',
						Organisation_Unit__c = 'Portfolio Management',
						School_Division_Name__c = 'eSolutions',
						Source__c = 'SAP');
		insert con;

		List<Contact__e> conEvents = new List<Contact__e>();
		Contact__e contactEvent_update = new Contact__e(
						First_Name__c = 'Testing User',
						Last_Name__c = 'Staff 001',
						Email__c = 'staff0013@monash.edu',
						MobilePhone__c = '0412314400',
						Title__c = 'Senior Consultant',
						SAP_Staff_Id__c = '01118264',
						Status__c = 'Active',
						Authcate__c = 'tsta0006',
						Contract_Type__c = 'External - Unpaid',
						Faculty_Portfolio__c = 'Monash Connect Test',
						Organisation_Unit__c = 'Portfolio Management',
						School_Division_Name__c = 'eSolutions',
						Source__c = 'SAP',
						Event_Type__c = 'inbound',
						Last_Modified_Date__c = System.now());

		conEvents.add(contactEvent_update);

		Contact__e contactEvent_new = new Contact__e(
						First_Name__c = 'Testing User',
						Last_Name__c = 'Staff 002',
						Email__c = 'staff0002@monash.edu',
						MobilePhone__c = '0412318616',
						Title__c = 'Contractor',
						SAP_Staff_Id__c = '01118265',
						Status__c = 'Active',
						Authcate__c = 'tsta0007',
						Contract_Type__c = 'External - Unpaid',
						Faculty_Portfolio__c = 'Chief Information Officer',
						Organisation_Unit__c = 'Portfolio Management',
						School_Division_Name__c = 'eSolutions',
						Source__c = 'SAP',
						Event_Type__c = 'inbound',
						Last_Modified_Date__c = System.now());
						
		conEvents.add(contactEvent_new);

		Test.startTest();

			List<Database.SaveResult> sr = EventBus.publish(conEvents);

		Test.stopTest();

		List<Roles__c> roleCreated = new List<Roles__c>([SELECT Id, Contact__c, Name FROM Roles__c 
														WHERE Contact__c =:con.Id]);
		System.assertEquals(1, roleCreated.size());

		List<Contact> contactsCreated = new List<Contact>([SELECT Id, SAP_Staff_Id__c, Name, Email 
															FROM Contact 
															WHERE SAP_Staff_Id__c = '01118265']);
		System.assertEquals(1, contactsCreated.size());
	}

	@isTest static void updateStaffRecord_OutOfDate() {
		//create a staff contact
		Account acc = new Account(Name = 'Monash Connect Test', RecordTypeId = CommonServices.recordTypeId('Account', 'Faculty'));
		insert acc;

		Contact con = new Contact(FirstName = 'Testing User',
						LastName = 'Staff 001',
						Email = 'staff0013@monash.edu',
						MobilePhone = '0412314400',
						Title = 'Consultant',
						SAP_Staff_Id__c = '01118264',
						Status__c = 'Active',
						Authcate__c = 'tsta0006',
						Contract_Type__c = 'External - Unpaid',
						Faculty_Portfolio__c = 'Monash Connect Test',
						Organisation_Unit__c = 'Portfolio Management',
						School_Division_Name__c = 'eSolutions',
						Source__c = 'SAP',
						LastModifiedDate_API__c = System.now());
		insert con;

		//Testing invalid lastModifiedDate
		Contact__e contactEvent_update = new Contact__e(
						First_Name__c = 'Testing User',
						Last_Name__c = 'Staff 001',
						Email__c = 'staff0013@monash.edu',
						MobilePhone__c = '0412314400',
						Title__c = 'Senior Consultant',
						SAP_Staff_Id__c = '01118264',
						Status__c = 'Active',
						Authcate__c = 'tsta0006',
						Contract_Type__c = 'External - Unpaid',
						Faculty_Portfolio__c = 'Monash Connect Test',
						Organisation_Unit__c = 'Portfolio Management',
						School_Division_Name__c = 'eSolutions',
						Source__c = 'SAP',
						Event_Type__c = 'inbound',
						Last_Modified_Date__c = System.now()-3);

		Test.startTest();

			Database.SaveResult sr = EventBus.publish(contactEvent_update);
		
		Test.stopTest();
	}
	
}