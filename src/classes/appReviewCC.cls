/*******************************************************************************
* @author       Ant Custodio
* @date         10.May.2017         
* @description  controller class for appReview.cmp lightning component
* @revision     
*******************************************************************************/
public with sharing class appReviewCC {
	/*******************************************************************************
    * @author       Ant Custodio
    * @date         10.May.2017        
    * @description  retrieves the application by Id
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static Application__c retrieveApplication(String appId) {
        List<Application__c> applicationList = 	[	SELECT 	Id, Status__c, Citizenship_Type_Filter__c, 
                                                    Access_and_Equity_Question__c, Refused_Visa_Question__c, Breached_Visa_Question__c, 
                                                    Convicted_Any_Crime_Question__c, Issued_Protection_Visa_Question__c, 
                                                    Medical_Health_Visa_Question__c, Physical_Mental_Health_Question__c
													FROM 	Application__c
													WHERE 	Id =: appId ];
		
		Application__c appRecord = new Application__c();
		if (!applicationList.isEmpty()) {
			appRecord = applicationList[0];
		}
		//will return new application if application Id is not found
		return appRecord;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         10.May.2017        
    * @description  retrieves the qualification and map it by record type 
    					developer name
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static Map<String, List<Application_Qualification_Provided__c>> retrieveApplicationQualMap(String appId) {
        Map<String, List<Application_Qualification_Provided__c>> appQualMap = new Map<String, List<Application_Qualification_Provided__c>>();

        if (appId != '') {
            for (Application_Qualification_Provided__c aqpRecord : [   	SELECT  Id,
                                                                                Name,
												                                Application__c,
												                                Contact_Qualification__c,
												                                Contact_Qualification__r.RecordType.DeveloperName,
												                                Contact_Qualification__r.Institution_Name__c,
												                                Contact_Qualification__r.Other_Institution__c,
												                                Contact_Qualification__r.Qualification__c,
												                                Contact_Qualification__r.Qualification__r.Qualification_Name__c,
												                                Contact_Qualification__r.Other_Qualification__c,
												                                Contact_Qualification__r.Qualification_Document__c,
                                                                                Contact_Qualification__r.Country__c,
                                                                                Contact_Qualification__r.Instruction_in_English__c,
                                                                                Contact_Qualification__r.First_Year_Enrolled__c,
                                                                                Contact_Qualification__r.Last_Year_Enrolled__c,
												                                Contact_Qualification__r.Qualification_Document__r.Filename__c,
                                                                                Contact_Qualification__r.Qualification_Document__r.Comments__c,
                                                                                Contact_Qualification__r.Year_of_Completion__c,
                                                                                Contact_Qualification__r.State__c,
                                                                                Contact_Qualification__r.Status__c,
                                                                                Contact_Qualification__r.Score__c,
                                                                                Contact_Qualification__r.Date_Achieved__c,
                                                                                Contact_Qualification__r.Other_Qualification_Comments__c,
                                                                                Contact_Qualification__r.isTestCompleted__c,
                                                                                Contact_Qualification__r.Qualification_Document__r.Document_Type__c,
                                                                                Contact_Qualification__r.Essay_Rating__c,
                                                                                Contact_Qualification__r.Listening__c,
                                                                                Contact_Qualification__r.Reading__c,
                                                                                Contact_Qualification__r.Speaking__c,
                                                                                Contact_Qualification__r.Test_of_Written_English__c,
                                                                                Contact_Qualification__r.Writing__c
												                        FROM    Application_Qualification_Provided__c
												                        WHERE   Application__c =: appId 
												                        ORDER BY CreatedDate DESC ] ) {
            	if (appQualMap.containsKey(aqpRecord.Contact_Qualification__r.RecordType.DeveloperName)) {
            		appQualMap.get(aqpRecord.Contact_Qualification__r.RecordType.DeveloperName).add(aqpRecord);
        		} else {
        			appQualMap.put(aqpRecord.Contact_Qualification__r.RecordType.DeveloperName, new List<Application_Qualification_Provided__c> {aqpRecord});
        		}
            }
        }
        return appQualMap;
    }

    /*******************************************************************************
    * @author       Ant Custodio
    * @date         23.May.2017        
    * @description  retrieves the work experience and map it by record type 
                        developer name
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static List<Application_Work_Experience_Provided__c> retrieveApplicationWorkExpList(String appId) {
        List<Application_Work_Experience_Provided__c> appWorkExpList = new List<Application_Work_Experience_Provided__c>();

        if (appId != '') {
            for (Application_Work_Experience_Provided__c aweRecord : [    SELECT  Id,
                                                                                Name,
                                                                                Application__c,
                                                                                Work_Experience__c,
                                                                                Work_Experience__r.Contact_Person_Phone_c__c,
                                                                                Work_Experience__r.Contact_Person__c,
                                                                                Work_Experience__r.Contact_Person_City__c,
                                                                                Work_Experience__r.Contact_Person_Email__c,
                                                                                Work_Experience__r.Contact_Person_First_Name__c,
                                                                                Work_Experience__r.Contact_Person_Last_Name__c,
                                                                                Work_Experience__r.Contact_Person_Phone__c,
                                                                                Work_Experience__r.Contact_Person_PostalCode__c,
                                                                                Work_Experience__r.Contact_Person_State__c,
                                                                                Work_Experience__r.Contact_Person_Street__c,
                                                                                Work_Experience__r.Employer_Name__c,
                                                                                Work_Experience__r.End_Date__c,
                                                                                Work_Experience__r.Position__c,
                                                                                Work_Experience__r.Start_Date__c

                                                                        FROM    Application_Work_Experience_Provided__c
                                                                        WHERE   Application__c =: appId 
                                                                        ORDER BY Work_Experience__r.Start_Date__c DESC ] ) {
                appWorkExpList.add(aweRecord);
            }
        }
        return appWorkExpList;
    }

    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         23.May.2017        
    * @description  retrieves Application Documents
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static List<Application_Document_Provided__c> retrieveAppDocuments(String appId) {
        List<Application_Document_Provided__c> appDocuments = new List<Application_Document_Provided__c>();
        appDocuments = [SELECT Id, CreatedDate, Application__c, Contact_Document__c, Contact_Document__r.Name, 
                        Contact_Document__r.Document_Type_Name__c, Contact_Document__r.Filename__c, 
                        Document_Id__c, Contact_Document__r.Comments__c   
                        FROM Application_Document_Provided__c 
                        WHERE Application__c =:appId 
                        ORDER BY CreatedDate DESC 
                        LIMIT 20];
        return appDocuments;
    }


    /*******************************************************************************
    * @author       Ryan Wilson
    * @date         23.May.2017        
    * @description  retrieves Application Course Preferences
    * @revision     
    *******************************************************************************/
    @AuraEnabled
    public static List<CourseWrapper> retrieveApplicationCourses(String appId) {
        List<CourseWrapper> listCourse = new List<CourseWrapper>();

        List<Application_Course_Preference__c> applicationCourses = new List<Application_Course_Preference__c>();

        applicationCourses = [SELECT Academic_Calendar_Type__c,Academic_CI_Sequence_Number__c,Admission_Calendar_Type__c,
                                Admission_CI_Sequence_Number__c,Applicant__c,Application__c,Attendance_Type__c,Attendance_Mode_Description__c,
                                Commencement_Period__c,Attendance_Type_Description__c,Attendance_Mode__c, Type_of_Place_Description__c,
                                Course_Code__c,Course_Offering__c,Course_Title__c,Course_Version_Number__c,Course__c,CreatedDate,Id,
                                Location_Code__c,Name,Outcome_Status__c,Preference_Number__c,Status__c,Unit_Set_Code__c, Unit_Set_Description__c,
                                Offer_Response_Status__c,Offer_Condition__c, Offer_Date__c, Offer_Response_Date__c, Actual_Offer_Response_Date__c     
                                FROM Application_Course_Preference__c 
                                WHERE Application__c =:appId 
                                ORDER BY Preference_Number__c ASC];

        if(applicationCourses.size() >0){
            for(Application_Course_Preference__c appCourse: applicationCourses){
                listCourse.add(builCourseWrapper(appCourse));
            }
        }
        
        
        return listCourse;
    }

    private static CourseWrapper builCourseWrapper(Application_Course_Preference__c appCourse) {
        CourseWrapper course = new CourseWrapper();
        if(appCourse != null){
            course.courseId = appCourse.Id;
        
            course.courseDetail = appCourse.Course_Code__c+'-'+appCourse.Course_Title__c;
            if(appCourse.Unit_Set_Description__c != null){
                course.courseDetail += '/ '+appCourse.Unit_Set_Description__c;
            }
            if(appCourse.Type_of_Place_Description__c != null){
                course.courseDetail += '/ '+appCourse.Type_of_Place_Description__c; 
            }

            course.attMode = appCourse.Attendance_Type_Description__c+' '+appCourse.Attendance_Mode_Description__c;
            
            course.location = appCourse.Location_Code__c;
            course.commencementPeriod = appCourse.Commencement_Period__c;
        }
        
        return course;
    }

    public class CourseWrapper {
        @AuraEnabled public String courseId {get; set;}
        @AuraEnabled public String courseDetail {get; set;}
        @AuraEnabled public String attMode {get; set;}
        @AuraEnabled public String location {get; set;}
        @AuraEnabled public String commencementPeriod {get; set;}
    }
}