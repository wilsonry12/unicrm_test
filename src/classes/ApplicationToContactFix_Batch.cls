/*******************************************************************************
* @author       Ryan Wilson
* @date         4.OCT.2017
* @description  One off fix with applications link to the wrong contact
* @revision     
*******************************************************************************/
global class ApplicationToContactFix_Batch implements Database.Batchable<sObject> {
    
    private String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        query = 'SELECT Id, '
                +'Applicant__r.FirstName, ' 
                +'Applicant__r.LastName, '
                +'Applicant__r.Birthdate, '
                +'Applicant__r.Email, '
                +'Applicant__c '
                +'FROM Application__c '
                +'WHERE From_Portal__c = true '
                +'AND Status__c = \'Submitted\' '
                +'AND Applicant__r.Account.RecordType.Name = \'Individual\'';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Application__c> applications) {
        Map<String,List<Application__c>> mapContactsWithApplication = new Map<String,List<Application__c>>();
        
        for (Application__c appRec: applications) {
            List<Application__c> tempList = mapContactsWithApplication.containsKey(appRec.Applicant__c)? 
                                            mapContactsWithApplication.get(appRec.Applicant__c): 
                                            new List<Application__c>();
            templist.add(appRec);
            mapContactsWithApplication.put(appRec.Applicant__c, templist);
        }

        System.debug('*****CONTACTS WITH APPLICATION: '+mapContactsWithApplication.size());

        //Callista Student contacts
        List<Contact> conList = [SELECT FirstName, LastName, Email, Birthdate 
                                    FROM Contact 
                                    WHERE Id IN: mapContactsWithApplication.keySet() 
                                    AND Email != null];
        System.debug('****CONLIST: ' + conList.size());

        Map<String, Contact> contMap = new Map<String, Contact>();
        for (Contact conRec: conList) {
            String dobToUse = '';
            if (conRec.Birthdate == null) {
                dobToUse = 'null';
            } else {
                dobToUse = String.valueOf(conRec.Birthdate.day()+conRec.Birthdate.month()+conRec.Birthdate.year());
            }
            String dupeConKey = conRec.LastName + '~' + dobToUse +'~'+ conRec.Email;
            dupeConKey = dupeConKey.toUpperCase();
            contMap.put(dupeConKey, conRec);
        }

        Map<String, String> mapContactsToBeUpdated = new Map<String, String>();
        
        //Domestic Applicant contacts
        List<Contact> domContacts = [SELECT Id, FirstName, LastName, Email, Birthdate FROM Contact WHERE Account.Name = 'Domestic Applicants' AND Email != null];
        
        for (Contact conRec: domContacts) {
            String dobToUse = '';
            if (conRec.Birthdate == null) {
                dobToUse = 'null';
            } else {
                dobToUse = String.valueOf(conRec.Birthdate.day()+conRec.Birthdate.month()+conRec.Birthdate.year());
            }
            String dupeConKey = conRec.LastName + '~' + dobToUse +'~'+ conRec.Email;
            dupeConKey = dupeConKey.toUpperCase();
            if (contMap.containsKey(dupeConKey)) {
                mapContactsToBeUpdated.put(contMap.get(dupeConKey).Id, conRec.Id);
            }
        }

        System.debug('****MAP CONTACTS TO BE UPDATED: '+mapContactsToBeUpdated.size());

        //Update the applications with the correct contact....
        List<Application__c> applicationsForUpdate = new List<Application__c>();
        for(String strKey: mapContactsToBeUpdated.keySet()){
            for(Application__c app: mapContactsWithApplication.get(strKey)){
                app.Applicant__c = mapContactsToBeUpdated.get(strKey);
                applicationsForUpdate.add(app);
            }
        }

        if(applicationsForUpdate.size() >0){
            update applicationsForUpdate;
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        //do something...
    }
    
}