@istest
public class CaseServices_Assignment_Test {
    
    	@istest
        static void invokeAssignmentRuleTest() {
        
        Case_RecordType_CS__c customSettingRecord = new Case_RecordType_CS__c(Case_Assignment_Trigger__c = true, Check_Case_Origin__c = true, Case_Origin__c='Web',Recordtype_Name__c = 'HR',name='testCS1');
        insert customSettingRecord;
        
        Map<String,Schema.RecordTypeInfo> recordTypeInfoByName = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosByName();
        Id recTypeID = recordTypeInfoByName.get('HR').getRecordTypeId();
        list<case> caseRec = TestHelper.webToCaseForms(1);
        list<case> caseList = new list<case>();
        
        for(case cas:caseRec) {
            cas.Origin = 'Web';
            cas.recordtypeId = recTypeID;
            caseList.add(cas);
        }
            
        insert caseList;
    }

}