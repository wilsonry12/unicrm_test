/*******************************************************************************
* @author       Ryan Wilson
* @date         22.Jun.2017       
* @description  Batch job for applying Assessment Rule for each course offering
* @revision     
*******************************************************************************/
global class ApplySelectionRuleBatch implements Database.Batchable<sObject>, Database.Stateful {
	
	private String query;
	private Set<String> assessRuleForUpdate;
	private Set<String> assessRuleIds;
	
	global ApplySelectionRuleBatch(Set<String> setRuleIds) {
		assessRuleIds = setRuleIds;
		assessRuleForUpdate = new Set<String>();
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'SELECT Id, Override__c, Inactive__c, SelectionQuery__c, Review__c FROM Assessment_Selection_Rule__c WHERE Id IN:assessRuleIds';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Assessment_Selection_Rule__c> scope) {
		List<Course_Offering__c> coursesForUpdate = new List<Course_Offering__c>();

		for(Assessment_Selection_Rule__c assessRule: scope){
			List<Course_Offering__c> courses = new List<Course_Offering__c>();
			courses = Database.query(assessRule.SelectionQuery__c);
			System.debug('COURSES: '+courses);

			if(courses.size() > 0){
				for(Course_Offering__c crs: courses){
					if(assessRule.Inactive__c != true){
						if(crs.Assessment_Plan_Selection_Rule__c != null){
							//if Course Offering has existing Assessment Rule, mark for review
							Id existingRuleId = crs.Assessment_Plan_Selection_Rule__c;
							assessRuleForUpdate.add(existingRuleId);
							assessRuleForUpdate.add(assessRule.Id);

							//If override is TRUE, then change new selection rule....
							if(assessRule.Override__c){
								crs.Assessment_Plan_Selection_Rule__c = assessRule.Id;
							}
							
						} else{
							crs.Assessment_Plan_Selection_Rule__c = assessRule.Id;
						}
					} else{
						crs.Assessment_Plan_Selection_Rule__c = null;
					}

					coursesForUpdate.add(crs);
				}
			}
		}

		if(coursesForUpdate.size() >0){
			update coursesForUpdate;
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		if(assessRuleForUpdate.size() >0){
			List<Assessment_Selection_Rule__c> assessRules = new List<Assessment_Selection_Rule__c>([
																SELECT Id, Override__c, Review__c 
																FROM Assessment_Selection_Rule__c 
																WHERE Id IN:assessRuleForUpdate]);
			if(assessRules.size() >0){
				for(Assessment_Selection_Rule__c rule: assessRules){
					rule.Review__c = true;

					if(rule.Override__c){
						rule.Override__c = false;
					}
				}

				update assessRules;
			}
		}
	}
	
}