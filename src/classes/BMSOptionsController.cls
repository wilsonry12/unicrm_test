public with sharing class BMSOptionsController {
    public String typeParam {get;set;}
    public String optionParam {get;set;}
    public String BMS_PERSON_ID {get;set;}
    public String BMS_EMAIL_ADD {get;set;}
    public String BMS_SEARCH {get;set;}
    public String currentUserProfile {get;set;}

    public BMSOptionsController() {
        typeParam = ApexPages.currentPage().getParameters().get('type');
        BMS_PERSON_ID = 'personId';
        BMS_EMAIL_ADD = 'emailAdd';
        BMS_SEARCH = 'search';

        currentUserProfile = CommonUtilities.getProfileName();
        if(typeParam == 'sms'){

        }
    }

    public PageReference gotoSelectedOption(){
        PageReference redirect = null;

        if(optionParam == BMS_PERSON_ID){
            redirect = Page.BMSUploadIds;
            redirect.getParameters().put('type',typeParam);
        }
        else if(optionParam == BMS_EMAIL_ADD){
            redirect = Page.BMSUploadEmailAddress;
            redirect.getParameters().put('type',typeParam);
        }

        else if(optionParam == BMS_SEARCH){
            redirect = Page.BulkMessagingSystem;
            redirect.getParameters().put('type',typeParam);
        }
        return redirect;
    }

    public PageReference gotoPrevious(){
        PageReference redirect = Page.BMSHome;
        return redirect;
    }

    public Boolean showBackButton{
        get{return currentUserProfile != CommonUtilities.BMS_ADMIN && currentUserProfile != CommonUtilities.SYSTEM_ADMIN? false: true;}
    }

    public String toggleIcon{
        get{ return typeParam == 'sms'? 'mobileNumbers': 'emailAddIcon';}
    }
}