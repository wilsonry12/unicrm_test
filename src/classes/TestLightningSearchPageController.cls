/**************************************************************************************************   
Apex Class Name      :  TestLightningSearchPageController
Version              :  1.0                                                                          
Created Date         :                                                              
Function             :  This is test class for LightningSearchPageController & LightningSearchComponent.
Story                :                                                                               
Work Task            :                                                                               
Modification Log     :                                                                               
*--------------------------------------------------------------------------------------------------  
    Developer                        Date                        Description
    Veera Gandi                    03/07/2017                        Created.
***************************************************************************************************/
@istest
public class TestLightningSearchPageController {
    
    @testSetup
    public static void createData() {
        TestUtil.createCustomSettings();
    }
    
    @istest
    public static void getSearchResults() {
        faq__kav faq1=new faq__kav();
        faq1.question__c='hi';
        faq1.answer__c='there';
        faq1.Title='Veera';
        faq1.UrlName='test454365';
        insert faq1;
        faq__kav faq=[select id,KnowledgeArticleId,ArticleNumber,Title from faq__kav where id=:faq1.Id];
        KbManagement.PublishingService.publishArticle(String.valueOf(faq.KnowledgeArticleId),true);
        
        topic t=new topic();
        t.name='testTopic';
        t.description='xyz';
        insert t;
        
        TopicAssignment ta=new TopicAssignment();
        ta.TopicId= t.id;
        ta.EntityId=faq.Id;
        insert ta;   
        
        list<sobject> sObjectList = LightningSearchPageController.getSearchResults('testTopic');
        integer PageSize = LightningSearchPageController.getPageSize();
        string searchText = LightningSearchPageController.getSearchToken('testTopic?tabset-fe996=2');
        LightningSearchComponent.getSearchArticles('Veera');
        system.assertEquals(3, PageSize);
        system.assertEquals(1, sObjectList.size());
        system.assertEquals(searchText, '"TESTTOPIC"');
    }
    
}