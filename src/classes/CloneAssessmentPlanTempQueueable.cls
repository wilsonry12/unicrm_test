/*******************************************************************************
* @author       Anterey Custodio
* @date         22.Jun.2017
* @description  Queueable class for CloneAssessmentPlanTempBatch
* @revision     
*******************************************************************************/
public class CloneAssessmentPlanTempQueueable implements Queueable {
	
	private String appId;
	//key = application Id | value = set of Application Course Preference Ids
	private Map<String, Set<String>> acpMap;
	/*******************************************************************************
	* @author       Anterey Custodio
	* @date         22.Jun.2017         
	* @description  constructor called to know which Application Course Preference	
						ID to use
	* @revision     
	*******************************************************************************/
	public CloneAssessmentPlanTempQueueable(List<String> appIds) {
		//populates the Application Course Preference Ids to be used on the batch
		for (Application_Course_Preference__c acpRecord: [	SELECT	Id, 
																	Application__c
															FROM 	Application_Course_Preference__c
															WHERE 	Application__c IN: appIds ]) {
			if (acpMap == null) {
				//create a new instance
				acpMap = new Map<String, Set<String>>();
				acpMap.put(acpRecord.Application__c, new Set<String>{acpRecord.Id});
			} else {
				//add to existing
				acpMap.get(acpRecord.Application__c).add(acpRecord.Id);
			}
		}
	}

	public void execute(QueueableContext context) {
		//only queue it if there are records to execute
        if (!acpMap.isEmpty()) {
        	Id batchJobId = Database.executeBatch(new CloneAssessmentPlanTempBatch(acpMap), 200);
        }
	}
}