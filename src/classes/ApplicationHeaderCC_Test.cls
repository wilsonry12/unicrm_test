/*******************************************************************************
* @author		Ant Custodio
* @date         24.May.2017        
* @description  test class for ApplicationHeaderCC
* @revision     
*******************************************************************************/
@isTest
public with sharing class ApplicationHeaderCC_Test {
	private static Application__c appRecord;
	private static Contact_Qualification__c conQualRec;
	private static Work_Experience__c workExpRecord;
	private static Course_Offering__c courseOfferingRec;
	private static Course__c courseRec;
	/*******************************************************************************
	* @author       Ant Custodio
	* @date         24.May.2017        
	* @description  test the application success page
	* @revision     
	*******************************************************************************/
	static testMethod void test_retrieveApplication_hasChildren() {
		//create a community user
		User communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create required records
		createSampleData(communityUserRec.ContactId, communityUserRec.Id);

		Contact contactRec = [SELECT Id FROM Contact WHERE Id =: communityUserRec.ContactId];

		Application_Course_Preference__c coursePref = StudFirst_TestHelper.createCoursePreferences(appRecord, contactRec, courseOfferingRec, courseRec);
		insert coursePref;

		Document_Requested__c docRequest = StudFirst_TestHelper.createDocRequestRecord(contactRec.Id, appRecord.Id, coursePref.Id);
		insert docRequest;

		Application_Qualification_Provided__c aqpRecord = StudFirst_TestHelper.createAppQualProvided(appRecord.Id, conQualRec.Id);
		insert aqpRecord;

		Application_Work_Experience_Provided__c aweRecord = StudFirst_TestHelper.createAppWorkExpProvided(appRecord.Id, workExpRecord.Id);
		insert aweRecord;

		test.startTest();
		//only run if there is a Domestic Applicant Profile User
		if (communityUserRec != null) {
			System.runAs(communityUserRec) {

				//retrieve application
				System.assertNotEquals(null, appRecord.Id, 'Application not created');
				Application__c appRetrieve = ApplicationHeaderCC.retrieveApplicationRecord(appRecord.Id);
				System.assertEquals(appRetrieve.Id, appRecord.Id, 'Application not retrieved');

				String validationMessage = ApplicationHeaderCC.checkApplicationRecord(appRecord.Id, appRecord, true);
				//System.assertEquals('Valid', validationMessage, 'Application invalid');

				//delete application
				ApplicationHeaderCC.deleteApplicationRecord(appRetrieve.Id);

			}
		}
		test.stopTest();
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         24.May.2017        
	* @description  test the application success page - no course
	* @revision     
	*******************************************************************************/
	static testMethod void test_retrieveApplication_noCoursePref() {
		//create a community user
		User communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create required records
		createSampleData(communityUserRec.ContactId, communityUserRec.Id);

		test.startTest();
		//only run if there is a Domestic Applicant Profile User
		if (communityUserRec != null) {
			System.runAs(communityUserRec) {

				//retrieve application
				System.assertNotEquals(null, appRecord.Id, 'Application not created');
				Application__c appRetrieve = ApplicationHeaderCC.retrieveApplicationRecord(appRecord.Id);
				System.assertEquals(appRetrieve.Id, appRecord.Id, 'Application not retrieved');

				String validationMessage = ApplicationHeaderCC.checkApplicationRecord(appRecord.Id, appRecord, false);
				System.assertEquals('Please add the courses you want to apply to continue.', validationMessage, 'Application valid');

				//delete application
				Boolean hasError = false;
				try {
					//enter invalid ID
					ApplicationHeaderCC.deleteApplicationRecord('012345678901234');
				} catch (Exception ex) {
					hasError = true;
				}

				System.assert(hasError, 'force error not working');

			}
		}
		test.stopTest();
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         24.May.2017        
	* @description  test the application success page - has Qualification and Work
						experience but no course
	* @revision     
	*******************************************************************************/
	static testMethod void test_retrieveApplication_hasChildrenNoCourse() {
		//create a community user
		User communityUserRec = StudFirst_TestHelper.createCommunityUser();
		System.assertNotEquals(null, communityUserRec.ContactId, 'Contact not found');

		//create required records
		createSampleData(communityUserRec.ContactId, communityUserRec.Id);

		Contact contactRec = [SELECT Id FROM Contact WHERE Id =: communityUserRec.ContactId];

		Application_Qualification_Provided__c aqpRecord = StudFirst_TestHelper.createAppQualProvided(appRecord.Id, conQualRec.Id);
		insert aqpRecord;

		Application_Work_Experience_Provided__c aweRecord = StudFirst_TestHelper.createAppWorkExpProvided(appRecord.Id, workExpRecord.Id);
		insert aweRecord;

		test.startTest();
		//only run if there is a Domestic Applicant Profile User
		if (communityUserRec != null) {
			System.runAs(communityUserRec) {

				//retrieve application
				System.assertNotEquals(null, appRecord.Id, 'Application not created');
				Application__c appRetrieve = ApplicationHeaderCC.retrieveApplicationRecord(appRecord.Id);
				System.assertEquals(appRetrieve.Id, appRecord.Id, 'Application not retrieved');

				String validationMessage = ApplicationHeaderCC.checkApplicationRecord(appRecord.Id, appRecord, false);
				System.assertEquals('Please add the courses you want to apply to continue.', validationMessage, 'Application valid');

				//delete application
				Boolean hasError = false;
				try {
					//enter invalid ID
					ApplicationHeaderCC.deleteApplicationRecord('012345678901234');
				} catch (Exception ex) {
					hasError = true;
				}

				System.assert(hasError, 'force error not working');

			}
		}
		test.stopTest();
	}

	/*******************************************************************************
	* @author       Ant Custodio
	* @date         24.May.2017        
	* @description  creates all sample data required
	* @revision     
	*******************************************************************************/
	static void createSampleData (String contactId, String ownerId) {
		//create a new draft application
		appRecord = StudFirst_TestHelper.createApplication(contactId);
		appRecord.OwnerId = ownerId;
		insert appRecord;

		//Tertiary institution & qualification
		Institution__c tertInsRec = StudFirst_TestHelper.createTertiaryInstitution();
		tertInsRec.OwnerId = ownerId;
		insert tertInsRec;

		conQualRec = new Contact_Qualification__c();
		conQualRec.Country__c = 'Australia';
		conQualRec.First_Year_Enrolled__c = String.valueOf(date.today().year()-4);
		conQualRec.Last_Year_Enrolled__c = String.valueOf(date.today().year());
		conQualRec.Other_Qualification__c = 'Certificate III';
		conQualRec.Institution_Code__c = tertInsRec.Institution_Code__c;
		conQualRec.Institution_Name__c = tertInsRec.Institution_Name__c;
		conQualRec.Status__c = 'CURRENTLY STUDYING';
		conQualRec.RecordTypeId = Schema.SObjectType.Contact_Qualification__c.getRecordTypeInfosByName().get('Tertiary Education').getRecordTypeId();
		conQualRec.Contact__c = contactId;
		insert conQualRec;

		workExpRecord = new Work_Experience__c();
		workExpRecord = StudFirst_TestHelper.populateWorkExperienceRecord(workExpRecord);
		workExpRecord.Contact__c = contactId;
		insert workExpRecord;

		courseRec = StudFirst_TestHelper.createCourse();
		insert courseRec;

		courseOfferingRec = StudFirst_TestHelper.createCourseOffering(courseRec);
		insert courseOfferingRec;
	}
}