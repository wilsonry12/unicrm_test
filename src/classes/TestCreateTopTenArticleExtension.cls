/**************************************************************************************************   
Apex Class Name      :  TestCreateTopTenArticleExtension
Version              :  1.0                                                                          
Created Date         :                                                              
Function             :  This is test class for CreateTopTenArticleExtension.
Story                :                                                                               
Work Task            :                                                                               
Modification Log     :                                                                               
*--------------------------------------------------------------------------------------------------  
    Developer                        Date                        Description
    Vaibhav B                    19/05/2017                        Created.
***************************************************************************************************/
@isTest
public class TestCreateTopTenArticleExtension{
    @testSetup
    public static void createData() {
        TestUtil.createArticles();
        TestUtil.createCustomSettings();
    }
    public static testmethod void testTopTenExtension() {
        List<User> userList= TestHelper.createTestUsers('System Administrator',1);
        system.assertEquals(userList.size(),1);
        User u=userList[0];
        List<UserRole> roleList=[select id from UserRole where developerName='Monash_Connect_Officer'];
        if(roleList.size()>0) {
            u.userroleId=roleList[0].Id;
            update u;
        }        
        faq__kav faq=[select id,question__c,answer__c,title,UrlName,articleNumber from faq__kav where UrlName='test454365' and PublishStatus='Online' and language='en_US'];
        HR_FAQ__kav hrfaq=[select id,question__c,answer__c,title,UrlName,articleNumber  from HR_FAQ__kav where UrlName='test123' and PublishStatus='Online' and language='en_US'];
        test.startTest();
        system.runAs(u) {
            List<case> caseList=TestHelper.webToCaseForms(1);
            system.assertEquals(caseList.size(),1);            
            
            TopTenArticle__c topTen=new TopTenArticle__c();
            ApexPages.StandardController sc = new ApexPages.StandardController(topTen);                     
            PageReference pageRef = Page.createNewTopTenArticle;
            pageRef.getParameters().put('FAQId', faq.Id);
            pageRef.getParameters().put('articleNumber', faq.articleNumber);
            Test.setCurrentPage(pageRef);      
            createTopTenArticleExtension testTopTen= new createTopTenArticleExtension(sc);        
            testTopTen.save();
            system.assertnotEquals(topTen.Id,null);
            topTen=new TopTenArticle__c();
            testTopTen= new createTopTenArticleExtension(sc);
            pageRef.getParameters().put('FAQId', hrfaq.Id);
            pageRef.getParameters().put('articleNumber', hrfaq.articleNumber );
            testTopTen= new createTopTenArticleExtension(sc); 
            pageRef.getParameters().put('FAQId', null);  
            testTopTen= new createTopTenArticleExtension(sc); 
            testTopTen.save();
            system.assertEquals(topTen.Id,null);
            
        }
        List<TopTenArticle__c> articleListAsAdmin=LightningTopTenController.getTopTenArticles();
        userList=[select id from user where profile.name='ASK Monash Profile'];
        if(userList.size()>0) {
            system.runAs(userList[0]) {
                List<TopTenArticle__c> articleList=LightningTopTenController.getTopTenArticles();
            }            
        }
        userList=[select id from user where profile.name='Customer Community Student User'];
        if(userList.size()>0) {
            system.runAs(userList[0]) {
                List<TopTenArticle__c> articleList=LightningTopTenController.getTopTenArticles();
            }            
        }
        userList=[select id from user where profile.name='Customer Community Staff User'];
        if(userList.size()>0) {
            system.runAs(userList[0]) {
                List<TopTenArticle__c> articleList=LightningTopTenController.getTopTenArticles();                
            }            
        }
        test.stopTest();
    }    
}