/**
 * This class contains unit tests for validating the behavior of UserServices methods
 * these methods are part of the testing framework implemented throughout UniCRM
 * 
 */
@isTest
private class UserServices_Test {

    static testMethod void testUserInfoClearout(){
        Test.startTest();
        Contact contactRecord = new Contact();
        contactRecord.FirstName='Test';
        contactRecord.LastName = 'Test 2';
        contactRecord.Email = 'Test@testmail.com';
        //contactRecord.Birthdate = '09.12.2016';
        //contactRecord.Salutation = contactUserMap.get(contactRec.Id).App_Salutation__c;
        contactRecord.Gender__c = 'M';
        contactRecord.MobilePhone = '1234567890';
        contactRecord.Phone = '1234567890';
        contactRecord.Citizenship__c = 'DOMESTIC';
        contactRecord.Previous_Surname__c = 'Testing';
        contactRecord.Previous_Monash_ID__c = '998929282928';
        contactRecord.Previous_Monash_Institution__c = 'Monash University';
        contactRecord.Country_of_Residence__c = 'Australia';
        contactRecord.Previously_Studied_at_Monash__c = TRUE;
        contactRecord.Residency_Status__c = 'DOM-NZ';
        contactRecord.HomePhone = '1234567890';
        //contactRecord.HasDisabilities__c = contactUserMap.get(contactRec.Id).App_HasDisabilities__c;
        contactRecord.Aboriginal_or_Torres_Strait_Islander__c = 'No';
        contactRecord.MailingStreet = 'Test';
        contactRecord.MailingCity = 'Test';
        contactRecord.MailingState = 'NSW';
        contactRecord.MailingPostalCode = '12345';
        contactRecord.MailingCountry = 'Test';
        contactRecord.Other_Given_Name__c = 'Test';
        contactRecord.Address_Type__c = 'Test';           
        List<Contact> ContactList=new List<Contact>();      
        ContactList.add(contactRecord);
        insert contactRecord;
        // User
       
        String profileName = 'Domestic Applicants';
        Id profileId = [select id from Profile where name =:profileName limit 1].Id;
        system.assertNotEquals(null,profileId,'No user profile found for user setup');
        
        List<User> users = new List<User>();     

        Integer rnd = Integer.valueOf(Math.random()*9999);
        users.add(new User(Alias = String.valueOf(rnd)+ 'usr', 
                        Email= String.valueOf(rnd) +'testuser@mutesting.edu', 
                        EmailEncodingKey='UTF-8', 
                        LastName='TestingUser', 
                        LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_GB', 
                        ContactId = contactRecord.Id,
                        ProfileId = profileId, 
                        TimeZoneSidKey='Australia/Sydney', 
                        UserName= String.valueOf(rnd) +'testuser@mutesting.edu'));
        
        insert users;

        User usr = StudFirst_TestHelper.createCommunityUser();



        usr.Queue__c = 'test';
        usr.Location__c = 'test';
        usr.Enquiry_Role__c = 'test';        
        
        System.runAs(usr){
            UserServices.resetMyInformation();
            UserServices.MonashConnectOfficerUsers.contains(usr.id);
            usr = UserServices.contextUser;
            UserServices.updateUsers(users);
        }        
        Test.stopTest();
    }
    
}